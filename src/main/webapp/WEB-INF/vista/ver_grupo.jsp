<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<html>
<title>${sitio} - APGP</title>
	<c:import url="/includes/includes.jsp"></c:import>
	<c:import url="/includes/headerLogged.jsp"></c:import> 
	<script>
		function verProyectos(){
			document.getElementById("divProyectos").style = "";
			document.getElementById("divMiembros").style = "display:none;";
			document.getElementById("divSubgrupos").style = "display:none;";
			document.getElementById("divEquipos").style = "display:none;";
		}
		function verMiembros(){
			document.getElementById("divProyectos").style = "display:none;";
			document.getElementById("divMiembros").style = "";
			document.getElementById("divSubgrupos").style = "display:none;";
			document.getElementById("divEquipos").style = "display:none;";
		}
		function verSubgrupos(){
			document.getElementById("divProyectos").style = "display:none;";
			document.getElementById("divMiembros").style = "display:none;";
			document.getElementById("divSubgrupos").style = "";
			document.getElementById("divEquipos").style = "display:none;";
		}
		function verEquipos(){
			document.getElementById("divProyectos").style = "display:none;";
			document.getElementById("divMiembros").style = "display:none;";
			document.getElementById("divSubgrupos").style = "display:none;";
			document.getElementById("divEquipos").style = "";
		}
		
		<c:if test="${puedeCrearProyecto==true}">
		var crearProyectoAPartirDePlantilla = "";
		function actionMostrarCreacionPorPlantilla(){
			document.getElementById("divDeProyectos").style="display:none;";
			document.getElementById("divDePlantillas").style="";
		}
		function actionMostrarProyectos(){
			document.getElementById("divDePlantillas").style="display:none;";
			document.getElementById("divDeProyectos").style="";
		}
		</c:if>
		
		<c:if test="${organigrama!=null}">
		var calculosProyecto = [<c:set var="b" value="true"/>
		                        <c:forEach items="${organigrama}" var="calculo">
									<c:if test="${b==false}">,{</c:if><c:if test="${b==true}"><c:set var="b" value="false"/>{</c:if>
										loginUsuario : "${calculo.value0.login}",
										vistaTareas : [<c:set var="c" value="true"/>
								                        <c:forEach items="${calculo.value1}" var="tarea">
														<c:if test="${c==false}">,</c:if><c:if test="${c==true}"><c:set var="c" value="false"/></c:if>
														{
															tarea : {
																idTarea : "${tarea.value0.idTarea}",
																nombreTarea : "${tarea.value0.nombreTarea}",
																idProyectoPerteneciente : "${tarea.value0.idProyectoPerteneciente}"
															},
															empieza : "${tarea.value1}",
															finaliza : "${tarea.value2}"
														}
										               </c:forEach>]
									}
		                        </c:forEach>];
		
		var diasProyecto = ${diasGrupo};
		
		function go_usuarios(){
			document.getElementById("divCronograma").style="display:none;";
			document.getElementById("divPlanning").style="";
		}
		
		function go_organigrama(){
			document.getElementById("divCronograma").style="";
			document.getElementById("divPlanning").style="display:none;";
		}
		
		function mostrarEstadisticas(){
			fechaProyecto = mostrarFecha(diasProyecto);
			innerHtml = 
				"<div id='divCronograma'><ul class='nav nav-tabs' role='tablist'>"+
				  "<li class='active'><a href='#'>Cronograma</a></li>"+
				  "<li><a href='javascript:go_usuarios()'>Usuarios</a></li>"+
				"</ul>";
			innerHtml += "<h3>CRONOGRAMA PARA PROYECTOS DEL GRUPO ${grupoPerteneciente.nombreGrupo}</h3>";
			innerHtml += "<table class='table table-striped'><thead><tr><th>Usuario</th><th style='width:100em;'>CRONOGRAMA</th></tr></thead><tbody>";
			for(var i=0;i<calculosProyecto.length;i++){
				calculo = calculosProyecto[i];
				innerHtml += "<tr><td>"+calculo.loginUsuario+"</td><td><div class='progress'>";
				vari = 0;
				for(var j=0;j<calculo.vistaTareas.length;j++){
					vistaTarea = calculo.vistaTareas[j];
					if(parseInt(vari)<parseInt(vistaTarea.empieza)){
						porcentajeWidth = parseInt(((parseInt(vistaTarea.empieza)-parseInt(vari))/parseInt(diasProyecto))*100);
						innerHtml += "<div class='progress-bar progress-bar-danger progress-bar-striped active' style='width:"+porcentajeWidth+"%'>";
						innerHtml += "Espera</div>";
					}
					if(j%4==0){
						innerHtml += "<div class='progress-bar progress-bar-success' style='width:";
					} else if(j%4==1){
						innerHtml += "<div class='progress-bar progress-bar-info' style='width:";
					} else if(j%4==2){
						innerHtml += "<div class='progress-bar' style='width:";
					} else if(j%4==3){
						innerHtml += "<div class='progress-bar progress-bar-warning' style='width:";
					}
					porcentajeWidth = parseInt(((parseInt(vistaTarea.finaliza)-parseInt(vistaTarea.empieza))/parseInt(diasProyecto))*100);
					innerHtml += porcentajeWidth+"%'>"+vistaTarea.tarea.nombreTarea+" - "+(vistaTarea.finaliza-vistaTarea.empieza)+" d�as</div>";
					vari=vistaTarea.finaliza;
				}
			}
				innerHtml += "</div></td></tr>";
				innerHtml += "</tbody></table>";
				innerHtml += "</div>";
				innerHtml += 
				"<div id='divPlanning' style='display:none;'><ul class='nav nav-tabs' role='tablist'>"+
				  "<li><a href='javascript:go_organigrama()'>Cronograma</a></li>"+
				  "<li class='active'><a href='#'>Usuarios</a></li>"+
				"</ul><h3>PLANNING DE USUARIOS</h3>";
				for(var p=0;p<calculosProyecto.length;p++){
					cp = calculosProyecto[p];
					innerHtml += '<div class="panel panel-default">'+
						'<div class="panel-heading">PLANNING DE '+cp.loginUsuario+'</div>'+
						'<table class="table table-bordered"><thead><tr><th>Acci�n</th><th></th><th>Fecha Inicio</th><th></th><th>Fecha Fin</th></tr></thead><tbody>';
					seQuedo = 0;
					for(var l=0;l<cp.vistaTareas.length;l++){
						vt = cp.vistaTareas[l];
						fechaEmpieza = mostrarFecha(vt.empieza);
						fechaFinaliza = mostrarFecha(vt.finaliza)
						if(seQuedo<vt.empieza){
							innerHtml += '<tr class="warning"><td>En espera desde el </td><td>';
							innerHtml += mostrarFecha(seQuedo)+'</td><td> hasta el </td><td>'+fechaEmpieza;
							innerHtml += '</td></tr>';
						}
						innerHtml += '<tr><td>Realiza la tarea '+vt.tarea.nombreTarea+' del proyecto '+
						proyectos[indices_proyectos[vt.tarea.idProyectoPerteneciente]].nombreProyecto+' </td><td> desde el </td><td>';
						innerHtml += fechaEmpieza+'</td><td> hasta el </td><td>'+fechaFinaliza;
						innerHtml += '</td></tr>';
						seQuedo = vt.finaliza;
					}
					if(seQuedo<diasProyecto){
						innerHtml += '<tr class="warning"><td>En espera de nuevas tareas desde el </td><td>';
						innerHtml += mostrarFecha(seQuedo)+'</td><td> hasta el </td><td>'+fechaProyecto;
						innerHtml += '</td></tr>';
					}
					innerHtml += "</tbody></table><br>";
				}
				innerHtml += "</div></div><br><h4>Quedan "+diasProyecto+" d�as para finalizar todos los proyectos("+fechaProyecto+")</h4>";
				showLightbox(innerHtml);
		}
		</c:if>
		<c:if test="${horasDia!=0 && diasSemana!=0}">
		function reajustar() {
			diasSemanaP =  document.getElementById("diasSemana").value;
			horasDiaP =  document.getElementById("horasDia").value;
			if(diasSemanaP=='' || diasSemanaP<1 || diasSemanaP>7 || horasDiaP=='' || horasDiaP<1 || horasDiaP>24){
				alert("Datos err�neos");
			} else {
				$.ajax({
					url: '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/horas.htm',
					data: '{"horasDia":'+horasDiaP+',"diasSemana":'+diasSemanaP+'}',
					type: 'PUT',
					success: function(data){
						alert("Se ha realizado el cambio con �xito");
						location.href = location.href;
					}
				}).fail(function(data){alert(data.responseText);})
			}
		}
		
		function mostrarAjustes(){
			innerHtml = "<h4>Ajustes</h4>";
			innerHtml += "�Cu�nto tiempo quieres dedicar al grupo?<br>";
		    innerHtml += "<input id='diasSemana' type='number' name='diasSemana' value='${diasSemana}' style='width:2em;' min='1' max='7'/> d�as a la semana<br>";
		    innerHtml += "<input id='horasDia' type='number' name='horasDia' value='${horasDia}' style='width:2em;' min='1' max='24'/> horas al d�a<br>";
		    innerHtml += "<button class='btn btn-info' onclick='reajustar()'>Actualizar</button>";
		    showLightbox(innerHtml);
		}
		</c:if>
	</script>
</head>
<body>
	<h3 style="text-align:center;">${grupoPerteneciente.nombreGrupo}<c:if test="${horasDia!=0 && diasSemana!=0}"><a href='javascript:mostrarAjustes();'><i class="glyphicon glyphicon-cog"></i></a></c:if></h3><h5>Le dedicas ${diasSemana} d�as a la semana y ${horasDia} horas cada d�a</h5>
	<!-- Deben haber 4 vistas: proyectos, miembros, subgrupos e Equipos-->
	<div id="divProyectos" <c:if test="${verProyectos!=true}">style="display:none;"</c:if>>		
		<c:if test="${horasDia!=0 && diasSemana!=0}"><div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<ul class="nav nav-tabs" role="tablist">
			  <li class='active'><a href="#">Proyectos</a></li>
			  <li><a href="javascript:verMiembros()">Miembros</a></li>
			  <li><a href="javascript:verSubgrupos()">Subgrupos</a></li>
			  <c:if test="${puedeInvitarMiembros}"><li><a href="javascript:verEquipos()">Equipos</a></li></c:if>
			</ul>
		</div></c:if>
		<br>
		<div id="divDeProyectos">
			<c:if test="${organigrama!=null}">
				<h4>Fecha aproximada para la finalizacion de todos los proyectos: ${fechaFinPrevista}</h4>
				<button class='btn btn-info' onclick="mostrarEstadisticas()">Mostrar Planning</button><br>
			</c:if>
			<c:set var="esAGrupos" value="true"/>
			<h3>PROYECTOS DEL GRUPO ${grupoPerteneciente.nombreGrupo}<a href="${urlBase}/grupos/${grupoPerteneciente.idGrupo}.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
			<c:import url="/pages/page_proyectos.jsp"></c:import>  
		</div>
		<c:if test="${puedeCrearProyecto==true}">
		<div id="divDePlantillas" style="display:none;">
			<c:import url="/pages/page_plantillas.jsp"></c:import>  
		</div>
		</c:if> 
	</div>
	<c:if test="${horasDia!=0 && diasSemana!=0}">
	<div id="divMiembros" <c:if test="${verMiembros!=true}">style="display:none;"</c:if>>
		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<ul class="nav nav-tabs" role="tablist">
			  <li><a href="javascript:verProyectos()">Proyectos</a></li>
			  <li class='active'><a href="#">Miembros</a></li>
			  <li><a href="javascript:verSubgrupos()">Subgrupos</a></li>
			  <c:if test="${puedeInvitarMiembros}"><li><a href="javascript:verEquipos()">Equipos</a></li></c:if>
			</ul>
		</div>
		<br><c:import url="/pages/page_miembros.jsp"></c:import>  
	</div>
	<div id="divSubgrupos" <c:if test="${verSubgrupos!=true}">style="display:none;"</c:if>>
		<a href="javascript:verProyectos()">Proyectos</a> | <a href="javascript:verMiembros()">Miembros</a> | Subgrupos
		<c:if test="${puedeInvitarMiembros}"> | <a href="javascript:verEquipos()">Equipos</a></c:if>
		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<ul class="nav nav-tabs" role="tablist">
			  <li><a href="javascript:verProyectos()">Proyectos</a></li>
			  <li><a href="javascript:verMiembros()">Miembros</a></li>
			  <li class='active'><a href="#">Subgrupos</a></li>
			  <c:if test="${puedeInvitarMiembros}"><li><a href="javascript:verEquipos()">Equipos</a></li></c:if>
			</ul>
		</div>
		<br><c:import url="/pages/page_grupos.jsp"></c:import> 
	</div>
	
		<div id="divEquipos" <c:if test="${verEquipos!=true}">style="display:none;"</c:if>>
			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<ul class="nav nav-tabs" role="tablist">
			  <li><a href="javascript:verProyectos()">Proyectos</a></li>
			  <li><a href="javascript:verMiembros()">Miembros</a></li>
			  <li><a href="javascript:verSubgrupos()">Subgrupos</a></li>
			  <li class='active'><a href="#">Equipos</a></li>
			</ul>
		</div>
			<br><c:import url="/pages/page_equipos.jsp"></c:import> 
		</div>
	</c:if>
</body>
</html>