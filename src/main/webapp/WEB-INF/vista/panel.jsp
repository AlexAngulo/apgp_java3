<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<html>
<title>${sitio} - APGP</title>
	<c:import url="/includes/includes.jsp"></c:import>
	<c:import url="/includes/headerLogged.jsp"></c:import> 
	<script>
		function verProyectosAdmin(){
			document.getElementById("divProyectosAdmin").style = "";
			document.getElementById("divUsuariosAdmin").style = "display:none;";
			document.getElementById("divGruposAdmin").style = "display:none;";
		}
		function verUsuariosAdmin(){
			document.getElementById("divProyectosAdmin").style = "display:none;";
			document.getElementById("divUsuariosAdmin").style = "";
			document.getElementById("divGruposAdmin").style = "display:none;";
		}
		function verGruposAdmin(){
			document.getElementById("divProyectosAdmin").style = "display:none;";
			document.getElementById("divUsuariosAdmin").style = "display:none;";
			document.getElementById("divGruposAdmin").style = "";
		}
	</script>
</head>
<body>
	<h3>PANEL DE ADMINISTRACIÓN</h3>
	<div id="divProyectosAdmin" <c:if test="${verProyectosAdmin!=true}">style="display:none;"</c:if>>
		<ul class='nav nav-tabs' role='tablist'>
		  <li class='active'><a href='#'>Proyectos</a></li>
		  <li><a href='javascript:verUsuariosAdmin()'>Usuarios</a></li>
		  <li><a href='javascript:verGruposAdmin()'>Grupos</a></li>
		</ul>
		<c:import url="/pages_admin/page_proyectos_admin.jsp"></c:import>   
	</div>
	<div id="divUsuariosAdmin" <c:if test="${verUsuariosAdmin!=true}">style="display:none;"</c:if>>
		<ul class='nav nav-tabs' role='tablist'>
		  <li><a href='javascript:verProyectosAdmin()'>Proyectos</a></li>
		  <li class='active'><a href='#'>Usuarios</a></li>
		  <li><a href='javascript:verGruposAdmin()'>Grupos</a></li>
		</ul>
		<c:import url="/pages_admin/page_usuarios_admin.jsp"></c:import>   
	</div>
	<div id="divGruposAdmin" <c:if test="${verGruposAdmin!=true}">style="display:none;"</c:if>>
		<ul class='nav nav-tabs' role='tablist'>
		  <li><a href='javascript:verProyectosAdmin()'>Proyectos</a></li>
		  <li><a href='javascript:verUsuariosAdmin()'>Usuarios</a></li>
		  <li class='active'><a href='#'>Grupos</a></li>
		</ul>
		<c:import url="/pages_admin/page_grupos_admin.jsp"></c:import>   
	</div>
</body>
</html>