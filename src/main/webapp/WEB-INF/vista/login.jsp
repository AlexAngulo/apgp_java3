<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<html>
<head>
	<jsp:include page="/includes/includes.jsp"></jsp:include>
	<script>
	function doLogin(login, password) {
		formLoginInnerHtml = "<select name='email_o_login'><option value='"+login+"' selected></option><select>";
		formLoginInnerHtml += "<select name='password'><option value='"+password+"' selected></option><select>";
		document.getElementById("formLogin").innerHTML = formLoginInnerHtml;
		$('#formLogin').submit();
		//hacer un Login
	}
	
	function registrar() {
		el_login = document.getElementById("login").value;
		el_email = document.getElementById("email").value;
		el_password = document.getElementById("password").value;
		el_repassword = document.getElementById("repassword").value;
		diasSemanaP = document.getElementById("diasSemana").value;
		horasDiaP = document.getElementById("horasDia").value;
		if (el_login=="" || el_email=="" || el_password=="" || el_password!=el_repassword || diasSemanaP=='' || diasSemanaP<1 || diasSemanaP>7 || horasDiaP=='' || horasDiaP<1 || horasDiaP>24){
			alert("Datos err�neos");
		} else {
			$.postJSON("${urlBase}/registrar.htm", {idUsuario:0,login:el_login,email:el_email,password:el_password,emailVerificado:false,diasSemana:diasSemanaP,horasDia:horasDiaP}, function(data) {
				switch(data) {
				case 1:
					alert("Usuario creado con �xito");
					doLogin(el_login,el_password);
					break;
				case 0:
					alert("Debes rellenar el login, el email y el password");
					break;
				case 2:
					alert("El email introducido no se corresponde con un email");
					break;
				case 3:
					alert("El login introducido debe tener caracteres alfanumericos - y _");
					break;
				case 4:
					alert("Login ya existente, por favor, pon otro");
					break;
				case 5:
					alert("Email ya existente, por favor, pon otro");
					break;
				case 6:
					alert("Error interno en el servidor, disculpe las molestias");
					break;
				}
            }).fail(function(data){alert(data.responseText);});
		}
	}
	function mostrarRegistro() {
		document.getElementById("formLogin").style = "display:none;";
		document.getElementById("formRegistro").style = "";
	}
	
	function mostrarLogin() {
		document.getElementById("formRegistro").style = "display:none;";
		document.getElementById("formLogin").style = "";
	}
</script>
</head>
<body>
	<jsp:include page="/includes/headerWithoutLogin.jsp"></jsp:include>

	<form id='formLogin' method='POST' action='${urlBase}/doLogin.htm'
	 style="<c:if test="${sitio=='Registro'}" >display:none;</c:if>">
	  <br>
	  <fieldset style='text-align:center;'>
	    <legend>LOGU�ATE � <a href="javascript:mostrarRegistro();">REG�STRATE</a></legend>
	    <input type='text'  id='email_o_login' name='email_o_login' placeholder='Email o Login'/><br/>
	    <input type='password'  id='password_login' name='password' placeholder='Password'/><br>
	    <input type='submit' class='btn btn-info' value="Login"/>
	  </fieldset>
	</form>
	  
	<div id='formRegistro' 
	 style="<c:if test="${sitio=='Login'}" >display:none;</c:if>">
	  <br>
	  <fieldset style='text-align:center;'>
	    <legend><a href="javascript:mostrarLogin();">LOGU�ATE</a> � REG�STRATE</legend>
	    <input id='email'  type='email' name='email' placeholder='Email'/><br>
	    <input id='login'  type='text' name='login' placeholder='Login'/><br>
	    <input id='password'  type='password' name='password' placeholder='Password'/><br>
	    <input id='repassword'  type='password' name='repassword' placeholder='Repite password'/><br>
	    �Cu�nto tiempo piensas dedicar a tus proyectos?<br>
	    <input id='diasSemana' type='number' name='diasSemana' value='5' style='width:2em;' min='1' max='7'/> d�as a la semana<br>
	    <input id='horasDia' type='number' name='horasDia' value='4' style='width:2em;' min='1' max='24'/> horas al d�a<br>
	    <button onclick='registrar()' class='btn btn-info'>Registrar</button>
	  </fieldset>
	</div>
	<script>if('${message}'!='')
		alert('${message}')</script>
</body>
</html>
