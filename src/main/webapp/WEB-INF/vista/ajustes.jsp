<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<html>
<head>
	<c:import url="/includes/includes.jsp"></c:import>
	<c:set var="urlBase" value="${pageContext.request.contextPath}" />
</head>
<body>
	<c:import url="/includes/headerLogged.jsp"></c:import> 
	<c:import url="/pages/page_ajustes.jsp"></c:import> 
</body>
</html>