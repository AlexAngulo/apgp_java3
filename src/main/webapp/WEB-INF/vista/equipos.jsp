<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<html>
<head>
	<c:import url="/includes/includes.jsp"></c:import>
</head>
<body>
	<c:import url="/includes/headerLogged.jsp"></c:import> 
	<c:set var="deUsuario" value="true" />
	<c:import url="/pages/page_equipos.jsp"></c:import>  
</body>
</html>