<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<html>
<head>
	<c:import url="/includes/includes.jsp"></c:import>  
	<c:if test="${puedeCrearProyecto==true}">
	<script>
		var crearProyectoAPartirDePlantilla = "";
		function actionMostrarCreacionPorPlantilla(){
			document.getElementById("divDeProyectos").style="display:none;";
			document.getElementById("divDePlantillas").style="";
		}
		function actionMostrarProyectos(){
			document.getElementById("divDePlantillas").style="display:none;";
			document.getElementById("divDeProyectos").style="";
		}
		function mostrarProyectosCompartidos(){
			document.getElementById("divProyectosPropios").style="display:none;";
			document.getElementById("divProyectosCompartidos").style="";
			document.getElementById("divTareasAsignadas").style="display:none;";
		}
		function mostrarMisProyectos(){
			document.getElementById("divProyectosCompartidos").style="display:none;";
			document.getElementById("divProyectosPropios").style="";
			document.getElementById("divTareasAsignadas").style="display:none;";
		}
		function mostrarTareasAsignadas(){
			document.getElementById("divProyectosCompartidos").style="display:none;";
			document.getElementById("divProyectosPropios").style="display:none;";
			document.getElementById("divTareasAsignadas").style="";
		}
	</script>
	</c:if>
</head>
<body>
	<c:import url="/includes/headerLogged.jsp"></c:import>  
	<div id="divProyectosPropios">
		<div id="divDeProyectos">
			<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
				<ul class="nav nav-tabs" role="tablist">
				  <li class="active"><a href='#'>Proyectos creados por m�</a></li>
				  <li><a href="javascript:mostrarProyectosCompartidos()">Proyectos Compartidos</a></li>
				  <li><a href="javascript:mostrarTareasAsignadas()">Tareas Asignadas</a></li>
				</ul>
			</div>
			<c:set var="esCompartidos" value="" />
			<h3>MIS PROYECTOS<a href="${urlBase}/"><i class='glyphicon glyphicon-refresh'></i></a></h3>
			<%@include file="/pages/page_proyectos.jsp" %> 
		</div>
		<c:if test="${puedeCrearProyecto==true}">
		<div id="divDePlantillas" style="display:none;">
			<%@include file="/pages/page_plantillas.jsp" %> 
		</div>
		</c:if>
	</div>
	<div id="divProyectosCompartidos" style="display:none;">
		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<ul class="nav nav-tabs" role="tablist">
			  <li><a href="javascript:mostrarMisProyectos()">Proyectos creados por m�</a></li>
			  <li class="active"><a href="#">Proyectos Compartidos</a></li>
			  <li><a href="javascript:mostrarTareasAsignadas()">Tareas Asignadas</a></li>
			</ul>
		</div>
		<c:set var="proyectos" value="${proyectosCompartidos}" />
		<c:set var="puedeCrearProyecto" value="false" />
		<c:set var="puedeEditarProyecto" value="false" />
		<c:set var="puedeEliminarProyecto" value="false" />
		<c:set var="esCompartidos" value="compartido" />
		<c:set var="esCreador" value="false" />
		<h3>PROYECTOS COMPARTIDOS<a href="${urlBase}/proyectos/compartidos.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<%@include file="/pages/page_proyectos.jsp" %> 
	</div>
	<div id="divTareasAsignadas" style="display:none;">
		<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			<ul class="nav nav-tabs" role="tablist">
			  <li><a href="javascript:mostrarMisProyectos()">Proyectos creados por m�</a></li>
			  <li><a href="javascript:mostrarProyectosCompartidos()">Proyectos Compartidos</a></li>
			  <li class='active'><a href="#">Tareas Asignadas</a></li>
			</ul>
		</div>
		<c:set var="tareas" value="${tareasAsignadas}" />
		<h3>TAREAS ASIGNADAS<a href="${urlBase}/tareas/asignadas.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<%@include file="/pages/page_tareas.jsp" %> 
	</div>
	<c:if test="${irACompartidos==true}">
		<script>mostrarProyectosCompartidos();</script>
	</c:if>
	<c:if test="${irAAsignadas}">
		<script>mostrarTareasAsignadas();</script>
	</c:if>
</body>
</html>