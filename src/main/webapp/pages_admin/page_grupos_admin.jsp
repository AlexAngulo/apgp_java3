<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />

<script>
var grupos = [
              <c:set var="b" value="true" />
              <c:forEach items="${grupos}" var="grupo">
              	<c:if test="${b==false}">
              		,{
              	</c:if>
              	<c:if test="${b==true}">
	          		{
	          		<c:set var="b" value="false"/>
	          	</c:if>
              			idGrupo: "${grupo.idGrupo}",
              			nombreGrupo: "${grupo.nombreGrupo}",
              			descGrupo: "${grupo.descGrupo}",
              			breveDesc: "${grupo.breveDesc}",
              			permisos: 9999,
              			idSupergrupo: "${grupo.idSupergrupo}"
              		}
              </c:forEach>];

function actualizarEliminandoGrupo(idInterno) {
	idGrupo = grupos[idInterno].idGrupo;
	grupos[idInterno] = null;
	fila = document.getElementById("todo-grupo-"+idGrupo);
	document.getElementById("tbodyTodosGrupos").removeChild(fila);
}

function actualizarGrupo(idGrupo,nombreGrupo,descGrupo){
	grupos[indices_grupos[idGrupo]].nombreGrupo = nombreGrupo;
	grupos[indices_grupos[idGrupo]].descGrupo = descGrupo;
	innerNombre = "<a href='${urlBase}/grupos/"+idGrupo+".htm'>"+nombreGrupo+"</a>"
	if(descGrupo.length>50)
		innerDesc = descGrupo.substring(0,47) + '...';
	else
		innerDesc = descGrupo;
	document.getElementById("td-nombreGrupo-"+idGrupo).innerHTML = innerNombre;
	document.getElementById("td-descGrupo-"+idGrupo).innerHTML = innerDesc;
}

function editarGrupo(idGrupo){
	nombreGrupo = document.getElementById("nombreGrupo").value;
	descGrupo = document.getElementById("descGrupo").value;
	if(nombreGrupo==''){
		alert("Debes rellenar el nombre del grupo");
	} else {
		$.ajax({
			url: '${urlBase}/grupos/'+idGrupo+'.htm',
			data: '{"nombreGrupo":"'+nombreGrupo+'","descGrupo":"'+descGrupo+'"}',
			type: 'PUT',
			success: function(data){
				switch(data) {
				case "0":
					alert("No se ha podido editar el grupo");
					break;
				default:
					actualizarGrupo(idGrupo,nombreGrupo,descGrupo);
					if(confirm("Se ha editado el grupo llamado '"+nombreGrupo+"' �Quieres ver la p�gina del grupo?"))
						location.href="grupos/"+idEditar+".htm";
				}
			}
		}).fail(function(data){alert(data.responseText);});
	}
}

function eliminarGrupo(idInterno){
	if(confirm("�Est�s seguro de querer eliminar el Grupo?")){
		$.ajax({
			url: '${urlBase}/grupos/'+grupos[idInterno].idGrupo+'.htm',
			type: 'DELETE',
			success: function(){
				nombreAnterior = grupos[idInterno].nombreGrupo;
				actualizarEliminandoGrupo(idInterno);
				alert("Grupo '"+nombreAnterior+"' eliminado con �xito");
			}
		}).fail(function(data){alert(data.responseText);});
	}
}

var indices_grupos = [];

function actualizar_indices_grupos(){
	indices_grupos = [];
	for(i=0;i<grupos.length;i++){
		indices_grupos[grupos[i].idGrupo] = i;
	}
}

actualizar_indices_grupos();

function actionMostrarEditarGrupo(idInterno) {
	grupo = grupos[idInterno];
	innerHtml = "<h3>EDITAR GRUPO "+grupo.nombreGrupo+"</h3>";
	innerHtml += "<input id='nombreGrupo' value='"+grupo.nombreGrupo+"'/><br>";
	innerHtml += "<input id='descGrupo' value='"+grupo.descGrupo+"'/><br>";
	innerHtml += "<button class='btn btn-info' onclick='editarGrupo("+grupo.idGrupo+")'>Editar Grupo</button>";
	showLightbox(innerHtml);
}
</script>
<c:if test="${grupos.size()>0}">
	<div style="text-align:center;">
		<h3>GRUPOS EN EL SISTEMA<a href="${urlBase}/admin/grupos.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<table class='table table-striped'>
				<thead>
					<tr>
						<th>Nombre del grupo </th>
						<th> Descripci�n </th>
						<th> Creado por </th>
						<th> Acciones</th>
					</tr>
				</thead>
				<tbody id="tbodyTodosGrupos">
					<c:set var="inc" value="0" />
					<c:forEach items="${grupos}" var="grupo">
					<tr id="todo-grupo-${grupo.idGrupo}">
						<td id="td-nombreGrupo-${grupo.idGrupo}"><a href='${urlBase}/grupos/${grupo.idGrupo}.htm'>${grupo.nombreGrupo}</a></td>
						<td id="td-descGrupo-${grupo.idGrupo}">${grupo.breveDesc}</td>
						<td>${grupo.nombreCreador}</td>
						<td>
							<button class='btn btn-info' onclick="actionMostrarEditarGrupo(${inc})">Editar Grupo</button>
							<button class='btn btn-info' onclick="eliminarGrupo(${inc})">Eliminar Grupo</button>
						</td>
						<c:set var="inc" value="${inc+1}" />
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</c:if>
<c:if test="${grupos.size()==0}">
	No extisten grupos
</c:if>