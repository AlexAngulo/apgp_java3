<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />

<script>
	function eliminar_fila(idUsuario) {
		fila = document.getElementById("usuario"+idUsuario);
		document.getElementById("tbodyUsuarios").removeChild(fila);
	}

	function eliminarUsuario(idUsuario){
		if(confirm("�Est�s seguro de querer eliminar el usuario?")){
			$.ajax({
				url: '${urlBase}/usuarios/'+idUsuario+'.htm',
				type: 'DELETE',
				success: function(){
					eliminar_fila(idUsuario);
					alert("Se ha eliminado al usuario.");
				}
			}).fail(function(data){alert(data.responseText);});
		}
	}
</script>
<div style="text-align:center;">
	<h3>Usuarios del sistema<a href="${urlBase}/admin/usuarios.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
		<table class='table table-striped' id='tableUsuarios'>
			<thead>
			<tr>
				<th>Login del Usuario </th>
				<th> Acciones</th>
			</tr>
			</thead>
			<tbody id="tbodyUsuarios">
			<c:forEach items="${usuarios}" var="usuario">
				<tr id="usuario${usuario.idUsuario}">
				<td><a href="${urlBase}/proyectos-de/${usuario.login}.htm">${usuario.login}</a></td>
				<td>
					<c:if test="${usuario.esAdministrador==false}"><button class="btn btn-info" onclick="eliminarUsuario(${usuario.idUsuario})"><i class='glyphicon glyphicon-trash'></i></button></c:if>
				</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
</div>