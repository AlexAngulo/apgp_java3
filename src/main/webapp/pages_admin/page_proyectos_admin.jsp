<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
	<script>
		var crearProyectoAPartirDePlantilla = "";
		function actionMostrarCreacionPorPlantilla(){
			document.getElementById("divDeProyectos").style="display:none;";
			document.getElementById("divDePlantillas").style="";
		}
		function actionMostrarProyectos(){
			document.getElementById("divDePlantillas").style="display:none;";
			document.getElementById("divDeProyectos").style="";
		}
		
		function mostrarTodosLosProyectos(){
			document.getElementById("divTodosLosProyectos").style="";
			document.getElementById("divGestionPlantillas").style="display:none;";
			document.getElementById("divProyectosPropios").style="display:none;";
			document.getElementById("divProyectosCompartidos").style="display:none;";
			document.getElementById("divTareasAsignadas").style="display:none;";
		}
		function mostrarPlantillas(){
			document.getElementById("divTodosLosProyectos").style="display:none;";
			document.getElementById("divGestionPlantillas").style="";
			document.getElementById("divProyectosCompartidos").style="display:none;";
			document.getElementById("divProyectosPropios").style="display:none;";
			document.getElementById("divTareasAsignadas").style="display:none;";
		}
		function mostrarProyectosCompartidos(){
			document.getElementById("divTodosLosProyectos").style="display:none;";
			document.getElementById("divGestionPlantillas").style="display:none;";
			document.getElementById("divProyectosPropios").style="display:none;";
			document.getElementById("divProyectosCompartidos").style="";
			document.getElementById("divTareasAsignadas").style="display:none;";
		}
		function mostrarMisProyectos(){
			document.getElementById("divTodosLosProyectos").style="display:none;";
			document.getElementById("divGestionPlantillas").style="display:none;";
			document.getElementById("divProyectosCompartidos").style="display:none;";
			document.getElementById("divProyectosPropios").style="";
			document.getElementById("divTareasAsignadas").style="display:none;";
		}
		function mostrarTareasAsignadas(){
			document.getElementById("divTodosLosProyectos").style="display:none;";
			document.getElementById("divGestionPlantillas").style="display:none;";
			document.getElementById("divProyectosCompartidos").style="display:none;";
			document.getElementById("divProyectosPropios").style="display:none;";
			document.getElementById("divTareasAsignadas").style="";
		}
	</script>
	<div id="divTodosLosProyectos">
		<div>
			Todos los proyectos | 
			<a href="javascript:mostrarPlantillas()">Plantillas</a> | 
			<a href="javascript:mostrarMisProyectos()">Proyectos creados por m�</a> | 
			<a href="javascript:mostrarProyectosCompartidos()">Proyectos Compartidos</a> | 
			<a href="javascript:mostrarTareasAsignadas()">Tareas Asignadas</a>
		</div>
		<c:set var="proyectos" value="${todosLosProyectos}" />
		<c:set var="puedeCrearProyecto" value="false" />
		<c:set var="puedeEditarProyecto" value="true" />
		<c:set var="puedeEliminarProyecto" value="true" />
		<c:set var="esCreador" value="false" />
		<c:set var="esCompartidos" value="todosLosProyectos" />
		<h3>PROYECTOS DEL SISTEMA<a href="${urlBase}/admin/proyectos.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<%@include file="/pages/page_proyectos.jsp" %> 
	</div>
	<div id="divGestionPlantillas" style="display:none;">
		<div>
			<a href="javascript:mostrarTodosLosProyectos()">Todos los proyectos</a> | 
			Plantillas | 
			<a href="javascript:mostrarMisProyectos()">Proyectos creados por m�</a> | 
			<a href="javascript:mostrarProyectosCompartidos()">Proyectos Compartidos</a> | 
			<a href="javascript:mostrarTareasAsignadas()">Tareas Asignadas</a>
		</div>
		<c:set var="proyectos" value="${todasLasPlantillas}" />
		<c:set var="puedeCrearProyecto" value="true" />
		<c:set var="puedeEditarProyecto" value="true" />
		<c:set var="puedeEliminarProyecto" value="true" />
		<c:set var="compartirProyecto" value="false" />
		<c:set var="esCreador" value="true" />
		<c:set var="esCompartidos" value="plantillas" />
		<h3>GESTI�N DE PLANTILLAS<a href="${urlBase}/admin/plantillas.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<%@include file="/pages/page_proyectos.jsp" %> 
	</div>
	<div id="divProyectosPropios" style="display:none;">
		<div id="divDeProyectos">
			<div>
				<a href="javascript:mostrarTodosLosProyectos()">Todos los proyectos</a> | 
				<a href="javascript:mostrarPlantillas()">Plantillas</a> | 
				Proyectos creados por m� | 
				<a href="javascript:mostrarProyectosCompartidos()">Proyectos Compartidos</a> | 
				<a href="javascript:mostrarTareasAsignadas()">Tareas Asignadas</a>
			</div>
			<c:set var="proyectos" value="${proyectosCreadosPorMi}" />
			<c:set var="puedeCrearProyecto" value="true" />
			<c:set var="puedeEditarProyecto" value="true" />
			<c:set var="puedeEliminarProyecto" value="true" />
			<c:set var="compartirProyecto" value="true" />
			<c:set var="esCreador" value="true" />
			<c:set var="esCompartidos" value="" />
			<h3>PROYECTOS PROPIOS<a href="${urlBase}/admin/proyectos/propios.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
			<%@include file="/pages/page_proyectos.jsp" %> 
		</div>
		<c:if test="${puedeCrearProyecto==true}">
		<div id="divDePlantillas" style="display:none;">
			<%@include file="/pages/page_plantillas.jsp" %> 
		</div>
		</c:if>
	</div>
	<div id="divProyectosCompartidos" style="display:none;">
		<div>
			<a href="javascript:mostrarTodosLosProyectos()">Todos los proyectos</a> | 
			<a href="javascript:mostrarPlantillas()">Plantillas</a> | 
			<a href="javascript:mostrarMisProyectos()">Proyectos creados por m�</a> | 
			Proyectos Compartidos | 
			<a href="javascript:mostrarTareasAsignadas()">Tareas Asignadas</a>
		</div>
		<c:set var="proyectos" value="${proyectosCompartidos}" />
		<c:set var="puedeCrearProyecto" value="false" />
		<c:set var="puedeEditarProyecto" value="false" />
		<c:set var="puedeEliminarProyecto" value="false" />
		<c:set var="compartirProyecto" value="true" />
		<c:set var="esCompartidos" value="compartido" />
		<c:set var="esCreador" value="false" />
		<h3>PROYECTOS COMPARTIDOS<a href="${urlBase}/admin/proyectos/compartidos.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<%@include file="/pages/page_proyectos.jsp" %> 
	</div>
	<div id="divTareasAsignadas" style="display:none;">
		<div>
			<a href="javascript:mostrarTodosLosProyectos()">Todos los proyectos</a> | 
			<a href="javascript:mostrarPlantillas()">Plantillas</a> | 
			<a href="javascript:mostrarMisProyectos()">Proyectos creados por m�</a> | 
			<a href="javascript:mostrarProyectosCompartidos()">Proyectos Compartidos</a> | 
			Tareas Asignadas
		</div>
		<h3>TAREAS ASIGNADAS<a href="${urlBase}/admin/tareas/asignadas.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<c:set var="tareas" value="${tareasAsignadas}" />
		<%@include file="/pages/page_tareas.jsp" %> 
	</div>
	<c:if test="${verTodosLosProyectos==true}">
		<script>mostrarTodosLosProyectos();</script>
	</c:if>
	<c:if test="${verPlantillas==true}">
		<script>mostrarPlantillas();</script>
	</c:if>
	<c:if test="${verMisProyectos==true}">
		<script>mostrarMisProyectos();</script>
	</c:if>
	<c:if test="${verProyectosCompartidos==true}">
		<script>mostrarProyectosCompartidos();</script>
	</c:if>
	<c:if test="${verTareasAsignadas==true}">
		<script>mostrarTareasAsignadas();</script>
	</c:if>