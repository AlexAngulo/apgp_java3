<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>
var miembros = [<c:set var="b" value="${true}"/><c:forEach items="${miembros}" var="miembro"><c:if test="${b==false}">,</c:if><c:if test="${b==true}">
<c:set var="b" value="${false}"/></c:if>"${miembro}"</c:forEach>]
function agregar_a_array(idInterno,objeto,array){
	for(var i=array.length;i>idInterno;i--){
		array[i] = array[i-1];
	}
	array[idInterno] = objeto;
	return array;
}
function agregar_a_array(idInterno,objeto,array){
	for(var i=array.length;i>idInterno;i--){
		array[i] = array[i-1];
	}
	array[idInterno] = objeto;
	return array;
}
function eliminar_elemento(idFila){
	filaProyecto = document.getElementById(idFila);
	if(filaProyecto){
		tabla = filaProyecto.parentNode;
		tabla.removeChild(filaProyecto);
	}
}

		var ordenacionesTareas = [<c:set var="b" value="${true}"/>
		                          	<c:forEach items="${ordenTareasTotales}" var="ordenTareaTotal" >
		                              	<c:if test="${b==false}">,</c:if>
		                          		<c:if test="${b==true}"><c:set var="b" value="${false}"/></c:if>
		                          			"${ordenTareaTotal.value1}"
	                          		</c:forEach>]

		var tareas = [
      	<c:set var="inc" value="${0}"/>
      	<c:forEach items="${tareas}" var="tarea" >
          	<c:if test="${inc>0}">
      			,{
      		</c:if>
      		<c:if test="${inc==0}">
      			{
      		</c:if>
      		idTarea:"${tarea.idTarea}",
			idProyecto:"${tarea.idProyecto}",
			nombreTarea:"${tarea.nombreTarea}",
			descTarea:"${tarea.descTarea}",
			nombreProyecto:"${tarea.nombreProyecto}",
			idGrupo:"${tarea.idGrupo}",
			nombreGrupo:"${tarea.nombreGrupo}",
			idSupertarea:"<c:if test='${tarea.idSupertarea>0}'>${tarea.idSupertarea}</c:if>",
			duracion:"${tarea.duracion}",
			holgura:"${tarea.holgura}",
			completadoTotal:"${tarea.completadoTotal}",
			tieneHijos:
				<c:if test="${tareas.size()==(inc+1)}">false</c:if>
				<c:if test="${tareas.size()>(inc+1)}">
					<c:if test="${tarea.nivel<tareas.get(inc+1).nivel}">true</c:if>
					<c:if test="${tarea.nivel>=tareas.get(inc+1).nivel}">false</c:if>
				</c:if>,
			porcentaje:"${tarea.porcentaje}",
			completadoUsuario:"${tarea.completado}",
			nivel: ${tarea.nivel},
			espacios: "${tarea.espacios}",
			debeActualizarDependencias: false,
			numTarea: "${tarea.numTarea}",
			asignados: [
							<c:set var="c" value="${true}"/>
							<c:forEach items="${tarea.usuariosAsignados}" var="asignado" >
						  		<c:if test="${c==false}">
									,{
								</c:if>
								<c:if test="${c==true}">
									{<c:set var="c" value="${false}"/>
								</c:if>
									login: "${asignado.value0}",
									completado: ${asignado.value1},
									porcentaje: ${asignado.value2}
									}
							</c:forEach>
		                    ],
		    duracionAsignada: "${tarea.duracionAsignada}"
			<c:set var="inc" value="${inc+1}" />
      		}
      	</c:forEach>
      	];
		var indices_tareas = [];
		function actualizar_indices_tareas(){
			indices_tareas = [];
			for(a=0;a<tareas.length;a++){
				indices_tareas[tareas[a].idTarea] = a;
			}
		}
		actualizar_indices_tareas();
		
		

	
		<c:if test="${puedeEditarTareas}">
		function cambiar_orden_tareas(valor){
			if(valor=="") valor = 0;
			$.ajax({
				url: '${urlBase}/orden-tareas.htm',
				data: ''+valor,
				type: 'PUT',
				success: function(data){
					switch(data) {
					case "0":
						alert("No se ha podido editar la ordenaci�n de tareas en el proyecto");
						break;
					default:
						//TODO realizar cambios est�ticamente
						location.href = location.href;
					}
				}
			}).fail(function(data){alert(data.responseText);});
		}
		</c:if>

		

		
		<c:if test="${puedeAsignarTareas || puedeEditarAsignar || puedeEliminarAsignar}">
		function usuarioSeEncuentraAsignado(lista,login){
			for(var j=0;j<lista.length;j++){
				if(lista[j].login==login)
					return true;
			}
			return false;
		}
		
		function obtenerPorcentajeMaximo(usuariosAsignados){
			sum = 100;
			for(var k=0;k<usuariosAsignados.length;k++){
				sum = sum - usuariosAsignados[k].porcentaje;
			}
			if(sum<0) return 0;
			return sum;
		}
		
		function asignar_tarea(idTarea,idProyecto){
			log =document.getElementById("loginAsignar").value;
			por = document.getElementById("porcentajeAsignar").value;
			if(log=='' || por==''){
				alert('Debes rellenar el login del miembro y el procentaje de tarea que le corresponde al miembro.');
			} else {
				$.postJSON("${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/asignar.htm", {login:log,porcentaje:por}, function(data) {
					switch(data) {
					case "0":
						alert("No se ha podido crear el proyecto");
						break;
					default:
						location.href = location.href;
					}
	            }).fail(function(data){alert(data.responseText);});
			}
		}
		
		function editar_asignar(idTarea,idProyecto,log){
			por = document.getElementById("porcentaje-"+idTarea+"-"+log).value;
			elJSON = '{"login":"'+log+'","porcentaje":'+por+'}';
			$.ajax({
			    url: "${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/asignar.htm",
			    type: 'PUT',
			    data: elJSON,
			    success: function(data) {
			    	switch(data) {
					case '0':
						alert("No se ha podido editar el asignamiento");
					default:
						alert("Tarea asignada cambiado con �xito");
						location.href = location.href;
					}
			    }
			}).fail(function(data){alert(data.responseText);});
		}
		
		function eliminar_asignar(idTarea,idProyecto,log){
			$.ajax({
			    url: "${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/desasignar.htm",
			    type: 'DELETE',
			    data: log,
			    success: function(data) {
			    	switch(data) {
					case '0':
						alert("No se ha podido eliminar el asignamiento.");
					default:
						alert("Compartido cambiado con �xito");
						location.href = location.href;
					}
			    }
			}).fail(function(data){alert(data.responseText);});
		}
		
		function asignarTarea(idInterno){
			tarea = tareas[idInterno];
			usuariosAsignados = tarea.asignados;
			innerHtml ="";
			<c:if test="${puedeAsignarTareas}">
			innerHtml += "ASIGNAR:<br>";
			innerHtml += "Asignar tarea a:<select id='loginAsignar'><option value='' selected></option>";
			if(!usuarioSeEncuentraAsignado(usuariosAsignados,'${usuario.login}'))
				innerHtml += "<option value='${usuario.login}'>Yo</option>";
			for(var j=0;j<miembros.length;j++)
				if(miembros[j]!='${usuario.login}' && !usuarioSeEncuentraAsignado(usuariosAsignados,miembros[j]))
					innerHtml += "<option value='"+miembros[j]+"'>"+miembros[j]+"</option>";
			porcMax = obtenerPorcentajeMaximo(usuariosAsignados);
			innerHtml += "<br><input id='porcentajeAsignar' type='number' min='1' max='"+porcMax+"' />% Max: "+porcMax;
			innerHtml += "<br><button class='btn btn-info' onclick='asignar_tarea("+tarea.idTarea+","+tarea.idProyecto+")'>Asignar Tarea</button>";
			</c:if>
			if(usuariosAsignados.length>0){
				innerHtml += "<table class='table table-striped' id='tableAsignados'><thead><tr>";
				innerHtml += "<th>Login</th><th>Completado</th><th>Porcentaje</th>";
				<c:if test="${puedeEditarAsignar}">
					innerHtml += "<th>Acciones</th></tr></thead><tbody>";
					for(var l=0;l<usuariosAsignados.length;l++){
						innerHtml += "<tr><td>"+usuariosAsignados[l].login+"</td><td>"+
						usuariosAsignados[l].completado+"</td><td>"+
						"<input id='porcentaje-"+tarea.idTarea+"-"+usuariosAsignados[l].login+"' type='number' min='"+usuariosAsignados[l].completado+"' max='"+
						(porcMax+parseInt(usuariosAsignados[l].porcentaje))+
						"' value='"+usuariosAsignados[l].porcentaje+"'/>%";
						innerHtml += "</td><td><button class='btn btn-info' onclick='editar_asignar("+tarea.idTarea+","+tarea.idProyecto+",\""+usuariosAsignados[l].login+"\")'"+usuariosAsignados[l].login+"')'><i class='glyphicon glyphicon-edit'></i></button>";
						<c:if test='${puedeEliminarAsignar}'>
							innerHtml += "<button class='btn btn-info' onclick='eliminar_asignar("+tarea.idTarea+","+tarea.idProyecto+",\""+usuariosAsignados[l].login+"\")'"+usuariosAsignados[l].login+"')'><i class='glyphicon glyphicon-trash'></i></button>";
						</c:if>
						innerHtml += "</td></tr>";
					}
				</c:if>
				<c:if test="${puedeEditarAsignar==false}">
					<c:if test="${puedeEliminarAsignar}">innerHtml += "<th>Acciones</th>";</c:if>
					innerHtml += "</tr></thead><tbody>";
					for(var l=0;l<usuariosAsignados.length;l++){
						innerHtml += "<tr><td>"+usuariosAsignados[l].login+"</td><td>"+
						usuariosAsignados[l].completado+"</td><td>"+
						usuariosAsignados[l].porcentaje;
						innerHtml += "</td>";
						<c:if test='${puedeEliminarAsignar}'>
							innerHtml += "<td><button class='btn btn-info' onclick='eliminar_asignar_tarea("+tarea.idTarea+",'"+usuariosAsignados[l].login+"')'><i class='glyphicon glyphicon-trash'></i></button></td>";
						</c:if>
						innerHtml += "</tr>";
					}
				</c:if>
				innerHtml += "</tbody></table>";
			} else {
				innerHtml += "No hay asignamientos";
			}
			showLightbox(innerHtml);
		}
		</c:if>
		
		function cambiarPorcentaje(cambiarA,idInterno){
			tarea = tareas[idInterno];
			if(parseInt(cambiarA)<=parseInt(tarea.porcentaje)){
				progress_bar = document.getElementById("progressbar-"+tarea.idTarea);
				progress_bar.setAttribute("aria-valuenow",cambiarA);
				progress_bar.style="width:"+cambiarA+"%;";
				progress_bar.innerHTML = cambiarA+"%";
				document.getElementById("buttonUpdateTarea-"+tarea.idTarea).style="";
			}
		}
		function updateTarea(idInterno){
			tarea = tareas[idInterno];
			cambiarA = document.getElementById("inputCompletado-"+tarea.idTarea).value;
			if(document.getElementById("buttonUpdateTarea-"+tarea.idTarea).style!="display:none;" &&
				parseInt(cambiarA)<=parseInt(tarea.porcentaje)){
				$.ajax({
				    url: "${urlBase}/proyectos/"+tarea.idProyecto+"/tareas/asignadas/"+tarea.idTarea+"/completar.htm",
				    type: 'PUT',
				    data: cambiarA,
				    success: function(data) {
						alert("Tarea Asignada Completada con �xito");
						document.getElementById("buttonUpdateTarea-"+tarea.idTarea).style="display:none;";
				    }
				}).fail(function(data){alert(data.responseText);});
			}
		}
	</script>
		<select onchange="cambiar_orden_tareas(this.value)">
			<c:forEach items="${ordenTareasTotales}" var="ordenTareaTotal">
				<option value="${ordenTareaTotal.value0}" <c:if test="${ordenTareaTotal.value0==proyecto.ordenacionTareas}">selected</c:if>
				>${ordenTareaTotal.value1}</option>
			</c:forEach>
		</select>
		<c:if test="${tareas.size()>0 }">
			<div style="text-align:center;">
				Tareas asignadas a ${usuario.login}
				<br>
				<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
					<table class='table table-striped'>
						<thead>
						<tr>
							<th>#</th>
							<th>Nombre de la tarea</th>
							<th>Duraci�n asignada</th>
							<th>Proyecto</th>
							<th>Grupo</th>
							<th></th>
							<th>Completado</th>
							<th>Porcentaje asignado</th>
						</tr>
						</thead>
						<tbody>
						<c:set var="inc" value="0" />
						<c:forEach items="${tareas}" var="tarea" >
						<tr id="tarea${tarea.idTarea}" class="${tarea.superclase}">
							<td>${tarea.numTarea}</td>
							<td>${tarea.espacios}${tarea.nombreTarea}</td>
							<td>${tarea.duracionAsignada} horas</td>
							<td><a href="${urlBase}/proyectos/${tarea.idProyecto}.htm">${tarea.nombreProyecto}</a></td>
							<td><a href="${urlBase}/grupos/${tarea.idGrupo}.htm">${tarea.nombreGrupo}</a></td>
							<td><input id="inputCompletado-${tarea.idTarea}" type="number" min="0" max="${tarea.porcentaje}" 
									onkeyup="cambiarPorcentaje(this.value,${inc})" onchange="cambiarPorcentaje(this.value,${inc})" style="width:3em;" value="${tarea.completado}"/>
									<button class='btn btn-info' id="buttonUpdateTarea-${tarea.idTarea}" onclick="updateTarea(${inc})"><i class="glyphicon glyphicon-hand-right"></i></button>
									
							</td>
							<td>
								<div class="progress">
								  <div id="progressbar-${tarea.idTarea}" class="progress-bar progress-bar-info" 
								  role="progressbar" aria-valuenow="${tarea.completado}" aria-valuemin="0" 
								  aria-valuemax="${tarea.porcentaje}" style="width: ${tarea.completado / tarea.porcentaje * 100}%">
								    ${tarea.completado}%
								  </div>
								</div>
							</td>
							<td>${tarea.porcentaje}%</td>
							<c:set var="inc" value="${inc+1}" />
						</tr>
						</c:forEach>
						</tbody>		
					</table>
				</div>
			</div>
		</c:if>
		<c:if test="${tareas.size()==0 }">
			<p class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>Careces de tareas aignadas</p>
		</c:if>