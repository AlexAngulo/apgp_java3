<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>
var miembros = [<c:set var="b" value="${true}"/><c:forEach items="${miembros}" var="miembro"><c:if test="${b==false}">,</c:if><c:if test="${b==true}">
<c:set var="b" value="${false}"/></c:if>"${miembro}"</c:forEach>]
var equipos = [<c:set var="b" value="${true}"/><c:forEach items="${equipos}" var="equipo"><c:if test="${b==false}">,{</c:if><c:if test="${b==true}">
{<c:set var="b" value="${false}"/></c:if>idEquipo:"${equipo.idEquipo}",nombreEquipo:"${equipo.nombreEquipo}"}</c:forEach>]
var compartidos = [<c:set var="b" value="${true}"/><c:forEach items="${compartidos}" var="compartido"><c:if test="${b==false}">,</c:if><c:if test="${b==true}">
<c:set var="b" value="${false}"/></c:if>"${compartido}"</c:forEach>];
function agregar_a_array(idInterno,objeto,array){
	for(var i=array.length;i>idInterno;i--){
		array[i] = array[i-1];
	}
	array[idInterno] = objeto;
	return array;
}

function eliminar_elemento(idFila){
	filaProyecto = document.getElementById(idFila);
	if(filaProyecto){
		tabla = filaProyecto.parentNode;
		tabla.removeChild(filaProyecto);
	}
}

		var ordenacionesTareas = [<c:set var="b" value="${true}"/>
		                          	<c:forEach items="${ordenTareas}" var="orden" >
		                              	<c:if test="${b==false}">,</c:if>
		                          		<c:if test="${b==true}"><c:set var="b" value="${false}"/></c:if>
		                          			"${orden.value1}"
	                          		</c:forEach>]

		var tareas = [
      	<c:set var="b" value="${true}"/>
      	<c:forEach items="${tareas}" var="tarea" >
          	<c:if test="${b==false}">
      			,{
      		</c:if>
      		<c:if test="${b==true}">
      			{
      			<c:set var="b" value="${false}"/>
      		</c:if>
      		idTarea:"${tarea.idTarea}",
			idProyectoPerteneciente:"${tarea.idProyectoPerteneciente}",
			idSupertarea:"<c:if test='${tarea.idSupertarea>0}'>${tarea.idSupertarea}</c:if>",
			nombreTarea:"${tarea.nombreTarea}",
			descTarea:"${tarea.descTarea}",
			duracion:"${tarea.duracion}",
			prioridad:"${tarea.prioridadTarea}",
			completado:"${tarea.completado}",
			tieneHijos:"${tarea.puedeSerHijoDe.size()<(tareas.size()-1)}",
			puedeDependerDe: [
								<c:set var="c" value="${true}"/>
								<c:forEach items="${tarea.puedeDependerDe}" var="puedeDepender" >
							  		<c:if test="${c==false}">
										,
									</c:if>
									<c:if test="${c==true}">
										<c:set var="c" value="${false}"/>
									</c:if>
										${puedeDepender}
								</c:forEach>
			                    ],
			puedeSerHijoDe: [
								<c:set var="c" value="${true}"/>
								<c:forEach items="${tarea.puedeSerHijoDe}" var="serHijoDe" >
							  		<c:if test="${c==false}">
										,
									</c:if>
									<c:if test="${c==true}">
										<c:set var="c" value="${false}"/>
									</c:if>
										${serHijoDe}
								</c:forEach>
			                    ],
            dependencias: [
							<c:set var="c" value="${true}"/>
							<c:forEach items="${tarea.dependencias}" var="dependencia" >
						  		<c:if test="${c==false}">
									,
								</c:if>
								<c:if test="${c==true}">
									<c:set var="c" value="${false}"/>
								</c:if>
									${dependencia}
							</c:forEach>
		                    ],
             dependenciasPublicas: [
							<c:set var="c" value="${true}"/>
							<c:forEach items="${tarea.dependenciasPublicas}" var="dependencia" >
						  		<c:if test="${c==false}">
									,
								</c:if>
								<c:if test="${c==true}">
									<c:set var="c" value="${false}"/>
								</c:if>
									"${dependencia}"
							</c:forEach>
		                    ],
		    usuariosAsignados: [
							<c:set var="c" value="${true}"/>
							<c:forEach items="${tarea.usuariosAsignados}" var="usuarioAsignado" >
									<c:if test="${c==false}">
									,{
								</c:if>
								<c:if test="${c==true}">
									<c:set var="c" value="${false}"/>{
								</c:if>
									login: "${usuarioAsignado.login}",
									porcentaje: "${usuarioAsignado.porcentaje}",
									completado: "${usuarioAsignado.completado}"
									}
							</c:forEach>
		                        ],
             equiposAsignados: [
     							<c:set var="c" value="${true}"/>
     							<c:forEach items="${tarea.equiposAsignados}" var="equipoAsignado" >
     									<c:if test="${c==false}">
     									,{
     								</c:if>
     								<c:if test="${c==true}">
     									<c:set var="c" value="${false}"/>{
     								</c:if>
     									nombreEquipo: "${equipoAsignado.nombreEquipo}",
     									idEquipo: "${equipoAsignado.idEquipo}",
     									porcentaje: "${equipoAsignado.porcentaje}",
     									completado: "${equipoAsignado.completado}"
     									}
     							</c:forEach>
     		                        ],
			superclase: "${tarea.superclase}",
			nivel: ${tarea.nivel},
			espacios: "${tarea.espacios}",
			debeActualizarDependencias: false,
			numTarea: "${tarea.numTarea}"
      		}
      	</c:forEach>
      	];
		var indices_tareas = [];
		function actualizar_indices_tareas(){
			indices_tareas = [];
			for(a=0;a<tareas.length;a++){
				indices_tareas[tareas[a].idTarea] = a;
			}
		}
		actualizar_indices_tareas();
		
		
		function modificarTarea(idTarea) {
			//PUT: idTarea, {nombreTarea, descTarea, idSupertarea, duracion, holgura, completado, comoCreadorProyecto, dependenciaAnterior[1..hasta el ultimo], dependencia[1..hasta el ultimo]}
			tarea = tareas[indices_tareas[idTarea]];	
			nombreTarea = document.getElementById("nombreTarea").value;
			descTarea = document.getElementById("descTarea").value;
			idSupertarea = document.getElementById("idSupertarea").value;
			if(idSupertarea=='') idSupertarea = 0;
			duracion = document.getElementById("duracion").value;
			if(duracion=='') duracion = 0;
			holgura = document.getElementById("holgura").value;
			if(holgura=='') holgura = 0;
			com = document.getElementById("completado");
			if(com)
				completado = com.value;
			else
				completado = tarea.completado;
			if(completado=='') completado = 0;
			comoCreadorProyecto = '${esCreador}';
			if(nombreTarea==''){
				alert('Debes rellenar el titulo');
			} else {
				elJSON = '{"nombreTarea":"'+nombreTarea+'","descTarea":"'+descTarea+'","idSupertarea":'+idSupertarea+
					',"duracion":'+duracion+',"holgura":'+holgura+',"completado":'+completado+',"comoCreadorProyecto":'+comoCreadorProyecto+',"idProyecto":'+tarea.idProyectoPerteneciente+','+
					'"desdeTareasAsignadas":false';
				for(b=0;b<tarea.dependencias.length;b++){
					elJSON += ',"dependenciaAnterior'+b+'":'+tarea.dependencias[b];
				}
				c=0;
				for(b=0;document.getElementById("idDependencia"+b)!=undefined;b++){
					valor = document.getElementById("idDependencia"+b).value;
					if(valor!='' && valor!='0'){
						elJSON += ',"dependencia'+c+'":'+valor;
						c++;
					}
				}
				elJSON += '}';
				
				$.ajax({
					url: '${urlBase}/tareas/'+idTarea+'.htm',
					data: elJSON,
					type: 'PUT',
					success: function(){
						actualizar_tareas_con_organigrama();
						actionOcultarCrearTarea();actionMostrarCrearTarea();
						alert("Se ha editado la tarea llamada "+nombreTarea);
					}
				}).fail(function(data){alert(data.responseText);});
			}
		}
		
		function eliminar(idInterno){
			tarea = tareas[idInterno];
			if(confirm('�Realmente deseas borrar "'+tarea.nombreTarea+'"?')){
				//if(tarea.tieneHijos=="true" && confirm('�Deseas borrar todas sus subtareas?')){
					//codEliminacion = 0;
				//} else 
				if(tarea.tieneHijos=="true") {
					if(confirm('�Deseas que las subtareas sean hijas del padre de la tarea a eliminar?')){
						codEliminacion = 2;
					} else {
						codEliminacion = 1;
					}
				} else {
					codEliminacion = 1;
				}
				comoCreadorProyecto = '${esCreador}';
				elJSON = '{"comoCreadorProyecto":'+comoCreadorProyecto+',"codEliminacion":'+codEliminacion+'}';
				$.ajax({
					url: '${urlBase}/tareas/'+tarea.idProyectoPerteneciente+'/'+tarea.idTarea+'.htm',
					data: elJSON,
					type: 'DELETE',
					success: function(idTarea){
						actualizar_tareas_con_organigrama();
						alert("Se ha eliminado la tarea llamada "+tarea.nombreTarea);
					}
				}).fail(function(data){alert(data.responseText);});
			}
		}
		function eliminar_tarea_y_dejar_huerfanos_a_sus_hijos(tarea){
			
		}
		function eliminar_fila(idTarea){
			eliminar_elemento("tarea"+idTarea);
		}
		function actualizar_dependencias_en_fila(idTarea,dependencias){
			innerHtmlDependencias = "";
			for(i=0;i<dependencias.length;i++){
				innerHtmlDependencias += " "+dependencias[i];
			}
			document.getElementById("dependencias"+idTarea).innerHTML = innerHtmlDependencias;
		}
		function eliminar_dependencias(idTarea){
			//Eliminarlo de tareas
			tareas.splice(idTarea,1);
			//volver a establecer "posibles dependencias" en la variable tareas
			//Eliminarlo de la tabla
			for(j=0;j<tareas.length;j++){
				for(k=0;k<tareas[j].dependencias.length;k++){
					if(tareas[j].dependencias[k]==idTarea){
						tareas[j].dependencias.splice(k,1);
						tareas[j].debeActualizarDependencias = true;
					}
				}
			}
		}
		function actualizar_dependencias_de_las_que_debe() {
			for(l=0;l<tareas.length;l++){
				if(tareas[l].debeActualizarDependencias){
					actualizar_dependencias_en_fila(tareas[l].idTarea,tareas[l].dependencias);
					tareas[l].debeActualizarDependencias = false;
				}
			}
		}
		function eliminar_tarea_y_sus_subtareas(tarea, empezar_desde) {
			if(tarea.tieneHijos=="true"){
				for(var m=empezar_desde;m<tareas.length;m++){
					if(tareas[m].idSupertarea==tarea.idTarea){
						eliminar_tarea_y_sus_subtareas(tareas[m],m+1);
					} else {
						m=tareas.length;
					}
				}
			}
			eliminar_fila(tarea.idTarea);
			eliminar_dependencias(tarea.idTarea);
		}
		
		function crearTarea(){
			//POST: {idProyectoPerteneciente, nombreTarea, descTarea, idSupertarea, duracion, holgura, comoCreadorProyecto, dependecia[1..hasta el final]}
			idProyectoPerteneciente = '${proyecto.idProyecto}';
			nombreTarea = document.getElementById("nombreTarea").value;
			descTarea = document.getElementById("descTarea").value;
			idSupertarea = document.getElementById("idSupertarea").value;
			if(idSupertarea=='') idSupertarea = 0;
			duracion = document.getElementById("duracion").value;
			if(duracion=='') duracion = 0;
			holgura = document.getElementById("holgura").value;
			if(holgura=='') holgura = 0;
			comoCreadorProyecto = '${esCreador}';
			if(idProyectoPerteneciente=='' || nombreTarea==''){
				alert('Debes rellenar el titulo');
			} else {
				elJSON = '{"idProyectoPerteneciente":'+idProyectoPerteneciente+',"nombreTarea":"'+nombreTarea+'","descTarea":"'+descTarea+'",'+
					'"idSupertarea":'+idSupertarea+',"duracion":'+duracion+',"holgura":'+holgura+',"comoCreadorProyecto":'+comoCreadorProyecto;
				o=0;
				for(n=0;document.getElementById("idDependencia"+n)!=undefined;n++){
					valor = document.getElementById("idDependencia"+n).value;
					if(valor!='' && valor!='0'){
						elJSON += ',"dependencia'+o+'":'+valor;
						o++;
					}
				}
				elJSON += '}';
				
				$.ajax({
					url: '${urlBase}/tareas.htm',
					data: elJSON,
					type: 'POST',
					success: function(){
						actualizar_tareas_con_organigrama();
						actionOcultarCrearTarea();actionMostrarCrearTarea();
						alert("Se ha creado una nueva tarea llamada "+nombreTarea);
					}
				}).fail(function(data){alert(data.responseText);});
			}
		}
	
		function obtenerTareaPorIdTarea(idTarea){
			for(var d=0;d<tareas.length;d++)
				if(tareas[d].idTarea==idTarea)
					return tareas[d];
			return null;
		}
	
		function descompletarCompletado(){
			document.getElementById("divCompletado").innerHTML = 'Completado:<input type="number" max="100" min="0" id="completado" name="completado" value="99"/>%<br>';
			document.getElementById("divCompletado").innerHTML +='<input type="button" id="completoCompletado" onclick="completarCompletado()" value="Completar"/>';
			document.getElementById("completoCompletado").setAttribute("onclick","completarCompletado()");
		}
	
		function completarCompletado(){
			document.getElementById("completado").setAttribute("value","100");
			document.getElementById("completado").setAttribute("disabled","disabled");
			document.getElementById("completoCompletado").setAttribute("value","Incompletar");
			document.getElementById("completoCompletado").setAttribute("onclick","descompletarCompletado()");
		}
	
		function addDependencia(contador, valor){
			divDependencias = document.getElementById("divDependencias");
			dependenciaPosterior = document.getElementById('idDependencia'+(contador+1));
			dependenciaActual = document.getElementById('idDependencia'+contador);
	
			if(contador>0 && valor=='' && !dependenciaPosterior){
				divDependencias.removeChild(dependenciaActual);
				document.getElementById('idDependencia'+(contador-1)).setAttribute('onchange','addDependencia('+(contador-1)+',this.value)');
			} else if(valor!='' && !dependenciaPosterior) {
				dependenciaActual.setAttribute('onchange','');
				br = document.createElement('br');
				divDependencias.appendChild(br);
				select = document.createElement('select');
				select.id = 'idDependencia'+(contador+1);
				select.name = 'idDependencia'+(contador+1);	
				select.setAttribute('onchange','addDependencia('+(contador+1)+',this.value)');
				select.appendChild(document.createElement('option'));
	
				for(var i=0;i<tareas.length;i++){
					option = document.createElement('option');
					option.value = tareas[i].idTarea;
					option.appendChild(document.createTextNode(tareas[i].numTarea+' - '+tareas[i].nombreTarea));
					select.appendChild(option);
				}
				
				divDependencias.appendChild(select);
			}
		}
	
		function tareaPuedeSerPadre(idTarea,tareaPadre) {
			if(tareaPadre.idSupertarea==''){
				return true;
			} else if(tareaPadre.idTarea==idTarea || tareaPadre.idSupertarea==idTarea){
				return false;
			} else {
				return tareaPuedeSerPadre(idTarea, obtenerTareaPorIdTarea(tareaPadre.idSupertarea));
			}
		}
	
		function tareasDeLasQuePuedeSerPadre(idTarea) {
			tareasPadre = [];
			for(var i=0;i<tareas.length;i++){
				if(tareaPuedeSerPadre(idTarea, tareas[i])){
					tareasPadre.push(tareas[i]);
				}
			}
			return tareasPadre;
		}
	
		function tareaPuedeDepender(idTarea,tareaDepende) {
			if(idTarea==tareaDepende.idTarea) return false;
			for(var j=0;j<tareaDepende.tareasDepende.length;j++){
				if(tareaDepende.tareasDepende[j].idTarea==idTarea){
					return false;
				} else {
					return tareaPuedeDepender(idTarea, obtenerTareaPorIdTarea(tareaDepende.tareasDepende[j].idTarea))
				}
			}
			return true;
		}
	
		function tareasDeLasQuePuedeDepender(idTarea) {
			tareasDepende = [];
			for(var i=0;i<tareas.length;i++){
				if(tareaPuedeDepender(idTarea, tareas[i])){
					tareasDepende.push(tareas[i]);
				}
			}
			return tareasDepende;
		}
		
		function lista_contiene_objeto(lista,objeto){
			for(var i=0;i<lista.length;i++){
				if(lista[i]==objeto){
					return true;
				}
			}
			return false;
		}
		
		function convertirSubmitEnCrearTarea(){
			//document.getElementById("formCreacionTarea").setAttribute("action", "<?php echo base_url(); ?>index.php/tareas/crear/<?php echo $proyecto->idProyecto;?>");
			document.getElementById("submitCrearTarea").setAttribute("value","Crear Tarea");
			document.getElementById("submitCrearTarea").setAttribute("onclick","crearTarea()");
			document.getElementById("legendary").innerHTML = 'CREAR NUEVA TAREA';
			document.getElementById("nombreTarea").setAttribute("value",'');
			document.getElementById("duracion").setAttribute("value",'');
			document.getElementById("holgura").setAttribute("value",'');
			document.getElementById("divCompletado").innerHTML = '';
			divIdSupertarea = 'Tarea padre:<select id="idSupertarea" name="idSupertarea">';
			divIdSupertarea += '<option value="" selected></option>';
			divDependencias = 'Depende de:<br><select id="idDependencia0" name="idDependencia0" onchange="addDependencia(0,this.value)">';
			divDependencias += '<option value="" selected></option>';
			for(var i=0;i<tareas.length;i++){
				divIdSupertarea += '<option value="'+tareas[i].idTarea+'">'+tareas[i].numTarea+' - '+tareas[i].nombreTarea+'</option>';
				divDependencias += '<option value="'+tareas[i].idTarea+'">'+tareas[i].numTarea+' - '+tareas[i].nombreTarea+'</option>';
			}
			divIdSupertarea += '</select>';
			divDependencias += '</select>';
			document.getElementById("divIdSupertarea").innerHTML = divIdSupertarea;
			document.getElementById("divDependencias").innerHTML = divDependencias;
		}
	
		function convertirSubmitEnEditarTarea(tarea){
			//document.getElementById("formCreacionTarea").setAttribute("action", "<?php echo base_url(); ?>index.php/tareas/editar/"+tarea.idTarea);
			document.getElementById("submitCrearTarea").setAttribute("value","Editar Tarea");
			document.getElementById("submitCrearTarea").setAttribute("onclick","modificarTarea("+tarea.idTarea+")");
			document.getElementById("legendary").innerHTML = 'EDITAR TAREA: '+tarea.nombreTarea+" CON ID "+tarea.idTarea;
			document.getElementById("nombreTarea").setAttribute("value",tarea.nombreTarea);
			document.getElementById("descTarea").setAttribute("value",tarea.descTarea);
			document.getElementById("duracion").setAttribute("value",tarea.duracion);
			document.getElementById("holgura").setAttribute("value",tarea.prioridad);
			if(tarea.tieneHijos=="true" || "${proyecto.idGrupoPerteneciente>0}"=="true" || tarea.usuariosAsignados.length>0){
				document.getElementById("divCompletado").innerHTML = 'Completado al '+tarea.completado+'%';
			} else {
				document.getElementById("divCompletado").innerHTML = 'Completado:<input type="number" max="100" min="0" id="completado" name="completado" value="'+tarea.completado+'"/>%<br>';
				document.getElementById("divCompletado").innerHTML +='<input type="button" id="completoCompletado" onclick="completarCompletado()" value="Completar"/>';
			}
			if(tarea.completado>99)
				completarCompletado();
			divIdSupertarea = 'Tarea padre: <select id="idSupertarea" name="idSupertarea">';
			divIdSupertarea += '<option value=""></option>';
			for(var i=0;i<tarea.puedeSerHijoDe.length;i++){
				indices_tareas
				divIdSupertarea += '<option value="'+tarea.puedeSerHijoDe[i]+'" ';
				if(tarea.puedeSerHijoDe[i]==tarea.idSupertarea) divIdSupertarea += 'selected';
				divIdSupertarea += '>'+tareas[indices_tareas[tarea.puedeSerHijoDe[i]]].numTarea+' - '+tareas[indices_tareas[tarea.puedeSerHijoDe[i]]].nombreTarea+'</option>';
			}
			divIdSupertarea += '</select>';
			document.getElementById("divIdSupertarea").innerHTML = divIdSupertarea;
	
			divDependencias = 'Depende de:<br>';
			for(var j=0;j<tarea.dependencias.length;j++){
				dependencia = tarea.dependencias[j];
				divDependencias += '<br><select id="idDependencia'+j+'" name="idDependencia'+j+'">';
				divDependencias += '<option value=""></option>';
				for(var i=0;i<tarea.puedeDependerDe.length;i++){
					divDependencias += '<option value="'+tarea.puedeDependerDe[i]+'" ';
					if(tarea.puedeDependerDe[i]==dependencia) divDependencias += 'selected';
					divDependencias += '>'+tareas[indices_tareas[tarea.puedeDependerDe[i]]].numTarea+' - '+tareas[indices_tareas[tarea.puedeDependerDe[i]]].nombreTarea+'</option>';
				}
				divDependencias += '</select>';
			}
			divDependencias += '<br><select id="idDependencia'+tarea.dependencias.length+'" name="idDependencia'+tarea.dependencias.length+'" onchange="addDependencia('+tarea.dependencias.length+',this.value)">';
			divDependencias += '<option value="" selected></option>';
			for(var i=0;i<tarea.puedeDependerDe.length;i++){
				divDependencias += '<option value="'+tarea.puedeDependerDe[i]+'">'+tareas[indices_tareas[tarea.puedeDependerDe[i]]].numTarea+' - '+tareas[indices_tareas[tarea.puedeDependerDe[i]]].nombreTarea+'</option>';
			}
			divDependencias += '</select>';
			document.getElementById("divDependencias").innerHTML = divDependencias;
		}
	
		function mostrarCrearTarea() {
			document.getElementById("formCrearTarea").setAttribute("style", "");
			aux1 = document.getElementById("buttonMostrarCrearTarea");
			aux2 = document.getElementById("buttonOcultarCrearTarea");
			if(aux1) aux1.setAttribute("style", "display: none;");
			if(aux2) aux2.setAttribute("style", "");
			convertirSubmitEnCrearTarea();
		}
	
		function ocultarCrearTarea() {
			document.getElementById("formCrearTarea").setAttribute("style", "display: none;");
			aux1 = document.getElementById("buttonMostrarCrearTarea");
			aux2 = document.getElementById("buttonOcultarCrearTarea");
			if(aux1) aux1.setAttribute("style", "");
			if(aux2) aux2.setAttribute("style", "display: none;");
			convertirSubmitEnCrearTarea();
		}
	
		function editarTarea(idTarea){
			tarea = tareas[idTarea];
			mostrarCrearTarea();
			convertirSubmitEnEditarTarea(tarea);
		}
	
		function actionMostrarCrearTarea() {
			mostrarCrearTarea();
		}
	
		function actionOcultarCrearTarea() {
			ocultarCrearTarea();
		}
	
		function actionMostrarEditarTarea(idTarea) {
			location.href='#formCrearTarea';
			editarTarea(idTarea);
		}
	
		function actionMostrarCrearSubtarea(idTarea) {
			tarea = obtenerTareaPorIdTarea(idTarea);
			actionMostrarCrearTarea();
			divIdSupertarea = 'Superproyecto: <select id="idSupertarea" name="idSupertarea">';
			divIdSupertarea += '<option value=""></option>';
			for(var i=0;i<tareas.length;i++){
				if(tareas[i].idTarea==tarea.idTarea)
					divIdSupertarea += '<option value="'+tareas[i].idTarea+'" selected>'+tareas[i].numTarea+' - '+tareas[i].nombreTarea+'</option>';
				else 
					divIdSupertarea += '<option value="'+tareas[i].idTarea+'">'+tareas[i].numTarea+' - '+tareas[i].nombreTarea+'</option>';
			}
			divIdSupertarea += '</select>';
			document.getElementById("divIdSupertarea").innerHTML = divIdSupertarea;
		}
	
		function ocultarSubtareas(idTarea) {
			subtareas = document.getElementsByClassName("subtareaDe"+idTarea);
			for(var i=0;i<subtareas.length;i++){
				if(subtareas[i].getAttribute("style")=="display:none;font-size:medium;")
					subtareas[i].setAttribute("style","display:none;font-size:large;");
				else if(subtareas[i].getAttribute("style")=="display:none;font-weight:normal;")
					subtareas[i].setAttribute("style","display:none;font-size:medium;");
				else if(subtareas[i].getAttribute("style")=="display:none;font-weight:bold;")
					subtareas[i].setAttribute("style","display:none;font-weight:normal;");
				else if(subtareas[i].getAttribute("style")=="display:none;text-decoration:line-through;")
					subtareas[i].setAttribute("style","display:none;font-weight:bold;");
				else if(subtareas[i].getAttribute("style")=="display:none;text-decoration:underline;")
					subtareas[i].setAttribute("style","display:none;text-decoration:line-through;");
				else if(subtareas[i].getAttribute("style")=="display:none;text-decoration:overline;")
					subtareas[i].setAttribute("style","display:none;text-decoration:underline;");
				else if(subtareas[i].getAttribute("style")=="display:none;")
					subtareas[i].setAttribute("style","display:none;text-decoration:overline;");
				else
					subtareas[i].setAttribute("style","display:none;");
			}
			document.getElementById("ocultaMuestraTareas"+idTarea).setAttribute("value","+");
			document.getElementById("ocultaMuestraTareas"+idTarea).setAttribute("onclick","mostrarTareas("+idTarea+")");
		}
	
		function mostrarSubtareas(idTarea) {
			subtareas = document.getElementsByClassName("subtareaDe"+idTarea);
			for(var i=0;i<subtareas.length;i++){
				if(subtareas[i].getAttribute("style")=="display:none;font-size:large;")
					subtareas[i].setAttribute("style","display:none;font-size:medium;");
				else if(subtareas[i].getAttribute("style")=="display:none;font-size:medium;")
					subtareas[i].setAttribute("style","display:none;font-weight:normal;");
				else if(subtareas[i].getAttribute("style")=="display:none;font-weight:normal;")
					subtareas[i].setAttribute("style","display:none;font-weight:bold;");
				else if(subtareas[i].getAttribute("style")=="display:none;font-weight:bold;")
					subtareas[i].setAttribute("style","display:none;text-decoration:line-through;");
				else if(subtareas[i].getAttribute("style")=="display:none;text-decoration:line-through;")
					subtareas[i].setAttribute("style","display:none;text-decoration:underline");
				else if(subtareas[i].getAttribute("style")=="display:none;text-decoration:underline;")
					subtareas[i].setAttribute("style","display:none;text-decoration:overline;");
				else if(subtareas[i].getAttribute("style")=="display:none;text-decoration:overline;")
					subtareas[i].setAttribute("style","display:none;");
				else
					subtareas[i].setAttribute("style","");
			}
			document.getElementById("ocultaMuestraTareas"+idTarea).setAttribute("value","-");
			document.getElementById("ocultaMuestraTareas"+idTarea).setAttribute("onclick","ocultarTareas("+idTarea+")");
		}
		<c:if test="${puedeEditarTareas}">
		function cambiar_orden_tareas(valor){
			if(valor=="") valor = 0;
			$.ajax({
				url: '${urlBase}/<c:if test="${proyecto.esPlantilla}">plantillas</c:if><c:if test="${proyecto.esPlantilla==false}">proyectos</c:if>/${proyecto.idProyecto}/orden-tareas.htm',
				data: ''+valor,
				type: 'PUT',
				success: function(){
					location.href = location.href;
				}
			}).fail(function(data){alert(data.responseText);});
		}
		</c:if>
		function mostrar_boton_editar_eliminar_varios(){
			document.getElementById('buttonEliminarVarios').disabled = "";
		}
		function ocultar_boton_editar_eliminar_varios(){
			document.getElementById('buttonEliminarVarios').disabled = "disabled";
		}
		var ids_seleccionados = [];
		function seleccionar_fila(id,idTarea){
			fila = $('#'+id);
			check = $('#'+id+'-check');
			if(fila.hasClass('info')){
				$('#'+id).removeClass('info');
				if(filas_seleccionadas.length==0){
					ocultar_boton_editar_eliminar_varios();
				}
				document.getElementById(id+'-check').checked = 0;
				indexOf = ids_seleccionados.indexOf(idTarea);
				ids_seleccionados = ids_seleccionados.slice(0,indexOf).concat(ids_seleccionados.slice(indexOf+1,ids_seleccionados.length))
			} else {
				filas_seleccionadas = document.getElementsByClassName('info');
				if(filas_seleccionadas.length==0){
					mostrar_boton_editar_eliminar_varios();
				}
				$('#'+id).addClass('info');
				document.getElementById(id+'-check').checked = 1;
				ids_seleccionados.push(idTarea);
			}
		}
		
		function action_mostrar_editar_tareas(){
			
		}
		function eliminar_tareas(){
			if(ids_seleccionados.length>0){
				nombreTareas = tareas[indices_tareas[ids_seleccionados[0]]].nombreTarea;
				for(var i=1;i<ids_seleccionados.length;i++){
					nombreTareas += ', '+tareas[indices_tareas[ids_seleccionados[i]]].nombreTarea;
				}
				if(confirm('�Quieres borrar las tareas '+nombreTareas+'?')){
					elim = confirm('�Quieres que las subtareas de las tareas a eliminar sean heredadas por el abuelo?');
					if(elim){codEliminacion = 2;} else {codEliminacion = 1;}
					elJSON = '{"idTarea0":'+ids_seleccionados[0];
					for(var i=1;i<ids_seleccionados.length;i++){
						elJSON += ',"idTarea'+i+'":'+ids_seleccionados[i];
					}
					elJSON += ',"comoCreadorProyecto":${esCreador}';
					elJSON += ',"codEliminacion":'+codEliminacion;
					elJSON += '}';
					$.ajax({
						url: '${urlBase}/tareas/${proyecto.idProyecto}.htm',
						data: elJSON,
						type: 'DELETE',
						success: function(){
							actualizar_tareas_con_organigrama();
							alert("Se han borrado todas las tareas con �xito");
						}
					}).fail(function(data){alert(data.responseText);});
				}
			}
		}
		
		<c:if test="${puedeAsignarTareas || puedeEditarAsignar || puedeEliminarAsignar}">
		function usuarioSeEncuentraAsignado(lista,login){
			for(var j=0;j<lista.length;j++){
				if(lista[j].login==login)
					return true;
			}
			return false;
		}
		
		function equipoSeEncuentraAsignado(lista,idEquipo){
			for(var j=0;j<lista.length;j++){
				if(lista[j].idEquipo==idEquipo)
					return true;
			}
			return false;
		}
		
		function obtenerPorcentajeMaximo(usuariosAsignados, equiposAsignados){
			sum = 100;
			for(var k=0;k<usuariosAsignados.length;k++){
				sum = sum - usuariosAsignados[k].porcentaje;
			}
			for(var k=0;k<equiposAsignados.length;k++){
				sum = sum - equiposAsignados[k].porcentaje;
			}
			if(sum<0) return 0;
			return sum;
		}
		
		function asignar_tarea(idTarea,idProyecto){
			esAEquipo = false;
			<c:if test="${proyecto.idGrupoPerteneciente>0}">
				radioUsuario = document.getElementById('asignarAUsuario');
				radioEquipo = document.getElementById('asignarAEquipo');
				if(radioUsuario.getAttribute("class")!="btn btn-info" && radioEquipo.getAttribute("class")!="btn btn-info"){
					alert('Debes marcar hacia quien va dirigida la asignaci�n');return;
				}
				esAEquipo = radioEquipo!=null && radioEquipo.getAttribute("class")=="btn btn-info";
			</c:if>
			if(esAEquipo){
				log=document.getElementById("equipoAsignar").value;
				url = "${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/asignar/equipos/"+log+".htm";
				por = document.getElementById("porcentajeAsignar").value;
				elJSON = {idEquipo:log,porcentaje:por};
			}else{
				log =document.getElementById("loginAsignar").value;
				url = "${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/asignar.htm";
				por = document.getElementById("porcentajeAsignar").value;
				elJSON = {login:log,porcentaje:por};
			}
			if(log=='' || por=='' || document.getElementById("porcentajeAsignar").validationMessage!=''){
				alert('Debes rellenar el login del miembro y el porcentaje de tarea que le corresponde al miembro.');
			} else {
				$.postJSON(url, elJSON, function(data) {
					hideLightbox();
					actualizar_tareas_con_organigrama();
					alert("Se ha asignado la tarea con �xito");
	            }).fail(function(data){alert(data.responseText);});
			}
		}
		
		function editar_asignar(idTarea,idProyecto,log,esAEquipo){
			por = document.getElementById("porcentaje-"+idTarea+"-"+log).value;
			if(por=='' || document.getElementById("porcentaje-"+idTarea+"-"+log).validationMessage!=''){
				alert("Debes introducir un porcentaje v�lido");
			} else {
				if(esAEquipo){att = 'idEquipo'; elUrl="${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/asignar/equipos/"+log+".htm"}
				else {att = 'login';elUrl="${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/asignar.htm";log='"'+log+'"'}
				elJSON = '{"'+att+'":'+log+',"porcentaje":'+por+'}';
				$.ajax({
				    url:elUrl,
				    type: 'PUT',
				    data: elJSON,
				    success: function() {
				    	hideLightbox();
				    	actualizar_tareas_con_organigrama();
						alert("Asignaci�n editada con �xito");
				    }
				}).fail(function(data){alert(data.responseText);});
			}
		}
		
		function eliminar_asignar(idTarea,idProyecto,log,esAEquipo){
			if(esAEquipo) {elUrl = "${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/desasignar/equipos/"+log+".htm";log='';}
			else elUrl = "${urlBase}/proyectos/"+idProyecto+"/tareas/"+idTarea+"/desasignar.htm";
			$.ajax({
			    url: elUrl,
			    type: 'DELETE',
			    data: log,
			    success: function() {
			    	hideLightbox();
			    	actualizar_tareas_con_organigrama();
			    	alert("Asignaci�n eliminada con �xito");
			    }
			}).fail(function(data){alert(data.responseText);});
		}
		function mostrarUsuarioAsignar(){
			document.getElementById("asignarAUsuario").setAttribute('class','btn btn-info');
			document.getElementById("asignarAEquipo").setAttribute('class','btn');
			document.getElementById("loginAsignar").style="";
			document.getElementById("equipoAsignar").style="display:none;";
			document.getElementById("form-Asignar").style="";
		}
		function mostrarEquipoAsignar(){
			document.getElementById("asignarAUsuario").setAttribute('class','btn');
			document.getElementById("asignarAEquipo").setAttribute('class','btn btn-info');
			document.getElementById("loginAsignar").style="display:none;";
			document.getElementById("equipoAsignar").style="";
			document.getElementById("form-Asignar").style="";
		}
		
		function asignarTarea(idInterno){
			tarea = tareas[idInterno];
			usuariosAsignados = tarea.usuariosAsignados;
			equiposAsignados = tarea.equiposAsignados;
			porcMax = obtenerPorcentajeMaximo(usuariosAsignados,equiposAsignados);
			innerHtml ="";
			<c:if test="${puedeAsignarTareas}">
			innerHtml += "ASIGNAR TAREA "+tarea.nombreTarea+":<br>";
			<c:if test="${proyecto.idGrupoPerteneciente>0}">
				<c:if test="${miembros.size()>0}">
					innerHtml += "<button class='btn' id='asignarAUsuario' onclick='mostrarUsuarioAsignar()'>ASIGNAR A USUARIO</button>";
				</c:if>
				<c:if test="${equipos.size()>0}">
					innerHtml += "<button class='btn' id='asignarAEquipo' onclick='mostrarEquipoAsignar()'>ASIGNAR A EQUIPO</button><br>";
				</c:if>
			</c:if>
			innerHtml += "<div id='form-Asignar' ";
			<c:if test="${proyecto.idGrupoPerteneciente>0}">
				innerHtml += "style='display:none;'";
			</c:if>
			innerHtml += ">Asignar tarea a:<select id='loginAsignar' <c:if test='${proyecto.idGrupoPerteneciente>0}'>style='display:none;'</c:if>><option value='' selected></option>";
			innerOptions = "";
			if(!usuarioSeEncuentraAsignado(usuariosAsignados,'${usuario.login}') && "${proyecto.idGrupoPerteneciente>0}"=="true")
				innerOptions += "<option value='${usuario.login}'>Yo</option>";
			for(var j=0;j<miembros.length;j++)
				if(miembros[j]!='${usuario.login}' && !usuarioSeEncuentraAsignado(usuariosAsignados,miembros[j]))
					innerOptions += "<option value='"+miembros[j]+"'>"+miembros[j]+"</option>";
			for(var p=0;p<compartidos.length;p++){
				if(compartidos[p]!='${usuario.login}' && !usuarioSeEncuentraAsignado(usuariosAsignados,compartidos[p]))
					innerOptions += "<option value='"+compartidos[p]+"'>"+compartidos[p]+"</option>";
			}
			innerOptions += "</select>";
			<c:if test='${proyecto.idGrupoPerteneciente>0}'>
				innerOptions += "<select id='equipoAsignar' style='display:none;'><option value=''></option>";
				for(var j=0;j<equipos.length;j++)
					if(!equipoSeEncuentraAsignado(equiposAsignados,equipos[j].idEquipo))
						innerOptions += "<option value='"+equipos[j].idEquipo+"'>"+equipos[j].nombreEquipo+"</option>";
				innerOptions += "</select>";
			</c:if>
					
			if(innerOptions==""){
				innerHtml = "No hay gente a la que poder asignar la tarea<br><br>";
			} else {
				innerHtml += innerOptions;
				innerHtml += "<br><input id='porcentajeAsignar' type='number' min='1' max='"+porcMax+"' />% Max: "+porcMax;
				innerHtml += "<br><button class='btn btn-info' onclick='asignar_tarea("+tarea.idTarea+","+tarea.idProyectoPerteneciente+")'>Asignar Tarea</button></div>";
			}
			</c:if>
			if(usuariosAsignados.length>0 || equiposAsignados.length>0){
				innerHtml += "U=Usuario E=Equipo<br>";
				innerHtml += "<table class='table table-striped table-hover' id='tableAsignados'><thead><tr>";
				innerHtml += "<th></th><th>Nombre</th><th>Completado</th><th>Porcentaje</th>";
				<c:if test="${puedeEditarAsignar}">
					innerHtml += "<th>Acciones</th></tr></thead><tbody>";
					for(var l=0;l<usuariosAsignados.length;l++){
						innerHtml += "<tr id='usuarioAsignado-"+usuariosAsignados[l].login+"'><td>U</td><td>"+usuariosAsignados[l].login+"</td><td>"+
						usuariosAsignados[l].completado+"</td><td>"+
						"<input id='porcentaje-"+tarea.idTarea+"-"+usuariosAsignados[l].login+"' type='number' min='"+usuariosAsignados[l].completado+"' max='"+
						(porcMax+parseInt(usuariosAsignados[l].porcentaje))+
						"' value='"+usuariosAsignados[l].porcentaje+"'/>%";
						innerHtml += "</td><td><button  class='btn btn-info' onclick='editar_asignar("+tarea.idTarea+","+tarea.idProyectoPerteneciente+",\""+usuariosAsignados[l].login+"\",false)'"+usuariosAsignados[l].login+"')'><i class='glyphicon glyphicon-edit'></i></button>";
						<c:if test='${puedeEliminarAsignar}'>
							innerHtml += "<button  class='btn btn-info' onclick='eliminar_asignar("+tarea.idTarea+","+tarea.idProyectoPerteneciente+",\""+usuariosAsignados[l].login+"\",false)'"+usuariosAsignados[l].login+"')'><i class='glyphicon glyphicon-trash'></i></button>";
						</c:if>
						innerHtml += "</td></tr>";
					}
					for(var g=0;g<equiposAsignados.length;g++){
						innerHtml += "<tr id='equipoAsignado-"+equiposAsignados[g].idEquipo+"'><td>E</td><td>"+equiposAsignados[g].nombreEquipo+"</td><td>"+
						equiposAsignados[g].completado+"</td><td>"+
						"<input id='porcentaje-"+tarea.idTarea+"-"+equiposAsignados[g].idEquipo+"' type='number' min='"+equiposAsignados[g].completado+"' max='"+
						(porcMax+parseInt(equiposAsignados[g].porcentaje))+
						"' value='"+equiposAsignados[g].porcentaje+"'/>%";
						innerHtml += "</td><td><button  class='btn btn-info' onclick='editar_asignar("+tarea.idTarea+","+tarea.idProyectoPerteneciente+","+equiposAsignados[g].idEquipo+",true)'"+equiposAsignados[g].nombreEquipo+"')'><i class='glyphicon glyphicon-edit'></i></button>";
						<c:if test='${puedeEliminarAsignar}'>
							innerHtml += "<button  class='btn btn-info' onclick='eliminar_asignar("+tarea.idTarea+","+tarea.idProyectoPerteneciente+","+equiposAsignados[g].idEquipo+",true)'"+equiposAsignados[g].nombreEquipo+"')'><i class='glyphicon glyphicon-trash'></i></button>";
						</c:if>
						innerHtml += "</td></tr>";
					}
				</c:if>
				<c:if test="${puedeEditarAsignar==false}">
					<c:if test="${puedeEliminarAsignar}">innerHtml += "<th>Acciones</th>";</c:if>
					innerHtml += "</tr></thead><tbody>";
					for(var l=0;l<usuariosAsignados.length;l++){
						innerHtml += "<tr id='usuarioAsignado-"+usuariosAsignados[l].login+"'><td>U</td><td>"+usuariosAsignados[l].login+"</td><td>"+
						usuariosAsignados[l].completado+"</td><td>"+
						usuariosAsignados[l].porcentaje;
						innerHtml += "</td>";
						<c:if test='${puedeEliminarAsignar}'>
							innerHtml += "<td><button  class='btn btn-info' onclick='eliminar_asignar("+tarea.idTarea+",\""+usuariosAsignados[l].login+"\",false)'><i class='glyphicon glyphicon-trash'></i></button></td>";
						</c:if>
						innerHtml += "</tr>";
					}
					for(var l=0;l<equiposAsignados.length;l++){
						innerHtml += "<tr id='equipoAsignado-"+equiposAsignados[l].idEquipo+"'><td>E</td><td>"+equiposAsignados[l].nombreEquipo+"</td><td>"+
						equiposAsignados[l].completado+"</td><td>"+
						equiposAsignados[l].porcentaje;
						innerHtml += "</td>";
						<c:if test='${puedeEliminarAsignar}'>
							innerHtml += "<td><button  class='btn btn-info' onclick='eliminar_asignar("+tarea.idTarea+","+equiposAsignados[l].idEquipo+",true)'><i class='glyphicon glyphicon-trash'></i></button></td>";
						</c:if>
						innerHtml += "</tr>";
					}
				</c:if>
				innerHtml += "</tbody></table>";
			} else {
				innerHtml += "<br><br>No hay asignamientos para la tarea";
				<c:if test="${proyecto.idGrupoPerteneciente==0}">innerHtml += ", por lo tanto ${proyecto.nombreCreador} es el responsable del 100% de �sta."</c:if>
			}
			showLightbox(innerHtml);
		}
		</c:if>
		
		function actualizar_tabla_tareas(){
			inner = "";
			for(var t=0;t<tareas.length;t++){
				tarea = tareas[t];
				
				tarea.tieneHijos = tarea.puedeSerHijoDe.length<(tareas.length-1);
				inner += '<tr id="tarea'+tarea.idTarea+'" class="'+tarea.superclase+'">';
				onclickFilas = '';
				<c:if test="${puedeEditarTareas || puedeEliminarTareas}">
					onclickFilas = "seleccionar_fila('tarea"+tarea.idTarea+"',"+tarea.idTarea+")";
					inner += '<td onclick="'+onclickFilas+'"><input id="tarea'+tarea.idTarea+'-check" type="checkbox" /></td>';
				</c:if>
				inner += '<td onclick="'+onclickFilas+'">'+tarea.numTarea+'</td>';
				inner += '<td onclick="'+onclickFilas+'">'+tarea.espacios+tarea.nombreTarea+'</td>';
				inner += '<td onclick="'+onclickFilas+'">'+tarea.duracion+' horas</td>';
				inner += '<td onclick="'+onclickFilas+'"><div class="progress">'+
				  '<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'+tarea.prioridad+'0" aria-valuemin="0" '+
					  'aria-valuemax="100" style="width: '+tarea.prioridad+'0%">'+
					    tarea.prioridad+
					  '</div>'+
				'</div></td>';
				b = false;
				inner += '<td id="dependencias'+tarea.idTarea+'" onclick="'+onclickFilas+'">';
				for(var f=0;f<tarea.dependenciasPublicas.length;f++){
					if(b) inner += ';';
					else b=true;
					inner += tarea.dependenciasPublicas[f];
				}
				inner += '</td>';
				inner += '<td onclick="'+onclickFilas+'">';
				inner += '<div class="progress">'+
					  '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="'+tarea.completado+'" aria-valuemin="0"'+
						  'aria-valuemax="100" style="width: '+tarea.completado+'%">'+
						    tarea.completado+
						  '%</div>'+
						'</div></td>';
				<c:if test="${puedeEditarTareas || puedeEliminarTareas || puedeAsignarTareas}">
					inner += '<td>'+
						<c:if test="${puedeEditarTareas}">
							'<button  class="btn btn-info" onclick="actionMostrarEditarTarea('+t+')"><i class="glyphicon glyphicon-edit"></i></button>'+
						</c:if>
						<c:if test="${puedeEliminarTareas}">
							'<button  class="btn btn-info" onclick="eliminar('+t+')"><i class="glyphicon glyphicon-trash"></i></button>'+
						</c:if>
							'';
						<c:if test="${puedeAsignarTareas || puedeEditarAsignar || puedeEliminarAsignar}">
							//alert("tarea: "+tarea.nombreTarea+"   "+tarea.tieneHijos+ " O puedeSerHijoDe:"+tarea.puedeSerHijoDe.length + "<tareas:"+tareas.length+" = "+(tarea.puedeSerHijoDe.length<tareas.length));
							if(!tarea.tieneHijos){
								inner += '<button  class="btn btn-info" onclick="asignarTarea('+t+')"><i class="glyphicon glyphicon-hand-right"></i></button>';
							}
						</c:if>
					inner += '</td>';
				</c:if>
				<c:if test="${puedeEditarTareas && proyecto.ordenacionTareas==8}">
					inner += "<td id='subir-tarea-"+tarea.idTarea+"'>";
					if(t>0) inner += "<button  class='btn btn-info' onclick='cambiarTarea("+tarea.idTarea+","+tareas[t-1].idTarea+")'><i class='glyphicon glyphicon-open'></i></button>";
					inner += "</td>"+
					inner += "<td id='bajar-tarea-"+tarea.idTarea+"'>";
					if(t<tareas.length)
						inner += "<button  class='btn btn-info' onclick='cambiarTarea("+tarea.idTarea+","+tareas[t+1].idTarea+")'><i class='glyphicon glyphicon-save'></i></button>";
					inner += "</td>";
				</c:if>
				inner += '</tr>';
			}
			document.getElementById("tbodyTareas").innerHTML = inner;
		}
		
		function actualizar_tareas_con_organigrama(){
			document.getElementById("divTodosTareas").style='';
			document.getElementById("tareasP").style='display:none;';
			$.ajax({
				url: '${urlBase}/proyectos/${proyecto.idProyecto}/tareas/actualizar.htm',
				type: 'GET',
				success: function(object){
			        objeto = JSON.parse(object);
			        <c:if test="${calculosProyecto!=null && proyecto.esPlantilla==false}">
			        	document.getElementById('fecha-fin-prevista').innerHTML = "Fecha de Fin Prevista: "+objeto.fechaPrevista;
			        	diasProyecto = objeto.diasProyecto;
			        	calculosProyecto = objeto.organigrama;
			        </c:if>
			        tareas = objeto.tareas;
			        actualizar_tabla_tareas();
			        actualizar_indices_tareas();
				}
			}).fail(function(){return;});
		}
		
		<c:if test="${calculosProyecto!=null && proyecto.esPlantilla==false}">
		var calculosProyecto = [<c:set var="b" value="true"/>
		                        <c:forEach items="${calculosProyecto}" var="calculo">
									<c:if test="${b==false}">,{</c:if><c:if test="${b==true}"><c:set var="b" value="false"/>{</c:if>
										loginUsuario : "${calculo.value0.login}",
										vistaTareas : [<c:set var="c" value="true"/>
								                        <c:forEach items="${calculo.value1}" var="tarea">
														<c:if test="${c==false}">,</c:if><c:if test="${c==true}"><c:set var="c" value="false"/></c:if>
														{
															tarea : {
																idTarea : "${tarea.value0.idTarea}",
																nombreTarea : "${tarea.value0.nombreTarea}"
															},
															empieza : "${tarea.value1}",
															finaliza : "${tarea.value2}"
														}
										               </c:forEach>]
									}
		                        </c:forEach>];
		
		var diasProyecto = ${diasProyecto};
		
		function cambiarTarea(idTareaA,idTareaB){
			$.ajax({
			    url: '${urlBase}/proyectos/${proyecto.idProyecto}/tareas/cambiar-orden.htm',
			    type: 'PUT',
			    data: '{"idTareaA":'+idTareaA+',"idTareaB":'+idTareaB+'}',
			    success: function() {
			    	actualizar_tareas_con_organigrama();
			    }
			}).fail(function(data){alert(data.responseText);});
		}
		
		function go_usuarios(){
			document.getElementById("divCronograma").style="display:none;";
			document.getElementById("divPlanning").style="";
			document.getElementById("divTareasFin").style="display:none;";
		}
		
		function go_organigrama(){
			document.getElementById("divCronograma").style="";
			document.getElementById("divPlanning").style="display:none;";
			document.getElementById("divTareasFin").style="display:none;";
		}
		
		function go_tareas(){
			document.getElementById("divCronograma").style="display:none;";
			document.getElementById("divPlanning").style="display:none;";
			document.getElementById("divTareasFin").style="";
		}
		
		function mostrarEstadisticas(){
			fechaProyecto = mostrarFecha(diasProyecto);
			innerHtml = 
				"<div id='divCronograma'><ul class='nav nav-tabs' role='tablist'>"+
				  "<li class='active'><a href='#'>Cronograma</a></li>"+
				  "<li><a href='javascript:go_usuarios()'>Usuarios</a></li>"+
				"</ul>";
			innerHtml += "<h3>CRONOGRAMA DEL PROYECTO ${proyecto.nombreProyecto}</h3>";
			innerHtml += "<table class='table table-striped table-hover'><thead><tr><th>Usuario</th><th style='width:100em;'>CRONOGRAMA</th></tr></thead><tbody>";
			for(var i=0;i<calculosProyecto.length;i++){
				calculo = calculosProyecto[i];
				innerHtml += "<tr><td>"+calculo.loginUsuario+"</td><td><div class='progress'>";
				vari = 0;
				for(var j=0;j<calculo.vistaTareas.length;j++){
					vistaTarea = calculo.vistaTareas[j];
					if(parseInt(vari)<parseInt(vistaTarea.empieza)){
						porcentajeWidth = parseInt(((parseInt(vistaTarea.empieza)-parseInt(vari))/parseInt(diasProyecto))*100);
						innerHtml += "<div class='progress-bar progress-bar-danger progress-bar-striped active' style='width:"+porcentajeWidth+"%'>";
						innerHtml += "Espera</div>";
					}
					if(j%4==0){
						innerHtml += "<div class='progress-bar progress-bar-success' style='width:";
					} else if(j%4==1){
						innerHtml += "<div class='progress-bar progress-bar-info' style='width:";
					} else if(j%4==2){
						innerHtml += "<div class='progress-bar' style='width:";
					} else if(j%4==3){
						innerHtml += "<div class='progress-bar progress-bar-warning' style='width:";
					}
					porcentajeWidth = parseInt(((parseInt(vistaTarea.finaliza)-parseInt(vistaTarea.empieza))/parseInt(diasProyecto))*100);
					innerHtml += porcentajeWidth+"%'>"+vistaTarea.tarea.nombreTarea+" - "+(vistaTarea.finaliza-vistaTarea.empieza)+" d�as</div>";
					vari=vistaTarea.finaliza;
				}
			}
				innerHtml += "</div></td></tr>";
				innerHtml += "</tbody></table>";
				innerHtml += "</div>";
				innerHtml += 
				"<div id='divPlanning' style='display:none;'><ul class='nav nav-tabs' role='tablist'>"+
				  "<li><a href='javascript:go_organigrama()'>Cronograma</a></li>"+
				  "<li class='active'><a href='#'>Usuarios</a></li>"+
				"</ul><h3>PLANNING DE USUARIOS</h3>";
				for(var p=0;p<calculosProyecto.length;p++){
					cp = calculosProyecto[p];
					innerHtml += '<div class="panel panel-default">'+
						'<div class="panel-heading">PLANNING DE '+cp.loginUsuario+'</div>'+
						'<table class="table table-bordered"><thead><tr><th>Acci�n</th><th></th><th>Fecha Inicio</th><th></th><th>Fecha Fin</th></tr></thead><tbody>';
					seQuedo = 0;
					for(var l=0;l<cp.vistaTareas.length;l++){
						vt = cp.vistaTareas[l];
						fechaEmpieza = mostrarFecha(vt.empieza);
						fechaFinaliza = mostrarFecha(vt.finaliza)
						if(seQuedo<vt.empieza){
							innerHtml += '<tr class="warning"><td>En espera</td><td> desde el </td><td>';
							innerHtml += mostrarFecha(seQuedo)+'</td><td> hasta el </td><td>'+fechaEmpieza;
							innerHtml += '</td></tr>';
						}
						innerHtml += '<tr><td>Realiza la tarea '+vt.tarea.nombreTarea+' </td><td> desde el </td><td>';
						innerHtml += fechaEmpieza+'</td><td> hasta el </td><td>'+fechaFinaliza;
						innerHtml += '</td></tr>';
						seQuedo = vt.finaliza;
					}
					if(seQuedo<diasProyecto){
						innerHtml += '<tr class="warning"><td>En espera de nuevas tareas </td><td>desde el </td><td>';
						innerHtml += mostrarFecha(seQuedo)+'</td><td> hasta el </td><td>'+fechaProyecto;
						innerHtml += '</td></tr>';
					}
					innerHtml += "</tbody></table><br>";
				}
				innerHtml += "</div></div><div id='divTareasFin' style='display:none;'><ul class='nav nav-tabs' role='tablist'>"+
				  "<li><a href='javascript:go_organigrama()'>Cronograma</a></li>"+
				  "<li><a href='javascript:go_usuarios()'>Usuarios</a></li>"+
				  "<li class='active'><a href='#'>Tareas</a></li>"+
				"</ul><h3>TAREAS</h3>";
				listaTareas = [];listaTareasMin = [];
				calculosProyecto.forEach(function(cp){
					cp.vistaTareas.forEach(function(vt){
						if(listaTareas[vt.tarea.idTarea] == undefined){
							listaTareas[vt.tarea.idTarea] = vt.finaliza
						} else {
							listaTareas[vt.tarea.idTarea] = Math.max(listaTareas[vt.tarea.idTarea],vt.finaliza);
						}
						if(listaTareasMin[vt.tarea.idTarea] == undefined){
							listaTareasMin[vt.tarea.idTarea] = vt.empieza
						} else {
							listaTareasMin[vt.tarea.idTarea] = Math.min(listaTareasMin[vt.tarea.idTarea],vt.empieza);
						}
					})
				});
				innerHtml += "<div style='text-align:left;'><ul>";
				innerHtml += get_inner_tareas(tareas,0,listaTareas,listaTareasMin);
				innerHtml += "</ul></div>";
				innerHtml += "</div><br><h4>Quedan "+diasProyecto+" d�as para finalizar todos los proyectos("+fechaProyecto+")</h4>";
				showLightbox(innerHtml);
		}
		
		function get_inner_tareas(tareas,id_tarea,listaTareas,listaTareasMin){
			innerHtml = "";
			tareas.forEach(function(tarea){
				if(tarea.idSupertarea == id_tarea){
					innerHtml += "<li>"+ tarea.numTarea+" - "+tarea.nombreTarea+":";
					if(listaTareas[tarea.idTarea] == undefined){
						innerHtml += mostrarFecha(get_dias_minimos(tarea.idTarea,tareas,listaTareasMin)) + " - " + mostrarFecha(get_dias_maximos(tarea.idTarea,tareas,listaTareas));
						innerHtml += "<ul>";
						innerHtml += get_inner_tareas(tareas,tarea.idTarea,listaTareas,listaTareasMin);
						innerHtml += "</ul>";
					} else {
						innerHtml += mostrarFecha(listaTareasMin[tarea.idTarea])+" - "+mostrarFecha(listaTareas[tarea.idTarea]);
					}
					innerHtml += "</li>";
				}
			});
			return innerHtml;
		}
		
		function get_dias_maximos(id_tarea,tareas,lista_tareas){
			sum = 0;
			tareas.forEach(function(tarea){
				if(tarea.idSupertarea==id_tarea){
					if(lista_tareas[tarea.idTarea] == undefined){
						sum += get_dias_maximos(tarea.idTarea,tareas,lista_tareas)
					} else {
						sum += lista_tareas[tarea.idTarea];
					}
				}
			});
			return sum;
		}
		
		function get_dias_minimos(id_tarea,tareas,lista_tareas){
			aux = 999999999999999999999999;
			tareas.forEach(function(tarea){
				if(tarea.idSupertarea==id_tarea){
					if(lista_tareas[tarea.idTarea] == undefined){
						aux = Math.min(get_dias_minimos(tarea.idTarea,tareas,lista_tareas),aux);
					} else {
						aux = Math.min(aux,lista_tareas[tarea.idTarea]);
					}
				}
			});
			return aux;
		}
		</c:if>
	</script>
	
	<c:if test="${proyecto!=null}">
		<h3>PROYECTO ${proyecto.nombreProyecto}<a href="${urlBase}/<c:if test='${proyecto.esPlantilla==false}'>proyectos</c:if><c:if test='${proyecto.esPlantilla==true}'>plantillas</c:if>/${proyecto.idProyecto}.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<br>
		Fecha de finalizaci�n ideal: 
		${proyecto.fechaFinFormateada}
		<br>
		<p id='fecha-fin-prevista'>Fecha de fin Prevista:
		${fechaFinPrevista}</p>
		<c:if test="${calculosProyecto!=null && proyecto.esPlantilla==false}">
			<button  class='btn btn-info' onclick="mostrarEstadisticas()">Mostrar Planning</button>
		</c:if>
		<c:if test="${esCreador}" >
			<a href='${urlBase}/'><button  class='btn btn-info'>Ir a Mis Proyectos</button></a>
		</c:if>
		<c:if test="${proyecto.nombreGrupoPerteneciente!=null}" >
			<a href='${urlBase}/grupos/${proyecto.idGrupoPerteneciente}.htm'><button  class='btn btn-info'>Ver Proyectos de ${proyecto.nombreGrupoPerteneciente}</button></a>
		</c:if>
		<c:if test="${puedeCrearTareas}">
			<button  class='btn btn-info' id="buttonMostrarCrearTarea" onclick="actionMostrarCrearTarea()">Crear Tarea</button>
		</c:if>
		<c:if test="${puedeCrearTareas || puedeEditarTareas}">
			<button  class='btn btn-info' id="buttonOcultarCrearTarea" onclick="actionOcultarCrearTarea()" style='display:none;'>Ocultar Creaci�n de Tarea</button>
		</c:if>
		<c:if test="${puedeEliminarTareas}">
			<button  class='btn btn-info' id="buttonEliminarVarios" onclick="eliminar_tareas()" disabled="disabled">Eliminar Tareas</button>
		</c:if>
		<c:if test="${puedeEditarTareas}">
		<select onchange="cambiar_orden_tareas(this.value)">
			<c:forEach items="${ordenTareas}" var="ordenTarea">
				<option value="${ordenTarea.value0}" <c:if test="${ordenTarea.value0==proyecto.ordenacionTareas}">selected</c:if>
				>${ordenTarea.value1}</option>
			</c:forEach>
		</select>
		<br>
		</c:if>
		<div id='formCrearTarea' style='display:none;'>
			<!-- form id='formCreacionTarea' / -->
			  <br>
			  <fieldset style='text-align:center;'>
			    <legend id='legendary'>CREAR NUEVA TAREA</legend>
			      T�tulo:<input type='text' id='nombreTarea' name='nombreTarea' /><br>
			      Descripci�n:<input type='text' id='descTarea' name='descTarea' /><br>
			      Duraci�n:<input id='duracion' type='number' name='duracion' /><br>
			      Prioridad:<input id='holgura' type='number' name='holgura' /><br>
			      <div id='divCompletado'></div>
			      <div id='divIdSupertarea'></div>
			      <div id='divDependencias'></div>
			    <input id='submitCrearTarea' type='button' value='Crear Tarea' onclick='crearTarea()'/>
			  </fieldset>
			<!-- /form -->
		</div>
		<c:if test="${tareas.size()>0 }">
			<div style="text-align:center;" class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12" id="divTodosTareas">
				<table class='table table-striped table-hover'>
					<thead>
					<tr>
						<c:if test="${puedeEditarTareas || puedeEliminarTareas}">
							<th></th>
						</c:if>
						<th>#</th>
						<th>Nombre de la tarea</th>
						<th>Duraci�n estimada</th>
						<th>Prioridad</th>
						<th>Depende de las tareas</th>
						<th>Completado</th>
						<c:if test="${puedeEditarTareas || puedeEliminarTareas}">
							<th <c:if test="${puedeEditarTareas && proyecto.ordenacionTareas==8}">colspan='3'</c:if>>Acciones</th>
						</c:if>
					</tr>
					</thead>
					<tbody id='tbodyTareas'>
					<c:set var="inc" value="0" />
					<c:forEach items="${tareas}" var="tarea" >
					<tr id="tarea${tarea.idTarea}" class="${tarea.superclase}">
						<c:if test="${puedeEditarTareas || puedeEliminarTareas}">
							<c:set var="onclickFilas" value="seleccionar_fila('tarea${tarea.idTarea}',${tarea.idTarea})" />
							<td onclick="seleccionar_fila('tarea${tarea.idTarea}',${tarea.idTarea})"><input id="tarea${tarea.idTarea}-check" type="checkbox" /></td>
						</c:if>
						<td onclick="${onclickFilas}">${tarea.numTarea}</td>
						<td onclick="${onclickFilas}">${tarea.espacios}${tarea.nombreTarea}</td>
						<td onclick="${onclickFilas}">${tarea.duracion} horas</td>
						<td onclick="${onclickFilas}"><div class="progress">
							  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="${tarea.prioridadTarea}0" aria-valuemin="0" 
							  aria-valuemax="100" style="width: ${tarea.prioridadTarea}0%">
							    ${tarea.prioridadTarea}
							  </div>
						</div></td>
						<td id="dependencias${tarea.idTarea}" onclick="seleccionar_fila('tarea${tarea.idTarea}',${tarea.idTarea})">
							<c:set var="b" value="true" />
							<c:forEach items="${tarea.dependenciasPublicas}" var="dependencia"><c:if test="${b==false}">; </c:if><c:if test="${b==true}"><c:set var="b" value="false" /></c:if>${dependencia}</c:forEach>
						</td>
						<td onclick="seleccionar_fila('tarea${tarea.idTarea}',${tarea.idTarea})">
						<div class="progress">
						  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="${tarea.completado}" aria-valuemin="0" 
						  aria-valuemax="100" style="width: ${tarea.completado}%">
						    ${tarea.completado}%
						  </div>
						</div>
						</td>
						<c:if test="${puedeEditarTareas || puedeEliminarTareas || puedeAsignarTareas}">
							<td>
								<c:if test="${puedeEditarTareas}">
									<button  class='btn btn-info' onclick="actionMostrarEditarTarea(${inc})"><i class="glyphicon glyphicon-edit"></i></button>
								</c:if>
								<c:if test="${puedeEliminarTareas}">
									<button  class='btn btn-info' onclick="eliminar(${inc})"><i class="glyphicon glyphicon-trash"></i></button>
								</c:if>
								<c:if test="${tarea.puedeSerHijoDe.size()>=(tareas.size()-1) && (puedeAsignarTareas || puedeEditarAsignar || puedeEliminarAsignar)}">
									<button  class='btn btn-info' onclick="asignarTarea(${inc})"><i class="glyphicon glyphicon-hand-right"></i></button>
								</c:if>
							</td>
						</c:if>
						<c:if test="${puedeEditarTareas && proyecto.ordenacionTareas==8}">
							<td id='subir-tarea-${tarea.idTarea}'><c:if test="${inc>0}">
								<button  class='btn btn-info' onclick="cambiarTarea(${tarea.idTarea},${tareas.get(inc-1).idTarea},true)"><i class="glyphicon glyphicon-open"></i></button>
							</c:if></td>
							<td id='bajar-tarea-${tarea.idTarea}'><c:if test="${inc<(tareas.size()-1)}">
								<button  class='btn btn-info' onclick="cambiarTarea(${tarea.idTarea},${tareas.get(inc+1).idTarea},false)"><i class="glyphicon glyphicon-save"></i></button>
							</c:if></td>
						</c:if>
						<c:set var="inc" value="${inc+1}" />
					</tr>
					</c:forEach>
					</tbody>		
				</table>
			</div>
			<p id='tareasP' display:none;></p>
		</c:if>
		<c:if test="${tareas.size()==0 }">
			<div style="text-align:center;" id="divTodosTareas" class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
				<table class='table table-striped table-hover'>
					<thead>
					<tr>
						<c:if test="${puedeEditarTareas || puedeEliminarTareas}">
							<th></th>
						</c:if>
						<th>#</th>
						<th>Nombre de la tarea</th>
						<th>Duraci�n estimada</th>
						<th>Prioridad</th>
						<th>Depende de las tareas</th>
						<th>Completado</th>
						<c:if test="${puedeEditarTareas || puedeEliminarTareas}">
							<th <c:if test="${puedeEditarTareas && proyecto.ordenacionTareas==8}">colspan='3'</c:if>>Acciones</th>
						</c:if>
					</tr>
					</thead>
					<tbody id='tbodyTareas'>
					</tbody>
				</table>
			</div>
			<p id='tareasP' class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
			Este proyecto carece de tareas
			<c:if test="${puedeCrearTareas}">
				, <a href="javascript:actionMostrarCrearTarea()">crea una</a>
			</c:if>
			</p>
		</c:if>
	</c:if>