<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>
	function arrayContiene(array,elemento){
		for(var f=0;f<array.length;f++){
			if(array[f]==elemento)
				return true;
		}
		return false;
	}
	var equipos = [
              <c:set var="b" value="true" /><c:forEach items="${equipos}" var="equipo"><c:if test="${b==false}">,{</c:if>
              	<c:if test="${b==true}">{<c:set var="b" value="false"/></c:if>
              		idEquipo: "${equipo.idEquipo}",
              		nombreEquipo: "${equipo.nombreEquipo}",
              		descEquipo: "${equipo.descEquipo}",
              		miembros: [<c:set var="c" value="${true}"/><c:forEach items="${equipo.loginMiembros}" var="miembro" >
        			 <c:if test="${c==false}">,</c:if><c:if test="${c==true}"><c:set var="c" value="${false}"/></c:if>"${miembro}"</c:forEach>],
        			 tareas: [<c:set var="c" value="${true}"/><c:forEach items="${equipo.tareasAsignadas}" var="tareaAsignada" >
        			 <c:if test="${c==false}">,{</c:if><c:if test="${c==true}">{<c:set var="c" value="${false}"/></c:if>
        			 	idTarea:"${tareaAsignada.value0.idTarea}",
        			 	nombreTarea:"${tareaAsignada.value0.nombreTarea}",
        			 	descTarea:"${tareaAsignada.value0.descTarea}",
        			 	duracion:"${tareaAsignada.value0.duracion}",
        			 	completado:"${tareaAsignada.value0.completado}",
        			 	idProyectoPerteneciente:"${tareaAsignada.value0.idProyectoPerteneciente}",
        			 	porcentaje:"${tareaAsignada.value1}"
        			 }</c:forEach>]
        			 }
              </c:forEach>];
	var puedesInvitarA = [<c:set var="c" value="${true}"/><c:forEach items="${puedesInvitarA}" var="miembro" >
	 <c:if test="${c==false}">,</c:if><c:if test="${c==true}"><c:set var="c" value="${false}"/></c:if>"${miembro}"</c:forEach>]
	
	function actualizar_equipos(){
		inner = "";
		for(var g=0;g<equipos.length;g++){
			equipo = equipos[g];
			id = equipo.idEquipo;
			nombre = equipo.nombreEquipo;
			if(equipo.descEquipo.length>50)
				breveDesc = equipo.descEquipo.substring(0,47) + '...';
			else
				breveDesc = equipo.descEquipo;
			inner +=  '<tr  id="equipo-'+id+'"><td><a href="javascript:mostrarEditarEquipo('+g+')">'+nombre+'</a></td>';
			inner += '<td>'+breveDesc+'</td>';
			inner += '<td><a id"mostrarMiembros-'+equipo.idEquipo+'" href="javascript:mostrarMiembrosEquipo('+g+')">0 integrantes</a></td>';
			inner += '<td><a href="javascript:mostrarTareasEquipo('+g+')">0 tareas asignadas</a></td>';
			<c:if test="${puedeEditarEquipos || puedeEliminarEquipos}">
				inner += '<td>';
				<c:if test="${puedeEditarEquipos}">
					inner += '<button class="btn btn-info" onclick="mostrarEditarEquipo('+g+')"><i class="glyphicon glyphicon-edit"></i></button>';
				</c:if>
				<c:if test="${puedeEliminarEquipos}">
					inner += '<button class="btn btn-info" onclick="eliminarEquipo('+id+')"><i class="glyphicon glyphicon-trash"></i></button>';
				</c:if>
				inner += '</td>';
			</c:if>
			inner += '</tr>';
		}
		document.getElementById("tbodyEquipos").innerHTML = inner;
	}
	
	function visualizar_agregarNuevoEquipo(id,nombre,desc){
		equipo = {idEquipo:id,nombreEquipo:nombre,descEquipo:desc,miembros:[],tareas:[]};
		equipos.push(equipo);
		actualizar_indices_equipos();
		document.getElementById("divTodosEquipos").style="";
		document.getElementById("equipoP").style="display:none;";
		actualizar_equipos();
	}
	
	function visualizar_editarEquipo(id,nombre,desc){
		intEditar = -1;
		for(k=0;k<equipos.length&&intEditar==-1;k++){
			if(equipos[k].idEquipo==id)
				intEditar = k;
		}
		if(intEditar>-1){
			equipo = {idEquipo:id,nombreEquipo:nombre,descEquipo:desc,miembros:[],tareas:[]};
			equipos[intEditar] = equipo;
			actualizar_indices_equipos();
			
			actualizar_equipos();
		}
	}
	
	function visualizar_eliminarEquipo(id){
		filaEq = document.getElementById("equipo-"+id);
		tproy = document.getElementById('tbodyEquipos');
		tproy.removeChild(filaEq);
		intEliminar = -1;
		for(k=0;k<equipos.length&&intEliminar==-1;k++){
			if(equipos[k].idEquipo==id)
				intEliminar = k;
		}
		if(intEliminar>-1){
			equipos = eliminar_del_array(equipos,intEliminar);;
			actualizar_indices_equipos();
			
			actualizar_equipos();
		}
	}
	
	function showLightboxEquipo(mensaje,idEquipo) {
		innerHtml = "<div id='content'><p style='text-align:right;'>";
		<c:if test="${puedeEliminarEquipos}">
		if(idEquipo>0){
			innerHtml += "<button class='btn btn-info' onclick='eliminarEquipo("+idEquipo+")'><i class='glyphicon glyphicon-trash'></i></button>"
		}
		</c:if>
		innerHtml += "<button class='btn btn-info' onclick='hideLightbox();'><i class='glyphicon glyphicon-remove'></i></button></p>";
		innerHtml += mensaje;
		innerHtml += "</div>";
		document.getElementById('over').innerHTML = innerHtml;
		document.getElementById('over').style.display='block';
		document.getElementById('fade').style.display='block';
	}
	
	var indices_equipos = [];
	function actualizar_indices_equipos(){
		indices_equipos = [];
		for(a=0;a<equipos.length;a++){
			indices_equipos[equipos[a].idEquipo] = a;
		}
	}
	actualizar_indices_equipos();
	
	<c:if test='${puedeCrearEquipos}'>
	function crearEquipo(){
		nombre = document.getElementById("nombreEquipoCrear").value;
		if(nombre==''){
			alert('Debes introducir un nombre para el equipo');
			return;
		}
		desc = document.getElementById("descEquipoCrear").value;
		$.ajax({
			url: '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos.htm',
			data: '{"nombreEquipo":"'+nombre+'","descEquipo":"'+desc+'"}',
			type: 'POST',
			success: function(idEquipo){
				//location.href = location.href;
				visualizar_agregarNuevoEquipo(idEquipo,nombre,desc);
				alert('Nuevo equipo creado');
				hideLightbox();
			}
		}).fail(function(data){alert(data.responseText);});
	}
	</c:if>
	<c:if test='${puedeEditarEquipos}'>
	function editarEquipo(idEquipo){
		nombre = document.getElementById("nombreEquipoEditar").value;
		if(nombre==''){
			alert('Debes introducir un nombre para el equipo');
			return;
		}
		desc = document.getElementById("descEquipoEditar").value;
		$.ajax({
			url: '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/'+idEquipo+'.htm',
			data: '{"nombreEquipo":"'+nombre+'","descEquipo":"'+desc+'"}',
			type: 'PUT',
			success: function(idTarea){
				visualizar_editarEquipo(idEquipo,nombre,desc);
				hideLightbox();
				alert("Se ha editado el equipo llamado "+nombre);
			}
		}).fail(function(data){alert(data.responseText);});
	}
	function visualizar_eliminarMiembro(login,idEquipo){
		intEditar = 0;
		for(var h=0;h<equipos.length&&intEditar==0;h++){
			if(equipos[h].idEquipo==idEquipo)
				intEditar = h;
		}
		if(intEditar>0){
			idMiembro = $.inArray(login, equipos[intEditar].miembros)
			if(idMiembro>-1)
				eliminar_del_array(equipos[intEditar].miembros,idMiembro);
		}
		actualizar_cantidad_integrantes(intEditar,idEquipo);
	}
	function actualizar_cantidad_integrantes(idInterno,idEquipo){
		cant_integrantes = equipos[idInterno].miembros.length;
		if(cant_integrantes==1)
			document.getElementById('mostrarMiembros-'+idEquipo).innerHtml = '1 integrante';
		else
			document.getElementById('mostrarMiembros-'+idEquipo).innerHtml = cant_integrantes+' integrantes';
	}
	function eliminarMiembroEquipo(idEquipo,login){
		$.ajax({
			url: '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/'+idEquipo+'/miembros.htm',
			data: login,
			type: 'DELETE',
			success: function(){
				alert("Se ha eliminado el miembro "+login+" del equipo");
				location.href = '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/ver.htm';
			}
		}).fail(function(data){alert(data.responseText);});
	}
	</c:if>
	<c:if test='${puedeEliminarEquipos}'>
	function eliminarEquipo(idEquipo){
		if(!confirm("�Est�s seguro de querer eliminar el equipo "+equipos[indices_equipos[idEquipo]].nombreEquipo+" permanentemente?")){
			return;
		}
		$.ajax({
			url: '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/'+idEquipo+'.htm',
			type: 'DELETE',
			success: function(idTarea){
				visualizar_eliminarEquipo(idEquipo)
				alert("Se ha eliminado el equipo con id "+idEquipo);
			}
		}).fail(function(data){alert(data.responseText);});
	}
	</c:if>
	<c:if test='${puedeCrearEquipos || puedeEditarEquipos}'>
	function visualizar_agregarMiembro(login,idEquipo){
		intEditar = 0;
		for(var h=0;h<equipos.length&&intEditar==0;h++){
			if(equipos[h].idEquipo==idEquipo)
				intEditar = h;
		}
		if(intEditar>0){
			if($.inArray(login, equipos[intEditar].miembros)==-1)
				equipos[intEditar].miembros.push(login);
		}
		actualizar_cantidad_integrantes(intEditar,idEquipo);
	}
	
	function invitarMiembroEquipo(idEquipo){
		login = document.getElementById("selectMiembro").value;
		if(login==''){alert('Debes elegir un usuario');return;}
		$.ajax({
			url: '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/'+idEquipo+'/miembros.htm',
			data: login,
			type: 'POST',
			success: function(){
				alert("Se ha invitado al miembro "+login+" a un equipo");
				location.href = '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/ver.htm';
			}
		}).fail(function(data){alert(data.responseText);});
	}
	</c:if>
	
	function mostrarCrearEquipo(){
		mostrarLightboxEquipo(false,false,false,true,null);
	}
	function mostrarEditarEquipo(idInterno){
		mostrarLightboxEquipo(true,false,false,false,equipos[idInterno]);
	}
	function mostrarMiembrosEquipo(idInterno){
		mostrarLightboxEquipo(false,true,false,false,equipos[idInterno]);
	}
	function mostrarTareasEquipo(idInterno){
		mostrarLightboxEquipo(false,false,true,false,equipos[idInterno]);
	}
	
	function editarVisible(){
		document.getElementById("divEditarEquipo").style="";
		document.getElementById("divMiembrosEquipo").style="display:none;";
		document.getElementById("divCrearEquipo").style="display:none;";
		document.getElementById("divTareasEquipo").style="display:none;";
	}
	function miembrosVisible(){
		document.getElementById("divEditarEquipo").style="display:none;";
		document.getElementById("divMiembrosEquipo").style="";
		document.getElementById("divCrearEquipo").style="display:none;";
		document.getElementById("divTareasEquipo").style="display:none;";
	}
	function tareasVisible(){
		document.getElementById("divEditarEquipo").style="display:none;";
		document.getElementById("divMiembrosEquipo").style="display:none;";
		document.getElementById("divCrearEquipo").style="display:none;";
		document.getElementById("divTareasEquipo").style="";
	}
	function crearVisible(){
		document.getElementById("divEditarEquipo").style="display:none;";
		document.getElementById("divMiembrosEquipo").style="display:none;";
		document.getElementById("divCrearEquipo").style="";
		document.getElementById("divTareasEquipo").style="display:none;";
	}
	
	<c:if test="${puedeEditarEquipos}">
		var titleEditar = "Editar Equipo";
	</c:if>
	<c:if test="${puedeEditarEquipos!=true}">
		var titleEditar = "Informaci�n del Equipo";
	</c:if>
	
	function generarDivEditarEquipo(equipo, mostrar){
		innerHtml = "<div id='divEditarEquipo'";
		if(!mostrar) innerHtml += " style='display:none;'";
		innerHtml += ">"
		innerHtml += 
		"<ul class='nav nav-tabs' role='tablist'>"+
		  "<li class='active'><a href='#'>"+titleEditar+"</a></li>"+
		  "<li><a href='javascript:miembrosVisible()'>Miembros</a></li>"+
		  "<li><a href='javascript:tareasVisible()'>Tareas</a></li>"+
		  <c:if test="${puedeCrearEquipos}">
		  	"<li><a href='javascript:crearVisible()'>Crear Equipo</a></li>"+
		  </c:if>
		"</ul>";
		<c:if test="${puedeEditarEquipos}">
			innerHtml += "<h3>EDITAR EQUIPO "+equipo.nombreEquipo+"</h3>";
			innerHtml += "Nombre: <input id='nombreEquipoEditar' type='text' value='"+equipo.nombreEquipo+"'/><br>";
			innerHtml += "Descripci�n: <input id='descEquipoEditar' type='text' value='"+equipo.descEquipo+"'/><br>";
			innerHtml += "<button class='btn btn-info' onclick='editarEquipo("+equipo.idEquipo+")'>Editar</button><br>";
		</c:if>
		<c:if test="${puedeEditarEquipos!=true}">
			innerHtml += "<h3>INFORMACI�N DEL EQUIPO "+equipo.nombreEquipo+"</h3>";
			innerHtml += "Nombre: "+equipo.nombreEquipo+"<br>";
			innerHtml += "Descripci�n: "+equipo.descEquipo+"<br>";
		</c:if>
		innerHtml += "</div>";
		return innerHtml;
	}
	function generarDivMiembrosEquipo(equipo, mostrar){
		innerHtml = "<div id='divMiembrosEquipo'";
		if(!mostrar) innerHtml += " style='display:none;'";
		innerHtml += ">";
		innerHtml += 
		"<ul class='nav nav-tabs' role='tablist'>"+
		  "<li><a href='javascript:editarVisible()'>"+titleEditar+"</a></li>"+
		  "<li class='active'><a href='#'>Miembros</a></li>"+
		  "<li><a href='javascript:tareasVisible()'>Tareas</a></li>"+
		  <c:if test="${puedeCrearEquipos}">
		  	"<li><a href='javascript:crearVisible()'>Crear Equipo</a></li>"+
		  </c:if>
		"</ul>";
		innerHtml += "<h3>MIEMBROS DEL EQUIPO "+equipo.nombreEquipo+"</h3>";
		<c:if test="${puedeEditarEquipos}">
			innerHtml += "<select id='selectMiembro'><option value=''></option>";
			for(var g=0;g<miembros.length;g++){
				if(!arrayContiene(equipo.miembros,miembros[g].login)){
					innerHtml += "<option value='"+miembros[g].login+"'>"+miembros[g].login+"</option>";
				}
			}
			innerHtml += "</select><button class='btn btn-info' onclick='invitarMiembroEquipo("+equipo.idEquipo+")'>Invitar</button>";
		</c:if>
		if(equipo.miembros.length>0){
			innerHtml += "<table class='table table-striped'><thead><tr><th>Login</th><c:if test='${puedeEditarEquipos}'><th>Acciones</th></c:if></tr></thead><tbody>";
			for(var f=0;f<equipo.miembros.length;f++){
				innerHtml += "<tr><td>"+equipo.miembros[f]+"</td><c:if test='${puedeEditarEquipos}'><th>"+
					"<button class='btn btn-info' onclick='eliminarMiembroEquipo("+equipo.idEquipo+",\""+equipo.miembros[f]+"\")'><i class='glyphicon glyphicon-trash'></i></button>"+
				"</th></c:if></tr>";
			}
			innerHtml += "</tbody></table>";
		}
		innerHtml += "</div>";
		return innerHtml;
	}
	
	function updateCompletado(cambiarA,idEquipo,idInternoTarea){
		idInternoEquipo = indices_equipos[idEquipo];
		tarea = equipos[idInternoEquipo].tareas[idInternoTarea];
		if(cambiarA>0 && cambiarA<=tarea.porcentaje){
			$.ajax({
			    url: "${urlBase}/proyectos/"+tarea.idProyectoPerteneciente+"/tareas/asignadas/"+tarea.idTarea+"/completar/equipos/"+idEquipo+".htm",
			    type: 'PUT',
			    data: cambiarA,
			    success: function(data) {
			    	equipos[idInternoEquipo].tareas[idInternoTarea].completado = cambiarA;
					alert("Tarea Asignada Completada con �xito");
			    }
			}).fail(function(data){alert(data.responseText);});
		}
	}
	
	function generarDivTareasEquipo(equipo, mostrar){
		innerHtml = "<div id='divTareasEquipo'";
		if(!mostrar) innerHtml += " style='display:none;'";
		innerHtml += ">";
		innerHtml += 
		"<ul class='nav nav-tabs' role='tablist'>"+
		  "<li><a href='javascript:editarVisible()'>"+titleEditar+"</a></li>"+
		  "<li><a href='javascript:miembrosVisible()'>Miembros</a></li>"+
		  "<li class='active'><a href='#'>Tareas</a></li>"+
		  <c:if test="${puedeCrearEquipos}">
		  	"<li><a href='javascript:crearVisible()'>Crear Equipo</a></li>"+
		  </c:if>
		"</ul>";
		innerHtml += "<h3>TAREAS ASIGNADAS AL EQUIPO "+equipo.nombreEquipo+"</h3>";
		if(equipo.tareas.length>0){
			innerHtml += "<table class='table table-striped'><thead><tr><th>Nombre</th><th>Duraci�n</th><th>Completado</th><th></th><th>Acciones</th></tr></thead><tbody>";
			for(var f=0;f<equipo.tareas.length;f++){
				innerHtml += "<tr><td>"+equipo.tareas[f].nombreTarea+"</td>"+
					"<td>"+equipo.tareas[f].duracion+" horas</td>"+
					<c:if test="${deUsuario}">
						"<td><input type='number' min='0' max='"+equipo.tareas[f].porcentaje+"' value='"+equipo.tareas[f].completado+
							"' onchange='updateCompletado(this.value,"+equipo.idEquipo+","+f+")'/>%</td>"+
					</c:if>
					<c:if test="${deUsuario==false}">
						"<td>"+equipo.tareas[f].completado+"%</td>"+
					</c:if>
					"<td>de "+equipo.tareas[f].porcentaje+"%</td>"+
					"<td><a href='${urlBase}/proyectos/"+equipo.tareas[f].idProyectoPerteneciente+".htm'><button class='btn btn-info'>Ver Proyecto</button></a></td>"
				"</tr>"
			}
			innerHtml += "</tbody></table>";
		}
		innerHtml += "</div>";
		return innerHtml;
	}
	function generarDivCrearEquipo(mostrar,puedesIrAMasSitios){
		innerHtml = "<div id='divCrearEquipo'";
		if(!mostrar) innerHtml += " style='display:none;'";
		innerHtml += ">";
		if(puedesIrAMasSitios){
			innerHtml += 
				"<ul class='nav nav-tabs' role='tablist'>"+
				  "<li><a href='javascript:editarVisible()'>"+titleEditar+"</a></li>"+
				  "<li><a href='javascript:miembrosVisible()'>Miembros</a></li>"+
				  "<li><a href='javascript:tareasVisible()'>Tareas</a></li>"+
				  "<li class='active'><a href='#'>Crear Equipo</a></li>"+
				"</ul>";
		}
		innerHtml += "<h3>CREAR EQUIPO</h3>";
		innerHtml += "Nombre: <input id='nombreEquipoCrear' type='text' value=''/><br>";
		innerHtml += "Descripci�n: <input id='descEquipoCrear' type='text' value=''/><br>";
		innerHtml += "<button class='btn btn-info' onclick='crearEquipo()'>Crear</button><br>";
		innerHtml += "</div>";
		return innerHtml;
	}
	
	function mostrarLightboxEquipo(mostrarEditar,mostrarMiembros,mostrarTareas,mostrarCrear,equipo){
		innerHtml = "";
		if(equipo!=null){
			innerHtml += generarDivEditarEquipo(equipo,mostrarEditar);
			innerHtml += generarDivMiembrosEquipo(equipo,mostrarMiembros);
			innerHtml += generarDivTareasEquipo(equipo,mostrarTareas);
			innerHtml += generarDivCrearEquipo(mostrarCrear,true);
		} else {
			innerHtml += generarDivCrearEquipo(mostrarCrear,false);
		}
		
		if(equipo==null)
			showLightboxEquipo(innerHtml,0);
		else
			showLightboxEquipo(innerHtml,equipo.idEquipo);
	}
</script>
<c:if test="${puedeCrearEquipos}">
	<button class="btn btn-info" onclick="mostrarCrearEquipo()"><i class="glyphicon glyphicon-plus"></i></button>
</c:if>
<c:if test="${equipos.size()>0}">
	<div style="text-align:center;" id='divTodosEquipos' >
		<h3><c:if test="${grupoPerteneciente!=null}">EQUIPOS DE ${grupoPerteneciente.nombreGrupo}</c:if>
		<c:if test="${grupoPerteneciente==null}">MIS EQUIPOS</c:if><a href="${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/ver.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<table class='table table-striped'>
				<thead>
				<tr>
					<th>Nombre del Equipo </th>
					<th> Descripci�n </th>
					<th> Formado por </th>
					<c:if test="${puedeEditarEquipos || puedeEliminarEquipos}">
						<th> Acciones</th>
					</c:if>
				</tr>
				</thead>
				<c:set var="inc" value="0" />
				<tbody id='tbodyEquipos'>
				<c:forEach items="${equipos}" var="equipo">
				<tr id="equipo-${equipo.idEquipo}">
					<td><a href="javascript:mostrarEditarEquipo(${inc})">${equipo.nombreEquipo}</a></td>
					<td>${equipo.breveDesc}</td>
					<td><a id='mostrarMiembros-${equipo.idEquipo}' href='javascript:mostrarMiembrosEquipo(${inc})'>${equipo.loginMiembros.size()}<c:if test="${equipo.loginMiembros.size()==1}"> integrante</c:if><c:if test="${equipo.loginMiembros.size()!=1}"> integrantes</c:if></a></td>
					<td><a href='javascript:mostrarTareasEquipo(${inc})'>${equipo.tareasAsignadas.size()}<c:if test="${equipo.tareasAsignadas.size()==1}"> tarea asignada</c:if><c:if test="${equipo.tareasAsignadas.size()!=1}"> tareas asignadas</c:if></a></td>
					<c:if test="${puedeEditarEquipos || puedeEliminarEquipos}">
						<td>
							<c:if test="${puedeEditarEquipos}"><button class="btn btn-info" onclick="mostrarEditarEquipo(${inc})"><i class="glyphicon glyphicon-edit"></i></button></c:if>
							<c:if test="${puedeEliminarEquipos}"><button class="btn btn-info" onclick="eliminarEquipo(${equipo.idEquipo})"><i class="glyphicon glyphicon-trash"></i></button></c:if>
						</td>
					</c:if>
					<c:set var="inc" value="${inc+1}" />
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<p id='equipoP' style='display:none;'></p>
</c:if>
<c:if test="${equipos.size()==0}">
	<div style="text-align:center;" id='divTodosEquipos' style='display:none;'>
		<h3><c:if test="${grupoPerteneciente!=null}">EQUIPOS DE ${grupoPerteneciente.nombreGrupo}</c:if>
		<c:if test="${grupoPerteneciente==null}">MIS EQUIPOS</c:if>
		<a href="${urlBase}/grupos/${grupoPerteneciente.idGrupo}/equipos/ver.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<table class='table table-striped' style='display:none;'>
				<thead>
				<tr>
					<th>Nombre del Equipo </th>
					<th> Descripci�n </th>
					<th> Formado por </th>
					<c:if test="${puedeEditarEquipos || puedeEliminarEquipos}">
						<th> Acciones</th>
					</c:if>
				</tr>
				</thead>
				<c:set var="inc" value="0" />
				<tbody id='tbodyEquipos'>
				</tbody>
			</table>
		</div>
	</div>
	<c:if test="${equipoPerteneciente==null}"><br/><p id='equipoP'>No existe ning�n equipo.</p></c:if>
</c:if>