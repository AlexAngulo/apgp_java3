<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>
var grupos = [
              <c:set var="b" value="true" />
              <c:forEach items="${grupos}" var="grupo">
              	<c:if test="${b==false}">
              		,{
              	</c:if>
              	<c:if test="${b==true}">
	          		{
	          		<c:set var="b" value="false"/>
	          	</c:if>
              			idGrupo: "${grupo.idGrupo}",
              			nombreGrupo: "${grupo.nombreGrupo}",
              			descGrupo: "${grupo.descGrupo}",
              			breveDesc: "${grupo.breveDesc}",
              			puedeEditar: "${grupo.editarGrupo}",
              			puedeEliminar: "${grupo.eliminarGrupo}",
              			puedeCrearSubgrupos: "${grupo.crearSubgrupo}",
              			idSupergrupo: "${grupo.idSupergrupo}",
              			puedeSerHijoDe: [<c:set var="c" value="${true}"/>
        								<c:forEach items="${grupo.puedeSerHijoDe}" var="serHijoDe" >
        							  		<c:if test="${c==false}">,</c:if>
        									<c:if test="${c==true}"><c:set var="c" value="${false}"/></c:if>
        										${serHijoDe}
        								</c:forEach>]
              		}
              </c:forEach>];

function actualizar_grupos() {
	document.getElementById("divTodosGrupos").style="";
	document.getElementById("gruposP").style="display:none;";
	<c:if test="${grupoPerteneciente!=null}">
		elUrl = '${urlBase}/grupos/${grupoPerteneciente.idGrupo}/actualizar.htm'
	</c:if>
	<c:if test="${grupoPerteneciente==null}">
		elUrl = '${urlBase}/grupos/actualizar.htm'
	</c:if>
	$.ajax({
		url: elUrl,
		type: 'GET',
		success: function(object){
	        grupos = JSON.parse(object);
	        actualizar_tabla_grupos();
	        actualizar_indices_grupos();
		}
	}).fail(function(){return;});
}

function eliminarGrupo(idInterno){
	if(confirm("�Est�s seguro de querer eliminar el Grupo?")){
		$.ajax({
			url: '${urlBase}/grupos/'+grupos[idInterno].idGrupo+'.htm',
			type: 'DELETE',
			success: function(idGrupo){
				switch(idGrupo) {
				case "0":
					alert("No se ha podido eliminar el grupo '"+grupos[idInterno].nombreGrupo+"'");
					break;
				default:
					actualizar_grupos();
					actualizarTabGrupos();
					alert("Grupo '"+grupos[idInterno].nombreGrupo+"' eliminado con �xito");
					//location.href = location.href;
				}
			}
		}).fail(function(data){alert(data.responseText);});
	}
}

function crearOEditarGrupo(idEditar){
	nombreGrupo = document.getElementById("nombreGrupo").value;
	descGrupo = document.getElementById("descGrupo").value;
	idSupergrupo = document.getElementById("idSupergrupo").value;
	if(idSupergrupo=='') idSupergrupo=0;
	elJSON  = '{"nombreGrupo":"'+nombreGrupo+'","descGrupo":"'+descGrupo+'","idSupergrupo":'+idSupergrupo+'}';
	if(idEditar==0){
		$.ajax({
			url: '${urlBase}/grupos.htm',
			data: elJSON,
			type: 'POST',
			success: function(idGrupo){
				switch(idGrupo) {
				case "0":
					alert("No se ha podido crear el grupo");
					break;
				default:
					actionOcultarCrearGrupo();
					actualizar_grupos();
					actualizarTabGrupos();
					if(confirm("Se ha creado el grupo con �xito �Quieres acceder a la p�gina del grupo '"+nombreGrupo+"'?")){
						location.href="grupos/"+idGrupo+".htm";
					}
					//location.href = location.href;
				}
			}
		}).fail(function(data){alert(data.responseText);});
	} else {
		$.ajax({
			url: '${urlBase}/grupos/'+idEditar+'.htm',
			data: elJSON,
			type: 'PUT',
			success: function(data){
				switch(data) {
				case "0":
					alert("No se ha podido editar el grupo");
					break;
				default:
					actionOcultarCrearGrupo();
					actualizar_grupos();
					actualizarTabGrupos();
					if(confirm("Se ha editado el grupo llamado '"+nombreGrupo+"' �Quieres ver la p�gina del grupo?"))
						location.href="${urlBase}/grupos/"+idEditar+".htm";
					//location.href = location.href;
				}
			}
		}).fail(function(data){alert(data.responseText);});
	}
}

function actualizarTabGrupos(){
	<c:if test="${grupoPerteneciente==null}">
		innerHtml = "";
		for(var inc=0;inc<grupos.length;inc++){
			grupo = grupos[inc];
			innerHtml += "<li><a href='${urlBase}/grupos/"+grupo.idGrupo+".htm'>"+grupo.nombreGrupo+"</a></li>";
			if(i>=5) break;
		}
		if(grupos.length>0){
			innerHtml += "<li><a href='${urlBase}/grupos.htm'>Ver m�s Grupos...</a></li>"+
				"<li class='divider'></li>";
		}
		innerHtml += "<li><a href='${urlBase}/grupos/crear.htm'>Crear Grupo</a></li>";
		
		document.getElementById("dropdown-grupos").innerHTML = innerHtml;
	</c:if>
}

var indices_grupos = [];
function actualizar_indices_grupos(){
	indices_grupos = [];
	for(i=0;i<grupos.length;i++){
		indices_grupos[grupos[i].idGrupo] = i;
	}
}
actualizar_indices_grupos();

function puedeSerSubgrupoDe(seleccionado,grupo){
	divSubgrupo = "Es subgrupo de:<select id='idSupergrupo'>";
	<c:if test="${grupoPerteneciente==null}">
		divSubgrupo += "<option value=''></option>";
	</c:if>
	<c:if test="${grupoPerteneciente!=null}">
		divSubgrupo += "<option value='${grupoPerteneciente.idGrupo}'>${grupoPerteneciente.idGrupo} - ${grupoPerteneciente.nombreGrupo}</option>";
	</c:if>
	if(grupo==""){
		for(i=0;i<grupos.length;i++){
			if(grupos[i].puedeCrearSubgrupos=='true'){
				divSubgrupo += "<option value='"+grupos[i].idGrupo+"' ";
				if(seleccionado==grupos[i].idGrupo) divSubgrupo += "selected";
				divSubgrupo += ">"+grupos[i].idGrupo+" - "+grupos[i].nombreGrupo+"</option>";
			}
		}
	} else {
		puedeSerHijoDe = grupo.puedeSerHijoDe;
		for(i=0;i<puedeSerHijoDe.length;i++){
			grupo = grupos[indices_grupos[puedeSerHijoDe[i]]];
			if(grupo.puedeCrearSubgrupos=='true'){
				divSubgrupo += "<option value='"+grupo.idGrupo+"' ";
				if(seleccionado==grupo.idGrupo) divSubgrupo += "selected";
				divSubgrupo += ">"+grupo.idGrupo+" - "+grupo.nombreGrupo+"</option>";
			}
		}
	}
	document.getElementById("divSupergrupo").innerHTML = divSubgrupo;
}

function convertirSubmitEnCrearGrupo(idSupergrupo){
	document.getElementById("submitCrearGrupo").innerHTML = "Crear Grupo";
	document.getElementById("submitCrearGrupo").setAttribute("onclick","crearOEditarGrupo(0)");
	document.getElementById("legendary").innerHTML = 'CREAR NUEVO GRUPO';
	document.getElementById("nombreGrupo").value = '';
	document.getElementById("descGrupo").value = '';
	puedeSerSubgrupoDe(idSupergrupo,"");
}

function mostrarCrearGrupo(idSupergrupo) {
	document.getElementById("formCrearGrupo").setAttribute("style", "");
	document.getElementById("buttonMostrarCrearGrupo").setAttribute("style", "display: none;");
	document.getElementById("buttonOcultarCrearGrupo").setAttribute("style", "");
	convertirSubmitEnCrearGrupo(idSupergrupo);
}

function actionMostrarCrearGrupo(idSupergrupo) {
	mostrarCrearGrupo(idSupergrupo);
}

function ocultarCrearGrupo() {
	document.getElementById("formCrearGrupo").setAttribute("style", "display: none;");
	document.getElementById("buttonMostrarCrearGrupo").setAttribute("style", "");
	document.getElementById("buttonOcultarCrearGrupo").setAttribute("style", "display: none;");
	convertirSubmitEnCrearGrupo(0);
}

function actionOcultarCrearGrupo() {
	ocultarCrearGrupo();
}

function convertirSubmitEnEditarGrupo(grupo){
	document.getElementById("submitCrearGrupo").innerHTML = "Editar Grupo";
	document.getElementById("submitCrearGrupo").setAttribute("onclick","crearOEditarGrupo("+grupo.idGrupo+")");
	document.getElementById("legendary").innerHTML = 'EDITAR GRUPO '+grupo.nombreGrupo+" CON ID "+grupo.idGrupo;
	document.getElementById("nombreGrupo").value = grupo.nombreGrupo;
	document.getElementById("descGrupo").value = grupo.descGrupo;
	puedeSerSubgrupoDe(grupo.idSupergrupo, grupo);
}

function editarGrupo(idInterno){
	grupo = grupos[idInterno];
	mostrarCrearGrupo(0);
	convertirSubmitEnEditarGrupo(grupo);
}

function actionMostrarEditarGrupo(idInterno) {
	location.href='#formCrearGrupo';
	editarGrupo(idInterno);
}

function actualizar_tabla_grupos(){
	innerHtml = "";
	for(var f=0;f<grupos.length;f++){
		grupo = grupos[f];
		innerHtml += "<tr>";
		innerHtml += "<td>"+grupo.espacios+"<a href='${urlBase}/grupos/"+grupo.idGrupo+".htm'>"+grupo.nombreGrupo+"</a></td>";
		innerHtml += "<td>"+grupo.breveDesc+"</td>";
		if(grupo.nombreGrupo=='') innerHtml += "<td>-</td>";
		else innerHtml += "<td>"+grupo.nombreSupergrupo+"</td>";
		innerHtml += "<td>"+grupo.nombreCreador+"</td>";
		innerHtml += "<td>";
		if(grupo.puedeEditar) innerHtml += "<button class='btn btn-info' onclick='actionMostrarEditarGrupo("+f+")'><i class='glyphicon glyphicon-edit'></i></button>";
		if(grupo.puedeEliminar) innerHtml += "<button class='btn btn-info' onclick='eliminarGrupo("+f+")'><i class='glyphicon glyphicon-trash'></i></button>";
		if(grupo.puedeCrearSubgrupo) innerHtml += "<button class='btn btn-info' onclick='actionMostrarCrearGrupo("+f+")'>Crear Subgrupo</button>";
		innerHtml += "</td>";
		innerHtml += "</tr>";
	}
	document.getElementById("tbodyGrupos").innerHTML = innerHtml;
}
</script>
<c:if test="${grupoPerteneciente==null || puedeCrearSubgrupos}">
	<button class='btn btn-info' id="buttonMostrarCrearGrupo" onclick="actionMostrarCrearGrupo(0)">Crear Grupo</button>
	<button class='btn btn-info' id="buttonOcultarCrearGrupo" onclick="actionOcultarCrearGrupo()" style='display:none;'>Ocultar Creaci�n de Grupos</button>
	<div id='formCrearGrupo' style='display:none;'>
		  <br>
		  <fieldset style='text-align:center;'>
		    <legend id='legendary'>CREAR NUEVO GRUPO</legend>
		      T�tulo:<input type='text' id='nombreGrupo' name='nombreGrupo' /><br>
		      Descripci�n:<input type='text' id='descGrupo' name='descGrupo' /><br>
		      <div id="divSupergrupo"></div>
		    <button id='submitCrearGrupo' class='btn btn-info'>Crear Grupo</button>
		  </fieldset>
	</div>
</c:if>
<c:if test="${grupos.size()>0}">
	<div style="text-align:center;" id="divTodosGrupos">
		<h3>
		<c:if test="${grupoPerteneciente!=null}">SUBGRUPOS DE ${grupoPerteneciente.nombreGrupo}<a href="${urlBase}/grupos/${grupoPerteneciente.idGrupo}/subgrupos.htm"><i class='glyphicon glyphicon-refresh'></i></a></c:if>
		<c:if test="${grupoPerteneciente==null}">GRUPOS A LOS QUE PERTENECES<a href="${urlBase}/grupos.htm"><i class='glyphicon glyphicon-refresh'></i></a></c:if>
		</h3>
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<table class='table table-striped'>
				<thead>
				<tr>
					<th>Nombre del grupo </th>
					<th> Descripci�n </th>
					<th> Supergrupo </th>
					<th> Creado por </th>
					<th> Acciones</th>
				</tr>
				</thead>
				<tbody id="tbodyGrupos">
				<c:set var="inc" value="0" />
				<c:forEach items="${grupos}" var="grupo">
				<tr>
					<td>${grupo.espacios}<a href='${urlBase}/grupos/${grupo.idGrupo}.htm'>${grupo.nombreGrupo}</a></td>
					<td>${grupo.breveDesc}</td>
					<td><c:if test="${grupo.nombreSupergrupo!=''}">${grupo.nombreSupergrupo}</c:if><c:if test="${grupo.nombreSupergrupo==''}">-</c:if></td>
					<td>${grupo.nombreCreador}</td>
					<td>
						<c:if test="${grupo.editarGrupo}"><button class='btn btn-info' onclick="actionMostrarEditarGrupo(${inc})"><i class='glyphicon glyphicon-edit'></i></button></c:if>
						<c:if test="${grupo.eliminarGrupo}"><button class='btn btn-info' onclick="eliminarGrupo(${inc})"><i class='glyphicon glyphicon-trash'></i></button></c:if>
						<c:if test="${grupo.crearSubgrupo}"><button class='btn btn-info' onclick="actionMostrarCrearGrupo(${grupo.idGrupo})">Crear Subgrupo</button></c:if>
					</td>
					<c:set var="inc" value="${inc+1}" />
				</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	<p id="gruposP" style="display:none;"></p>
</c:if>
<c:if test="${grupos.size()==0}">
	<div style="text-align:center;" id="divTodosGrupos" style="display:none;">
		<h3>
		<c:if test="${grupoPerteneciente!=null}">SUBGRUPOS DE ${grupoPerteneciente.nombreGrupo}<a href="${urlBase}/grupos/${grupoPerteneciente.idGrupo}/subgrupos.htm"><i class='glyphicon glyphicon-refresh'></i></a></c:if>
		<c:if test="${grupoPerteneciente==null}">GRUPOS A LOS QUE PERTENECES<a href="${urlBase}/grupos.htm"><i class='glyphicon glyphicon-refresh'></i></a></c:if>
		</h3>
		<div  class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<table class='table table-striped'>
				<thead>
				<tr>
					<th>Nombre del grupo </th>
					<th> Descripci�n </th>
					<th> Supergrupo </th>
					<th> Creado por </th>
					<th> Acciones</th>
				</tr>
				</thead>
				<tbody id="tbodyGrupos">
				</tbody>
			</table>
		</div>
	</div>
	<c:if test="${grupoPerteneciente!=null}">
		<p id="gruposP" class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>No existe ning�n subgrupo
		<c:if test="${puedeCrearSubgrupo}">
			, <a href='javascript:actionMostrarCrearGrupo()'>crea uno</a>.
		</c:if>
		</p>
	</c:if>
</c:if>
<c:if test="${crear==true}"><script>actionMostrarCrearGrupo(0);</script></c:if>