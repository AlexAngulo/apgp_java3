<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<c:if test='${esCompartidos=="" && usuario.esAdministrador==false}'>
 	<c:set var='urlReload' value='${urlBase}' />
 </c:if>
<c:if test='${esCompartidos=="compartido" && usuario.esAdministrador==false}'>
	<c:set var='urlReload' value='${urlBase}/proyectos/compartidos.htm' />
</c:if>
<c:if test='${esCompartidos=="" && usuario.esAdministrador==true}'>
 	<c:set var='urlReload' value='${urlBase}/admin/proyectos/propios.htm' />
 </c:if>
<c:if test='${esCompartidos=="compartido" && usuario.esAdministrador==true}'>
	<c:set var='urlReload' value='${urlBase}/admin/proyectos/compartidos.htm' />
</c:if>
<c:if test='${esCompartidos=="plantillas"}'>
	<c:set var='urlReload' value='${urlBase}/admin/plantillas.htm' />
</c:if>
<c:if test='${esCompartidos=="todosLosProyectos"}'>
	<c:set var='urlReload' value="${urlBase}" />
</c:if>

<script>
	<c:if test="${esCompartidos!='plantillas'}">
		<c:set var="raiz" value="${urlBase}/proyectos" />
	</c:if>
	<c:if test="${esCompartidos=='plantillas'}">
		<c:set var="raiz" value="${urlBase}/plantillas" />
	</c:if>
		var noCompartidosCon = [<c:set var="d" value="true" /><c:forEach items="${noCompartidosCon}" var="noCompartidoCon">
									<c:if test="${d==false}">,</c:if>
									<c:if test="${d==true}"><c:set var="d" value="false"/></c:if>
									"${noCompartidoCon.login}"
		                        </c:forEach>];
		var ${esCompartidos}ordenProyectos = "${ordenProyectos}";
		var ${esCompartidos}proyectos = [
		             	<c:set var="b" value="${true}"/>
		             	<c:forEach items="${proyectos}" var="proyecto" >
			             	<c:if test="${b==false}">
		             			,{
		             		</c:if>
		             		<c:if test="${b==true}">
		             			{
		             			<c:set var="b" value="${false}"/>
		             		</c:if>
		             		idProyecto:"${proyecto.idProyecto}",
		             		idCreador:"${proyecto.idCreador}",
		             		idGrupoperteneciente:"${proyecto.idGrupoPerteneciente}",
		             		nombreProyecto:"${proyecto.nombreProyecto}",
		             		descProyecto:"${proyecto.descProyecto}",
		             		diaFin:"${proyecto.diaFin}",
		             		mesFin:"${proyecto.mesFin}",
		             		anoFin:"${proyecto.anoFin}",
		             		horaFin:"${proyecto.horaFin}",
		             		minFin:"${proyecto.minFin}",
		             		prioridad: "${proyecto.prioridad}",
		             		compartidosA: [<c:set var="c" value="${true}"/>
				             	<c:forEach items="${proyecto.compartidosA}" var="permisoProyecto" >
					             		<c:if test="${c==false}">
				             				,{
				             			</c:if>
				             			<c:if test="${c==true}">
				             				{
				             				<c:set var="c" value="${false}"/>
				             			</c:if>
				             				login: "${permisoProyecto.login}",
				             				crearTareas: "${permisoProyecto.crearTareas}",
				             				editarTareas: "${permisoProyecto.editarTareas}",
				             				eliminarTareas: "${permisoProyecto.eliminarTareas}",
				             				editarProyecto: "${permisoProyecto.editarProyecto}",
				             				eliminarProyecto: "${permisoProyecto.eliminarProyecto}",
				             				compartir: "${permisoProyecto.compartir}",
				             				editarCompartir: "${permisoProyecto.editarCompartir}",
				             				eliminarCompartir: "${permisoProyecto.eliminarCompartir}",
				             				asignarTarea: "${permisoProyecto.asignarTarea}",
				             				editarAsignar: "${permisoProyecto.editarAsignar}",
				             				eliminarAsignar: "${permisoProyecto.eliminarAsignar}"
				             			}
				             	</c:forEach>],
				             	<c:if test="${esCompartidos!='compartido'}">
					        		editarProyecto: "${puedeEditarProyecto}",
					        		eliminarProyecto: "${puedeEliminarProyecto}",
					        		compartir: "${puedeCompartirProyecto}",
					        		crearTareas: "${puedeCrearTareas}",
					        		editarTareas: "${puedeEditarTareas}",
					        		eliminarTareas: "${puedeEliminarTareas}",
					        		editarCompartir: "${puedeEditarCompartir}",
					        		eliminarCompartir: "${puedeEliminarCompartir}",
					        		asignarTarea: "${puedeAsignarTarea}",
					        		editarAsignar: "${puedeEditarAsignar}",
					        		eliminarAsignar: "${puedeEliminarAsignar}"
					        	</c:if>
				             	<c:if test="${esCompartidos=='compartido'}">
					        		editarProyecto: "${proyecto.editarProyecto}",
					        		eliminarProyecto: "${proyecto.eliminarProyecto}",
					        		compartir: "${proyecto.compartir}",
					        		crearTareas: "${proyecto.crearTareas}",
					        		editarTareas: "${proyecto.editarTareas}",
					        		eliminarTareas: "${proyecto.eliminarTareas}",
					        		editarCompartir: "${proyecto.editarCompartir}",
					        		eliminarCompartir: "${proyecto.eliminarCompartir}",
					        		asignarTarea: "${proyecto.asignarTarea}",
					        		editarAsignar: "${proyecto.editarAsignar}",
					        		eliminarAsignar: "${proyecto.eliminarAsignar}"
					        	</c:if>
		             		}
		             	</c:forEach>
		             	];
		var ${esCompartidos}indices_proyectos = [];
		function ${esCompartidos}actualizar_indices_proyectos(){
			${esCompartidos}indices_proyectos = [];
			for(i=0;i<${esCompartidos}proyectos.length;i++)
				if(${esCompartidos}proyectos[i]!=null)
					${esCompartidos}indices_proyectos[${esCompartidos}proyectos[i].idProyecto] = i;
		}
		${esCompartidos}actualizar_indices_proyectos();
		
		function ${esCompartidos}actualizarTabProyectos(){
			projects = ${esCompartidos}proyectos;
			innerHtml = "";
			for(var h=0;h<projects.length;h++){
				project = projects[h];
				innerHtml += "<li><a href='${urlBase}/proyectos/"+project.idProyecto+".htm'>"+project.nombreProyecto+"</a></li>";
				if(h>=5) break;
			}
			if(projects.length>0){
			innerHtml += "<li><a href='${urlBase}/'>Ver m�s Proyectos...</a></li><li class='divider'></li>";
			}
			innerHtml += "<li><a href='${urlBase}/proyectos/crear.htm'>Crear Proyecto</a></li>"+
				"<li class='divider'></li>"+
				"<li><a href='${urlBase}/proyectos/compartidos.htm'>Ver Proyectos Compartidos</a></li>"+
				"<li><a href='${urlBase}/tareas/asignadas.htm'>Ver mis Tareas Asignadas</a></li>";
			document.getElementById("dropdown-proyectos").innerHTML = innerHtml;
		}
		
		function ${esCompartidos}crearOEditarProyecto(idProyectoP){
			nombreProyectoP = document.getElementById("${esCompartidos}nombreProyecto").value;
			descProyectoP = document.getElementById("${esCompartidos}descProyecto").value;
			prioridadP = document.getElementById("${esCompartidos}prioridad").value;
			if(nombreProyectoP=='' || prioridadP==''){
				alert('Debes rellenar el nombre y la prioridad del nuevo proyecto');
				return;
			}
			<c:if test="${grupoPerteneciente==null}">
				if(idProyectoP==0){
					idGrupoPertenecienteP = 0;
				} else {
					idGrupoPertenecienteP = ${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idProyectoP]].idGrupoperteneciente;
				}
			</c:if>
			<c:if test="${grupoPerteneciente!=null}">
				idGrupoPertenecienteP = ${grupoPerteneciente.idGrupo};
			</c:if>
			diaFin = document.getElementById("${esCompartidos}diaFin").value;
			mesFin = document.getElementById("${esCompartidos}mesFin").value;
			anoFin = document.getElementById("${esCompartidos}anoFin").value;
			if(anoFin=='' || mesFin=='' || diaFin==''){
				anoFin='0000';mesFin='00';diaFin='00';
			}
			horaFin = document.getElementById("${esCompartidos}horaFin").value;
			minFin = document.getElementById("${esCompartidos}minFin").value;
			if(nombreProyectoP=='')
				alert('Debes escribir un t�tulo para el proyecto');
			else {
				laFecha = '';
				if(diaFin!='0000' && mesFin!='00' && anoFin!='00'){
					if(horaFin=='' || minFin==''){
						horaFin = '00'; minFin = '00';
					}
					laFecha=anoFin+'-'+mesFin+'-'+diaFin+' '+horaFin+':'+minFin+':00';
				}
				if(crearProyectoAPartirDePlantilla==""){
					crearProyectoAPartirDePlantilla=0;
				}
				if(idProyectoP==0){
					$.postJSON("${raiz}.htm", {idProyecto:0,idCreador:0,idGrupoPerteneciente:idGrupoPertenecienteP,nombreProyecto:nombreProyectoP,descProyecto:descProyectoP,fechaFinString:laFecha,plantilla:crearProyectoAPartirDePlantilla,prioridad:prioridadP}, function(idProyecto) {
						<c:if test="${ordenProyectos==6 && editarProyecto && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}">
							if(confirm("Se ha creado un nuevo proyecto llamado "+nombreProyectoP+" �Quieres verlo en detalle?")){
								location.href = "${raiz}/"+idProyecto+'.htm';
							}
							location.href = '${raiz}.htm';
						</c:if>
						${esCompartidos}agregar_fila_proyectos(idProyecto, nombreProyectoP, descProyectoP, idGrupoPertenecienteP, anoFin, mesFin, diaFin, horaFin, minFin, prioridadP);
						if(crearProyectoAPartirDePlantilla==""){
							${esCompartidos}actionOcultarCrearProyecto();
						} else {
							${esCompartidos}actionMostrarAtras();
							${esCompartidos}actionMostrarProyectos();
						}
						${esCompartidos}actualizarTabProyectos();
						if(confirm("Se ha creado un nuevo proyecto llamado "+nombreProyectoP+" �Quieres verlo en detalle?")){
							location.href = "${raiz}/"+idProyecto+'.htm';
						}
		            }).fail(function(data){alert(data.responseText);});
				} else {
					elJSON = '{"idProyecto":0,"idCreador":0,"idGrupoPerteneciente":'+idGrupoPertenecienteP+',"nombreProyecto":"'+nombreProyectoP+'","descProyecto":"'+descProyectoP+'","fechaFinString":"'+laFecha+'","prioridad":'+prioridadP+'}';
					$.ajax({
						url: '${raiz}/'+idProyectoP+'.htm',
						data: elJSON,
						type: 'PUT',
						success: function(){
							<c:if test="${ordenProyectos==6 && editarProyecto && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}">
								if(confirm("Se ha editado el proyecto '"+nombreProyectoP+"' �Quieres verlo en detalle?")){
									location.href = "${raiz}/"+idProyectoP+'.htm';
								}
								location.href = '${raiz}.htm';
							</c:if>
							${esCompartidos}eliminarFilaProyecto(idProyectoP);
							${esCompartidos}agregar_fila_proyectos(idProyectoP, nombreProyectoP, descProyectoP, idGrupoPertenecienteP, anoFin, mesFin, diaFin, horaFin, minFin, prioridadP);
							${esCompartidos}actionOcultarCrearProyecto();
							${esCompartidos}actualizarTabProyectos();
							if(confirm("Se ha editado el proyecto '"+nombreProyectoP+"' �Quieres verlo en detalle?")){
								location.href = "${raiz}/"+idProyectoP+'.htm';
							}
						}
					}).fail(function(data){alert(data.responseText);})
					
				}
			}
		}
		function ${esCompartidos}eliminarProyectoConIdProyecto(idProyectoP){
			$.ajax({
			    url: '${raiz}/'+idProyectoP+'.htm',
			    type: 'DELETE',
			    success: function() {
					<c:if test="${ordenProyectos==6 && editarProyecto && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}">
						alert("Se ha eliminado el proyecto con id "+idProyectoP);
						location.href = '${raiz}.htm';
					</c:if>
					${esCompartidos}eliminarFilaProyecto(idProyectoP);
					${esCompartidos}actualizarTabProyectos();
					alert("Se ha eliminado el proyecto con id "+idProyectoP);
					}
			}).fail(function(data){alert(data.responseText);});
		}
		function ${esCompartidos}agregar_a_proyectos(idInterno,proyecto){
			for(i=proyectos.length;i>idInterno;i--){
				proyectos[i] = proyectos[i-1];
			}
			proyectos[idInterno] = proyecto;
		}
		function condicional_esta_es_la_fila_donde_debe_estar(pr,idProyecto,nombreProyecto,anoFin,mesFin,diaFin,horaFin,minFin,se_ordena_por){
			switch(se_ordena_por){
			case "0":
				return anoFin<pr.anoFin || anoFin==pr.anoFin && (mesFin<pr.mesFin || mesFin==pr.mesFin && (diaFin<pr.diaFin || diaFin==pr.diaFin &&
						(horaFin<pr.horaFin || horaFin==pr.horaFin && minFin<pr.minFin)));
			case "1":
				return anoFin>pr.anoFin || anoFin==pr.anoFin && (mesFin>pr.mesFin || mesFin==pr.mesFin && (diaFin>pr.diaFin || diaFin==pr.diaFin &&
						(horaFin>pr.horaFin || horaFin==pr.horaFin && minFin>pr.minFin)));
			case "2":
				return idProyecto<pr.idProyecto;
			case "3":
				return idProyecto>pr.idProyecto;
			case "4":
				return nombreProyecto<pr.nombreProyecto;
			default:
				return nombreProyecto>pr.nombreProyecto;
		}
		}
		function ${esCompartidos}hallar_fila_donde_debe_estar(idProyecto, nombreProyecto, anoFin, mesFin, diaFin, horaFin, minFin, se_ordena_por){
			i=0;
			while(i<proyectos.length){
				if(proyectos[i]!=null){
					pr = proyectos[i];
					if(condicional_esta_es_la_fila_donde_debe_estar(proyectos[i],idProyecto,nombreProyecto,anoFin,mesFin,diaFin,horaFin,minFin,se_ordena_por))
						return i;
				}
				i++;
			}
			return i;
		}
		function ${esCompartidos}agregar_fila_proyectos(idProy, nombreProy, descProy, idGrupoPert, ano, mes, dia, hora, min, pri){
			//hallar la posicion donde debe estar
			numFila = ${esCompartidos}hallar_fila_donde_debe_estar(idProy, nombreProy, ano, mes, dia, hora, min, ordenProyectos);
			proyecto = { idProyecto:idProy,
     		idCreador:${usuario.idUsuario},
     		idGrupoperteneciente:idGrupoPert,
     		nombreProyecto:nombreProy,
     		descProyecto:descProy,
     		diaFin:dia,
     		mesFin:mes,
     		anoFin:ano,
     		horaFin:hora,
     		minFin:min,
     		prioridad:pri,
     		compartidosA:[],
     		editarProyecto: true,
    		eliminarProyecto: true,
    		compartir: true,
    		crearTareas: true,
    		editarTareas: true,
    		eliminarTareas: true,
    		editarCompartir: true,
    		eliminarCompartir: true,
    		asignarTarea: true,
    		editarAsignar: true,
    		eliminarAsignar: true
     		};
			${esCompartidos}agregar_a_proyectos(numFila,proyecto);
			${esCompartidos}actualizar_indices_proyectos();
			
			// Creamos el nuevo párrafo
		    var nueva_fila = document.createElement('tr');
		    nueva_fila.setAttribute("id","${esCompartidos}proyecto"+idProy)
			innerHTMLNuevaFila = '<td><a href="${raiz}/'+idProy+'.htm">'+nombreProy+'</a></td>';
			innerHTMLNuevaFila += '<td><div class="progress">'+
			  '<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="'+pri+'0" aria-valuemin="0" '+
			  'aria-valuemax="100" style="width: '+pri+'0%">'+
			    pri+
			  '</div>'+
			'</div></td>';
			if(ano=="0000")
				innerHTMLNuevaFila += '<td></td>';
			else
				innerHTMLNuevaFila += '<td>'+dia+'/'+mes+'/'+ano+' '+hora+':'+min+'</td>';
			innerHTMLNuevaFila += '<td>0 horas</td><td><div class="progress"><div class="progress-bar progress-bar-info" style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="0" role="progressbar"> 0% </div></div></td><td>0.0 horas</td>';	
			
			<c:choose>
			<c:when test="${permisosGrupo==null}">
				innerHTMLNuevaFila += '<td><button class="btn btn-info" onclick="${esCompartidos}actionMostrarEditarProyecto('+idProy+')"><span class="glyphicon glyphicon-edit"></span>';
				innerHTMLNuevaFila += '<button class="btn btn-info" onclick="${esCompartidos}eliminarProyecto('+idProy+')"><span class="glyphicon glyphicon-trash"></span></button>';
				innerHTMLNuevaFila += '<button class="btn btn-info" onclick="${esCompartidos}compartirProyecto('+idProy+')"><i class="glyphicon glyphicon-share"></i></button></td>';
			</c:when>
			<c:otherwise>
				<c:if test="${puedeEditarProyecto || puedeBorrarProyecto}">
					innerHTMLNuevaFila += '<td>';
					<c:if test="${puedeEditarProyecto}">
						innerHTMLNuevaFila += '<button class="btn btn-info" onclick="${esCompartidos}actionMostrarEditarProyecto('+idProy+')"><span class="glyphicon glyphicon-edit"></span>';
					</c:if>
					<c:if test="${puedeBorrarProyecto}">
						innerHTMLNuevaFila += '<button class="btn btn-info" onclick="${esCompartidos}eliminarProyecto('+idProy+')"><span class="glyphicon glyphicon-trash"></span></button>';
					</c:if>
					innerHTMLNuevaFila += '</td>';
				</c:if>
			</c:otherwise>
			</c:choose>
			nueva_fila.innerHTML = innerHTMLNuevaFila;
		    
			tableProyectos = document.getElementById('${esCompartidos}tbodyProyectos');
		    var fila_actual = tableProyectos.getElementsByTagName('tr')[numFila];
		    if(fila_actual){
		    	// Y ahora lo insertamos
			    tableProyectos.insertBefore(nueva_fila,fila_actual);
		    } else {
		    	tableProyectos.appendChild(nueva_fila);
		    }
		    document.getElementById("${esCompartidos}tableProyectos").style = "float:center;";
		    document.getElementById("${esCompartidos}proyectosP").style = "display:none;";
		    
		}
		function ${esCompartidos}eliminar_fila(idProyecto){
			filaProyecto = document.getElementById("${esCompartidos}proyecto"+idProyecto);
			if(filaProyecto){
				tabla = filaProyecto.parentNode;
				tabla.removeChild(filaProyecto);
			}
		}
		function ${esCompartidos}eliminarFilaProyecto(idProyecto){
			idInterno = ${esCompartidos}indices_proyectos[idProyecto];
			//eliminar del array proyectos
			${esCompartidos}proyectos = ${esCompartidos}proyectos.slice(0,idInterno).concat(${esCompartidos}proyectos.slice(idInterno+1,${esCompartidos}proyectos.length+1));
			${esCompartidos}actualizar_indices_proyectos();
			//eliminar fila de proyectos
			${esCompartidos}eliminar_fila(idProyecto);
		}
		function ${esCompartidos}eliminarProyecto(idProyecto){
			if(confirm("�Est�s seguro de querer eliminar el proyecto?")){
				idInterno = ${esCompartidos}indices_proyectos[idProyecto];
				${esCompartidos}eliminarProyectoConIdProyecto(${esCompartidos}proyectos[idInterno].idProyecto);
			}
		}
		function ${esCompartidos}convertirSubmitEnCrearProyecto(){
			document.getElementById("${esCompartidos}submitCrearProyecto").innerHTML = "Crear Proyecto";
			document.getElementById("${esCompartidos}submitCrearProyecto").setAttribute("href","javascript:${esCompartidos}crearOEditarProyecto('');");
			document.getElementById("${esCompartidos}legendaryProyectos").innerHTML = 'CREAR NUEVO PROYECTO';
			document.getElementById("${esCompartidos}nombreProyecto").value = "";
			document.getElementById("${esCompartidos}descProyecto").value = "";
			document.getElementById("${esCompartidos}prioridad").value = "";
			${esCompartidos}resetear_los_cinco();
		}
	
		function ${esCompartidos}convertirSubmitEnEditarProyecto(proyecto){
			document.getElementById("${esCompartidos}submitCrearProyecto").innerHTML = "Editar Proyecto";
			document.getElementById("${esCompartidos}submitCrearProyecto").setAttribute("href","javascript:${esCompartidos}crearOEditarProyecto("+proyecto.idProyecto+")");
			document.getElementById("${esCompartidos}legendaryProyectos").innerHTML = 'EDITAR PROYECTO '+proyecto.nombreProyecto+" CON ID "+proyecto.idProyecto;
			document.getElementById("${esCompartidos}nombreProyecto").value = proyecto.nombreProyecto;
			document.getElementById("${esCompartidos}descProyecto").value = proyecto.descProyecto;
			document.getElementById("${esCompartidos}prioridad").value = proyecto.prioridad;
			if(proyecto.anoFin!=0){
				${esCompartidos}actualizar_ano(proyecto.anoFin);
				${esCompartidos}actualizar_mes(proyecto.mesFin);
				${esCompartidos}actualizar_dia_segun_ano_y_mes(proyecto.diaFin, proyecto.mesFin, proyecto.anoFin);
				${esCompartidos}actualizar_hora(proyecto.horaFin);
				${esCompartidos}actualizar_min(proyecto.minFin);
			} else {
				${esCompartidos}resetear_los_cinco();
			}
		}
	
		function ${esCompartidos}mostrarCrearProyecto() {
			fcp = document.getElementById("${esCompartidos}formCrearProyecto");
			if(fcp)fcp.setAttribute("style", "");
			fcp = document.getElementById("${esCompartidos}buttonMostrarCrearProyecto");
			if(fcp)fcp.setAttribute("style", "display: none;");
			fcp = document.getElementById("${esCompartidos}buttonOcultarCrearProyecto");
			if(fcp)fcp.setAttribute("style", "");
			${esCompartidos}convertirSubmitEnCrearProyecto();
		}
	
		function ${esCompartidos}ocultarCrearProyecto() {
			fcp = document.getElementById("${esCompartidos}formCrearProyecto");
			if(fcp)fcp.setAttribute("style", "display: none;");
			fcp = document.getElementById("${esCompartidos}buttonMostrarCrearProyecto");
			if(fcp)fcp.setAttribute("style", "");
			fcp = document.getElementById("${esCompartidos}buttonOcultarCrearProyecto");
			if(fcp)fcp.setAttribute("style", "display: none;");
			${esCompartidos}convertirSubmitEnCrearProyecto();
		}
	
		function ${esCompartidos}editarProyecto(idInterno){
			proyecto = ${esCompartidos}proyectos[idInterno];
			${esCompartidos}mostrarCrearProyecto();
			${esCompartidos}convertirSubmitEnEditarProyecto(proyecto);
		}
	
		function ${esCompartidos}actionMostrarCrearProyecto() {
			//document.getElementById("parrafoMensajeCrearProyecto").innerHTML = '';
			${esCompartidos}mostrarCrearProyecto();
		}
	
		function ${esCompartidos}actionOcultarCrearProyecto() {
			//document.getElementById("parrafoMensajeCrearProyecto").innerHTML = '';
			${esCompartidos}ocultarCrearProyecto();
		}
	
		function ${esCompartidos}actionMostrarEditarProyecto(idProyecto) {
			idInterno = ${esCompartidos}indices_proyectos[idProyecto];
			//document.getElementById("parrafoMensajeCrearProyecto").innerHTML = '';
			location.href='#formCrearProyecto';
			${esCompartidos}editarProyecto(idInterno);
		}
		function ${esCompartidos}submitCrearProyecto() {
			if(document.getElementById("${esCompartidos}nombreProyecto").value=="" ||
				document.getElementById("${esCompartidos}dia-fin").value==0 ||
				document.getElementById("${esCompartidos}mes-fin").value==0 ||
				document.getElementById("${esCompartidos}ano-fin").value==0) {
				alert("Debes poner el nombre del proyecto y la fecha prevista de fin");
			} else {
				document.getElementById("${esCompartidos}formCreacionProyecto").submit();
			}
		}
		<c:if test="${esAGrupos!=true || puedeEditarProyecto}">
		function ${esCompartidos}reordenar_por(valor){
			$.ajax({
				<c:if test="${esAGrupos==true}">
					url: "${urlBase}/grupos/${grupoPerteneciente.idGrupo}/proyectos/orden.htm",
				</c:if>
				<c:if test="${esAGrupos!=true}">
					url: "${urlBase}/proyectos/orden.htm",
				</c:if>
				data: ''+valor,
				type: 'PUT',
				success: function() {
					location.reload();
					//TODO
				}
			}).fail(function(data){alert(data.responseText);})
		}
		</c:if>
		
		function ${esCompartidos}ya_esta_compartido(login,compartidosA){
			for(var k=0;k<compartidosA.length;k++){
				if(compartidosA[k].login==login)
					return true;
			}
			return false;
		}
		
		function ${esCompartidos}make_compartido(log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela){
			return {login:log, crearTareas:crt
				, editarTareas:edt, eliminarTareas:elt
				, editarProyecto:edp, eliminarProyecto:elp
				, compartir:com, editarCompartir:edc
				, eliminarCompartir:elc, asignarTarea:ast
				, editarAsignar:eda, eliminarAsignar:ela};
		}
		
		function ${esCompartidos}visualizar_compartir_proyecto(idp,log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela){
			compartido = ${esCompartidos}make_compartido(log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela);
			${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].compartidosA.push(compartido);
		}
		
		function ${esCompartidos}visualizar_editar_compartir(idp,log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela){
			intEditar = 0;
			compartidosA = ${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].compartidosA;
			for(var g=0;g<compartidosA.length && intEditar==0;g++){
				if(compartidosA[g].login = log){
					intEditar = g;
				}
			}
			if(intEditar==0){
				compartido = ${esCompartidos}make_compartido(log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela);
				${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].compartidosA[intEditar] = compartido;
			}
		}
		
		function ${esCompartidos}visualizar_eliminar_compartir(idp,log){
			intEliminar = 0;
			compartidosA = ${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].compartidosA;
			for(var g=0;g<compartidosA.length && intEditar==0;g++){
				if(compartidosA[g].login = log){
					intEliminar = g;
				}
			}
			if(intEliminar==0){
				${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].compartidosA = eliminar_de_array(compartidosA,intEliminar);
			}
		}
		
		function ${esCompartidos}compartir_proyecto(idp) {
			log =document.getElementById("loginCompartir").value;
			if(log==''){alert('Debes elegir a quien quieres compartir el proyecto');return;}
			aux =document.getElementById("${esCompartidos}crearTareas"); if(aux){crt = aux.checked;} else crt = false;
			aux =document.getElementById("${esCompartidos}editarTareas"); if(aux){edt = aux.checked;} else edt = false;
			aux =document.getElementById("${esCompartidos}eliminarTareas"); if(aux){elt = aux.checked;} else elt = false;
			aux =document.getElementById("${esCompartidos}editarProyecto"); if(aux){edp = aux.checked;} else edp = false;
			aux =document.getElementById("${esCompartidos}eliminarProyecto"); if(aux){elp = aux.checked;} else elp = false;
			aux =document.getElementById("${esCompartidos}compartir"); if(aux){com = aux.checked;} else com = false;
			aux =document.getElementById("${esCompartidos}editarCompartir"); if(aux){edc = aux.checked;} else edc = false;
			aux =document.getElementById("${esCompartidos}eliminarCompartir"); if(aux){elc = aux.checked;} else elc = false;
			aux =document.getElementById("${esCompartidos}asignarTarea"); if(aux){ast = aux.checked;} else ast = false;
			aux =document.getElementById("${esCompartidos}editarAsignar"); if(aux){eda = aux.checked;} else eda = false;
			aux =document.getElementById("${esCompartidos}eliminarAsignar"); if(aux){ela = aux.checked;} else ela = false;
			
			$.postJSON("${raiz}/compartir/"+idp+".htm", {idProyecto:idp,crearTareas:crt,editarTareas:edt,eliminarTareas:elt,
				editarProyecto:edp,eliminarProyecto:elp,compartir:com,editarCompartir:edc,eliminarCompartir:elc,
				asignarTarea:ast,editarAsignar:eda,eliminarAsignar:ela,loginMiembro:log}, function(idProyecto) {
					hideLightbox();
					${esCompartidos}visualizar_compartir_proyecto(idp,log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela);
					alert("Proyecto "+${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].nombreProyecto+" compartido con "+log);
            }).fail(function(data){alert(data.responseText);});
		}
		             			
		function ${esCompartidos}editar_compartir(idp,log){
			aux =document.getElementById("${esCompartidos}crearTareas-"+idp+'-'+log); if(aux){crt = aux.value=='1';} else crt = false;
			aux =document.getElementById("${esCompartidos}editarTareas-"+idp+'-'+log); if(aux){edt = aux.value=='1';} else edt = false;
			aux =document.getElementById("${esCompartidos}eliminarTareas-"+idp+'-'+log); if(aux){elt = aux.value=='1';} else elt = false;
			aux =document.getElementById("${esCompartidos}editarProyecto-"+idp+'-'+log); if(aux){edp = aux.value=='1';} else edp = false;
			aux =document.getElementById("${esCompartidos}eliminarProyecto-"+idp+'-'+log); if(aux){elp = aux.value=='1';} else elp = false;
			aux =document.getElementById("${esCompartidos}compartir-"+idp+'-'+log); if(aux){com = aux.value=='1';} else com = false;
			aux =document.getElementById("${esCompartidos}editarCompartir-"+idp+'-'+log); if(aux){edc = aux.value=='1';} else edc = false;
			aux =document.getElementById("${esCompartidos}eliminarCompartir-"+idp+'-'+log); if(aux){elc = aux.value=='1';} else elc = false;
			aux =document.getElementById("${esCompartidos}asignarTarea-"+idp+'-'+log); if(aux){ast = aux.value=='1';} else ast = false;
			aux =document.getElementById("${esCompartidos}editarAsignar-"+idp+'-'+log); if(aux){eda = aux.value=='1';} else eda = false;
			aux =document.getElementById("${esCompartidos}eliminarAsignar-"+idp+'-'+log); if(aux){ela = aux.value=='1';} else ela = false;
			elJSON = '{"idProyecto":'+idp+',"crearTareas":'+crt+',"editarTareas":'+edt+',"eliminarTareas":'+elt+','+
					'"editarProyecto":'+edp+',"eliminarProyecto":'+elp+',"compartir":'+com+',"editarCompartir":'+edc+',"eliminarCompartir":'+elc+
					',"asignarTarea":'+ast+',"editarAsignar":'+eda+',"eliminarAsignar":'+ela+',"loginMiembro":"'+log+'"}';
			$.ajax({
			    url: '${raiz}/compartir/'+idp+'.htm',
			    type: 'PUT',
			    data: elJSON,
			    success: function() {
					hideLightbox();
					${esCompartidos}visualizar_editar_compartir(idp,log,crt,edt,elt,edp,elp,com,edc,elc,ast,eda,ela);
					alert("Se han editado los permisos que el usuario "+log+" tiene sobre el proyecto "+${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idp]].nombreProyecto);
			    }
			}).fail(function(data){alert(data.responseText);});
		}
		
		function ${esCompartidos}eliminar_compartir(idProyecto,login){
			$.ajax({
			    url: '${raiz}/compartir/'+idProyecto+'.htm',
			    type: 'DELETE',
			    data: login,
			    success: function() {
			    	hideLightbox();
			    	${esCompartidos}visualizar_eliminar_compartir(idProyecto,log);
					alert("El proyecto "+${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idProyecto]].nombreProyecto+" no est� compartido con el usuario "+log);
			    }
			}).fail(function(data){alert(data.responseText);});
		}
		
		function get_texto_select(id,es_si){
			retorno = "<td><select id='"+id+"'>";
			if(es_si=='true'){
				retorno += "<option value='0'>NO</option>";
				retorno += "<option value='1' selected>S�</option>";
			} else {
				retorno += "<option value='0' selected>NO</option>";
				retorno += "<option value='1'>S�</option>";
			}
			return retorno+"</select></td>";
		}
		
		function ${esCompartidos}compartirProyecto(idProyecto) {
			proyecto = ${esCompartidos}proyectos[${esCompartidos}indices_proyectos[idProyecto]];
			compartidosA = proyecto.compartidosA;
			innerHtml = "";
			if(proyecto.compartir || "${puedeCompartirProyecto}"=="true"){
				innerHtml += "<select id='loginCompartir'>";
				innerHtml += "<option value=''></option>";
				innerOptions = "";
				for(var j=0;j<noCompartidosCon.length;j++){
					if(!ya_esta_compartido(noCompartidosCon[j],compartidosA))
						innerOptions += "<option value='"+noCompartidosCon[j]+"'>"+noCompartidosCon[j]+"</option>";
				}
				if(innerOptions==""){
					innerHtml = "No hay personas a quien compartir";
				} else {
					innerHtml += innerOptions+"</select><br>";
				}
				if(proyecto.crearTareas) innerHtml += "<input type='checkbox' id='${esCompartidos}crearTareas'/>CREAR TAREAS ";
				if(proyecto.editarTareas) innerHtml += "<input type='checkbox' id='${esCompartidos}editarTareas'/>EDITAR TAREAS ";
				if(proyecto.eliminarTareas) innerHtml += "<input type='checkbox' id='${esCompartidos}eliminarTareas'/>ELIMINAR TAREAS ";
				if(proyecto.editarProyecto) innerHtml += "<input type='checkbox' id='${esCompartidos}editarProyecto'/>EDITAR PROYECTO<br>";
				if(proyecto.compartir) innerHtml += "<input type='checkbox' id='${esCompartidos}compartir'/>COMPARTIR ";
				if(proyecto.eliminarProyecto) innerHtml += "<input type='checkbox' id='${esCompartidos}eliminarProyecto'/>ELIMINAR PROYECTO ";
				if(proyecto.editarCompartir) innerHtml += "<input type='checkbox' id='${esCompartidos}editarCompartir'/>EDITAR COMPARTIR ";
				if(proyecto.eliminarCompartir) innerHtml += "<input type='checkbox' id='${esCompartidos}eliminarCompartir'/>ELIMINAR COMPARTIR<br>";
				if(proyecto.asignarTarea) innerHtml += "<input type='checkbox' id='${esCompartidos}asignarTarea'/>ASIGNAR TAREA";
				if(proyecto.editarAsignar) innerHtml += "<input type='checkbox' id='${esCompartidos}editarAsignar'/>EDITAR ASIGNACION";
				if(proyecto.eliminarAsignar) innerHtml += "<input type='checkbox' id='${esCompartidos}eliminarAsignar'/>ELIMINAR ASIGNACION<br>";
				innerHtml += "<button class='btn btn-info' onclick='${esCompartidos}compartir_proyecto("+idProyecto+")'>Compartir</button>";
			}
			if(compartidosA.length>0){
				innerHtml += "<br>COMPARTIDO CON:";
				innerHtml += "<table class='table table-striped' id='compartirTable'><thead><tr><th rowspan='2'>Login</th><th colspan='11' style='text-align:center'>puede:</th><th rowspan='2'>Acciones</th></tr>";
				innerHtml += "<tr><th>Crear tareas</th><th>Editar tareas</th><th>Eliminar tareas</th><th>Editar proyecto</th><th>Compartir</th><th>Eliminar proyecto</th><th>Editar compartir</th>"+
				"<th>Eliminar compartir</th><th>Asignar tarea</th><th>Editar asignaci�n</th><th>Eliminar asignaci�n</th></tr></thead>";
				innerHtml += "<tbody id='compartirTBody'>";
				if(proyecto.editarCompartir)
					for(var i=0;i<compartidosA.length;i++){
						innerHtml += "<tr><td>"+compartidosA[i].login+"</td>";
						innerHtml += get_texto_select('${esCompartidos}crearTareas-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].crearTareas);
						innerHtml += get_texto_select('${esCompartidos}editarTareas-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].editarTareas);
						innerHtml += get_texto_select('${esCompartidos}eliminarTareas-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].eliminarTareas);
						innerHtml += get_texto_select('${esCompartidos}editarProyecto-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].editarProyecto);
						innerHtml += get_texto_select('${esCompartidos}compartir-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].compartir);
						innerHtml += get_texto_select('${esCompartidos}eliminarProyecto-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].eliminarProyecto);
						innerHtml += get_texto_select('${esCompartidos}editarCompartir-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].editarCompartir);
						innerHtml += get_texto_select('${esCompartidos}eliminarCompartir-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].eliminarCompartir);
						innerHtml += get_texto_select('${esCompartidos}asignarTarea-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].asignarTarea);
						innerHtml += get_texto_select('${esCompartidos}editarAsignar-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].editarAsignar);
						innerHtml += get_texto_select('${esCompartidos}eliminarAsignar-'+idProyecto+'-'+compartidosA[i].login,compartidosA[i].eliminarAsignar);
						innerHtml += "<td><button class='btn btn-info' onclick='${esCompartidos}editar_compartir("+idProyecto+",\""+compartidosA[i].login+"\")'><i class='glyphicon glyphicon-edit'></i></button>";
						if(proyecto.eliminarCompartir)
							innerHtml += "<button class='btn btn-info' onclick='${esCompartidos}eliminar_compartir("+idProyecto+",\""+compartidosA[i].login+"\")'><i class='glyphicon glyphicon-trash'></i></button>";
						innerHtml += "</td></tr>";
					}
				else
					for(var i=0;i<compartidosA.length;i++){
						innerHtml += "<tr><td>"+compartidosA[i].login+"</td>";
						if(compartidosA[i].crearTareas=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].editarTareas=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].eliminarTareas=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].editarProyecto=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].compartir=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].eliminarProyecto=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].editarCompartir=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].eliminarCompartir=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].asignarTarea=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].editarAsignar=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(compartidosA[i].eliminarAsignar=="true") innerHtml += "<td>S�</td>"; else innerHtml += "<td>NO</td>";
						if(proyecto.eliminarCompartir)
							innerHtml += "<td><button class='btn btn-info' onclick='${esCompartidos}eliminar_compartir("+idProyecto+",'"+compartidosA[i].login+"')'><i class='glyphicon glyphicon-trash'></i></button></td></tr>";
						else
							innerHtml += "<td>-</td></tr>";
					}
				innerHtml += "</tbody>";
				innerHtml += "</table>";
				
			} else {
				innerHtml += "<br><br>No hay usuarios a los que se les haya compartido este proyecto.";
			}
			showLightbox(innerHtml);
		}
		
		<c:if test="${ordenProyectos==6 && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}">
			function ${esCompartidos}cambiarProyecto(idProyectoA, idProyectoB, esSubida){
				$.ajax({
				    url: '${urlBase}/proyectos/cambiar-orden.htm',
				    type: 'PUT',
				    data: '{"idProyectoA":'+idProyectoA+',"idProyectoB":'+idProyectoB+'}',
				    success: function() {
				    	location.href= '${urlReload}';
				    }
				}).fail(function(data){alert(data.responseText);});
			}
			
		</c:if>
	</script>

	<c:if test="${puedeCrearProyecto==true}">
		<button class="btn btn-info" id="${esCompartidos}buttonMostrarCrearProyecto" onclick="${esCompartidos}actionMostrarCrearProyecto()">Crear Proyecto</button>
	</c:if>
	<c:if test="${esCompartidos=='compartido' || puedeCrearProyecto==true || puedeEditarProyecto==true}">
		<button class="btn btn-info" id="${esCompartidos}buttonOcultarCrearProyecto" onclick="${esCompartidos}actionOcultarCrearProyecto()" style='display:none;'>Ocultar Creaci�n de Proyecto</button>
	</c:if>
	<c:if test="${esCompartidos!='plantillas' && puedeCrearProyecto==true}">
		<button class="btn btn-info" id="${esCompartidos}buttonMostrarCrearPorPlantilla" onclick="actionMostrarCreacionPorPlantilla()" >Mostrar creaci�n por plantillas >></button>
	</c:if>
	<c:if test="${esAGrupos!=true || puedeEditarProyecto}">
		<select onchange="${esCompartidos}reordenar_por(this.value)">
			<option value="0"<c:if test="${ordenProyectos==0}">selected</c:if>>Fecha ascendente</option>
			<option value="1"<c:if test="${ordenProyectos==1}">selected</c:if>>Fecha descendente</option>
			<option value="7"<c:if test="${ordenProyectos==7}">selected</c:if>>Prioridad Ascendente</option>
			<option value="8"<c:if test="${ordenProyectos==8}">selected</c:if>>Prioridad Descendente</option>
			<option value="2"<c:if test="${ordenProyectos==2}">selected</c:if>>Por creaci�n ascendente</option>
			<option value="3"<c:if test="${ordenProyectos==3}">selected</c:if>>Por creaci�n descendente</option>
			<option value="4"<c:if test="${ordenProyectos==4}">selected</c:if>>Alfab�ticamente</option>
			<option value="5"<c:if test="${ordenProyectos==5}">selected</c:if>>Alfab�ticamente descendente</option>
			<option value="6"<c:if test="${ordenProyectos==6}">selected</c:if>>
				<c:if test="${esCompartidos=='plantillas'}">Por creaci�n ascendente</c:if>
				<c:if test="${esCompartidos!='plantillas'}">Personalizado</c:if>
			</option>
		</select>
	</c:if>
	<c:if test="${esCompartidos=='compartido' || puedeCrearProyecto==true || puedeEditarProyecto==true}">
		<div id='${esCompartidos}formCrearProyecto' style='display:none;'>
		  <br>
		  <fieldset style='text-align:center;'>
		    <legend id='${esCompartidos}legendaryProyectos'>CREAR NUEVO PROYECTO</legend>
		      T�tulo:<input type='text' id='${esCompartidos}nombreProyecto' name='nombreProyecto' /><br>
		      Descripci�n:<input type="text" id='${esCompartidos}descProyecto' name='descProyecto' /><br>
		      Prioridad:<input type="number" min='0' max='10' id='${esCompartidos}prioridad' name='prioridad' /><br>
		      <%@include file="/includes/fechaFin.jsp" %>
		      <br>
		    <button class='btn btn-info' id='${esCompartidos}submitCrearProyecto' onclick="${esCompartidos}crearOEditarProyecto('');">Crear Proyecto</button>
		  </fieldset>
		</div>
	</c:if>
	<div id ="${esCompartidos}elDivDeProyectos" style="text-align:center;">
	<c:choose>
	<c:when test="${proyectos!=null && proyectos.size()>0 }">
		<div class="col-lg-offset-1 col-lg-10 col-md-12 col-sm-12 col-xs-12 table-responsive">
		<table id="${esCompartidos}tableProyectos" class='table table-striped'>
			<thead>
			<tr>
				<th>Nombre del proyecto</th>
				<th>Prioridad</th>
				<th>Fecha final ideal</th>
				<th>Duracion</th>
				<th>Completado</th>
				<th>Quedan</th>
				<c:if test="${esCompartidos=='compartido' || puedeEditarProyecto || puedeBorrarProyecto}"><th <c:if test="${ordenProyectos=='6' && editarProyecto && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}"> colspan='3'</c:if>>Acciones</th></c:if>
			</tr>
			</thead>
			<tbody id="${esCompartidos}tbodyProyectos">
			<c:if test="${esCompartidos!='compartido'}">
				<c:set var="editarProyecto" value="${puedeEditarProyecto}" />
				<c:set var="eliminarProyecto" value="${puedeEliminarProyecto}" />
				<c:set var="compartirProyecto" value="${puedeCompartirProyecto}" />
			</c:if>
			<c:set var="inc" value="0" />
			<c:forEach items="${proyectos}" var="proyecto">
				<c:if test="${esCompartidos=='compartido'}">
					<c:set var="editarProyecto" value="${proyecto.editarProyecto}" />
					<c:set var="eliminarProyecto" value="${proyecto.eliminarProyecto}" />
					<c:set var="compartirProyecto" value="${proyecto.compartir}" />
				</c:if>
				<tr id="${esCompartidos}proyecto${proyecto.idProyecto}">
					<td><a href="${raiz}/${proyecto.idProyecto}.htm">${proyecto.nombreProyecto}</a></td>
					<td><div class="progress">
						  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="${proyecto.prioridad}0" aria-valuemin="0" 
						  aria-valuemax="100" style="width: ${proyecto.prioridad}0%">
						    ${proyecto.prioridad}
						  </div>
					</div></td>
					<td>${proyecto.fechaFinFormateada}</td>
					<td>${proyecto.duracion} horas</td>
					<td>
						<div class="progress">
						  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="${proyecto.completado}" aria-valuemin="0" 
						  aria-valuemax="100" style="width: ${proyecto.completado}%">
						    ${proyecto.completado}%
						  </div>
						</div>
					</td>
					<td>${((100-proyecto.completado)*proyecto.duracion)/100} horas</td>
					<c:if test="${editarProyecto || eliminarProyecto || compartirProyecto}">
						<td>
						<c:if test="${editarProyecto}">
							<button class="btn btn-info" onclick="${esCompartidos}actionMostrarEditarProyecto(${proyecto.idProyecto})"><span class="glyphicon glyphicon-edit"></span></button>
						</c:if>
						<c:if test="${eliminarProyecto}">
							<button class="btn btn-info" onclick="${esCompartidos}eliminarProyecto(${proyecto.idProyecto})"><i class="glyphicon glyphicon-trash"></i></button>
						</c:if>
						<c:if test="${compartirProyecto && esCompartidos!='plantillas'}">
							<button class="btn btn-info" onclick="${esCompartidos}compartirProyecto(${proyecto.idProyecto})"><i class="glyphicon glyphicon-share"></i></button>
						</c:if>
						<c:if test="${ordenProyectos==6 && editarProyecto && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}">
							<td id='${esCompartidos}subir-proyecto-${proyecto.idProyecto}'><c:if test="${inc>0}">
							<button class="btn btn-info" onclick="${esCompartidos}cambiarProyecto(${proyecto.idProyecto},${proyectos.get(inc-1).idProyecto},true)"><i class="glyphicon glyphicon-open"></i></button>
							</c:if></td>
							<td id='${esCompartidos}bajar-proyecto-${proyecto.idProyecto}'><c:if test="${inc<(proyectos.size()-1)}">
							<button class="btn btn-info" onclick="${esCompartidos}cambiarProyecto(${proyecto.idProyecto},${proyectos.get(inc+1).idProyecto},false)"><i class="glyphicon glyphicon-save"></i></button>
							</c:if></td>
						</c:if>
						</td>
					</c:if>
				</tr>
				<c:set var="inc" value="${inc+1}" />
			</c:forEach>
			</tbody>
		</table>
		</div>
		<p id='${esCompartidos}proyectosP' style="text-align:center;"></p>
	</c:when>
	<c:otherwise>
		<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
			<table id="${esCompartidos}tableProyectos" style='display:none;' class="table">
				<thead>
				<tr>
					<th>Nombre del proyecto</th>
					<th>Prioridad</th>
					<th>Fecha final ideal</th>
					<th>Duracion</th>
					<th>Completado</th>
					<th>Quedan</th>
					<c:if test="${esCompartidos=='compartido' || puedeEditarProyecto || puedeBorrarProyecto}"><th <c:if test="${ordenProyectos=='6' && editarProyecto && esCompartidos!='todosLosProyectos' && esCompartidos!='plantillas'}"> colspan='3'</c:if>>Acciones</th></c:if>
				</tr>
				</thead>
				<tbody id="${esCompartidos}tbodyProyectos">
				</tbody>
			</table>
		</div>
		<br>
		<c:if test="${puedeCrearProyecto}" >
			<p id='${esCompartidos}proyectosP' class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style="text-align:center;">No tienes Proyectos, <a href="javascript:${esCompartidos}actionMostrarCrearProyecto();">puedes crear uno</a>.</p>
		</c:if>
		<c:if test="${puedeCrearProyecto==false}" >
			<p id='${esCompartidos}proyectosP' class='col-lg-12 col-md-12 col-sm-12 col-xs-12' style="text-align:center;">No hay proyectos.</p>
		</c:if>
	</c:otherwise>
	</c:choose>
	</div>
<c:if test="${crear==true}"><script>actionMostrarCrearProyecto();</script></c:if>