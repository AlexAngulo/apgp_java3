<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>

var plantillas = [
	             	<c:set var="b" value="${true}"/>
	             	<c:forEach items="${plantillas}" var="plantilla" >
		             	<c:if test="${b==false}">
	             			,{
	             		</c:if>
	             		<c:if test="${b==true}">
	             			{
	             			<c:set var="b" value="${false}"/>
	             		</c:if>
	             		idProyecto:"${plantilla.idProyecto}",
	             		nombreProyecto:"${plantilla.nombreProyecto}"
				          }
				   </c:forEach>
				   ]
				   
var indices_plantillas = [];
for(i=0;i<plantillas.length;i++)
	if(plantillas[i]!=null)
		indices_plantillas[plantillas[i].idProyecto] = i;
		
function verPlantilla(idProyecto){
	for(i=0;i<plantillas.length;i++){
		document.getElementById("plantilla"+plantillas[i].idProyecto).style="display:none;";
	}
	document.getElementById("plantilla"+idProyecto).style = "";
}
function actionMostrarSiguiente(){
	idProyecto = document.getElementById("plantillaAAplicar").value;
	crearProyectoAPartirDePlantilla = idProyecto;
	document.getElementById("divDePlantillas").style="display:none;";
	document.getElementById("buttonMostrarCrearPorPlantilla").style = "display:none;";
	document.getElementById("formCrearProyecto").style = "";
	botonAtras = document.getElementById("buttonMostrarCrearProyecto");
	botonAtras.innerHTML = "<< Volver atr�s";
	botonAtras.setAttribute("onclick","actionMostrarAtras()");
	document.getElementById("legendaryProyectos").innerHTML = "CREAR PROYECTO A PARTIR DE LA PLANTILLA " + 
		plantillas[indices_plantillas[idProyecto]].nombreProyecto;
	document.getElementById("elDivDeProyectos").style="display:none;";
	document.getElementById("divDeProyectos").style="";
}
function actionMostrarAtras(){
	crearProyectoAPartirDePlantilla = "";
	document.getElementById("divDePlantillas").style="";
	document.getElementById("buttonMostrarCrearPorPlantilla").style = "";
	document.getElementById("formCrearProyecto").style = "display:none;";
	botonAtras = document.getElementById("buttonMostrarCrearProyecto");
	botonAtras.innerHTML = "Crear proyecto";
	botonAtras.setAttribute("onclick","actionMostrarCrearProyecto()");
	document.getElementById("legendaryProyectos").innerHTML = "CREAR NUEVO PROYECTO";
	document.getElementById("elDivDeProyectos").style="";
	document.getElementById("divDeProyectos").style="display:none;";
}
</script>
<button class="btn btn-info" onclick="actionMostrarProyectos()"> << Mostrar Proyectos</button><br>
�Qu� plantilla querr�as aplicar?<br>
<select id="plantillaAAplicar" onchange="verPlantilla(this.value)">
	<c:forEach items="${plantillas}" var="plantilla">
		<option value="${plantilla.idProyecto}">${plantilla.nombreProyecto}</option>
	</c:forEach>
</select>
<button class="btn btn-info" onclick="actionMostrarSiguiente()"> Crear Proyecto >></button><br>
<div id="divPlantillas">
	<c:set var="b" value="${true}"/>
  	<c:forEach items="${plantillas}" var="plantilla" >
   		<div id="plantilla${plantilla.idProyecto}"
   		<c:if test="${b==false}">
  			style="display:none;"
  		</c:if>
  		<c:if test="${b==true}">
  			<c:set var="b" value="${false}"/>
  		</c:if>
			>
			<div style="text-align:center;">
				${plantilla.nombreProyecto}
				<br>${plantilla.descProyecto}<br>
				<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
					<table class='table table-striped'>
						<tr>
							<th>Nombre de la tarea</th>
							<th>Duraci�n estimada</th>
							<th>Depende de las tareas</th>
							<th>Completado</th>
						</tr>
						<c:set var="inc" value="0" />
						<c:forEach items="${plantilla.tareas}" var="tarea" >
						<tr class="${tarea.superclase}">
							<td>${tarea.espacios}${tarea.nombreTarea}</td>
							<td>${tarea.duracion} horas</td>
							<td>
								<c:forEach items="${tarea.dependencias}" var="dependencia">
									${dependencia} 
								</c:forEach>
							</td>
							<td>${tarea.completado}%</td>
						</tr>
						</c:forEach>		
					</table>
				</div>
			</div>
		
		</div>
	</c:forEach>
</div>