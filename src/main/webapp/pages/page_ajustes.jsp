<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>
	function eliminarmeUsuario(){
		if(confirm("�Est�s seguro de eliminar tu usuario? Los proyectos, tareas y grupos creados por ti ser�n eliminados del sistema")){
			$.ajax({
				url: '${urlBase}/usuarios/${usuario.idUsuario}.htm',
				type: 'DELETE',
				success: function(){
					location.href = '${urlBase}/logout.htm';
				}
			}).fail(function(data){alert(data.responseText);});
		}
	}
		function passwordEsDelUsuario(email,login,password,nuevaPassword,diasSemana,horasDia){
			$.ajax({
				url: '${urlBase}/ajustes/authentication.htm',
				data: '{"email":"'+email+'","login":"'+login+'","password":"'+password+'","horasDia":'+horasDia+',"diasSemana":'+diasSemana+'}',
				type: 'POST',
				success: function(){
					ajustarCambios(email,login,nuevaPassword,diasSemana,horasDia);
				}
			}).fail(function(data){alert(data.responseText);})
		}
		function submitAjustes(){
			email = document.getElementById("email").value;
			login = document.getElementById("login").value;
			password =  document.getElementById("password").value;
			nuevaPassword =  document.getElementById("nuevaPassword").value;
			reNuevaPassword =  document.getElementById("reNuevaPassword").value;
			diasSemanaP =  document.getElementById("diasSemana").value;
			horasDiaP =  document.getElementById("horasDia").value;
			if(email=='' || login==''|| diasSemanaP=='' || diasSemanaP<1 || diasSemanaP>7 || horasDiaP=='' || horasDiaP<1 || horasDiaP>24){
				alert('Debes rellenar el email y el login');
			}else if(password==''){
				alert('Debes rellenar tu contrase�a actual');
			} else if(nuevaPassword!=reNuevaPassword){
				alert('Ambas nuevas contrase�as deben coincidir')
			} else {
				passwordEsDelUsuario(email,login,password,nuevaPassword,diasSemanaP,horasDiaP)
			}
		}
		function ajustarCambios(email,login,nuevaPassword,diasSemana,horasDia){
			if(password=='')
				elJSON = '{"email":"'+email+'","login":"'+login+'","horasDia":'+horasDia+',"diasSemana":'+diasSemana+'}';
			else
				elJSON = '{"email":"'+email+'","login":"'+login+'","password":"'+nuevaPassword+'","horasDia":'+horasDia+',"diasSemana":'+diasSemana+'}';	
			$.ajax({
				url: '${urlBase}/ajustes.htm',
				data: elJSON,
				type: 'PUT',
				success: function(data){
					switch(data) {
					case "1":
						alert("Ajustes cambiados con �xito");
						document.getElementById("password").value = "";
						document.getElementById("rePassword").value = "";
						document.getElementById("reNuevaPassword").value = "";
						break;
					default:
						alert("No se han podido realizar los cambios");
						document.getElementById("password").value = "";
						document.getElementById("rePassword").value = "";
						document.getElementById("reNuevaPassword").value = "";
					}
				}
			}).fail(function(data){
				alert(data.responseText);
			})
		}
	</script>
	
	<br>
    <fieldset style='text-align:center;'>
      <legend>AJUSTES</legend>
      	Email*: <input type='email' id='email' name='email' value='${usuario.email}' /><br>
      	Login*: <input type='text' id='login' name='login' value='${usuario.login}' /><br>
      	�Cu�nto tiempo piensas dedicar a tus proyectos?<br>
	    <input id='diasSemana' type='number' name='diasSemana' value='${usuario.diasSemana}' style='width:2em;' min='1' max='7'/> d�as a la semana<br>
	    <input id='horasDia' type='number' name='horasDia' value='${usuario.horasDia}' style='width:2em;' min='1' max='24'/> horas al d�a<br>
      	Nueva contrase�a: <input type='password' id='nuevaPassword' name='nuevaPassword' value='' /><br>
      	Repita contrase�a: <input type='password' id='reNuevaPassword' name='reNuevaPassword' value='' /><br><br>
      	Contrase�a Actual*: <input type='password' id='password' name='password' value='' /><br>
      <button class="btn btn-info" id='submitAjustes' onclick='submitAjustes()'>Guardar Cambios</button>
      <br><br><button class="btn btn-info" onclick='eliminarmeUsuario()'><i class='glyphicon glyphicon-trash'></i> Quiero eliminar mi usuario</button>
    </fieldset>