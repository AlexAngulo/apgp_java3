<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<c:set var="urlBase" value="${pageContext.request.contextPath}" />
<script>
var miembros = [<c:set var="b" value="true" />
              <c:forEach items="${miembros}" var="miembro">
              	<c:if test="${b==false}">,{</c:if><c:if test="${b==true}">{<c:set var="b" value="false"/></c:if>
              	permisos : [<c:set var="c" value="true" /><c:forEach items="${miembro.listPermisos}" var="permiso">
              		<c:if test="${c==false}">,</c:if><c:if test="${c==true}"><c:set var="c" value="false"/></c:if>
              		${permiso}
              	</c:forEach>],
              	login: "${miembro.usuario.login}", tieneDerechoA:"${miembro.tieneDerechoA}"}
              </c:forEach>];
var noMiembros = [<c:set var="b" value="true" />
                <c:forEach items="${noMiembros}" var="noMiembro">
                	<c:if test="${b==false}">,</c:if><c:if test="${b==true}"><c:set var="b" value="false"/></c:if>
                	"${noMiembro.login}"
                </c:forEach>];

function echarMiembroGrupo(idInterno){
	if(confirm("�Est�s seguro de querer echar a "+miembros[idInterno].login+" del grupo?")){
		$.ajax({
			url: '${urlBase}/miembros/'+'${grupoPerteneciente.idGrupo}'+'.htm',
			data: miembros[idInterno].login,
			type: 'DELETE',
			success: function(){
				visualizar_eliminarMiembroGrupo(miembros[idInterno].login);
				alert("Se ha echado a "+miembros[idInterno].login+" del grupo con �xito");
			}
		}).fail(function(data){alert(data.responseText);});
	}
}
function encontrar_indice_interno_en_miembros(login){
	i=0;
	while(i<miembros.length){
		if(miembros[i]!=null && miembros[i].login==login) return i;
		i++;
	}
	return i;
}
function visualizar_eliminarMiembroGrupo(login){
	idInterno = encontrar_indice_interno_en_miembros(login);
	miembros[idInterno] = null;
	filaMiembro = document.getElementById("miembro"+login);
	if(filaMiembro){
		tabla = filaMiembro.parentNode;
		tabla.removeChild(filaMiembro);
	}
}
function visualizar_editarMiembroGrupo(login,tieneDerechoA, permisos){
	idInterno = encontrar_indice_interno_en_miembros(login);
	miembros[idInterno].permisos = permisos;
	document.getElementById("celdaTieneDerechoA"+login).innerHtml = tieneDerechoA;
}
function visualizar_agregarMiembroGrupo(loginP,tieneDerechoAP, permisosP){
	miembro = {
		permisos:permisosP,
		login:loginP,
		tieneDerechoA:tieneDerechoAP
	}
	miembros.push(miembro);
	
	inner = "<tr id='miembro"+loginP+"'>"+
		"<td>"+loginP+"</td>"+
		"<td>"+tieneDerechoAP+"</td>";
	<c:if test="${puedeEditarMiembros || puedeEliminarMiembros}">
		inner += "<td>"+
		<c:if test="${puedeEditarMiembros}">
			'<button class="btn btn-info" onclick="actionMostrarEditarMiembro('+(miembros.length-1)+')"><i class="glyphicon glyphicon-edit"></i></button>'+
		</c:if>
		<c:if test="${puedeEliminarMiembros}">
			'<button class="btn btn-info" onclick="echarMiembroGrupo('+(miembros.length-1)+')"><i class="glyphicon glyphicon-trash"></i></button>'+
		</c:if>
		"</td>";
	</c:if>
	inner += "</tr>";
	document.getElementById("tbodyMiembros").innerHTML += inner;
}

function invitarOEditarMiembro(idInterno){
	permisos = [];
	if(idInterno=='crear'){
		login = document.getElementById("login").value;
		if(login==''){
			alert('Debes rellenar el login');
		} else {
			elJSON = '{"login":"'+login+'"';
			for(var i=1;i<=${topePermisos.size()};i++){
				input = document.getElementById("permiso"+i);
				if(input){
					elJSON += ',"permiso'+i+'":'+input.checked;
					permisos.push(input.checked);
				} else {
					permisos.push(false);
				}
			}
			elJSON += '}';
			$.ajax({
				url: '${urlBase}/miembros/'+'${grupoPerteneciente.idGrupo}'+'.htm',
				data: elJSON,
				type: 'POST',
				success: function(tieneDerechoA){
					visualizar_agregarMiembroGrupo(login,tieneDerechoA,permisos);
					alert("Se ha enviado una invitaci�n a "+login+" para unirse al grupo con �xito.");
				}
			}).fail(function(data){alert(data.responseText);});
		}
	} else {
		login = miembros[idInterno].login;
		elJSON = '{"login":"'+login+'"';
		for(var i=1;i<=${topePermisos.size()};i++){
			input = document.getElementById("permiso"+i);
			if(input){
				elJSON += ',"permiso'+i+'":'+input.checked;
				permisos.push(input.checked);
			} else {
				permisos.push(false);
			}
		}
		elJSON += '}';
		$.ajax({
			url: '${urlBase}/miembros/${grupoPerteneciente.idGrupo}.htm',
			data: elJSON,
			type: 'PUT',
			success: function(tieneDerechoA){
				visualizar_editarMiembroGrupo(login,tieneDerechoA, permisos);
				actionOcultarInvitarMiembro();
				alert("Se ha editado la membres�a del usuario "+login+" con �xito.");
			}
		}).fail(function(data){alert(data.responseText);});
	}
}

function borrarNoMiembros(){
	document.getElementById("divLogin").innerHTML = "";
}

function obtenerNoMiembros(id, login){
	divNoMiembros = "Invitar a:<br><select id='login'>";
	divNoMiembros += "<option value=''></option>";
	for(i=0;i<noMiembros.length;i++){
		if(noMiembros[i]==login)
			divNoMiembros += "<option value='"+noMiembros[i]+"' selected>"+noMiembros[i]+"</option>";
		else
			divNoMiembros += "<option value='"+noMiembros[i]+"'>"+noMiembros[i]+"</option>";
	}
	divMiembros += "</select>";
	document.getElementById(id).innerHTML = divNoMiembros;
}

function obtenerPermisos(id,permisosSeleccionados){
	divPermisos = "Permisos:<br>";
	<c:set var="inc" value="0" />
	<c:forEach items="${topePermisos}" var="topePermiso">
		<c:if test="${inc>0 && permisosUsuario.get(inc)}">
			if(permisosSeleccionados[${inc}])
				divPermisos += '<input type="checkbox" id="permiso${inc}" checked/>${topePermiso}';
			else
				divPermisos += '<input type="checkbox" id="permiso${inc}"/>${topePermiso}';
			<c:if test="${inc%3==0}">divPermisos += "<br>";</c:if>
		</c:if>
		<c:set var="inc" value="${inc+1}" />
	</c:forEach>
	document.getElementById(id).innerHTML = divPermisos;
}

function convertirSubmitEnInvitarMiembro(permisoSeleccionado){
	document.getElementById("submitInvitarMiembro").setAttribute("value","Invitar Miembro");
	document.getElementById("submitInvitarMiembro").setAttribute("onclick","invitarOEditarMiembro('crear')");
	document.getElementById("legendaryMiembros").innerHTML = 'INVITAR A UN NUEVO MIEMBRO';
	obtenerNoMiembros("divLogin","");
	obtenerPermisos("divPermisos",permisoSeleccionado);
}

function mostrarInvitarMiembro(permisoSeleccionado) {
	document.getElementById("formInvitarMiembro").setAttribute("style", "");
	document.getElementById("buttonMostrarInvitarMiembro").setAttribute("style", "display: none;");
	document.getElementById("buttonOcultarInvitarMiembro").setAttribute("style", "");
	convertirSubmitEnInvitarMiembro(permisoSeleccionado);
}

function actionMostrarInvitarMiembro(idSupergrupo) {
	mostrarInvitarMiembro(idSupergrupo);
}

function ocultarInvitarMiembro() {
	document.getElementById("formInvitarMiembro").setAttribute("style", "display: none;");
	document.getElementById("buttonMostrarInvitarMiembro").setAttribute("style", "display: block;");
	document.getElementById("buttonOcultarInvitarMiembro").setAttribute("style", "display: none;");
	convertirSubmitEnInvitarMiembro(0);
}

function actionOcultarInvitarMiembro() {
	ocultarInvitarMiembro();
}

function convertirSubmitEnEditarMiembro(idInterno){
	document.getElementById("submitInvitarMiembro").setAttribute("value","Editar Miembro");
	document.getElementById("submitInvitarMiembro").setAttribute("onclick","invitarOEditarMiembro("+idInterno+")");
	document.getElementById("legendaryMiembros").innerHTML = 'EDITAR MIEMBRO '+miembros[idInterno].login;
	borrarNoMiembros();
	obtenerPermisos("divPermisos",miembros[idInterno].permisos);
}

function editarMiembro(idInterno){
	mostrarInvitarMiembro(0);
	convertirSubmitEnEditarMiembro(idInterno);
}

function actionMostrarEditarMiembro(idInterno) {
	editarMiembro(idInterno);
}
</script>
<c:if test="${puedeInvitarMiembros}">
	<button class="btn btn-info" id="buttonMostrarInvitarMiembro" onclick="actionMostrarInvitarMiembro('')">Invitar Miembro</button>
	<button class="btn btn-info" id="buttonOcultarInvitarMiembro" onclick="actionOcultarInvitarMiembro()" style='display:none;'>Ocultar Creaci�n de Grupos</button>
	<div id='formInvitarMiembro' style='display:none;'>
		  <br>
		  <fieldset style='text-align:center;'>
		    <legend id='legendaryMiembros'>INVITAR UN NUEVO MIEMBRO</legend>
		      <div id="divLogin"></div>
		      <div id="divPermisos"></div>
		    <input id='submitInvitarMiembro' type='submit' value='Invitar Miembro'/>
		  </fieldset>
	</div>
</c:if>
<div style="text-align:center;">
	<h3>USUARIOS DE ${grupoPerteneciente.nombreGrupo}<a href="${urlBase}/grupos/${grupoPerteneciente.idGrupo}/miembros.htm"><i class='glyphicon glyphicon-refresh'></i></a></h3>
	<div class="col-lg-offset-1 col-lg-10 col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
		<table class='table table-striped' id='tableMiembros'>
			<thead>
			<tr>
				<th>Login del Usuario </th>
				<th> Permisos </th>
				<c:if test="${puedeEditarMiembros || puedeEliminarMiembros}">
				<th> Acciones</th>
				</c:if>
			</tr>
			</thead>
			<tbody id="tbodyMiembros">
			<c:set var="inc" value="0" />
			<c:forEach items="${miembros}" var="miembro">
				<tr id="miembro${miembro.usuario.login}">
				<td>${miembro.usuario.login}</td>
				<td id='celdaTieneDerechoA${miembro.usuario.login}'>${miembro.tieneDerechoA.value1}</td>
				<c:if test="${puedeEditarMiembros || puedeEliminarMiembros}">
				<td>
					<c:if test="${miembro.usuario.login!=grupoPerteneciente.nombreCreador}">
						<c:if test="${puedeEditarMiembros}"><button class="btn btn-info" onclick="actionMostrarEditarMiembro(${inc})"><i class="glyphicon glyphicon-edit"></i></button></c:if>
						<c:if test="${puedeEliminarMiembros}"><button class="btn btn-info" onclick="echarMiembroGrupo(${inc})"><i class="glyphicon glyphicon-trash"></i></button></c:if>
					</c:if>
				</td>
				</c:if>
				<c:set var="inc" value="${inc+1}" />
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</div>
</div>