<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
Fecha de Fin Ideal:<br />
<select id="${esCompartidos}diaFin" name="diaFin">
	<option value=""></option>
	<c:forEach var="i" begin="1" end="31">
		<c:if test="${i<10}"><option value="0${i}">0${i}</option></c:if>
		<c:if test="${i>9}"><option value="${i}">${i}</option></c:if>
	</c:forEach>
</select>/
<select id="${esCompartidos}mesFin" name="mesFin" onchange="actualizar_dia_fin()">
	<option value=""></option>
	<c:forEach var="i" begin="1" end="12">
		<c:if test="${i<10}"><option value="0${i}">0${i}</option></c:if>
		<c:if test="${i>9}"><option value="${i}">${i}</option></c:if>
	</c:forEach>
</select>/
<select id="${esCompartidos}anoFin" name="anoFin" onchange="actualizar_dia_fin()">
	<option value=""></option>
	<c:forEach var="i" begin="2014" end="2114">
		<option value="${i}">${i}</option>
	</c:forEach>
</select>
<br>
<select id="${esCompartidos}horaFin" name="horaFin">
	<option value=""></option>
	<c:forEach var="i" begin="0" end="23">
		<c:if test="${i<10}"><option value="0${i}">0${i}</option></c:if>
		<c:if test="${i>9}"><option value="${i}">${i}</option></c:if>
	</c:forEach>
</select>:
<select id="${esCompartidos}minFin" name="minFin">
	<option value=""></option>
	<c:forEach var="i" begin="0" end="59">
		<c:if test="${i<10}"><option value="0${i}">0${i}</option></c:if>
		<c:if test="${i>9}"><option value="${i}">${i}</option></c:if>
	</c:forEach>
</select>
<script>
	if(${param.fechaFin != null}){
		actualizar_ano('${param.fechaFin.year}');
		actualizar_mes('${param.fechaFin.month}');
		actualizar_dia_segun_ano_y_mes('${param.fechaFin.day}','${param.fechaFin.month}','${param.fechaFin.year}');
		actualizar_hora('${param.fechaFin.hours}');
		actualizar_min('${param.fechaFin.minutes}');
	}
	function ${esCompartidos}resetear_los_cinco(){
		${esCompartidos}actualizar_ano('');
		${esCompartidos}actualizar_mes('');
		${esCompartidos}actualizar_dia(31,'');
		${esCompartidos}actualizar_hora(24);
		${esCompartidos}actualizar_min(60);
	}
	function ${esCompartidos}obtener_numeros_de_dos_cifras(num){
		if(num<10) return "0"+num; else return num;
	}
	function ${esCompartidos}actualizar_dia_fin(){
		dia = document.getElementById("${esCompartidos}diaFin").value;
		mes = document.getElementById("${esCompartidos}mesFin").value;
		ano = document.getElementById("${esCompartidos}anoFin").value;
		${esCompartidos}actualizar_dia_segun_ano_y_mes(dia,mes,ano);
	}
	function ${esCompartidos}actualizar_cualquiera_de_los_cinco(cualquiera_min,cualquiera_max,cualquiera_sel,cualquiera_id){
		htmlCualquieraFin = "<option value=''></option>";
		for(i=cualquiera_min;i<=cualquiera_max;i++){
			htmlCualquieraFin += "<option value='"+${esCompartidos}obtener_numeros_de_dos_cifras(i)+"' ";
			if(i==cualquiera_sel)
				htmlCualquieraFin += "selected";
			htmlCualquieraFin += ">"+obtener_numeros_de_dos_cifras(i)+"</option>";
		}
		document.getElementById(cualquiera_id).innerHTML = htmlCualquieraFin;
	}
	function ${esCompartidos}actualizar_dia(dia_max,dia_sel){
		actualizar_cualquiera_de_los_cinco(1,dia_max,dia_sel,"${esCompartidos}diaFin")
	}
	function ${esCompartidos}actualizar_mes(mes_sel){
		actualizar_cualquiera_de_los_cinco(1,12,mes_sel,"${esCompartidos}mesFin")
	}
	function ${esCompartidos}actualizar_ano(ano_sel){
		actualizar_cualquiera_de_los_cinco(2014,2114,ano_sel,"${esCompartidos}anoFin")
	}
	function ${esCompartidos}actualizar_hora(hora_sel){
		actualizar_cualquiera_de_los_cinco(0,23,hora_sel,"${esCompartidos}horaFin")
	}
	function ${esCompartidos}actualizar_min(min_sel){
		actualizar_cualquiera_de_los_cinco(0,59,min_sel,"${esCompartidos}minFin")
	}
	function ${esCompartidos}actualizar_dia_segun_ano_y_mes(dia,mes,ano){
		esBisiesto = ano%4==0;
		if (mes==2)
			if(esBisiesto)
				${esCompartidos}actualizar_dia(29,dia);
			else
				${esCompartidos}actualizar_dia(28,dia);
		else if(mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12)
			${esCompartidos}actualizar_dia(31,dia);
		else if(mes==4 || mes==6 || mes==9 || mes==11)
			${esCompartidos}actualizar_dia(30,dia);
	}
</script>