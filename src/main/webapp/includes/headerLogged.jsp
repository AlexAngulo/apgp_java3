<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<%@ page import= "modelos.conexion.Conexion" %> 
<%@ page import= "java.sql.Connection" %> 
<%@ page import= "modelos.clases.Usuario" %> 
<%@ page import= "controlador.utiles.UtilesSecurity" %> 
<%@ page import= "java.util.List" %> 
<%@ page import= "controlador.logica.LogicaProyecto" %> 
<%@ page import= "controlador.logica.VistaProyecto" %> 
<%@ page import= "modelos.clases.EquipoCompleto" %> 
<%@ page import= "controlador.logica.LogicaGrupo" %> 
<%@ page import= "controlador.logica.VistaGrupoMiembro" %> 

<c:set var="urlBase" value="${pageContext.request.contextPath}" />

<nav class="navbar navbar-default navbar-info" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="${urlBase}/">APGP</a>
    </div>
    
    <%
    	Connection conn = Conexion.obtenerConexion();
    	Usuario usuario = UtilesSecurity.usuarioEstaLogueado(request.getSession(),conn);
    %>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      <% 
      	if(!usuario.isEsAdministrador()){
      %>
	        <li class=<c:if test="${sitio == 'Mis Proyectos'}">"dropdown active"</c:if><c:if test="${sitio != 'Mis Proyectos'}">"dropdown"</c:if>>
	          <a href="${urlBase}/" class="dropdown-toggle" data-hover="dropdown">Mis Proyectos<span class="caret"></span></a>
	          <ul id="dropdown-proyectos" class="dropdown-menu" role="menu">
	          	<%
	          		List<VistaProyecto> proyectos = LogicaProyecto.obtenerProyectosPorUsuario(usuario.getIdUsuario(),usuario.getOrdenProyectos(), conn);
	          		int i = 0;
	          		for(VistaProyecto vp : proyectos){
	          	%>
	          		<c:set var="idProy" value="<%=vp.getIdProyecto()%>"/>
	          		<li <c:if test="${proyecto!=null && proyecto.idProyecto==idProy}">class='active'</c:if>><a href="${urlBase}/proyectos/${idProy}.htm"><%=vp.getNombreProyecto()%></a></li>
	          	<%
	          	if(i>=5){break;}i++;} 
	          	if(proyectos.size()>0){%>
	            	<li><a href="${urlBase}/">Ver m�s Proyectos...</a></li>
	            	<li class="divider"></li>
	            <%} %>
	            <li><a href="${urlBase}/proyectos/crear.htm">Crear Proyecto</a></li>
	            <li class="divider"></li>
	            <li><a href="${urlBase}/proyectos/compartidos.htm">Ver Proyectos Compartidos</a></li>
	            <li><a href="${urlBase}/tareas/asignadas.htm">Ver mis Tareas Asignadas</a></li>
	          </ul>
	        </li>
      <%
      	} else {
      %>
        	<li <c:if test="${sitio == 'Panel de Administraci�n'}">class="active"</c:if>><a href="${urlBase}/">Panel de Administraci�n</a></li>
      <%} 
      List<EquipoCompleto> equipos = EquipoCompleto.obtenerEquiposCompletosPorUsuario(0, usuario.getIdUsuario(),conn);
      int i=0;
      if(equipos.size()>0){%>
        <li class=<c:if test="${sitio == 'Mis Equipos'}">"dropdown active"</c:if><c:if test="${sitio != 'Mis Equipos'}">"dropdown"</c:if>>
          <a href="${urlBase}/equipos.htm" class="dropdown-toggle" data-hover="dropdown">Equipos<span class="caret"></span></a>
          <ul id="dropdown-equipos" class="dropdown-menu" role="menu">
          	<%
          		
          		for(EquipoCompleto ec : equipos){
          	%>
            <li><a href="#"><%=ec.getNombreEquipo()%></a></li>
            <% i++;if(i>=5) break;}
            %>
            <li><a href="${urlBase}/equipos.htm">Ver m�s Equipos...</a></li>
          </ul>
        </li>
        <%}else {%>
        <li <c:if test="${sitio == 'Mis Equipos'}">class="active"</c:if>>
          <a href="${urlBase}/equipos.htm">Equipos</a>
        </li>
        <%}%>
         <li class=<c:if test="${sitio == 'Mis Grupos'}">"dropdown active"</c:if><c:if test="${sitio != 'Mis Grupos'}">"dropdown"</c:if>>
          <a href="${urlBase}/grupos.htm" class="dropdown-toggle" data-hover="dropdown">Grupos<span class="caret"></span></a>
          <ul id="dropdown-grupos" class="dropdown-menu" role="menu">
            <%
          		List<VistaGrupoMiembro> grupos = LogicaGrupo.obtenerGruposOrdenadosPorUsuario(usuario.getIdUsuario(), conn);
          		i = 0;
          		for(VistaGrupoMiembro vgm : grupos){
          	%>
          		<c:set var="idGru" value="<%=vgm.getIdGrupo()%>"/>
          		<li <c:if test="${grupoPerteneciente!=null && grupoPerteneciente.idGrupo==idGru}">class='active'</c:if>><a href="${urlBase}/grupos/${idGru}.htm"><%=vgm.getNombreGrupo()%></a></li>
          	<%
          	if(i>=5){break;}i++;} 
          	if(grupos.size()>0){%>
            	<li><a href="${urlBase}/grupos.htm">Ver m�s Grupos...</a></li>
            	<li class="divider"></li>
            <%} %>
            <li><a href="${urlBase}/grupos/crear.htm">Crear Grupo</a></li>
          </ul>
        </li>
        <li <c:if test="${sitio == 'Ajustes'}">class="active"</c:if>>
          <a href="${urlBase}/ajustes.htm">Ajustes</a>
        </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
      	<li><a href="#">Hola ${usuario.login}</a></li>
        <li><a href="${urlBase}/logout.htm">Cambiar sesi�n</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<div id="over" class="overbox" style="display:none;">
	<div id="content"></div>
</div>
<div id="fade" class="fadebox" style="display:none;">&nbsp;</div>
<script>
if('${message}'!=''){
	alert('${message}');
}
var avisos = [
	             	<c:set var="b" value="${true}"/>
	             	<c:forEach items="${invitaciones}" var="invitacion" >
		             	<c:if test="${b==false}">
	             			,{
	             		</c:if>
	             		<c:if test="${b==true}">
	             			{
	             			<c:set var="b" value="${false}"/>
	             		</c:if>
	             		idAviso:"${invitacion.idAviso}",
	             		idAvisado:"${invitacion.idAvisado}",
	             		mensaje:"${invitacion.mensaje}"
	             		}
	             	</c:forEach>
	             	];
	function ver_aviso(
			idAviso){
		$.ajax({
			url: '${pageContext.request.contextPath}/avisos/'+idAviso+'.htm',
			type: 'DELETE'
		});
	}
	
	for(var g=0;g<avisos.length;g++){
		alert(avisos[g].mensaje);
		ver_aviso(avisos[g].idAviso);
	}
</script>