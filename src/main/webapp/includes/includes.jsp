<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="true" %>
<title>${title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<c:url value="/css/styles.css" />">
<link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css" />">
<script src="<c:url value="/js/scripts.js"/>"></script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script src="<c:url value="/js/bootstrap-hover-dropdown.js"/>"></script>
<script src="<c:url value="/js/jquery-1.7.2.js"/>"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<c:url value="/js/json.min.js"/>"></script>
<!-- TODO: de momento la urlBase es /APGP/ hasta modificarlo bien -->
<c:set var="urlIndex" value="/APGP/" />
<c:set var="urlActual" value="${pageContext.request.contextPath}" />