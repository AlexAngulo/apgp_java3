package controlador.logica;

import java.sql.ResultSet;
import java.sql.SQLException;

import modelos.clases.Aviso;
import modelos.enums.TopePermisos;

public class LogicaAviso {
	public static String generarMensajeNuevoMiembro(String login,
			String nombreGrupo, boolean permisos[]) {
		return login+" te ha invitado al grupo "+nombreGrupo+" con los permisos: "+TopePermisos.generarPermisosEnString(permisos);
	}
	
	public static void pasarDatosDelResultSetAlAviso(Aviso mg, String prefix, ResultSet rs) throws SQLException{
		mg.setIdAviso(rs.getInt(prefix+Aviso.CAMPOS.ID_AVISO));
		mg.setIdAvisado(rs.getInt(prefix+Aviso.CAMPOS.AVISADO));
		mg.setMensaje(rs.getString(prefix+Aviso.CAMPOS.MENSAJE));
	}
}
