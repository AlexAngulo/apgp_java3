package controlador.logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Grupo;
import modelos.clases.MiembroGrupo;
import modelos.clases.Proyecto;
import modelos.clases.Grupo.CAMPOS;

public class LogicaGrupo {
	public static List<VistaGrupoMiembro> obtenerGruposOrdenadosPorUsuario(int idUsuario,Connection conn) {
		List<VistaGrupoMiembro> lvgm = VistaGrupoMiembro.busquedaAvanzadaVistaGrupoMiembro(" "+MiembroGrupo.CAMPOS_MG.ID_USUARIO+"="+idUsuario, " ORDER BY g."+Grupo.CAMPOS.SUPERGRUPO, conn);
		lvgm = ordenarGrupos(lvgm,0,new ArrayList<VistaGrupoMiembro>(),0,"");
		lvgm = agregarListaDePosiblesPadresALosGrupos(lvgm);
		return lvgm;
	}
	
	private static List<VistaGrupoMiembro> agregarListaDePosiblesPadresALosGrupos(
			List<VistaGrupoMiembro> lvgm1) {
		List<VistaGrupoMiembro> lvgm2 = new ArrayList<VistaGrupoMiembro>();
		for(VistaGrupoMiembro vgm : lvgm1){
			vgm = agregarListaDePosiblesPadresAlGrupo(vgm,lvgm1);
			lvgm2.add(vgm);
		}
		return lvgm2;
	}

	private static VistaGrupoMiembro agregarListaDePosiblesPadresAlGrupo(
			VistaGrupoMiembro vgm, List<VistaGrupoMiembro> lvgm) {
		vgm.setPuedeSerHijoDe(pasarAListaDeEnteros(lvgm));
		vgm.removePuedeSerHijoDe(vgm.getIdGrupo());
		for(VistaGrupoMiembro vgmActual : lvgm){
			if(vgmActual.getIdGrupo()!=vgm.getIdGrupo() && vgmActual.getSuperclase().contains("SubgrupoDe"+vgm.getIdGrupo()))
				vgm.removePuedeSerHijoDe(vgmActual.getIdGrupo());
		}
		return vgm;
	}

	private static List<Integer> pasarAListaDeEnteros(
			List<VistaGrupoMiembro> lvgm) {
		List<Integer> ids = new ArrayList<Integer>();
		for(VistaGrupoMiembro vgm : lvgm){
			ids.add(vgm.getIdGrupo());
		}
		return ids;
	}

	private static List<VistaGrupoMiembro> ordenarGrupos(
			List<VistaGrupoMiembro> listaDesordenada, int idSupergrupo, List<VistaGrupoMiembro>listaOrdenada, int nivel, String superclase) {
		try{
			for(VistaGrupoMiembro vgm : listaDesordenada){
				if(!listaOrdenada.contains(vgm) && (idSupergrupo==0 || vgm.getIdSupergrupo()==idSupergrupo)){
					vgm.setNivel(nivel);
					vgm.setSuperclase(superclase);
					listaOrdenada.add(vgm);
					listaOrdenada = ordenarGrupos(listaDesordenada,vgm.getIdGrupo(),listaOrdenada,nivel+1,superclase+" SubgrupoDe"+vgm.getIdGrupo());
				}
			}
		} catch(Exception e){
			listaOrdenada = new ArrayList<VistaGrupoMiembro>();
		}
		return listaOrdenada;
	}

	public static Grupo obtenerGrupoPorId(int idGrupo,Connection conn){
		return new Grupo(idGrupo,conn);
	}
	
	public static boolean guardarGrupo(int idCreador, int idSupergrupo, String nombreGrupo,String descGrupo,Connection conn) {
		return guardarGrupo(new Grupo(idCreador,idSupergrupo, nombreGrupo, descGrupo),conn);
	}
	
	public static boolean guardarGrupo(Grupo grupo,Connection conn){
		if (grupo.guardarGrupo()) {
			boolean[] booleans = {true,true,true,true,true,true,true,true,true,true,true,true,true};
			return LogicaMiembroGrupo.agregarMiembro(grupo.getIdCreador(), grupo.getIdGrupo(),booleans,conn);
		} else {
			return false;
		}
	}

	public static boolean guardarGrupoPorUsuario(int idUsuario, Grupo grupo,
			Connection conn) {
		grupo.setConn(conn);
		grupo.setIdCreador(idUsuario);
		if(grupo.guardarGrupo()){
			boolean[] booleans = {true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true};
			return LogicaMiembroGrupo.agregarMiembro(idUsuario, grupo.getIdGrupo(), booleans, conn);
		}else{
			return false;
		}
	}

	public static List<VistaGrupoMiembro> aux_obtenerSubgruposPorIdSupergrupo(int idGrupo,int idUsuario,
			Connection conn) {
		return VistaGrupoMiembro.busquedaAvanzadaVistaGrupoMiembro(MiembroGrupo.CAMPOS_MG.ID_USUARIO+"="+idUsuario+" AND g."+Grupo.CAMPOS.SUPERGRUPO+"="+idGrupo, "", conn);
	}
	
	public static List<VistaGrupoMiembro> obtenerSubgruposPorIdSupergrupo(int idGrupo,int idUsuario, int nivel,String superclase,
			Connection conn) {
		List<VistaGrupoMiembro> lvgm = aux_obtenerSubgruposPorIdSupergrupo(idGrupo, idUsuario, conn);
		List<VistaGrupoMiembro> lvgm1 = new ArrayList<VistaGrupoMiembro>();
		lvgm1.addAll(lvgm);
		for(VistaGrupoMiembro vgm : lvgm){
			vgm.setSuperclase(superclase);
			vgm.setNivel(nivel);
			lvgm1.addAll(obtenerSubgruposPorIdSupergrupo(vgm.getIdGrupo(),idUsuario,nivel+1,superclase+" SubgrupoDe"+vgm.getIdGrupo(),conn));
		}
		lvgm1 = agregarListaDePosiblesPadresALosGrupos(lvgm1);
		return lvgm1;
	}
	
	public static void pasarDatosDelResultSetAlGrupo(Grupo grupo, ResultSet rs) throws SQLException{
		grupo.setIdGrupo(rs.getInt(Grupo.CAMPOS.ID_GRUPO));
		grupo.setIdCreador(rs.getInt(Grupo.CAMPOS.CREADOR));
		grupo.setIdSupergrupo(rs.getInt(CAMPOS.SUPERGRUPO));
		grupo.setNombreGrupo(rs.getString(CAMPOS.NOMBRE_GRUPO));
		grupo.setDescGrupo(rs.getString(CAMPOS.DESC_GRUPO));
		grupo.setOrdenProyectos(rs.getInt(CAMPOS.ORDEN_PROYECTOS));
	}
}
