package controlador.logica;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

import modelos.clases.Usuario;

public class LogicaUsuario {
	public static boolean existeUser(String login, String password,Connection conn) {
		HashMap<String, Object> filtros = new HashMap<String, Object>();
		filtros.put(Usuario.CAMPOS.LOGIN, login);
		filtros.put(Usuario.CAMPOS.PASSWORD, password);
		return Usuario.busquedaAvanzadaUsuario(filtros,conn).size()>0;
	}
	
	public static boolean existeLogin(String login,Connection conn) {
		HashMap<String, Object> filtros = new HashMap<String, Object>();
		filtros.put(Usuario.CAMPOS.LOGIN, login);
		return Usuario.busquedaAvanzadaUsuario(filtros,conn).size()>0;
	}
	
	public static boolean existeEmail(String email,Connection conn) {
		HashMap<String, Object> filtros = new HashMap<String, Object>();
		filtros.put(Usuario.CAMPOS.EMAIL, email);
		return Usuario.busquedaAvanzadaUsuario(filtros,conn).size()>0;
	}
	
	public static Usuario obtenerUsuarioPorId(int idUsuario,Connection conn) {
		Usuario usu = new Usuario();
		usu.setConn(conn);
		usu.obtenerUsuarioPorId(idUsuario);
		return usu;
	}
	
	public static Usuario obtenerUsuarioPorLogin(String login,Connection conn) {
		Usuario usu = new Usuario();
		usu.setConn(conn);
		usu.obtenerUsuarioPorNombreDeUsuario(login);
		return usu;
	}
	
	public static List<Usuario> obtenerTodosLosUsuarios(Connection conn) {
		return Usuario.obtenerUsuarios(conn);
	}
}
