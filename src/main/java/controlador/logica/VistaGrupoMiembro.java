package controlador.logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Grupo;
import modelos.clases.MiembroGrupo;
import modelos.clases.Usuario;
import modelos.clasesAbstractas.IntMiembroGrupo;
import controlador.clases.VistaGrupo;

public class VistaGrupoMiembro extends VistaGrupo{
	private static final long serialVersionUID = 1L;
	
	private String nombreSupergrupo = "";
	private int nivel = 0;
	private List<Integer> puedeSerHijoDe;
	private String superclase;
	
	private boolean crearTareas;
	
	public boolean isCrearTareas() {return crearTareas;}
	public void setCrearTareas(boolean crearTareas) {this.crearTareas = crearTareas;}
	
	public boolean isCrearProyectos() {return crearProyectos;}
	public void setCrearProyectos(boolean crearProyectos) {this.crearProyectos = crearProyectos;}
	
	public boolean isEditarTareas() {return editarTareas;}
	public void setEditarTareas(boolean editarTareas) {this.editarTareas = editarTareas;}
	
	public boolean isEditarProyectos() {return editarProyectos;}
	public void setEditarProyectos(boolean editarProyectos) {this.editarProyectos = editarProyectos;}
	
	public boolean isEliminarTareas() {return eliminarTareas;}
	public void setEliminarTareas(boolean eliminarTareas) {this.eliminarTareas = eliminarTareas;}
	
	public boolean isEliminarProyectos() {return eliminarProyectos;}
	public void setEliminarProyectos(boolean eliminarProyectos) {this.eliminarProyectos = eliminarProyectos;}
	
	public boolean isInvitarMiembros() {return invitarMiembros;}
	public void setInvitarMiembros(boolean invitarMiembros) {this.invitarMiembros = invitarMiembros;}
	
	public boolean isEditarMiembros() {return editarMiembros;}
	public void setEditarMiembros(boolean editarMiembros) {this.editarMiembros = editarMiembros;}
	
	public boolean isEliminarMiembros() {return eliminarMiembros;}
	public void setEliminarMiembros(boolean eliminarMiembros) {this.eliminarMiembros = eliminarMiembros;}
	
	public boolean isCrearSubgrupo() {return crearSubgrupo;}
	public void setCrearSubgrupo(boolean crearSubgrupo) {this.crearSubgrupo = crearSubgrupo;}
	
	public boolean isEliminarSubgrupo() {return eliminarSubgrupo;}
	public void setEliminarSubgrupo(boolean eliminarSubgrupo) {this.eliminarSubgrupo = eliminarSubgrupo;}
	
	public boolean isEditarGrupo() {return editarGrupo;}
	public void setEditarGrupo(boolean editarGrupo) {this.editarGrupo = editarGrupo;}
	
	public boolean isEliminarGrupo() {return eliminarGrupo;}
	public void setEliminarGrupo(boolean eliminarGrupo) {this.eliminarGrupo = eliminarGrupo;}
	
	private boolean crearProyectos;
	private boolean editarTareas;
	private boolean editarProyectos;
	private boolean eliminarTareas;
	private boolean eliminarProyectos;
	private boolean invitarMiembros;
	private boolean editarMiembros;
	private boolean eliminarMiembros;
	private boolean crearSubgrupo;
	private boolean eliminarSubgrupo;
	private boolean editarGrupo;
	private boolean eliminarGrupo;

	public VistaGrupoMiembro() {
	}
	
	public VistaGrupoMiembro(int idGrupo, Connection conn) {
		this.setConn(conn);
		this.obtenerVistaGrupoMiembroPorId(idGrupo);
	}
	public List<Integer> getPuedeSerHijoDe() {return puedeSerHijoDe;}
	public void setPuedeSerHijoDe(List<Integer> puedeSerHijoDe) {this.puedeSerHijoDe = puedeSerHijoDe;}
	
	public void addPuedeSerHijoDe(int idPadre){if(this.puedeSerHijoDe==null)this.puedeSerHijoDe=new ArrayList<Integer>(); this.puedeSerHijoDe.add((Integer)idPadre);}
	public void removePuedeSerHijoDe(int idPadre){if(this.puedeSerHijoDe!=null) this.puedeSerHijoDe.remove((Integer)idPadre);}
	
	public String getNombreSupergrupo() {return nombreSupergrupo;}
	public void setNombreSupergrupo(String nombreSupergrupo) {this.nombreSupergrupo = nombreSupergrupo;}

	public int getNivel() {return nivel;}
	public void setNivel(int nivel) {this.nivel = nivel;}
	
	public String getEspacios(){
		String espacios = "";
		for(int i=0;i<nivel;i++){
			espacios += "&nbsp;&nbsp;";
		}
		return espacios;
	}

	public String getSuperclase() {return superclase;}
	public void setSuperclase(String superclase) {this.superclase = superclase;}

	private final static String G="g.";
	private final static String U="u.";
	
	private final static String selectTodo = "SELECT "+G+CAMPOS.ID_GRUPO+","+G+CAMPOS.NOMBRE_GRUPO+","+
		G+CAMPOS.DESC_GRUPO+","+G+CAMPOS.CREADOR+","+G+CAMPOS.SUPERGRUPO+","+U+Usuario.CAMPOS.LOGIN+","+
		IntMiembroGrupo.selectPermisosMG+" FROM "+CAMPOS.NOMBRE_TABLA+" g,"+Usuario.CAMPOS.NOMBRE_TABLA+" u,"+
		IntMiembroGrupo.CAMPOS_MG.NOMBRE_TABLA;
	private final static String selectCasiTodo = "SELECT "+G+CAMPOS.ID_GRUPO+","+G+CAMPOS.NOMBRE_GRUPO+","+
			G+CAMPOS.DESC_GRUPO+","+G+CAMPOS.CREADOR+","+G+CAMPOS.SUPERGRUPO+","+U+Usuario.CAMPOS.LOGIN+
			" FROM "+CAMPOS.NOMBRE_TABLA+" g,"+Usuario.CAMPOS.NOMBRE_TABLA+" u";
	
	private final static String whereCasiTodo = " WHERE "+U+Usuario.CAMPOS.ID_USUARIO+"="+G+CAMPOS.CREADOR;
	private final static String whereTodo = " WHERE "+U+Usuario.CAMPOS.ID_USUARIO+"="+G+CAMPOS.CREADOR+" AND "+
	G+CAMPOS.ID_GRUPO+"="+IntMiembroGrupo.CAMPOS_MG.ID_GRUPO;
	
	public boolean obtenerVistaGrupoMiembroPorId(int idGrupo) {
		boolean encontrado = false;
		try {
			ResultSet rs = devolverResultSet(selectCasiTodo+whereCasiTodo+" AND "+
					CAMPOS.ID_GRUPO+"="+idGrupo, conn);
			if (rs.next()) {
				pasarDatosDelResultSetAVistaGrupoCompleto(this, rs, conn,true);
				encontrado = true;
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		} 
		
		return encontrado;
	}
	
	public static List<VistaGrupoMiembro> busquedaAvanzadaVistaGrupoMiembro(String where, String orderBy, Connection conn) {
		List<VistaGrupoMiembro> lvmg = new ArrayList<VistaGrupoMiembro>();
		try {
			ResultSet rs = devolverResultSet(selectTodo+whereTodo+" AND "+where+orderBy, conn);
			while(rs.next()) {
				VistaGrupoMiembro vmg = new VistaGrupoMiembro();
				pasarDatosDelResultSetAVistaGrupoCompleto(vmg, rs,conn,false);
				lvmg.add(vmg);
			}
		} catch (Exception e) {
			lvmg = new ArrayList<VistaGrupoMiembro>();
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		} 
		return lvmg;
	}
	
	public static void pasarDatosDelResultSetAVistaGrupoCompleto(VistaGrupoMiembro vistaGrupo, ResultSet rs,Connection conn, boolean casiTodo) throws SQLException{
		vistaGrupo.setIdGrupo(rs.getInt(G+Grupo.CAMPOS.ID_GRUPO));
		vistaGrupo.setIdCreador(rs.getInt(G+Grupo.CAMPOS.CREADOR));
		vistaGrupo.setIdSupergrupo(rs.getInt(G+CAMPOS.SUPERGRUPO));
		vistaGrupo.setNombreGrupo(rs.getString(G+CAMPOS.NOMBRE_GRUPO));
		vistaGrupo.setDescGrupo(rs.getString(G+CAMPOS.DESC_GRUPO));
		vistaGrupo.setNombreCreador(rs.getString(U+Usuario.CAMPOS.LOGIN));
		if(!casiTodo)
			pasarDatosDelResultSetAlIntMiembroGrupo(vistaGrupo,rs);
		if(vistaGrupo.getIdSupergrupo()>0)
			vistaGrupo.setNombreSupergrupo((new Grupo(vistaGrupo.getIdSupergrupo(),conn)).getNombreGrupo());
	}
	
	public static void pasarDatosDelResultSetAlIntMiembroGrupo(VistaGrupoMiembro mg,
			ResultSet rs) throws SQLException {
		mg.setCrearProyectos(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.CREAR_PROYECTOS));
		mg.setCrearSubgrupo(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.CREAR_SUBGRUPO));
		mg.setCrearTareas(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.CREAR_TAREAS));
		mg.setEditarGrupo(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.EDITAR_GRUPO));
		mg.setEditarProyectos(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.EDITAR_PROYECTOS));
		mg.setEditarMiembros(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.EDITAR_MIEMBROS));
		mg.setEditarTareas(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.EDITAR_TAREAS));
		mg.setEliminarGrupo(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.ELIMINAR_GRUPO));
		mg.setEliminarMiembros(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.ELIMINAR_MIEMBROS));
		mg.setEliminarProyectos(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.ELIMINAR_PROYECTOS));
		mg.setEliminarSubgrupo(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.ELIMINAR_SUBGRUPO));
		mg.setEliminarTareas(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.ELIMINAR_TAREAS));
		mg.setInvitarMiembros(rs.getBoolean(IntMiembroGrupo.CAMPOS_MG.INVITAR_MIEMBROS));
	}
	public static List<VistaGrupo> obtenerVistaGruposMiembro(Connection conn) {
		return VistaGrupo.busquedaAvanzadaVistaGrupo("", " ORDER BY " + CAMPOS.NOMBRE_GRUPO, conn);
	}
}
