package controlador.logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelos.clasesAbstractas.IntDependenciaTarea;
import modelos.conexion.Conexion;

public class VistaDependenciaTarea extends IntDependenciaTarea{
	private List<Integer> dependencias;

	public VistaDependenciaTarea(int idDepende){
		this.idTareaDepende = idDepende;
		this.dependencias = new ArrayList<Integer>();
	}
	
	public VistaDependenciaTarea(){
		super();
		dependencias = new ArrayList<Integer>();
	}
	
	public VistaDependenciaTarea(int idDepende, ArrayList<Integer> dependencias) {
		this.idTareaDepende = idDepende;
		this.dependencias = dependencias;
	}

	public List<Integer> getDependencias() {
		return dependencias;
	}

	public void setDependencias(List<Integer> dependencias) {
		this.dependencias = dependencias;
	}
	
	public void addDependencia(int dependencia){
		this.dependencias.add(dependencia);
	}
	
	public static List<VistaDependenciaTarea> busquedaAvanzadaVistaDependenciaTarea(String where, String orderBy,Connection conn){
		List<VistaDependenciaTarea> lvdt = new ArrayList<VistaDependenciaTarea>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
				lvdt = ejecutarQuery(lvdt, conn, query);
			}
		} catch (Exception e) {
			lvdt = new ArrayList<VistaDependenciaTarea>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return lvdt;
	}
	
	public static List<VistaDependenciaTarea> ejecutarQuery(List<VistaDependenciaTarea> dependenciaTareas, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		VistaDependenciaTarea dependenciaTarea = new VistaDependenciaTarea();
		boolean yaHePasado = false;
		int ultimoNumeroUsado = 0;
		while (rs.next()) {
			if(ultimoNumeroUsado!=rs.getInt(CAMPOS.TAREA_DEPENDE)){
				if(yaHePasado) dependenciaTareas.add(dependenciaTarea);
				dependenciaTarea = new VistaDependenciaTarea(rs.getInt(CAMPOS.TAREA_DEPENDE));
			}
			pasarDatosDelResultSetADependencia(dependenciaTarea, rs);
			ultimoNumeroUsado = dependenciaTarea.getIdTareaDepende();
			yaHePasado = true;
		}
		if(yaHePasado)
			dependenciaTareas.add(dependenciaTarea);
		return dependenciaTareas;
	}
	
	private static void pasarDatosDelResultSetADependencia(VistaDependenciaTarea dt, ResultSet rs) throws SQLException{
		dt.addDependencia(rs.getInt(CAMPOS.TAREA_DEPENDIENTE));
	}
	
	public boolean guardarVistaDependenciaTarea() { 
		boolean guardado = false;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(this.dependencias.size()>0){
			try {
				if(this.conn==null)
					this.conn = Conexion.obtenerConexion();
				PreparedStatement ps = null;
				
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.TAREA_DEPENDIENTE+","+CAMPOS.TAREA_DEPENDE+") "+
						"VALUES ("+this.dependencias.get(0)+","+this.idTareaDepende+")";
				for(int i=1;i<this.dependencias.size();i++){
					query += ",("+this.dependencias.get(i)+","+this.idTareaDepende+")";
				}
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				if (ps.executeUpdate() > 0) {
					guardado = true;
				}
			} catch (Exception e) {
				guardado = false;
				System.out.println("Error guardando el usuario: " + e.getMessage());
			}
		}
		
		return guardado;
	}
}
