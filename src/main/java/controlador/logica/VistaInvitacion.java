package controlador.logica;

import modelos.clases.Aviso;

public class VistaInvitacion extends Aviso{
	/*private String nombreInvita;
	private String nombreGrupo;
	private String nombreInvitado;
	
	public String getNombreInvita() {return nombreInvita;}
	public void setNombreInvita(String nombreInvita) {this.nombreInvita = nombreInvita;}
	
	public String getNombreGrupo() {return nombreGrupo;}
	public void setNombreGrupo(String nombreGrupo) {this.nombreGrupo = nombreGrupo;}
	
	public String getNombrePermisos(){return "";}
	
	public String getNombreInvitado() {return nombreInvitado;}
	public void setNombreInvitado(String nombreInvitado) {this.nombreInvitado = nombreInvitado;}

	private static final String U1 = "u1.";
	private static final String U2 = "u2.";
	private static final String G = "g.";
	private static final String I = "i.";
	
	public static List<VistaInvitacion> obtenerInvitacionesDeUnUsuario(int idUsuario, Connection conn){
		return obtenerInvitacionesDeUnUsuarioOGrupo(idUsuario, false, conn);
	}
	
	private static List<VistaInvitacion> obtenerInvitacionesDeUnUsuarioOGrupo(int idUsuarioOIdGrupo, boolean esGrupo, Connection conn){
		List<VistaInvitacion> lista = new ArrayList<VistaInvitacion>();
		try{
			if(conn==null) conn = Conexion.obtenerConexion();
			String query = "SELECT "+I+CAMPOS.ID_AVISO+","+I+CAMPOS.MENSAJE+","+I+CAMPOS.INVITADO+","+I+CAMPOS.AVISADO+","+
				I+CAMPOS.PERMISOS+","+U1+Usuario.CAMPOS.LOGIN+","+U2+Usuario.CAMPOS.LOGIN+","+G+Grupo.CAMPOS.NOMBRE_GRUPO+
				" FROM "+CAMPOS.TABLE_NAME+" i,"+
				Usuario.CAMPOS.NOMBRE_TABLA+" u1,"+Usuario.CAMPOS.NOMBRE_TABLA+" u2,"+Grupo.CAMPOS.NOMBRE_TABLA+" g WHERE "+
				I+CAMPOS.MENSAJE+"="+U1+Usuario.CAMPOS.ID_USUARIO+" AND "+I+CAMPOS.INVITADO+"="+U2+Usuario.CAMPOS.ID_USUARIO+
				" AND "+I+CAMPOS.AVISADO+"="+G+Grupo.CAMPOS.ID_GRUPO+" AND ";
			if(esGrupo)
				query += I+CAMPOS.AVISADO+"="+idUsuarioOIdGrupo;
			else
				query += I+CAMPOS.INVITADO+"="+idUsuarioOIdGrupo;
			ejecutarQueryVistaInvitacion(lista, conn, query);
			return lista;
		} catch (Exception e) {
			return new ArrayList<VistaInvitacion>();
		}
	}
	
	public static void ejecutarQueryVistaInvitacion(List<VistaInvitacion> vistaInvitaciones, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			VistaInvitacion vistaInvitacion = new VistaInvitacion();
			pasarDatosDelResultSetAVistaInvitacion(vistaInvitacion, rs);
			vistaInvitaciones.add(vistaInvitacion);
		}
	}
	
	private static void pasarDatosDelResultSetAVistaInvitacion(VistaInvitacion vi, ResultSet rs) throws SQLException{
		pasarDatosDelResultSetAlInvitacion(vi,I, rs);
		vi.setNombreGrupo(rs.getString(G+Grupo.CAMPOS.NOMBRE_GRUPO));
		vi.setNombreInvita(rs.getString(U1+Usuario.CAMPOS.LOGIN));
		vi.setNombreInvitado(rs.getString(U2+Usuario.CAMPOS.LOGIN));
	}
	public static Object obtenerInvitacionesDeUnGrupo(int idGrupo,
			Connection conn) {
		return obtenerInvitacionesDeUnUsuarioOGrupo(idGrupo, true, conn);
	}*/
}
