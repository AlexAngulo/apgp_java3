package controlador.logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Grupo;
import modelos.clases.MiembroGrupo;
import modelos.clases.Usuario;
import modelos.clasesAbstractas.IntMiembroGrupo;
import modelos.enums.TopePermisos;
import modelos.vistas.MiembroGrupoCompleto;

public class LogicaMiembroGrupo {
	public static boolean agregarMiembro(int idUsuario, int idGrupo, boolean[] permisos,Connection conn) {
		MiembroGrupo mg = new MiembroGrupo(idUsuario,idGrupo,permisos);
		mg.setConn(conn);
		return mg.guardarMiembroGrupo();
	}
	
	public static List<MiembroGrupoCompleto> obtenerMiembrosPorIdGrupo(int idGrupo,Connection conn) {
		return MiembroGrupoCompleto.busquedaAvanzadaUsuarioPermisos(" WHERE "+MiembroGrupo.CAMPOS_MG.ID_GRUPO+"="+idGrupo,TopePermisos.ObtenerOrderByPermisos(),conn);
	}
	
	public static List<Grupo> obtenerGruposPorIdUsuario(int idUsuario,Connection conn) {
		List<Grupo> grupos = new ArrayList<Grupo>();
		List<MiembroGrupo> mgs = MiembroGrupo.busquedaAvanzadaMiembroGrupo(" WHERE "+MiembroGrupo.CAMPOS_MG.ID_USUARIO+"="+idUsuario,"",conn);
		for (MiembroGrupo mg : mgs) {
			grupos.add(new Grupo(mg.getIdGrupo(),conn));
		}
		return grupos;
	}

	public static List<MiembroGrupoCompleto> obtenerNoMiembrosPorIdGrupo(int idGrupo,Connection conn) {
		return MiembroGrupoCompleto.busquedaAvanzadaUsuarioPermisos(" WHERE (u."+Usuario.CAMPOS.ID_USUARIO+"="+MiembroGrupo.CAMPOS_MG.ID_USUARIO+" AND "+
				MiembroGrupo.CAMPOS_MG.ID_GRUPO+"!="+idGrupo+") OR u."+Usuario.CAMPOS.ID_USUARIO+"!="+MiembroGrupo.CAMPOS_MG.ID_USUARIO,
				" ORDER BY u."+Usuario.CAMPOS.LOGIN+" ASC",conn);
	}
	
	public static List<MiembroGrupoCompleto> obtenerNoMiembrosPorIdGrupoBusqueda(int idGrupo,String busqueda,Connection conn) {
		return MiembroGrupoCompleto.busquedaAvanzadaUsuarioPermisos(" WHERE ((u."+Usuario.CAMPOS.ID_USUARIO+"="+MiembroGrupo.CAMPOS_MG.ID_USUARIO+" AND "+
				MiembroGrupo.CAMPOS_MG.ID_GRUPO+"!="+idGrupo+") OR u."+Usuario.CAMPOS.ID_USUARIO+"!="+MiembroGrupo.CAMPOS_MG.ID_USUARIO+")"+
				" AND u."+Usuario.CAMPOS.LOGIN+" like '%"+busqueda+"%'",
				" ORDER BY u."+Usuario.CAMPOS.LOGIN+" ASC",conn);
	}
	
	public static void pasarDatosDelResultSetAlMiembroGrupo(MiembroGrupo mg, ResultSet rs) throws SQLException{
		mg.setIdUsuario(rs.getInt(MiembroGrupo.CAMPOS_MG.ID_USUARIO));
		mg.setIdGrupo(rs.getInt(MiembroGrupo.CAMPOS_MG.ID_GRUPO));
		IntMiembroGrupo.pasarDatosDelResultSetAlIntMiembroGrupo(mg,rs);
	}
	
	/*public static MiembroGrupo obtenerMiembroGrupo(int idUsuario, int idGrupo,Connection conn){
		return new MiembroGrupo(idUsuario, idGrupo,conn);
	}
	
	public static boolean usuarioPuedeEliminarGrupo(int idUsuario, int idGrupo,Connection conn){
		return obtenerMiembroGrupo(idUsuario,idGrupo,conn).getPermisos()>TopePermisos.GRUPO_ELIMINAR_MIEMBROS;
	}
	
	public static boolean usuarioPuedeEditarGrupo(int idUsuario, int idGrupo,Connection conn){
		return obtenerMiembroGrupo(idUsuario,idGrupo,conn).getPermisos()>TopePermisos.GRUPO_EDITAR_MIEMBROS;
	}*/
}
