package controlador.logica;

import java.sql.ResultSet;
import java.sql.SQLException;

import modelos.clases.Equipo;
import modelos.clases.Equipo.CAMPOS;

public class LogicaEquipo {
	public static void pasarDatosDelResultSetAlGrupo(String pre, Equipo e,
			ResultSet rs) throws SQLException {
		e.setIdEquipo(rs.getInt(CAMPOS.ID_EQUIPO));
		e.setIdGrupo(rs.getInt(CAMPOS.GRUPO));
		e.setNombreEquipo(rs.getString(CAMPOS.NOMBRE_EQUIPO));
		e.setDescEquipo(rs.getString(CAMPOS.DESC_EQUIPO));
	}
}
