package controlador.logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Grupo;
import modelos.clases.PermisoProyecto;
import modelos.clases.Proyecto;
import modelos.clases.ProyectoCompartido;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;
import modelos.utiles.Orden;

public class VistaProyecto extends Proyecto {
	private String nombreCreador;
	private String nombreGrupoPerteneciente;
	
	public String getNombreCreador() {return nombreCreador;}
	public void setNombreCreador(String nombreCreador) {this.nombreCreador = nombreCreador;}
	
	public String getNombreGrupoPerteneciente() {return nombreGrupoPerteneciente;}
	public void setNombreGrupoPerteneciente(String nombreGrupoPerteneciente) {this.nombreGrupoPerteneciente = nombreGrupoPerteneciente;}
	
	public VistaProyecto(){
		super();
		this.nombreCreador = null;
		this.nombreGrupoPerteneciente = null;
	}
	
	private boolean crearTareas = false;
	private boolean editarTareas = false;
	private boolean eliminarTareas = false;
	private boolean editarProyecto = false;
	private boolean eliminarProyecto = false;
	private boolean compartir = false;
	private boolean editarCompartir = false;
	private boolean eliminarCompartir = false;
	private boolean asignarTarea = false;
	private boolean editarAsignar = false;
	private boolean eliminarAsignar = false;
	
	private List<PermisoProyecto> compartidosA = null;
	
	public List<PermisoProyecto> getCompartidosA() {return compartidosA;}
	public void setCompartidosA(List<PermisoProyecto> compartidosA) {this.compartidosA = compartidosA;}
	
	public boolean isCrearTareas() {return crearTareas;}
	public void setCrearTareas(boolean crearTareas) {this.crearTareas = crearTareas;}
	
	public boolean isEditarTareas() {return editarTareas;}
	public void setEditarTareas(boolean editarTareas) {this.editarTareas = editarTareas;}
	
	public boolean isEliminarTareas() {return eliminarTareas;}
	public void setEliminarTareas(boolean eliminarTareas) {this.eliminarTareas = eliminarTareas;}
	
	public boolean isEditarProyecto() {return editarProyecto;}
	public void setEditarProyecto(boolean editarProyecto) {this.editarProyecto = editarProyecto;}
	
	public boolean isEliminarProyecto() {return eliminarProyecto;}
	public void setEliminarProyecto(boolean eliminarProyecto) {this.eliminarProyecto = eliminarProyecto;}
	
	public boolean isCompartir() {return compartir;}
	public void setCompartir(boolean compartir) {this.compartir = compartir;}

	public boolean isEditarCompartir() {return editarCompartir;}
	public void setEditarCompartir(boolean editarCompartir) {this.editarCompartir = editarCompartir;}

	public boolean isEliminarCompartir() {return eliminarCompartir;}
	public void setEliminarCompartir(boolean eliminarCompartir) {this.eliminarCompartir = eliminarCompartir;}

	public boolean isAsignarTarea() {
		return asignarTarea;
	}
	public void setAsignarTarea(boolean asignarTarea) {
		this.asignarTarea = asignarTarea;
	}
	public boolean isEditarAsignar() {
		return editarAsignar;
	}
	public void setEditarAsignar(boolean editarAsignar) {
		this.editarAsignar = editarAsignar;
	}
	public boolean isEliminarAsignar() {
		return eliminarAsignar;
	}
	public void setEliminarAsignar(boolean eliminarAsignar) {
		this.eliminarAsignar = eliminarAsignar;
	}

	private final static String P = "p.";
	private final static String U = "u.";
	private final static String G = "g.";
	private final static String PC = "pc.";
	
	private final static String selectProyecto = P+CAMPOS.ID_PROYECTO+","+P+CAMPOS.CREADOR+","+P+CAMPOS.GRUPO_PERTENECIENTE+","+
		P+CAMPOS.NOMBRE_PROYECTO+","+P+CAMPOS.DESC_PROYECTO+","+P+CAMPOS.FECHA_FIN+","+P+CAMPOS.ES_PLANTILLA+","+P+CAMPOS.ORDEN_PROYECTO+","+
		P+CAMPOS.ORDENACION_TAREAS+","+P+CAMPOS.COMPLETADO+","+P+CAMPOS.DURACION+","+P+CAMPOS.FECHA_FIN_PREVISTA+","+P+CAMPOS.PRIORIDAD;
	private final static String selectTodo = "SELECT "+selectProyecto+","+U+Usuario.CAMPOS.LOGIN+","+G+Grupo.CAMPOS.NOMBRE_GRUPO+" FROM "+
	CAMPOS.NOMBRE_TABLA+" p,"+Usuario.CAMPOS.NOMBRE_TABLA+" u,"+Grupo.CAMPOS.NOMBRE_TABLA+" g ";
	protected final static String SELECT = "SELECT "+selectProyecto+","+U+Usuario.CAMPOS.LOGIN;
	protected final static String FROM = " FROM "+CAMPOS.NOMBRE_TABLA+" p,"+Usuario.CAMPOS.NOMBRE_TABLA+" u";
	protected final static String WHERE = " WHERE "+P+CAMPOS.CREADOR+"="+U+Usuario.CAMPOS.ID_USUARIO+
			" AND "+P+CAMPOS.ES_PLANTILLA+"=0";
	public final static String selectProyectoCompartido = PC+ProyectoCompartido.CAMPOS.USUARIO+","+PC+ProyectoCompartido.CAMPOS.PROYECTO+
			","+PC+ProyectoCompartido.CAMPOS.COMPARTIR+","+PC+ProyectoCompartido.CAMPOS.CREAR_TAREAS+
			","+PC+ProyectoCompartido.CAMPOS.EDITAR_PROYECTO+","+PC+ProyectoCompartido.CAMPOS.EDITAR_TAREAS+
			","+PC+ProyectoCompartido.CAMPOS.ELIMINAR_PROYECTO+","+PC+ProyectoCompartido.CAMPOS.ELIMINAR_TAREAS+
			","+PC+ProyectoCompartido.CAMPOS.EDITAR_COMPARTIR+","+PC+ProyectoCompartido.CAMPOS.ELIMINAR_COMPARTIR+
			","+PC+ProyectoCompartido.CAMPOS.ASIGNAR_TAREAS+","+PC+ProyectoCompartido.CAMPOS.EDITAR_ASIGNAR+
			","+PC+ProyectoCompartido.CAMPOS.ELIMINAR_ASIGNAR;
	
	public VistaProyecto(int idProyecto, Connection conn){
		this.setConn(conn);
		this.obtenerProyectoPorId(idProyecto, false);
	}
	
	public VistaProyecto(int idProyecto, boolean esPlantilla, Connection conn){
		this.setConn(conn);
		this.obtenerProyectoPorId(idProyecto, esPlantilla);
	}
	
	public boolean obtenerProyectoPorId(int idProyecto, boolean esPlantilla) {
		boolean encontrado = false;
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo+" WHERE "+CAMPOS.ID_PROYECTO+"="+idProyecto+
					" AND "+P+CAMPOS.CREADOR+"="+U+Usuario.CAMPOS.ID_USUARIO+" AND "+
					P+CAMPOS.GRUPO_PERTENECIENTE+"="+G+Grupo.CAMPOS.ID_GRUPO+" AND "+P+CAMPOS.ES_PLANTILLA+"=";
				if(esPlantilla)
					query +=1;
				else
					query +=0;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					pasarDatosDelResultSetAVistaProyecto(this, rs);
					encontrado = true;
				} else {
					query = SELECT+FROM+" WHERE "+CAMPOS.ID_PROYECTO+"="+idProyecto+
							" AND "+P+CAMPOS.CREADOR+"="+U+Usuario.CAMPOS.ID_USUARIO+
							" AND "+P+CAMPOS.ES_PLANTILLA+"=";
						if(esPlantilla)
							query +=1;
						else
							query +=0;
					ps = conn.prepareStatement(query);
					rs = ps.executeQuery();
					if (rs.next()) {
						pasarDatosDelResultSetAVistaSubProyecto(this, rs);
						encontrado = true;
					}
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	
	public static List<VistaProyecto> obtenerTodasLasVistaProyecto(int codOrdenacion, boolean esPlantilla, Connection conn){
		List<VistaProyecto> tareas = new ArrayList<VistaProyecto>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = SELECT + FROM + " WHERE "+P+CAMPOS.CREADOR+"="+U+Usuario.CAMPOS.ID_USUARIO + " AND "+
					P+CAMPOS.ES_PLANTILLA+"=";
				if(esPlantilla)
					query += 1;
				else
					query += 0;
				query += Orden.obtenerOrderByProyectos(codOrdenacion, P);
				ejecutarQuery(tareas, conn, query,false);
			}
		} catch (Exception e) {
			tareas = new ArrayList<VistaProyecto>();
			System.out.println("Error en busqueda avanzada vistaProyecto: " + e.getMessage());
		}
		
		return tareas;
	}
	
	public static List<VistaProyecto> busquedaAvanzadaVistaProyecto(String where, String orderBy,Connection conn){
		List<VistaProyecto> tareas = new ArrayList<VistaProyecto>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
				ejecutarQuery(tareas, conn, query,false);
			}
		} catch (Exception e) {
			tareas = new ArrayList<VistaProyecto>();
			System.out.println("Error en busqueda avanzada vistaProyecto: " + e.getMessage());
		}
		
		return tareas;
	}
	
	public static void ejecutarQuery(List<VistaProyecto> tareas, Connection conn,
			String query,boolean esCompartido) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			VistaProyecto proyecto= new VistaProyecto();
			if(esCompartido)
				pasarDatosDelResultSetAVistaProyectoCompartido(proyecto, rs);
			else
				pasarDatosDelResultSetAVistaProyecto(proyecto, rs);
			tareas.add(proyecto);
		}
	}
	
	public static void pasarDatosDelResultSetAVistaSubProyecto(VistaProyecto proyecto, ResultSet rs) throws SQLException{
		proyecto.setIdProyecto(rs.getInt(P+CAMPOS.ID_PROYECTO));
		proyecto.setDescProyecto(rs.getString(P+CAMPOS.DESC_PROYECTO));
		proyecto.setNombreProyecto(rs.getString(P+CAMPOS.NOMBRE_PROYECTO));
		proyecto.setIdGrupoPerteneciente(rs.getInt(P+CAMPOS.GRUPO_PERTENECIENTE));
		proyecto.setIdCreador(rs.getInt(P+CAMPOS.CREADOR));
		proyecto.setFechaFin(rs.getDate(P+CAMPOS.FECHA_FIN));
		proyecto.setOrdenProyecto(rs.getInt(P+CAMPOS.ORDEN_PROYECTO));
		proyecto.setOrdenacionTareas(rs.getInt(P+CAMPOS.ORDENACION_TAREAS));
		proyecto.setCompletado(rs.getInt(P+CAMPOS.COMPLETADO));
		proyecto.setDuracion(rs.getInt(P+CAMPOS.DURACION));
		proyecto.setEsPlantilla(rs.getBoolean(P+CAMPOS.ES_PLANTILLA));
		//proyecto.setFechaFinPrevista(rs.getDate(P+CAMPOS.FECHA_FIN_PREVISTA));
		proyecto.setNombreCreador(rs.getString(U+Usuario.CAMPOS.LOGIN));
		proyecto.setPrioridad(rs.getInt(P+CAMPOS.PRIORIDAD));
	}
	
	public static void pasarDatosDelResultSetAVistaProyecto(VistaProyecto proyecto, ResultSet rs) throws SQLException{
		pasarDatosDelResultSetAVistaSubProyecto(proyecto, rs);
		try{
		proyecto.setNombreGrupoPerteneciente(rs.getString(G+Grupo.CAMPOS.NOMBRE_GRUPO));
		} catch(Exception e){
			
		}
	}
	
	public static void pasarDatosDelResultSetAVistaProyectoCompartido(VistaProyecto proyecto, ResultSet rs) throws SQLException{
		pasarDatosDelResultSetAVistaProyecto(proyecto, rs);
		proyecto.setCrearTareas(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.CREAR_TAREAS));
		proyecto.setEditarTareas(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_TAREAS));
		proyecto.setEliminarTareas(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_TAREAS));
		proyecto.setEditarProyecto(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_PROYECTO));
		proyecto.setEliminarProyecto(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_PROYECTO));
		proyecto.setCompartir(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.COMPARTIR));
		proyecto.setEditarCompartir(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_COMPARTIR));
		proyecto.setEliminarCompartir(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_COMPARTIR));
		proyecto.setAsignarTarea(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ASIGNAR_TAREAS));
		proyecto.setEditarAsignar(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_ASIGNAR));
		proyecto.setEliminarAsignar(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_ASIGNAR));
		proyecto.setCompartidosA(PermisoProyecto.obtenerPermisosGrupoDeProyecto(proyecto.getIdProyecto(), proyecto.getConn()));
	}
	public static List<VistaProyecto> obtenerProyectosCompartidosCon(int idUsuario,
			int codOrdenacion, Connection conn) {
		String query = SELECT+","+selectProyectoCompartido+FROM+","+ProyectoCompartido.CAMPOS.NOMBRE_TABLA+" pc "+WHERE+
				" AND "+PC+ProyectoCompartido.CAMPOS.PROYECTO+"="+P+Proyecto.CAMPOS.ID_PROYECTO+
				" AND "+PC+ProyectoCompartido.CAMPOS.USUARIO+"="+idUsuario+" "+Orden.obtenerOrderByProyectos(codOrdenacion,P);
		List <VistaProyecto> vproyectos = new ArrayList<VistaProyecto>();
		try {
			ejecutarQuery(vproyectos,conn,query,true);
		} catch (SQLException e) {
			vproyectos = new ArrayList<VistaProyecto>();
			System.out.println("Error en obtener proyectos compartidos vistaProyecto: " + e.getMessage());
		}
		return vproyectos;
	}
	public static List<VistaProyecto> busquedaAvanzadaVistaProyectoSinGrupo(
			String where, String orderBy, Connection conn) {
		List<VistaProyecto> lista = new ArrayList<VistaProyecto>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = SELECT+FROM+WHERE+where+orderBy;
				PreparedStatement ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				while(rs.next()) {
					VistaProyecto vp = new VistaProyecto();
					pasarDatosDelResultSetAVistaSubProyecto(vp, rs);
					vp.setCompartidosA(PermisoProyecto.obtenerPermisosGrupoDeProyecto(vp.getIdProyecto(), conn));
					lista.add(vp);
				}
			}
		} catch (Exception e) {
			lista = new ArrayList<VistaProyecto>();
			System.out.println("Error en busqueda avanzada sin grupo vistaProyecto: " + e.getMessage());
		}
		
		return lista;
	}
}
