package controlador.clases;

import java.sql.Connection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelos.clases.Aviso;
import modelos.clases.Equipo;
//import modelos.clases.MiembroEquipo;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;
import modelos.vistas.MiembroGrupoCompleto;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import controlador.utiles.Exc;
import controlador.utiles.UtilesSecurity;

@Controller
public class MiembrosEquipo {	
	/**
	 * Crea el miembro de un equipo si el usuario de la sesi�n tiene privilegios para ello
	 * @param sesion Sesi�n del usuario
	 * @param login Login del nuevo miembro
	 * @param idGrupo Id del grupo al que pertenece el equipo
	 * @param idEquipo Id del equipo del cual se quiere crear la membres�a
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/grupos/{idGrupo}/equipos/{idEquipo}/miembros", method=RequestMethod.POST)
    protected @ResponseBody String invitarMiembro(HttpSession sesion, @RequestBody String login, 
    		@PathVariable("idGrupo")String idGrupo,@PathVariable("idEquipo")String idEquipo, HttpServletResponse res) throws Exception {
		try {
			if(!StringUtils.isNumeric(idGrupo) || !StringUtils.isNumeric(idEquipo)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			int codEquipo = Integer.parseInt(idEquipo);
			Connection conn = Conexion.obtenerConexion();
			MiembroGrupoCompleto mgc = UtilesSecurity.usuarioPuedeCrearEquipo(codGrupo, sesion, conn);
			if(mgc==null || mgc.getUsuario()==null || mgc.getGrupo()==null){
				mgc = UtilesSecurity.usuarioPuedeEditarEquipo(codGrupo, sesion, conn);
				if(mgc==null || mgc.getUsuario()==null || mgc.getGrupo()==null){
					conn.close();res.setStatus(401); return Exc.status401;
				}
			}
			Usuario u = new Usuario();
			u.setConn(conn);
			if(!u.obtenerUsuarioPorNombreDeUsuario(login)){
				conn.close();res.setStatus(404); return"Usuario no encontrado";
			}
			//if(MiembroEquipo.guardarNuevoMiembro(u.getIdUsuario(),codEquipo,conn)){
				Equipo e = new Equipo(codEquipo,conn);
				Usuario usuario = mgc.getUsuario();
				if(usuario.getIdUsuario()!=u.getIdUsuario()){
					Aviso.guardarNuevoAviso(u.getIdUsuario(), usuario.getLogin()+" te ha invitado a formar parte del equipo "+e.getNombreEquipo()+" en el grupo "+mgc.getGrupo().getNombreGrupo(), conn);
				}
				conn.close();
				res.setStatus(201);
				return "1";
			//}else{
				/*conn.close();
				res.setStatus(500); return Exc.status500;*/
			//}
		}catch(Exception e){
			res.setStatus(500); return Exc.status500;
		}
    }  
	
	/**
	 * Elimina el miembro de un equipo si el usuario de la sesi�n tiene privilegios para ello
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Id del grupo al que pertenece el equipo
	 * @param idEquipo Id del equipo del que se quiere eliminar la membres�a
	 * @param login Login del usuario del que se quiere eliminar la membres�a del equipo
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/grupos/{idGrupo}/equipos/{idEquipo}/miembros",method=RequestMethod.DELETE)
	protected @ResponseBody String echarMiembro(HttpSession sesion,@PathVariable("idGrupo")String idGrupo,
    		@PathVariable("idEquipo")String idEquipo, @RequestBody String login, HttpServletResponse res) throws Exception {
		//jsonContent: login
		try {
			if(!StringUtils.isNumeric(idGrupo) || !StringUtils.isNumeric(idEquipo)){return"0";/*TODO:status invalido 403 o 404*/}
			int codGrupo = Integer.parseInt(idGrupo);
			int codEquipo = Integer.parseInt(idEquipo);
			Connection conn = Conexion.obtenerConexion();
			MiembroGrupoCompleto mgc = UtilesSecurity.usuarioPuedeEditarEquipo(codGrupo, sesion, conn);
			if(mgc==null || mgc.getUsuario()==null || mgc.getGrupo()==null){
				conn.close();res.setStatus(401); return Exc.status401;
			}
			Usuario u = new Usuario();
			u.setConn(conn);
			if(!u.obtenerUsuarioPorNombreDeUsuario(login)){
				conn.close();res.setStatus(404); return "No existe el usuario";
			}
			/*if(MiembroEquipo.eliminarMiembroEquipo(u.getIdUsuario(),codEquipo,conn)){
				conn.close();
				return "1";
			}else{
				//TODO: devolver status 201 (creado)
				conn.close();
				res.setStatus(500); return Exc.status500;
			}*/
		}catch(Exception e){
			res.setStatus(500); return Exc.status500;
		}
		return login;
    } 
}
