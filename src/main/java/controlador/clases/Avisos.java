package controlador.clases;

import java.sql.Connection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelos.clases.Aviso;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;








import controlador.utiles.Exc;
import controlador.utiles.UtilesSecurity;

@Controller
public class Avisos {
	
	/**
	 * Elimina un aviso con un determinado ID
	 * @param sesion Sesi�n del usuario
	 * @param idAviso Id del Aviso a eliminar
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/avisos/{idAviso}", method=RequestMethod.DELETE)
    protected @ResponseBody String aceptarORechazarInvitacion(HttpSession sesion, @PathVariable("idAviso")String idAviso, HttpServletResponse res) throws Exception {
		Connection conn = null;
		try {
			idAviso = idAviso.replaceAll(".htm","");
			if(!StringUtils.isNumeric(idAviso)){res.setStatus(403);return Exc.status403;}
			conn = Conexion.obtenerConexion();
			int codAviso = Integer.parseInt(idAviso);
			Aviso aviso = new Aviso(codAviso,conn);
			if(aviso.getIdAviso()==0){conn.close();res.setStatus(404);return "Aviso no encontrado";}
			Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
			if(usuario.getIdUsuario()!=aviso.getIdAvisado()){conn.close();res.setStatus(401);return Exc.status401;}
			
			if(Aviso.eliminarAviso(codAviso, conn)){
				conn.close();
				return "1";
			} else {
				conn.close();
				res.setStatus(500);
				return Exc.status500;
			}
		} catch(Exception e){
			try {
			conn.close();
			} catch(Exception ex){
				res.setStatus(500);
				return Exc.status500;
			}
			res.setStatus(500);
			return Exc.status500;
		}
    }
}
