package controlador.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Grupo;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;

public class VistaGrupo extends Grupo{
	private static final long serialVersionUID = 1L;
	private String nombreCreador = "";
	
	public VistaGrupo(){
		super();
		setNombreCreador("");
	}
	
	public VistaGrupo(int idGrupo, Connection conn){
		this.setConn(conn);
		this.obtenerVistaGrupoPorId(idGrupo);
	}

	public String getNombreCreador() {return nombreCreador;}
	public void setNombreCreador(String nombreCreador) {this.nombreCreador = nombreCreador;}

	public String getBreveDesc(){
		if(this.getDescGrupo().length()>30){
			return this.getDescGrupo().substring(0, 27).concat("...");
		} else {
			return this.getDescGrupo();
		}
	}
	
	private final static String selectTodo = "SELECT "+ selectGrupo + ","+Usuario.CAMPOS.LOGIN+
			" FROM "+CAMPOS.NOMBRE_TABLA +","+Usuario.CAMPOS.NOMBRE_TABLA;
	
	public boolean obtenerVistaGrupoPorId(int idGrupo) {
		boolean encontrado = false;
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (this.conn != null) {
				ResultSet rs = devolverResultSet(selectTodo+" WHERE "+Usuario.CAMPOS.ID_USUARIO+"="+CAMPOS.CREADOR+" AND "+
						CAMPOS.ID_GRUPO+"="+idGrupo, this.conn);
				if (rs.next()) {
					pasarDatosDelResultSetAVistaGrupo(this, rs);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		} 
		
		return encontrado;
	}
	
	public static void pasarDatosDelResultSetAVistaGrupo(VistaGrupo vistaGrupo, ResultSet rs) throws SQLException{
		vistaGrupo.setIdGrupo(rs.getInt(Grupo.CAMPOS.ID_GRUPO));
		vistaGrupo.setIdCreador(rs.getInt(Grupo.CAMPOS.CREADOR));
		vistaGrupo.setIdSupergrupo(rs.getInt(CAMPOS.SUPERGRUPO));
		vistaGrupo.setNombreGrupo(rs.getString(CAMPOS.NOMBRE_GRUPO));
		vistaGrupo.setDescGrupo(rs.getString(CAMPOS.DESC_GRUPO));
		vistaGrupo.setNombreCreador(rs.getString(Usuario.CAMPOS.LOGIN));
	}

	public static List<VistaGrupo> busquedaAvanzadaVistaGrupo(String where,
			String orderBy, Connection conn) {
		List<VistaGrupo> vistaGrupos = new ArrayList<VistaGrupo>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where;
				if(where.length()>0)
					query += " AND ";
				else
					query += " WHERE ";
				query += CAMPOS.CREADOR+"="+Usuario.CAMPOS.ID_USUARIO+ orderBy;
				ejecutarQueryVistaGrupo(vistaGrupos, conn, query);
			}
		} catch (Exception e) {
			vistaGrupos = new ArrayList<VistaGrupo>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return vistaGrupos;
	}

	private static void ejecutarQueryVistaGrupo(List<VistaGrupo> vistaGrupos,
			Connection conn, String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			VistaGrupo vistaGrupo = new VistaGrupo();
			pasarDatosDelResultSetAVistaGrupo(vistaGrupo, rs);
			vistaGrupos.add(vistaGrupo);
		}
	}
}
