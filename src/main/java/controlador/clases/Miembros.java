package controlador.clases;

import java.sql.Connection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelos.clases.Aviso;
import modelos.clases.Grupo;
import modelos.clases.MiembroGrupo;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;
import modelos.enums.TopePermisos;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import controlador.logica.LogicaAviso;
import controlador.logica.LogicaMiembroGrupo;
import controlador.utiles.Exc;
import controlador.utiles.UtilesSecurity;

@Controller
public class Miembros {
	/**
	 * Edita la Membres�a del miembro de un grupo en caso de que el usuario de la sesi�n tenga esos privilegios
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Id del Grupo del cual se quiere editar la Membres�a
	 * @param jsonContent Objeto MiembroGrupo en formato JSON
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/miembros/{idGrupo}",method=RequestMethod.PUT)
	protected @ResponseBody String editarMembresia(HttpSession sesion, @PathVariable("idGrupo")String idGrupo, @RequestBody String jsonContent
			,HttpServletResponse res) throws Exception {
		//jsonContent: permisos, login
		Connection conn = null;
		try {
			Gson gson = new Gson();
			JsonObject jsonObject = gson.fromJson( jsonContent, JsonObject.class);
			boolean[] permisos = new boolean[TopePermisos.CANTIDAD_PERMISOS];
			for(int i=1;i<=TopePermisos.CANTIDAD_PERMISOS;i++){
				if(jsonObject.has("permiso"+i)){
					permisos[i-1] = jsonObject.get("permiso"+i).getAsBoolean();
				} else {
					permisos[i-1] = false;
				}
			}
			
			String loginMiembro = jsonObject.get("login").getAsString();
			if(!StringUtils.isNumeric(idGrupo) || StringUtils.isBlank(loginMiembro)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			conn = Conexion.obtenerConexion();
			Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
			if(usuario==null){conn.close();return"0";}
			Usuario invitado = new Usuario(loginMiembro,conn);
			if(invitado.getIdUsuario()==0){conn.close();res.setStatus(403);return Exc.status403;}
			MiembroGrupo mgUsuario = new MiembroGrupo(usuario.getIdUsuario(),codGrupo,conn);
			boolean editarMiembros;
			if(usuario.isEsAdministrador()){
				editarMiembros = true;
			}else{
				editarMiembros = mgUsuario.isEditarMiembros();

			}if(!editarMiembros){conn.close();return"0";}
			MiembroGrupo mg = new MiembroGrupo(invitado.getIdUsuario(),codGrupo,conn);
			if((new Grupo(mg.getIdGrupo(),conn)).getIdCreador()==invitado.getIdUsuario()){conn.close();res.setStatus(401);return Exc.status401;}
				mg.setPermisos(permisos);
			if(mg.editarMiembroGrupo()){
				conn.close();
				return TopePermisos.obtenerNombrePermisoMasAlto(permisos);
			} else {
				conn.close();
				res.setStatus(500);return Exc.status500;
			}
		} catch(Exception e){
			conn.close();
			res.setStatus(500);return Exc.status500;
		}
    } 
	
	/**
	 * Elimina la membres�a del miembro de un grupo en caso de que el usuario de la sesi�n tenga esos privilegios
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Id del grupo del que se quiere eliminar la membres�a
	 * @param loginMiembro Login del miembro del que se quiere eliminar la membres�a
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/miembros/{idGrupo}",method=RequestMethod.DELETE)
	protected @ResponseBody String echarMiembro(HttpSession sesion, @PathVariable("idGrupo")String idGrupo, @RequestBody String loginMiembro
			,HttpServletResponse res) throws Exception {
		Connection conn = null;
		try {
			if(!StringUtils.isNumeric(idGrupo) || StringUtils.isBlank(loginMiembro)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			conn = Conexion.obtenerConexion();
			Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
			if(usuario==null){conn.close();res.setStatus(401);return Exc.status401;}
			Usuario invitado = new Usuario(loginMiembro,conn);
			if(invitado.getIdUsuario()==0){conn.close();res.setStatus(403);return Exc.status403;}
			MiembroGrupo mgUsuario = new MiembroGrupo(usuario.getIdUsuario(),codGrupo,conn);
			boolean eliminarMiembro;
			if(usuario.isEsAdministrador())
				eliminarMiembro = true;
			else
				eliminarMiembro = mgUsuario.isEliminarMiembros();
			if(!eliminarMiembro){conn.close();res.setStatus(401);return Exc.status401;}
			MiembroGrupo mg = new MiembroGrupo(invitado.getIdUsuario(),codGrupo,conn);
			if((new Grupo(mg.getIdGrupo(),conn)).getIdCreador()==invitado.getIdUsuario()){conn.close();res.setStatus(400);return"Imposible borrar al creador";}
			if(mg.eliminarGrupo()){
				conn.close();
				return "1";
			} else {
				conn.close();
				res.setStatus(500);return Exc.status500;
			}
		} catch(Exception e){
			try {
				conn.close();
			}catch(Exception ex){
				res.setStatus(500);
				return Exc.status500;
			}
			res.setStatus(500);return Exc.status500;
		}
    } 
	
	/**
	 * Crea la membres�a del miembro de un grupo en caso de que el usuario de la sesi�n tenga esos privilegios
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Id del grupo del cual se quiere crear la membres�a
	 * @param jsonContent Objeto MiembroGrupo a crear en formato JSON
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/miembros/{idGrupo}", method=RequestMethod.POST)
    protected @ResponseBody String invitarMiembro(HttpSession sesion, @PathVariable("idGrupo")String idGrupo, 
    		@RequestBody String jsonContent,HttpServletResponse res) throws Exception {
		Connection conn = null;
		try {
			idGrupo = idGrupo.replaceAll(".htm", "");
			Gson gson = new Gson();
			JsonObject jsonObject = gson.fromJson( jsonContent, JsonObject.class);
			boolean[] permisos = new boolean[TopePermisos.CANTIDAD_PERMISOS];
			for(int i=1;i<=TopePermisos.CANTIDAD_PERMISOS;i++){
				if(jsonObject.has("permiso"+i)){
					permisos[i-1] = jsonObject.get("permiso"+i).getAsBoolean();
				} else {
					permisos[i-1] = false;
				}
			}
			String loginMiembro = jsonObject.get("login").getAsString();
			if(!StringUtils.isNumeric(idGrupo) || StringUtils.isBlank(loginMiembro)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			conn = Conexion.obtenerConexion();
			Usuario invitado = new Usuario(loginMiembro,conn);
			if(invitado.getIdUsuario()==0){conn.close();res.setStatus(404);return"Usuario no encontrado";}
			Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
			if(usuario==null){conn.close();res.setStatus(401);return Exc.status401;}
			MiembroGrupo mg = new MiembroGrupo(usuario.getIdUsuario(),codGrupo,conn);
			if(!mg.isInvitarMiembros()){
				conn.close();res.setStatus(401);return Exc.status401;
			}
			String mensaje = LogicaAviso.generarMensajeNuevoMiembro(usuario.getLogin(),(new Grupo(codGrupo,conn)).getNombreGrupo(),permisos);
			if(LogicaMiembroGrupo.agregarMiembro(invitado.getIdUsuario(), codGrupo, permisos, conn)
				&& Aviso.guardarNuevoAviso(invitado.getIdUsuario(),mensaje,conn)){
				conn.close();
				res.setStatus(201);
				return TopePermisos.obtenerNombrePermisoMasAlto(permisos);
			} else {
				conn.close();
				res.setStatus(500);return Exc.status500;
			}
		} catch(Exception e){
			try{
			conn.close();
			}catch(Exception ex){
				res.setStatus(500);
				return Exc.status500;
			}
			res.setStatus(500);return Exc.status500;
		}
    }  
}
