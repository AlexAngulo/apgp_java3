package controlador.clases;

import java.lang.reflect.Type;
import java.sql.Connection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelos.clases.Aviso;
import modelos.clases.MiembroGrupo;
import modelos.clases.Proyecto;
import modelos.clases.ProyectoCompartido;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import controlador.utiles.Exc;
import controlador.utiles.UtilesSecurity;

@Controller
public class CompartirProyectos {
	/**
	 * Comparte un proyecto con un usuario
	 * @param sesion Sesi�n del usuario
	 * @param idProyecto Id del Proyecto a compartir
	 * @param compartirJSON Objeto ProyectoCompartido en formato JSON
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/proyectos/compartir/{idProyecto}", method=RequestMethod.POST)
    public @ResponseBody String compartirProyecto(HttpSession sesion, @PathVariable("idProyecto")String idProyecto,
    		@RequestBody String  compartirJSON, HttpServletResponse res) throws Exception {
		try {
		if(!StringUtils.isNumeric(idProyecto)){return"0";}
		int codProyecto = Integer.parseInt(idProyecto);
		Connection conn = Conexion.obtenerConexion();
		Gson gson = new Gson();
		Type tipoProyectoCompartido = new TypeToken<ProyectoCompartido>(){}.getType();
		ProyectoCompartido pc = gson.fromJson(compartirJSON, tipoProyectoCompartido);
		pc.updateIdUsuario();
		pc.setIdProyecto(codProyecto);
		Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
		if(usuario==null) {
			conn.close();res.setStatus(401);return Exc.status401;
		}
		Proyecto proyecto = new Proyecto(codProyecto,conn);
		if(!usuario.isEsAdministrador() && proyecto.getIdCreador()!=usuario.getIdUsuario() && !ProyectoCompartido.usuarioPuedeCompartir(usuario.getIdUsuario(), codProyecto, conn)){
			if(proyecto.getIdGrupoPerteneciente()>0){
				MiembroGrupo mg = new MiembroGrupo();
				if(!mg.obtenerMiembroGrupoPorIds(usuario.getIdUsuario(), proyecto.getIdGrupoPerteneciente()) || /*TODO: mirar si esto esta bien COMPARTIR PROYECTOS.*/mg.isCompartirProyecto()){
					conn.close();res.setStatus(401);return Exc.status401;
				}
			}else {
				conn.close();res.setStatus(401);return Exc.status401;
			}
		}
		if(ProyectoCompartido.existeProyectoCompartido(pc.getIdUsuario(), pc.getIdProyecto(), conn)){
			conn.close();res.setStatus(404);return"Proyecto Compartido ya existente";
		}
		
		if(pc.guardarNuevoProyectoCompartido() && Aviso.guardarNuevoAviso(pc.getIdUsuario(), usuario.getLogin()+" te ha invitado al proyecto "+proyecto.getNombreProyecto(), conn)){
			conn.close();
			res.setStatus(201);
			return"1";
		} else {
			conn.close();
			res.setStatus(500);return Exc.status500;
		}
		} catch(Exception e){
			res.setStatus(500);
			return Exc.status500;
		}
    }   
	
	/**
	 * Edita el compartido de proyectos
	 * @param sesion Sesi�n del usuario
	 * @param idProyecto Id del proyecto del cual se quiere editar el compartido. 
	 * @param compartirJSON Compartido reci�n editado
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/proyectos/compartir/{idProyecto}", method=RequestMethod.PUT)
    public @ResponseBody String editarCompartirProyecto(HttpSession sesion, @PathVariable("idProyecto")String idProyecto,
    		@RequestBody String  compartirJSON, HttpServletResponse res) throws Exception {
		try {
		if(!StringUtils.isNumeric(idProyecto)){return"0";}
		int codProyecto = Integer.parseInt(idProyecto);
		Connection conn = Conexion.obtenerConexion();
		Gson gson = new Gson();
		Type tipoProyectoCompartido = new TypeToken<ProyectoCompartido>(){}.getType();
		ProyectoCompartido pc = gson.fromJson(compartirJSON, tipoProyectoCompartido);
		pc.updateIdUsuario();
		pc.setIdProyecto(codProyecto);
		Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
		if(usuario==null) {
			conn.close();res.setStatus(401);return Exc.status401;
		}
		Proyecto proyecto = new Proyecto(codProyecto,conn);
		if(!usuario.isEsAdministrador() && proyecto.getIdCreador()!=usuario.getIdUsuario() && !ProyectoCompartido.usuarioPuedeEditarCompartir(usuario.getIdUsuario(), codProyecto, conn)){
			if(proyecto.getIdGrupoPerteneciente()>0){
				MiembroGrupo mg = new MiembroGrupo();
				if(!mg.obtenerMiembroGrupoPorIds(usuario.getIdUsuario(), proyecto.getIdGrupoPerteneciente()) || mg.isEditarCompartir()){
					conn.close();res.setStatus(401);return Exc.status401;
				}
			}else {
				conn.close();res.setStatus(401);return Exc.status401;
			}
		}
		if(!ProyectoCompartido.existeProyectoCompartido(pc.getIdUsuario(), pc.getIdProyecto(), conn)){
			conn.close();res.setStatus(404);return "No existe el proyecto compartido";
		}
		
		if(pc.updateProyectoCompartido()){
			conn.close();
			return"1";
		} else {
			conn.close();
			res.setStatus(500);return Exc.status500;
		}
		} catch(Exception e){
			res.setStatus(500);
			return Exc.status500;
		}
    }   
		
	/**
	 * Elimina el compartido de un proyecto determinado
	 * @param sesion Sesi�n del usuario
	 * @param idProyecto Id del proyecto el se quiere descompartir
	 * @param loginMiembro Login del usuario al cual se va a descompartir el proyecto
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/proyectos/compartir/{idProyecto}", method=RequestMethod.DELETE)
    public @ResponseBody String eliminarCompartirProyecto(HttpSession sesion, @PathVariable("idProyecto")String idProyecto,
    		@RequestBody String  loginMiembro, HttpServletResponse res) throws Exception {
		try {
		if(!StringUtils.isNumeric(idProyecto)){return"0";}
		int codProyecto = Integer.parseInt(idProyecto);
		Connection conn = Conexion.obtenerConexion();
		Usuario miembroAEchar = new Usuario(loginMiembro,conn);
		if(miembroAEchar.getIdUsuario()==0){conn.close();res.setStatus(401);return Exc.status401;}
		Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
		if(usuario==null) {conn.close();res.setStatus(401);return Exc.status401;}
		Proyecto proyecto = new Proyecto(codProyecto,conn);
		if(!usuario.isEsAdministrador() && proyecto.getIdCreador()!=usuario.getIdUsuario() && !ProyectoCompartido.usuarioPuedeEliminarCompartir(usuario.getIdUsuario(), codProyecto, conn)){
			if(proyecto.getIdGrupoPerteneciente()>0){
				MiembroGrupo mg = new MiembroGrupo();
				if(!mg.obtenerMiembroGrupoPorIds(usuario.getIdUsuario(), proyecto.getIdGrupoPerteneciente()) || mg.isEliminarCompartir()){
					conn.close();res.setStatus(401);return Exc.status401;
				}
			}else {
				conn.close();res.setStatus(401);return Exc.status401;
			}
		}
		if(!ProyectoCompartido.existeProyectoCompartido(miembroAEchar.getIdUsuario(), codProyecto, conn)){
			conn.close();res.setStatus(404);return"Proyecto Compartido no encontrado";
		}
		
		if(ProyectoCompartido.eliminarProyectoCompartido(miembroAEchar.getIdUsuario(), codProyecto, conn)){
			conn.close();
			return"1";
		} else {
			conn.close();
			res.setStatus(500);return Exc.status500;
		}
		} catch(Exception e){
			res.setStatus(500);
			return Exc.status500;
		}
    }   
}
