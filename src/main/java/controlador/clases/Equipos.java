package controlador.clases;

import java.lang.reflect.Type;
import java.sql.Connection;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import modelos.clases.Aviso;
import modelos.clases.Equipo;
//import modelos.clases.EquipoCompleto;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;
import modelos.vistas.MiembroGrupoCompleto;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import controlador.utiles.Exc;
import controlador.utiles.UtilesSecurity;

@Controller
public class Equipos {
	/**
	 * Crea un nuevo equipo en un grupo
	 * @param sesion Sesi�n del usuario
	 * @param equipoJSON Nuevo Equipo en formato JSON
	 * @param idGrupo Id del Grupo al cual va a pertenecer el nuevo equipo
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/grupos/{idGrupo}/equipos", method=RequestMethod.POST)
    public @ResponseBody String crearEquipo(HttpSession sesion, @RequestBody String equipoJSON, 
    		@PathVariable("idGrupo")String idGrupo, HttpServletResponse res) throws Exception {
		try {
			if(!StringUtils.isNumeric(idGrupo)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			Connection conn = Conexion.obtenerConexion();
			MiembroGrupoCompleto mgc = UtilesSecurity.usuarioPuedeCrearEquipo(codGrupo, sesion, conn);
			if(mgc==null || mgc.getUsuario()==null || mgc.getGrupo()==null){
				conn.close();res.setStatus(401);return Exc.status401;
			}
			Gson gson = new Gson();
			Type tipoEquipo = new TypeToken<Equipo>(){}.getType();
			Equipo equipo = gson.fromJson(equipoJSON, tipoEquipo);
			equipo.setIdGrupo(codGrupo);
			if(equipo.guardarEquipo()){
				conn.close();
				res.setStatus(201);
				return ""+equipo.getIdEquipo();
			}else{
				conn.close();
				res.setStatus(500);return Exc.status500;
			}
		}catch(Exception e){
			res.setStatus(500);return Exc.status500;
		}
    }   
	
	/**
	 * Edita un Equipo
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Id del Grupo del Equipo el cual se quiere editar
	 * @param idEquipo Id del Equipo a editar
	 * @param equipoJSON Nuevo Equipo en formato JSON
* @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/grupos/{idGrupo}/equipos/{idEquipo}", method=RequestMethod.PUT)
    public @ResponseBody String editarEquipo(HttpSession sesion,@PathVariable("idGrupo")String idGrupo,@PathVariable("idEquipo")String idEquipo,
    		@RequestBody String  equipoJSON, HttpServletResponse res) throws Exception {
		try {
			if(!StringUtils.isNumeric(idGrupo) || !StringUtils.isNumeric(idEquipo)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			int codEquipo = Integer.parseInt(idEquipo);
			Connection conn = Conexion.obtenerConexion();
			MiembroGrupoCompleto mgc = UtilesSecurity.usuarioPuedeEditarEquipo(codGrupo, sesion, conn);
			if(mgc==null || mgc.getUsuario()==null || mgc.getGrupo()==null){
				conn.close();res.setStatus(401);return Exc.status401;
			}
			Gson gson = new Gson();
			Type tipoEquipo = new TypeToken<Equipo>(){}.getType();
			Equipo equipo = gson.fromJson(equipoJSON, tipoEquipo);
			equipo.setIdGrupo(codGrupo);
			equipo.setIdEquipo(codEquipo);
			if(equipo.guardarEquipo()){
				conn.close();
				return "1";
			}else{
				conn.close();
				res.setStatus(500);return Exc.status500;
			}
		}catch(Exception e){
			res.setStatus(500);
			return Exc.status500;
		}
    }  
	
	/**
	 * Elimina un equipo dentro de un grupo
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Id del grupo del Equipo a eliminar
	 * @param idEquipo Id del equipo a eliminar
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/grupos/{idGrupo}/equipos/{idEquipo}", method=RequestMethod.DELETE)
    public @ResponseBody String eliminarEquipo(HttpSession sesion,@PathVariable("idGrupo")String idGrupo,
    		@PathVariable("idEquipo")String idEquipo, HttpServletResponse res) throws Exception {
		try {
			if(!StringUtils.isNumeric(idGrupo) || !StringUtils.isNumeric(idEquipo)){res.setStatus(403);return Exc.status403;}
			int codGrupo = Integer.parseInt(idGrupo);
			int codEquipo = Integer.parseInt(idEquipo);
			Connection conn = Conexion.obtenerConexion();
			MiembroGrupoCompleto mgc = UtilesSecurity.usuarioPuedeEliminarEquipo(codGrupo, sesion, conn);
			if(mgc==null || mgc.getUsuario()==null || mgc.getGrupo()==null){
				conn.close();res.setStatus(401);return Exc.status401;
			}
			if(Equipo.eliminarEquipo(codEquipo,conn)){
				conn.close();
				return "1";
			}else{
				conn.close();
				res.setStatus(500);return Exc.status500;
			}
		}catch(Exception e){
			res.setStatus(500);return Exc.status500;
		}
    }  
	
	/**
	 * Construye un ModelAndView con los equipos a los que pertenece el usuario de la sesi�n
	 * @param sesion Sesi�n del usuario
	 * @return Devuelve un ModelAndView con los equipos a los que pertenece el usuario de la sesi�n
	 * @throws Exception
	 */
	@RequestMapping(value="/equipos", method=RequestMethod.GET)
    protected ModelAndView verEquipos(HttpSession sesion) throws Exception {
		return aux_verEquipos(sesion,0);
    }  
	
	/**
	 * Construye un ModelAndView con los equipos a los que pertenece el usuario de la sesi�n o los equipos de un grupo, dependiendo del parametro idGrupo
	 * @param sesion Sesi�n del usuario
	 * @param idGrupo Si idGrupo!=0 obtendr� los equipos de un grupo, si idGrupo==0 obtendr� los equipos del usuario de la sesi�n
	 * @return Devuelve un ModelAndView con los equipos a los que pertenece el usuario de la sesi�n o los equipos de un grupo
	 * @throws Exception
	 */
	public static ModelAndView aux_verEquipos(HttpSession sesion, int idGrupo) throws Exception {
		Connection conn = Conexion.obtenerConexion();
		Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion, conn);
		if(usuario==null){conn.close();return new ModelAndView("redirect:/");}
		ModelAndView mav = new ModelAndView("equipos");
		mav.addObject("sitio","Mis Equipos");
		mav.addObject("invitaciones",Aviso.obtenerAvisosPorUsuario(usuario.getIdUsuario(),conn));
		mav.addObject("usuario",usuario);
		//mav.addObject("equipos",EquipoCompleto.obtenerEquiposCompletosPorUsuario(idGrupo, usuario.getIdUsuario(),conn));
		conn.close();
		return mav;
	}
}
