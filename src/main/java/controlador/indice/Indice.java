package controlador.indice;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.classes.Category;
import model.entitites.ArticleEntity;
import model.entitites.SubCategoryEntity;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import controlador.utiles.Exc;
import controlador.utiles.ModelAndViewUtils;
import controlador.utiles.UtilesSecurity;
import controller.logic.LogicArticle;
import controller.logic.LogicCategory;

@Controller
public class Indice {
	/**
	 * 
	 * @param irALogin
	 * @param sesion
	 * @param mensaje
	 * @param irACompartidos
	 * @param irAAsignadas
	 * @return
	 * @throws Exception
	 */
	private static ModelAndView mandarALoginARegistroOAIndice(boolean irALogin, HttpSession sesion, String mensaje, boolean irACompartidos, boolean irAAsignadas, boolean irACrearProyecto) throws Exception {
		Connection conn = Conexion.obtenerConexion();
		String irA = "";
		if(irALogin) irA = "Login";
		else irA = "Registro";
		Usuario usuario = UtilesSecurity.usuarioEstaLogueado(sesion,conn);
		if(usuario!=null){
			if(usuario.isEsAdministrador()==false){
				conn.close();
//				return irAProyectos(usuario,"Mis Proyectos",mensaje,true,true,true,true,true,true,true,true,true,true,true,true,
//					usuario.getOrdenProyectos(),usuario.getOrdenTareas(),irACompartidos,irAAsignadas,irACrearProyecto);
			} else {
				//return irAPanelDeAdministracion(usuario,0,0,conn);
			}
			return null;
		} else {
			ModelAndView mav = new ModelAndView("login");
			mav.addObject("sitio",irA);
			mav.addObject("message",mensaje);
			conn.close();
			return mav;
		}
	}
	
	@RequestMapping("/al")
	protected ModelAndView mostrarAdmminLogin(HttpSession sesion) throws Exception {
		return ModelAndViewUtils.getSimplePage("al");
		//return ModelAndViewUtils.getGeneric("index", "index", userId, userLogin, userName, "X-tasis fashion es la ca�a");
	}
	
	@RequestMapping("/index")
	protected ModelAndView mostrarIndiceOLogin(HttpSession sesion) throws Exception {
		Integer idRol = (Integer) sesion.getAttribute(ModelAndViewUtils.USER_ROL);
		if(idRol == null || idRol <= 90){
			return ModelAndViewUtils.getGenericWithLogin("index", "index", "X-tasis fashion es la ca�a", "", sesion);
		} else {
			return ModelAndViewUtils.getGenericWithLogin("admin", "index", "Panel de administraci�n", "", sesion);
		}
	}
	
	/**
	 * Obtiene las tareas en formato JSON de un proyecto
	 * @param sesion Sesi�n del usuario
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @param stringIdProyecto Id del proyecto del cual se quieren obtener las tareas
	 * @return Devuelve las tareas de un proyecto, su fecha de fin, los dias que tardar� en finalizar y el organigrama en formato JSON en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/categories", method=RequestMethod.GET)
    public @ResponseBody String getCategoriesInHTML(HttpSession sesion,HttpServletResponse res) throws Exception {
		return getCategories(res,true);
	}
	
	 public String getCategories(HttpServletResponse res,boolean doHTML) throws Exception {
		Connection conn = null;
		try{
			conn = Conexion.obtenerConexion();
			
			List<Category> lcat = LogicCategory.getAllCategories(conn);
			
			String message= "";
			
			try {
				if(doHTML){
					for(Category cat : lcat){
						//TODO: obtener urlBase
						String urlBase = "";
						message += 
							"<div class='btn-group' style='width:100%'>" +
								"<button style='width:85%' type='button' class='btn btn-default' onclick='location.href=\""+urlBase+"category/"+cat.getId()+"\"'>"+cat.getName()+"</button>"+
								"<button style='width:15%' type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"+
							 		"<div style='padding-top: 1em'></div><span class='caret'></span><span class='sr-only'>Toggle Dropdown</span>"+
							 	"</button>"+
							 	"<ul class='dropdown-menu dropdown-menu-right'>";
						for(SubCategoryEntity scat : cat.getSubcategories()){
							message += "<li><a href='/subcategory/"+scat.getId()+"'>"+scat.getName()+"</a></li>";
						}
						message += "</ul></div>";
					}
					return message;
				} else {
					//TODO: do it for json
				}
				
			} catch (Exception e){}
			
			return message;
		} catch(Exception e){
			try {
			conn.close();
			}catch(Exception ex){
				res.setStatus(500);
				return Exc.status500;
			}
			res.setStatus(500);return Exc.status500;
		}
	}
	 
	 /**
	 * Obtiene las tareas en formato JSON de un proyecto
	 * @param sesion Sesi�n del usuario
	 * @param res Respuesta de la ejecuci�n, �til para guardar un status dependiendo del caso
	 * @param stringIdProyecto Id del proyecto del cual se quieren obtener las tareas
	 * @return Devuelve las tareas de un proyecto, su fecha de fin, los dias que tardar� en finalizar y el organigrama en formato JSON en caso de tener �xito o un mensaje de error en caso de no tenerlo
	 * @throws Exception
	 */
	@RequestMapping(value="/featured_articles", method=RequestMethod.GET)
    public @ResponseBody String getFeaturedArticles(HttpSession sesion,HttpServletResponse res) throws Exception {
		Connection conn = null;
		try{
			conn = Conexion.obtenerConexion();
			
			List<ArticleEntity> lArt = LogicArticle.getFeaturedArticles(conn,5);
			
			String message= "";
			
			try {
				//TODO: obtener urlBase
//				String urlBase = "/APGP/";
				message += "<div id='carousel-featured' class='carousel slide' data-ride='carousel'>";
				String indicators = "<ol class='carousel-indicators'>";
				String sliders = "<div class='carousel-inner' role='listbox'>";
				String controls = 
					"<a class='left carousel-control' href='#carousel-featured' role='button' data-slide='prev'>" +
					    "<span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>"+
					    "<span class='sr-only'>Previous</span>"+
					  "</a>"+
					  "<a class='right carousel-control' href='#carousel-featured' role='button' data-slide='next'>"+
					    "<span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>"+
					    "<span class='sr-only'>Next</span>"+
					  "</a>";
				int i = 0;
				for(ArticleEntity art : lArt){
					indicators += "<li data-target='#carousel-featured' data-slide-to='"+i+"'></li>";
					sliders += "<div class='item'>" + 
					      "<img src='"+ art.getUrlPrincipalImage() + "' alt='" + art.getName() + "' />"+
					      "<div class='carousel-caption'>"+
					        art.getName()+ " por " + art.getPrice() +
					      "</div>"+
					    "</div>";
					i++;
				}
				indicators += "</ol>";
				sliders += "</div>";
				message += indicators + sliders + controls + "</div>";
				return message;
				/*
				<div id='carousel-example-generic' class='carousel slide' data-ride='carousel'>
				  <!-- Indicators -->
				  <ol class='carousel-indicators'>
				    <li data-target='#carousel-example-generic' data-slide-to='0' class='active'></li>
				    <li data-target='#carousel-example-generic' data-slide-to='1'></li>
				    <li data-target='#carousel-example-generic' data-slide-to='2'></li>
				  </ol>
				
				  <!-- Wrapper for slides -->
				  <div class='carousel-inner' role='listbox'>
				    <div class='item active'>
				      <img src='${urlIndex}img/logo1.png' alt='...'>
				      <div class='carousel-caption'>
				        ...
				      </div>
				    </div>
				    <div class='item'>
				      <img src='${urlIndex}img/logo3.png' alt='...'>
				      <div class='carousel-caption'>
				        ...
				      </div>
				    </div>
				    ...
				  </div>
				
				  <!-- Controls -->
				  <a class='left carousel-control' href='#carousel-example-generic' role='button' data-slide='prev'>
				    <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
				    <span class='sr-only'>Previous</span>
				  </a>
				  <a class='right carousel-control' href='#carousel-example-generic' role='button' data-slide='next'>
				    <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
				    <span class='sr-only'>Next</span>
				  </a>
				</div>*/
				
			} catch (Exception e){}
			
			return message;
		} catch(Exception e){
			try {
			conn.close();
			}catch(Exception ex){
				res.setStatus(500);
				return Exc.status500;
			}
			res.setStatus(500);return Exc.status500;
		}
	}
	
	@RequestMapping("/registro")
	protected ModelAndView mostrarRegistro(HttpSession sesion) throws Exception {
		return mandarALoginARegistroOAIndice(false,sesion,null,false,false,false);
	}
	
	@RequestMapping("/proyectos/crear")
	protected ModelAndView mostrarIndiceOLoginCrear(HttpSession sesion) throws Exception {
		return mandarALoginARegistroOAIndice(true,sesion,null,false,false,true);
	}
}
