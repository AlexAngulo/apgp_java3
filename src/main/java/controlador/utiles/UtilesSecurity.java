package controlador.utiles;

import java.sql.Connection;

import javax.servlet.http.HttpSession;

import model.entitites.UserEntity;
import modelos.clases.Proyecto;
import modelos.clases.Usuario;
import modelos.enums.TopePermisos;
import modelos.vistas.MiembroGrupoCompleto;

import org.javatuples.Pair;

import controlador.logica.LogicaUsuario;

public class UtilesSecurity {
	public static Usuario usuarioEstaLogueado(HttpSession sesion,Connection conn) {
		String nombreUsuario = (String) sesion.getAttribute("nombreUsuarioLogueado");
 		if(nombreUsuario==null) return null;
		Usuario usuario2 = LogicaUsuario.obtenerUsuarioPorLogin(nombreUsuario,conn);
		if(usuario2 != null && usuario2.getIdUsuario()>0)
			return usuario2;
		else
			return null;
	}
	
	public static String obtenerPassUsuario(HttpSession sesion, Connection conn) {
		Integer idUsuario = (Integer) sesion.getAttribute("idUsuarioLogueado");
 		if(idUsuario==null) return null;
		return Usuario.obtenerPasswordUsuario(idUsuario,conn);
	}

	public static HttpSession loguearUsuario(UserEntity user, HttpSession sesion) {
		sesion.setAttribute("userId", (Long) user.getId());
		sesion.setAttribute("userLogin", user.getLogin());
		sesion.setAttribute("userName", user.getName());
		sesion.setAttribute("userRol", user.getRol());
		return sesion;
	}
	
	public static HttpSession reloguearLogin(String login, HttpSession sesion) {
		sesion.setAttribute("nombreUsuarioLogueado", login);
		return sesion;
	}
	
	public static HttpSession logoutUsuario(HttpSession sesion) {
		sesion.invalidate();
		return sesion;
	}
	
	public static MiembroGrupoCompleto aux_usuarioPuedeElProyectoOGrupo(int codProyectoOGrupo, HttpSession sesion, boolean esGrupo, Connection conn){
		Integer idUsuario = (Integer) sesion.getAttribute("idUsuarioLogueado");
		if(idUsuario==null || idUsuario==0) return null;
		return new MiembroGrupoCompleto(idUsuario,codProyectoOGrupo,esGrupo,conn);
	}

	public static Pair<Usuario, Proyecto> usuarioPuedeElProyecto(
			int codProyecto, int numTope,HttpSession sesion, Connection conn) {
		try{
			MiembroGrupoCompleto mgc = aux_usuarioPuedeElProyectoOGrupo(codProyecto,sesion,false,conn);
			if(mgc!=null && mgc.getArrayPermisos()[numTope] && mgc.getUsuario().getIdUsuario()>0 && mgc.getProyecto().getIdProyecto()>0){
				return Pair.with(mgc.getUsuario(),mgc.getProyecto());
			} else {
				return null;
			}
		} catch(Exception e){
			return null;
		}
	}
	
	public static Pair<Usuario, Proyecto> usuarioPuedeCrearProyecto(
			int codProyecto, HttpSession sesion, Connection conn) {
		return usuarioPuedeElProyecto(codProyecto,TopePermisos.CREAR_PROYECTOS,sesion,conn);
	}
	
	public static Pair<Usuario, Proyecto> usuarioPuedeEditarElProyecto(
			int codProyecto, HttpSession sesion, Connection conn) {
		return usuarioPuedeElProyecto(codProyecto,TopePermisos.EDITAR_PROYECTOS,sesion,conn);
	}
	
	public static Pair<Usuario, Proyecto> usuarioPuedeEliminarElProyecto(
			int codProyecto, HttpSession sesion, Connection conn) {
		return usuarioPuedeElProyecto(codProyecto,TopePermisos.ELIMINAR_PROYECTOS,sesion,conn);
	}
	
	public static MiembroGrupoCompleto usuarioPuedeVerElProyecto(
			int codProyecto, HttpSession sesion, Connection conn) {
		MiembroGrupoCompleto mgc = aux_usuarioPuedeElProyectoOGrupo(codProyecto,sesion,false,conn);
		if(mgc!=null && mgc.getUsuario()!=null && mgc.getUsuario().getIdUsuario()>0 && mgc.getProyecto().getIdProyecto()>0){
			return mgc;
		} else {
			return null;
		}
	}

	public static MiembroGrupoCompleto usuarioPuedeCrearSubgrupos(
			int idGrupo, HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.CREAR_SUBGRUPO,sesion,conn);
	}
	
	private static MiembroGrupoCompleto usuarioPuedeHacerCosasConElGrupo(
			int idGrupo, int numTope, HttpSession sesion, Connection conn) {
		try{
			MiembroGrupoCompleto mgc = aux_usuarioPuedeElProyectoOGrupo(idGrupo,sesion,true,conn);
			boolean usuarioPuedeGrupo;
			if(numTope==-1){
				usuarioPuedeGrupo = true;
			} else {
				usuarioPuedeGrupo = mgc.getArrayPermisos()[numTope];
			}
			if(mgc==null || !usuarioPuedeGrupo || mgc.getUsuario()==null || mgc.getGrupo()==null || mgc.getUsuario().getIdUsuario()==0 || mgc.getGrupo().getIdGrupo()==0) 
				return null;
			else
				return mgc;
		} catch(Exception e){
			return null;
		}
	}

	public static MiembroGrupoCompleto usuarioPuedeEditarElGrupo(int idGrupo,
			HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.CREAR_SUBGRUPO,sesion,conn);
	}
	
	public static MiembroGrupoCompleto usuarioPuedeEliminarElGrupo(int idGrupo,
			HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.ELIMINAR_GRUPO,sesion,conn);
	}

	public static MiembroGrupoCompleto usuarioPuedeVerElGrupo(int idGrupo,
			HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,-1,sesion,conn);
	}

	public static MiembroGrupoCompleto usuarioPuedeCrearTareas(int idGrupo,
			HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.CREAR_TAREAS,sesion,conn);
	}

	public static MiembroGrupoCompleto usuarioPuedeInvitarMiembros(
			int idGrupo, HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.INVITAR_MIEMBRO,sesion,conn);
	}

	public static HttpSession setOrdenProyectos(int nuevoOrden, HttpSession sesion) {
		sesion.setAttribute("ordenProyectos", nuevoOrden);
		return sesion;
	}
	
	public static int getOrdenProyectos(HttpSession sesion) {
		if(sesion.getAttribute("ordenProyectos")==null) return 0;
		else return (Integer) sesion.getAttribute("ordenProyectos");
	}

	public static MiembroGrupoCompleto usuarioPuedeEliminarTareas(
			int idGrupo, HttpSession sesion, Connection conn) {
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.ELIMINAR_TAREAS,sesion,conn);
	}

	public static MiembroGrupoCompleto usuarioPuedeAsignarTareas(int idGrupo,
			HttpSession sesion, Connection conn) {
		// TODO Auto-generated method stub
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.ASIGNAR_TAREAS,sesion,conn);
	}

	public static MiembroGrupoCompleto usuarioPuedeEditarAsignamiento(
			int idGrupo, HttpSession sesion, Connection conn) {
		// TODO Auto-generated method stub
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.EDITAR_ASIGNAMIENTO,sesion,conn);
	}
	
	public static MiembroGrupoCompleto usuarioPuedeEliminarAsignamiento(
			int idGrupo, HttpSession sesion, Connection conn) {
		// TODO Auto-generated method stub
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.ELIMINAR_ASIGNAMIENTO,sesion,conn);
	}

	public static MiembroGrupoCompleto usuarioPuedeCrearEquipo(int idGrupo,
			HttpSession sesion, Connection conn) {
		// TODO Auto-generated method stub
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.CREAR_EQUIPO,sesion,conn);
	}
	
	public static MiembroGrupoCompleto usuarioPuedeEditarEquipo(int idGrupo,
			HttpSession sesion, Connection conn) {
		// TODO Auto-generated method stub
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.EDITAR_EQUIPO,sesion,conn);
	}
	
	public static MiembroGrupoCompleto usuarioPuedeEliminarEquipo(int idGrupo,
			HttpSession sesion, Connection conn) {
		// TODO Auto-generated method stub
		return usuarioPuedeHacerCosasConElGrupo(idGrupo,TopePermisos.ELIMINAR_EQUIPO,sesion,conn);
	}
}
