package controlador.utiles;

import java.util.List;

import modelos.clases.TareaAsignada;
import modelos.clases.TareaAsignadaEquipo;
import modelos.clases.Usuario;

import org.javatuples.Pair;
import org.javatuples.Quintet;
import org.javatuples.Triplet;
import org.springframework.stereotype.Controller;

import controlador.logica.VistaGrupoMiembro;

@Controller
public class UtilesJSON {
	public static String deVistaGrupoMiembroAJson(List<VistaGrupoMiembro> grupos) {
		String json = "[";
		boolean haEmpezado = false;
		boolean haEmpezadoHijos = false;
		for(VistaGrupoMiembro vgm : grupos){
			if(haEmpezado) json += ",";
			json += "{\"idGrupo\":"+vgm.getIdGrupo()+","+
			"\"espacios\":\""+vgm.getEspacios()+"\","+
			"\"nombreGrupo\":\""+vgm.getNombreGrupo()+"\","+
			"\"breveDesc\":\""+vgm.getBreveDesc()+"\","+
			"\"descGrupo\":\""+vgm.getDescGrupo()+"\","+
			"\"idSupergrupo\":"+vgm.getIdSupergrupo()+","+
			"\"nombreSupergrupo\":\""+vgm.getNombreSupergrupo()+"\","+
			"\"nombreCreador\":\""+vgm.getNombreCreador()+"\","+
			"\"puedeEditar\":"+vgm.isEditarGrupo()+","+
			"\"puedeEliminar\":"+vgm.isEliminarGrupo()+","+
			"\"puedeCrearSubgrupo\":"+vgm.isCrearSubgrupo()+","+
			"\"puedeSerHijoDe\":[";
			haEmpezadoHijos = false;
			for(int serHijoDe : vgm.getPuedeSerHijoDe()){
				if(haEmpezadoHijos) json+=",";
				json += serHijoDe;
				haEmpezadoHijos = true;
			}
			json += "]";
			json += "}";
			haEmpezado = true;
		}
		return json + "]";
	}
}
