package controlador.utiles;

import java.util.Properties;

import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

public class Exc {
	public static String status500 = "Error interno";
	public static String status401 = "Credenciales incorrectas";
	public static String status403 = "Parámetros inválidos";
	public static SimpleMappingExceptionResolver getSimpleMappingExceptionResolver() {
		    SimpleMappingExceptionResolver result
		        = new SimpleMappingExceptionResolver();
		 
		    // Setting customized exception mappings
		    Properties p = new Properties();
		    p.put(Exception.class.getName(), "Errors/Exception1");
		    result.setExceptionMappings(p);
		 
		    // Unmapped exceptions will be directed there
		    result.setDefaultErrorView("Errors/Default");
		 
		    // Setting a default HTTP status code
		    result.setDefaultStatusCode(HttpStatus.BAD_REQUEST.value());
		 
		    return result;
		 
	}
}
