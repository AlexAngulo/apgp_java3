package controlador.utiles;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class UtilesValidacion {
	
	protected static final Pattern PATRON_CIF = Pattern.compile("[[A-H][J-N][P-S]UVW][0-9]{7}[0-9A-J]");
	 
	protected static final String CONTROL_SOLO_NUMEROS = "ABEH"; // Sólo admiten números como carácter de control
	protected static final String CONTROL_SOLO_LETRAS = "KPQS"; // Sólo admiten letras como carácter de control
	protected static final String CONTROL_NUMERO_A_LETRA = "JABCDEFGHI"; // Conversión de dígito a letra de control.
	
	protected static final String LETRAS_VALIDAS_NIF = "TRWAGMYFPDXBNJZSQVHLCKE";
	
	protected static final Pattern PATRON_NIF = Pattern.compile("[0-9]{8,8}");
	
	protected static final String PATRON_TELEFONO = "^[6789]{1}[0-9]{2} [0-9]{3} [0-9]{3}$";
	protected static final String PATRON_TELEFONO_MOVIL = "^[67]{1}[0-9]{2} [0-9]{3} [0-9]{3}$";
	
	protected static final String PATRON_MAIL = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
	protected static final String PATRON_PASS = "^[_A-Za-z0-9-]*$";
	protected static final String PATRON_NOMBRE_USUARIO = "^[_A-Za-zñÑ0-9-]*$";
	protected static final String PATRON_NOMBRE = "w*";
	
	protected static final String FORMATO_FECHA = "yyyy-MM-dd";
	
	protected static final String[] PREFIJOS_TELEFONO_INVALIDOS = {"803", "806", "807", "901", "902", 
																"905", "907", "908", "909"};
	
	protected static final String[] CARACTERES_INCORRECTOS = {"'", "\"", "?", "¿" };
	
	public static boolean validateNif(String nif) {
		nif = nif.toUpperCase();
		if(nif==null || nif.length()!=9) {
				return false;
			}
		String dni = nif.substring(0, 8);
		char letra = nif.charAt(8);
		
		long ldni = 0;
		try {
			ldni = Long.parseLong(dni);
		} catch(NumberFormatException e) {
			return false;
		}
		int indice = (int)(ldni % 23);
		return PATRON_NIF.matcher(dni).matches() && letra==LETRAS_VALIDAS_NIF.charAt(indice);
	}
	
	public static boolean validateNie(String nie) {
		nie = nie.toUpperCase();
		if(nie==null || nie.length()!=9) {
			return false;
		}
		char letraInicial = nie.charAt(0);
		int enteroSegunLetraInicial = 0;
		switch (letraInicial) {
		case 'X':
			enteroSegunLetraInicial = 0;
			break;
		case 'Y':
			enteroSegunLetraInicial = 1;
			break;
		case 'Z':
			enteroSegunLetraInicial = 2;
			break;
		default:
			return false;
		}
		String dni = enteroSegunLetraInicial + nie.substring(1, 8);
		char letraFinal = nie.charAt(8);
		
		long ldni = 0;
		try {
			ldni = Long.parseLong(dni);
		} catch(NumberFormatException e) {
			return false;
		}
		int indice = (int)(ldni % 23);
		return PATRON_NIF.matcher(dni).matches() && letraFinal==LETRAS_VALIDAS_NIF.charAt(indice);
	}
	
	public static boolean validateCif(String cif) {
	    try {
	        if (!PATRON_CIF.matcher(cif).matches()) {
	            // No cumple el patrón
	            return false;
	        }
	 
	        int parA = 0;
	        for (int i = 2; i < 8; i += 2) {
	            final int digito = Character.digit(cif.charAt(i), 10);
	            if (digito < 0) {
	                return false;
	            }
	            parA += digito;
	        }
	 
	        int nonB = 0;
	        for (int i = 1; i < 9; i += 2) {
	            final int digito = Character.digit(cif.charAt(i), 10);
	            if (digito < 0) {
	                return false;
	            }
	            int nn = 2 * digito;
	            if (nn > 9) {
	                nn = 1 + (nn - 10);
	            }
	            nonB += nn;
	        }
	 
	        final int parcialC = parA + nonB;
	        final int digitoE = parcialC % 10;
	        final int digitoD = (digitoE > 0) ?
	                (10 - digitoE) :
	                0;
	        final char letraIni = cif.charAt(0);
	        final char caracterFin = cif.charAt(8);
	 
	        final boolean esControlValido =
	            // ¿el carácter de control es válido como letra?
	            (CONTROL_SOLO_NUMEROS.indexOf(letraIni) < 0
	                    && CONTROL_NUMERO_A_LETRA.charAt(digitoD) == caracterFin)
	            ||
	            // ¿el carácter de control es válido como dígito?
	            (CONTROL_SOLO_LETRAS.indexOf(letraIni) < 0
	                    && digitoD == Character.digit(caracterFin, 10));
	        return esControlValido;
	 
	    } catch (Exception e) {
	        return false;
	    }
	}
	
	/**
	 * Valida que un campo no está vacio ni solo contiene espacios
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarRelleno(String campo) {
		return StringUtils.isNotBlank(campo);
	}
	
	/**
	 * Valida que un campo no está vacio ni tiene caracteres incorrectos
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condicion
	 */
	public static boolean validarRellenoYCorrecto(String campo) {
		
		boolean correcto = true;
		if (StringUtils.isNotBlank(campo)) {
			for (String caracter : CARACTERES_INCORRECTOS) {
				if (campo.contains(caracter)) {
					correcto = false;
					break;
				}
			}
		} else {
			correcto = false;
		}
		
		return correcto;
	}
	
	/**
	 * Valida que un campo sea numerico
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarEntero(String campo) {
		try {
			if (StringUtils.isNotBlank(campo)) {
				Integer.parseInt(campo);
				return true;
			} else {
				return false;
			}
		} catch (NumberFormatException ex) {
			return false;
		}
	}
	
	/**
	 * Valida que un campo sea numerico mayor que cero
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarEnteroMayorQueCero(String campo) {
		try {
			if (StringUtils.isNotBlank(campo)) {
				if(Integer.parseInt(campo) > 0)
					return true;
				else
					return false;
			} else {
				return false;
			}
		} catch (NumberFormatException ex) {
			return false;
		}
	}
	
	/**
	 * Valida que un campo sea decimal
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarDouble(String campo) {
		try {
			if (StringUtils.isNotBlank(campo)) {
				Double.parseDouble(campo);
				return true;
			} else {
				return false;
			}
		} catch (NumberFormatException ex) {
			return false;
		}
	}
	
	public static boolean validarFecha(String campo) {
		try {
			if (StringUtils.isNotBlank(campo)) {
				SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
				sdf.parse(campo);
				return true;
			} else {
				return false;
			}
		} catch (ParseException ex) {
			return false;
		}
	}
	
	/**
	 * Valida que un campo sea S/N
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarSiNo(String campo) {
		return (StringUtils.isNotBlank(campo) && (campo.equals("S") || campo.equals("N")));
	}
	
	/**
	 * Valida que hay un campo relleno y el otro no
	 * @param campo1 Campo a validar
	 * @param campo2 Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condicion
	 */
	public static boolean validarUnoUOtro(String campo1, String campo2) {
		return (StringUtils.isNotBlank(campo1) != StringUtils.isNotBlank(campo2));
	}

	/**
	 * Valida que un campo contenga un mail valido
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarEmail(String campo) {
		Pattern pat = null;
        Matcher mat = null;        
        
		pat = Pattern.compile(PATRON_MAIL);
        
        mat = pat.matcher(campo);
        
        return mat.find();
	}
	
	/**
	 * Valida que un campo contenga un telefono valido
	 * Comprobamos que no sea un telefono de pago 803, 806 ...
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarTelefono(String campo) {
		
		boolean telefonoValido = false;
		Pattern pat = null;
		Matcher mat = null;
		
		pat = Pattern.compile(PATRON_TELEFONO);
		
		mat = pat.matcher(campo);
		
		telefonoValido = mat.find();
		
		//Si es valido comprobamos que no sea de pago
		if (telefonoValido) {
			for (String prefijo : PREFIJOS_TELEFONO_INVALIDOS) {
				if (campo.startsWith(prefijo)) {
					telefonoValido = false;
					break;
				}
			}
		}
		
		return telefonoValido;
	}
	
	/**
	 * Valida que un campo contenga un telefono movil valido
	 * Comprobamos que el teléfono empiece por 6 o 7 y que contenga 9 digitos
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarTelefonoMovil(String campo) {
		
		Pattern pat = Pattern.compile(PATRON_TELEFONO_MOVIL);
		Matcher mat = pat.matcher(campo);
		
		return mat.find();
	}
	
	public static boolean validarPassCorrecta(String campo) {
		Pattern pat = null;
		Matcher mat = null;
		
		pat = Pattern.compile(PATRON_PASS);
		
		mat = pat.matcher(campo);
		
		return mat.find();
	}
	
	/**
	 * Valida que el nombre de usuario cumpla un patrón
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarNombreUsuario(String campo) {
		Pattern pat = null;
        Matcher mat = null;        
        
		pat = Pattern.compile(PATRON_NOMBRE_USUARIO);
        
        mat = pat.matcher(campo);
        
        return mat.find();
	}
	
	/**
	 * Valida que el nombre cumpla un patrón
	 * @param campo Campo a validar
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo True si cumple la condición
	 */
	public static boolean validarNombre(String campo) {
		Pattern pat;
        Matcher mat;        
        
		pat = Pattern.compile(PATRON_NOMBRE);
        
        mat = pat.matcher(campo);
        
        return mat.find();
	}
}
