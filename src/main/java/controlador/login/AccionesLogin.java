package controlador.login;

import java.lang.reflect.Type;
import java.sql.Connection;

import javax.servlet.http.HttpSession;

import model.entitites.UserEntity;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;
import modelos.utiles.UtilSHA;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import controlador.indice.Indice;
import controlador.utiles.UtilesSecurity;
import controller.logic.LogicUser;

//import vistacomun.utiles.UtilesInternacionalizacion;

@Controller
public class AccionesLogin {

	@RequestMapping(value="/doLogin", method=RequestMethod.POST)
	protected ModelAndView doLogin(HttpSession sesion, String emailOrLogin, String password) throws Exception {
		
		//Retornamos el control a la vista de la página.
		//ModelAndView modelAndView = new ModelAndView("proyectos");
		Connection conn = Conexion.obtenerConexion();
		UserEntity user = LogicUser.getUserByLogin(emailOrLogin,UtilSHA.codificarPass(password),conn);
		
		if(UtilesSecurity.usuarioEstaLogueado(sesion,conn)==null && user != null) {
			sesion = UtilesSecurity.loguearUsuario(user, sesion);
			conn.close();
			return new ModelAndView("redirect:/");
		} else {
			conn.close();
			return null;
			//return Indice.irAIndiceConMensaje("Credenciales Incorrectas", sesion);
		}
	}
	
	@RequestMapping(value="/al/action", method=RequestMethod.POST)
	protected ModelAndView doAdminLogin(HttpSession sesion, String login, String password) throws Exception {
		//Retornamos el control a la vista de la página.
		//ModelAndView modelAndView = new ModelAndView("proyectos");
		Connection conn = Conexion.obtenerConexion();
		UserEntity user = LogicUser.getAdminByLogin(login,UtilSHA.codificarPass(password),conn);
		
		if(UtilesSecurity.usuarioEstaLogueado(sesion,conn)==null && user != null) {
			sesion = UtilesSecurity.loguearUsuario(user, sesion);
			conn.close();
			return new ModelAndView("redirect:/");
		} else {
			conn.close();
			return new ModelAndView("redirect:/al.htm");
			//return Indice.irAIndiceConMensaje("Credenciales Incorrectas", sesion);
		}
	}
	
	@RequestMapping("/logout")
	protected ModelAndView logout(HttpSession sesion) throws Exception {
		sesion = UtilesSecurity.logoutUsuario(sesion);
		return new ModelAndView("redirect:/");
	}

}