package controlador.login;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import controlador.logica.LogicaUsuario;
import controlador.utiles.UtilesValidacion;
import controller.logic.LogicUser;

import java.lang.reflect.Type;
import java.sql.Connection;

import model.entitites.UserEntity;
import modelos.clases.Usuario;
import modelos.conexion.Conexion;
import modelos.utiles.UtilSHA;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class Registrar{

	@RequestMapping(value="/registrar", method=RequestMethod.POST)
    public @ResponseBody String registrarse(@RequestBody String  registro) throws Exception {
		Connection conn = Conexion.obtenerConexion();
		Gson gson = new Gson();
		Type tipoUsuario = new TypeToken<UserEntity>(){}.getType();
		UserEntity usuario = gson.fromJson(registro, tipoUsuario);
		String response = ""; 
		if(usuario==null || StringUtils.isBlank(usuario.getLogin()) || StringUtils.isBlank(usuario.getEmail()) || StringUtils.isBlank(usuario.getPassword())){
			response = "0";
		} else {
			if(!UtilesValidacion.validarEmail(usuario.getEmail()))
				response = "2";
			else if(!UtilesValidacion.validarNombreUsuario(usuario.getLogin()))
				response = "3";
			else if(LogicaUsuario.existeLogin(usuario.getLogin(),conn))
				response = "4";
			else if (LogicaUsuario.existeEmail(usuario.getEmail(),conn))
				response = "5";
			else {
				usuario.setPassword(UtilSHA.codificarPass(usuario.getPassword()));
				if(LogicUser.saveUser(null, usuario)!=null){
					response = "1";
				} else {
					response = "6";
				}
				
			}
		}
		conn.close();
		if(response.equals("1"))
			//TODO: devolver status 201 (creado)
			return response;
		else
			//TODO: devolver un status apropiado (300, 500, tropocientos, ...)
			return response;
    }   
}
