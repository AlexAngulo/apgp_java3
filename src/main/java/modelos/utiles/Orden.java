package modelos.utiles;

import java.util.ArrayList;
import java.util.List;

import modelos.clases.Proyecto;

import org.javatuples.Pair;

public class Orden {

//	public static boolean tareaCumpleLaCondicion(Tarea tarea1, Tarea tarea2,
//			int codOrdenacion) {
//		switch (codOrdenacion) {
//		case 0:
//			return tarea1.getPrioridadTarea()>tarea2.getPrioridadTarea();
//		case 1:
//			return tarea1.getPrioridadTarea()<tarea2.getPrioridadTarea();
//		case 2:
//			return tarea1.getDuracion()<tarea2.getDuracion();
//		case 3:
//			return tarea1.getDuracion()>tarea2.getDuracion();
//		case 4:
//			return tarea1.getNombreTarea().compareTo(tarea2.getNombreTarea())<0;
//		case 5:
//			return tarea1.getNombreTarea().compareTo(tarea2.getNombreTarea())>0;
//		case 6:
//			return tarea1.getIdTarea()<tarea2.getIdTarea();
//		case 7:
//			return tarea1.getIdTarea()>tarea2.getIdTarea();
//		case 8:
//			return tarea1.getOrdenTarea()<tarea2.getOrdenTarea();
//		default:
//			return tarea1.getPrioridadTarea()>tarea2.getPrioridadTarea();
//		}
//	}

//	public static String obtenerOrderByTareas(int codOrdenacion, String pre) {
//		switch (codOrdenacion) {
//		case 0:
//			return " ORDER BY " + pre+Tarea.CAMPOS.PRIORIDAD +" DESC";
//		case 1:
//			return " ORDER BY " + pre+Tarea.CAMPOS.PRIORIDAD +" ASC";
//		case 2:
//			return " ORDER BY " + pre+Tarea.CAMPOS.DURACION +" ASC";
//		case 3:
//			return " ORDER BY " + pre+Tarea.CAMPOS.DURACION +" DESC";
//		case 4:
//			return " ORDER BY " + pre+Tarea.CAMPOS.NOMBRE_TAREA +" ASC";
//		case 5:
//			return " ORDER BY " + pre+Tarea.CAMPOS.NOMBRE_TAREA +" DESC";
//		case 6:
//			return " ORDER BY " + pre+Tarea.CAMPOS.ID_TAREA +" ASC";
//		case 7:
//			return " ORDER BY " + pre+Tarea.CAMPOS.ID_TAREA +" DESC";
//		case 8:
//			return " ORDER BY " + pre+Tarea.CAMPOS.ORDEN_TAREA +" ASC";
//		default:
//			return " ORDER BY " + pre+Tarea.CAMPOS.PRIORIDAD +" DESC";
//		}
//	}

	public static List<Pair<Integer,String>> obtenerHashMapOrdenacionTareas() {
		List<Pair<Integer,String>> lp = new ArrayList<Pair<Integer,String>>();
		lp.add(Pair.with(0,"Por prioridad > m�s alta primero"));
		lp.add(Pair.with(1,"Por prioridad < m�s baja primero"));
		lp.add(Pair.with(2,"Por duraci�n < m�s bajo primero"));
		lp.add(Pair.with(3,"Por duraci�n > m�s alto primero"));
		lp.add(Pair.with(4,"Alfab�ticamente < de la A a la Z"));
		lp.add(Pair.with(5,"Alfab�ticamente > de la Z a la A"));
		lp.add(Pair.with(6,"Por fecha de creaci�n < de la primera a la �ltima"));
		lp.add(Pair.with(7,"Por fecha de creaci�n > de la �ltima a la primera"));
		return lp;
	}
	
	/*codOrdenacion:
	 * 0 = por fecha ascendente
	 * 1 = por fecha descendente
	 * 2 = por orden de creación ascendente
	 * 3 = por orden de creación descendente
	 * 4 = alfabeticamente ascendente
	 * >4 = alfabéticamente descendente*/
	
	public static String obtenerOrderByProyectos(int codOrdenacion, String pre){
		switch(codOrdenacion) {
		case 0:
			return " ORDER BY "+pre+Proyecto.CAMPOS.FECHA_FIN +" ASC";
		case 1:
			return " ORDER BY "+pre+Proyecto.CAMPOS.FECHA_FIN +" DESC";
		case 2:
			return " ORDER BY "+pre+Proyecto.CAMPOS.ID_PROYECTO +" ASC";
		case 3:
			return " ORDER BY "+pre+Proyecto.CAMPOS.ID_PROYECTO +" DESC";
		case 4:
			return " ORDER BY "+pre+Proyecto.CAMPOS.NOMBRE_PROYECTO +" ASC";
		case 5:
			return " ORDER BY "+pre+Proyecto.CAMPOS.NOMBRE_PROYECTO +" DESC";
		case 7:
			return " ORDER BY "+pre+Proyecto.CAMPOS.PRIORIDAD +" ASC";
		case 8:
			return " ORDER BY "+pre+Proyecto.CAMPOS.PRIORIDAD +" DESC";
		default:
			return " ORDER BY "+pre+Proyecto.CAMPOS.ORDEN_PROYECTO +" ASC";
		}
	}

}
