package modelos.utiles;

public class UtilesDatos {
	public static String convertirIntEnStringDeDosCifras(int integer){
		return (integer<10 ? "0"+integer : ""+integer);
	}
}
