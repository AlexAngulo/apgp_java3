package modelos.utiles;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

public class UtilBD {
	
	//---------------------------------------------------------------------------------------------------------------------
	
	//TODO(ROBERTO): Dividir en subfunciones este metodo
	@SuppressWarnings("rawtypes")
	private static String i_crea_predicado_segun_valor_de_campo(
									Entry<String, Object> campo, 
									boolean es_un_campo_de_caracteristicas,
									boolean string_exacto)
									
	{
		String valor_campo, valor_verdadero, valor_falso;
		
		if (es_un_campo_de_caracteristicas == false)
		{
			valor_campo = campo.getKey();
			valor_verdadero = "'S'";
			valor_falso = "'N'";
		}
		else
		{
			String pre = " ExtractValue(caracteristicas,'/caracteristicas/campo[";
			String post = "]/valor') ";
			valor_campo = pre + campo.getKey() + post;
			valor_verdadero = "'true'";
			valor_falso = "'false'";
		}
		
		String where = "";
		
		if (campo.getValue() == null) {
			where += valor_campo + " IS NULL ";
		//Si el campo es un string utilizamos la clausula like
		} else if (campo.getValue().getClass().equals(String.class)) {
			if(string_exacto == true)
				where += valor_campo + " = '" + campo.getValue() + "'";
			else
				where += valor_campo + " LIKE '%" + campo.getValue() + "%'";
		//Si el campo es un booleamos comparamos con S/N en lugar de con true/false
		} else	if (campo.getValue().getClass().equals(Boolean.class)) {
			if (Boolean.parseBoolean(campo.getValue().toString())) {
				where += valor_campo + "= " + valor_verdadero;
			} else {
				where += valor_campo + "= " + valor_falso;
			}
		//Si el campo es un rango buscamos entre maximo y minimo
		} else if (campo.getValue().getClass().equals(Rango.class)) {
			Rango r = (Rango) campo.getValue();
			
			//Para comparar con dobles el rango tiene que ser de dobles
			if(r.getMinimo().getClass().equals(Double.class) || r.getMaximo().getClass().equals(Double.class))
			{
				//Rango de dobles pero solo con minimo o solo con maximo
				if(r.getMinimo().equals(Double.NaN) || r.getMaximo().equals(Double.NaN))
				{
					if(r.getMaximo().equals(Double.NaN))
					{
						where += valor_campo + "<=" + r.getMinimo().toString();
					}
					if(r.getMinimo().equals(Double.NaN))
					{
						where += valor_campo + ">=" + r.getMaximo().toString();
					}
				}
				else
				{
					where += valor_campo + ">=" + r.getMinimo().toString() + " AND " + 
							valor_campo + "<=" + r.getMaximo();
				}
			}
			//Rango de entero
			else if(r.getMinimo().getClass().equals(Integer.class) || r.getMaximo().getClass().equals(Integer.class))
			{
				//Rango de enteros pero solo con minimo o solo con maximo
				if((Integer)r.getMinimo() == Integer.MIN_VALUE || (Integer)r.getMaximo() == Integer.MAX_VALUE)
				{
					if((Integer)r.getMaximo() == Integer.MAX_VALUE)
					{
						where += valor_campo + "<=" + r.getMinimo().toString();
					}
					if((Integer)r.getMinimo() == Integer.MIN_VALUE)
					{
						where += valor_campo + ">=" + r.getMaximo().toString();
					}
				}
				else
				{
					where += valor_campo + ">=" + r.getMinimo().toString() + " AND " + 
							valor_campo + "<=" + r.getMaximo();
				}
			}
			//Rango de strings (fechas) solo mínimo
			else if(r.getMaximo().equals("") && !r.getMinimo().equals("")){
				where += valor_campo + ">'" +  r.getMinimo() + "'";
			}
			//Rango de strings (fechas) solo máximo
			else if(r.getMinimo().equals("") && !r.getMaximo().equals("")){
				where += valor_campo + "<='" +  r.getMaximo() + "'";
			}
			//Rango de strings (fechas)
			else {
				where += valor_campo + " BETWEEN '" + r.getMinimo().toString() + "' AND '" + r.getMaximo() + "'";
			}
		//TODO: HACER QUE LA FECHA PUEDA SER NULL Y DE OTRA FORMA
		} else if (campo.getValue().getClass().equals(ArrayList.class) && ((ArrayList) campo.getValue()).get(0).getClass().equals(Rango.class)) {
			ArrayList<Rango> l = (ArrayList<Rango>) campo.getValue();
			boolean he_empezado = false;
			String where_rango = "";
			
			for(Rango r : l){
				if(r == null){
					if(he_empezado == true){
						where_rango += " OR " + valor_campo + " IS NULL ";
					} else {
						where_rango += valor_campo + " IS NULL ";
						he_empezado = true;
					}
				}else {
					//Para comparar con dobles el rango tiene que ser de dobles
					if(r.getMinimo().getClass().equals(Double.class) || r.getMaximo().getClass().equals(Double.class))
					{
						//Rango de dobles pero solo con minimo o solo con maximo
						if(r.getMinimo().equals(Double.NaN) || r.getMaximo().equals(Double.NaN))
						{
							if(r.getMaximo().equals(Double.NaN))
							{
								if(he_empezado == true){
									where_rango += " OR " + valor_campo + "<=" + r.getMinimo().toString();
								} else {
									where_rango += valor_campo + "<=" + r.getMinimo().toString();
									he_empezado = false;
								}
							}
							if(r.getMinimo().equals(Double.NaN))
							{
								if(he_empezado == true){
									where_rango += " OR " + valor_campo + ">=" + r.getMaximo().toString();
								} else {
									where_rango += valor_campo + ">=" + r.getMaximo().toString();
									he_empezado = true;
								}
							}
						}
						else
						{
							if(he_empezado == true){
								where_rango += " OR " + valor_campo + ">=" + r.getMinimo().toString() + " AND " + 
										valor_campo + "<=" + r.getMaximo();
							} else {
								where_rango += valor_campo + ">=" + r.getMinimo().toString() + " AND " + 
										valor_campo + "<=" + r.getMaximo();
								he_empezado = true;
							}
						}
					}
					//Rango de entero
					else if(r.getMinimo().getClass().equals(Integer.class) || r.getMaximo().getClass().equals(Integer.class))
					{
						//Rango de enteros pero solo con minimo o solo con maximo
						if((Integer)r.getMinimo() == Integer.MIN_VALUE || (Integer)r.getMaximo() == Integer.MAX_VALUE)
						{
							if((Integer)r.getMaximo() == Integer.MAX_VALUE)
							{
								if(he_empezado == true){
									where_rango += " OR " + valor_campo + "<=" + r.getMinimo().toString();
								} else {
									where_rango += valor_campo + "<=" + r.getMinimo().toString();
									he_empezado = true;
								}
							}
							if((Integer)r.getMinimo() == Integer.MIN_VALUE)
							{
								if(he_empezado == true){
									where_rango += " OR " + valor_campo + ">=" + r.getMaximo().toString();
								} else {
									where_rango += valor_campo + ">=" + r.getMaximo().toString();
									he_empezado = true;
								}
							}
						}
						else
						{
							if(he_empezado == true){
								where_rango += " OR " + valor_campo + ">=" + r.getMinimo().toString() + " AND " + 
										valor_campo + "<=" + r.getMaximo();
							} else {
								where_rango += valor_campo + ">=" + r.getMinimo().toString() + " AND " + 
										valor_campo + "<=" + r.getMaximo();
								he_empezado = true;
							}
						}
					}
					//Rango de strings (fechas) solo mínimo
					else if(r.getMaximo().equals("") && !r.getMinimo().equals("")){
						if(he_empezado == true){
							where_rango += " OR " + valor_campo + ">'" +  r.getMinimo() + "'";
						} else {
							where_rango += valor_campo + ">'" +  r.getMinimo() + "'";
							he_empezado = true;
						}
					}
					//Rango de strings (fechas) solo máximo
					else if(r.getMinimo().equals("") && !r.getMaximo().equals("")){
						if(he_empezado == true){
							where_rango += " OR " + valor_campo + "<='" +  r.getMaximo() + "'";
						} else {
							where_rango += valor_campo + "<='" +  r.getMaximo() + "'";
							he_empezado = true;
						}
					}
					//Rango de strings (fechas)
					else {
						if(he_empezado == true){
							where_rango += " OR " + valor_campo + " BETWEEN '" + r.getMinimo().toString() + "' AND '" + r.getMaximo() + "'";
						} else {
							where_rango += valor_campo + " BETWEEN '" + r.getMinimo().toString() + "' AND '" + r.getMaximo() + "'";
							he_empezado = true;
						}
					}
				}
			}
			
			if (l.size() > 0)
				where += " (" + where_rango + ") ";
			
		//Si el campo es una lista utilizamos IN (...)
		} else if (campo.getValue().getClass().equals(ArrayList.class)) {
			ArrayList l = (ArrayList) campo.getValue();
			
			where += valor_campo + " IN (";
			
			String lista = "";
			
			if (l.size() > 0) {
				for (Object valor : l) {
					if (StringUtils.isNotBlank(lista)) {
						lista += ", ";
					}
					
					if (valor.getClass().equals(Integer.class) ||
						valor.getClass().equals(Double.class)) {
						lista += valor.toString();
					} else {
						lista += "'" + valor.toString() + "'";
					}
				}
			}
			
			where += lista + ")";
		//Si el campo es de cualquier otro tipo utilizamos el =
		} else {
			where += valor_campo + "=" + campo.getValue();
		}
		
		return where;
	}
	
	//-----------------------------------------------------------------------------------------------------------------------
	
	private static String i_crea_predicados_where(boolean son_filtros_de_caracteristicas, HashMap<String, Object> filtros,
			boolean string_exacto)
	{
		String where = "";
		
		if (filtros != null && !filtros.entrySet().isEmpty()) 
		{
			Iterator<Entry<String, Object>> iterator = filtros.entrySet().iterator();
			
			while (iterator.hasNext() == true) 
			{
				Entry<String, Object> campo = iterator.next();
				
				if (campo.getValue() != null) 
				{
					if (StringUtils.isNotBlank(where)) 
						where += " AND ";
					
					where += i_crea_predicado_segun_valor_de_campo(campo, son_filtros_de_caracteristicas, string_exacto);
				}
				else if (campo.getValue() == null) 
				{
					if (StringUtils.isNotBlank(where)) 
						where += " AND ";
					
					where += campo.getKey() + " IS NULL";
				}
			}
		}
		
		return where;
	}
	
	/**
	 * Crea la clausula web de una sentencia sql en funcion de un mapa de filtros
	 * @param filtros Mapa de filtros
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula where
	 */

	public static String crearClausulaWhere(HashMap<String, Object> filtros) {
		boolean buscar_con_string_exacto = false;
		
		return i_crear_clausula_where(filtros, buscar_con_string_exacto);
	}
	
	public static String crearClausulaWhereStringExacto(HashMap<String, Object> filtros) {
		boolean buscar_con_string_exacto = true;
		
		return i_crear_clausula_where(filtros, buscar_con_string_exacto);
	}
	
	private static String i_crear_clausula_where(HashMap<String, Object> filtros,boolean string_exacto) {
		
		String where;
		boolean son_filtros_de_caracteristicas = false;
		
		where = i_crea_predicados_where(son_filtros_de_caracteristicas, filtros, string_exacto);
		
		if (StringUtils.isNotBlank(where) == true) 
			where = " WHERE " + where;
		
		return where;
	}
	
	/**
	 * Crea la clausula order by de una sentencia sql en funcion de un campo y el orden
	 * @param campo Campo por el que se quiere ordenar
	 * @param ascendente True si el orden se quiere ascendente, false si quiere descendente
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula order by
	 */
	
	public static String crearClausulaOrderBy(String campo, boolean ascendente) {
		String orderBy = "";
		
		if (StringUtils.isNotBlank(campo)) 
		{
			orderBy += " ORDER BY " + campo + (!ascendente ? " DESC " : "");
		}
		
		return orderBy;
	}
	
	/**
	 * Crea la clausula order by de una sentencia sql en funcion de varios campos y el orden
	 * @param campos_ordenacion Campos por lo que se quiere ordenar (se sigue el orden de la lista)
	 * @param ascendente True si el orden se quiere ascendente, false si quiere descendente
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula order by
	 */
	
	public static String crearClausulaOrderByVariosCampos(ArrayList<String> campos_ordenacion, boolean se_sigue_orden_ascendente) {
		String tipoNodo = "";
		boolean con_caracteristicas = false;
		return i_crearClausulaOrderByVariosCamposConOSinCaracteristicas(campos_ordenacion,tipoNodo,se_sigue_orden_ascendente, con_caracteristicas);
	}
	
	public static String crearClausulaOrderByVariosCamposConCaracteristicas(ArrayList<String> campos_ordenacion, String tipoNodo, boolean se_sigue_orden_ascendente)
	{
		boolean con_caracteristicas = true;
		return i_crearClausulaOrderByVariosCamposConOSinCaracteristicas(campos_ordenacion,tipoNodo,se_sigue_orden_ascendente, con_caracteristicas);
	}
	
	private static String i_crearClausulaOrderByVariosCamposConOSinCaracteristicas(ArrayList<String> campos_ordenacion, 
			String tipoNodo, boolean se_sigue_orden_ascendente,
			boolean con_caracteristicas)
	{
		ArrayList<Boolean> ordenaciones = null;
		boolean se_utiliza_el_array = false;
		return crearClausulaOrderByVariosCamposVariosTiposDeOrdenacionConOSinCaracteristicas(campos_ordenacion,tipoNodo,
				ordenaciones, con_caracteristicas, se_sigue_orden_ascendente, se_utiliza_el_array);
	}
	
	/**
	 * Crea la clausula order by de una sentencia sql en funcion de varios campos y el orden
	 * @param campos_ordenacion Campos por lo que se quiere ordenar (se sigue el orden de la lista)
	 * @param ascendente True si el orden se quiere ascendente, false si quiere descendente
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula order by
	 */
	
	public static String crearClausulaOrderByVariosCamposVariosTiposDeOrdenacion(ArrayList<String> campos_ordenacion,
			ArrayList<Boolean> ordenaciones) {
		String tipoNodo = "";
		boolean con_caracteristicas = false;
		boolean se_utiliza_el_array = true;
		return crearClausulaOrderByVariosCamposVariosTiposDeOrdenacionConOSinCaracteristicas(campos_ordenacion,tipoNodo, 
				ordenaciones, con_caracteristicas, false, se_utiliza_el_array);
	}
	
	public static String crearClausulaOrderByVariosCamposVariosTiposDeOrdenacionConCaracteristicas(ArrayList<String> campos_ordenacion,
			String tipoNodo, ArrayList<Boolean> ordenaciones)
	{
		boolean con_caracteristicas = true;
		boolean se_utiliza_el_array = true;
		return crearClausulaOrderByVariosCamposVariosTiposDeOrdenacionConOSinCaracteristicas(campos_ordenacion,tipoNodo,
				ordenaciones, con_caracteristicas, false, se_utiliza_el_array);
	}
	
	private static String crearClausulaOrderByVariosCamposVariosTiposDeOrdenacionConOSinCaracteristicas(ArrayList<String> campos_ordenacion, 
			String tipoNodo, ArrayList<Boolean> ordenaciones,
			boolean con_caracteristicas, boolean ordenacion,
			boolean se_utiliza_el_array)
	{
		String orderBy = "";
		
		if(se_utiliza_el_array == true && campos_ordenacion.size() > ordenaciones.size())
			return orderBy;
		
		for (int i = 0; i < campos_ordenacion.size(); i++)
		{
			
			String campo_ordenacion_i = campos_ordenacion.get(i);
			if(se_utiliza_el_array == true)
				ordenacion = ordenaciones.get(i);
			
			if (StringUtils.isNotBlank(campo_ordenacion_i)) 
			{
				if(con_caracteristicas == true){
					boolean es_un_numero = i_cadena_es_un_numero_entero(campo_ordenacion_i);
				
					if (es_un_numero == true)
						campo_ordenacion_i = i_campo_caracteristicas_segun_numero_nodo(campo_ordenacion_i, tipoNodo);
				}

				if (i == 0)
					orderBy += " ORDER BY " + campo_ordenacion_i;
				else
					orderBy += ", " + campo_ordenacion_i;
				
				if (i < campos_ordenacion.size())
					orderBy += (!ordenacion ? " DESC " : "");
			}
		}
		
		return orderBy;
	}
	
	
	
	/**
	 * Comprueba si existe la columna en el resultSet
	 * @param rs ResultSet que tenemos como resultado
	 * @param columnaBuscada nombre de la columna que queremos saber si existe en el resultset
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si existe la columna
	 */
	
	public static boolean existeColumna(ResultSet rs, String columnaBuscada) throws SQLException
	{
		boolean existe = false;
		
		ResultSetMetaData rsMetaData = rs.getMetaData();
		int numeroDeColumnas = rsMetaData.getColumnCount();

		for (int i = 1; i < numeroDeColumnas + 1 && !existe; i++) {
		    String nombreColumna = rsMetaData.getColumnName(i);
		    if (columnaBuscada.equals(nombreColumna))
		    {
		        existe = true;
		    }
		}
		
		return existe;
	}
	
	/**
	 * Crea la clausula where de una sentencia sql en funcion de un mapa de filtros de caracterśiticas
	 * @param filtros Mapa de filtros
	 * @param whereAnterior Cadena con el where de filtros normales, que puede o no preceder a la where de las características.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula where
	 */
	
	public static String crearClausulaWhereCaracteristicas(HashMap<String, Object> filtros, String whereAnterior)
	{
		String clausula_where = "";
		boolean buscar_con_string_exacto = false;
		
		String predicados = i_crea_predicados_where(true, filtros, buscar_con_string_exacto);
		
		//No hay clausula where anterior.
		if (StringUtils.isNotBlank(predicados) == true && StringUtils.isBlank(whereAnterior) == true) 
			clausula_where = " WHERE " + predicados;
			
		//Unir las dos clausulas where con un AND.
		if (StringUtils.isNotBlank(predicados) == true && StringUtils.isNotBlank(whereAnterior) == true) 
			clausula_where = " AND " + predicados;
		
		
		return clausula_where;
	}
	
	//---------------------------------------------------------------------------------------------------------------------
	
	private static String i_concatena_a_predicados_fijos_cada_filtro_separado_por_OR(
								HashMap<String, Object> filtros, 
								boolean son_filtros_de_caracteristicas,
								String cadena)
	{
		String cadena_resultante = "";
		
		if (filtros != null && !filtros.entrySet().isEmpty()) 
		{
			Iterator<Entry<String, Object>> iterator = filtros.entrySet().iterator();
			
			while (iterator.hasNext() == true) 
			{
				Entry<String, Object> campo = iterator.next();
				String cadena_i;
				String predicado_filtro;
				boolean buscar_con_string_exacto = false;
				
				predicado_filtro = i_crea_predicado_segun_valor_de_campo(campo, son_filtros_de_caracteristicas, buscar_con_string_exacto);
				
				cadena_i = "(" + cadena + " AND " + predicado_filtro + ")";
				
				if (iterator.hasNext() == true)
					cadena_i += " OR ";
				
				cadena_resultante += cadena_i;
			}
		}
		else
		{
			cadena_resultante = cadena;
		}
		
		return cadena_resultante;
	}
	
	//---------------------------------------------------------------------------------------------------------------------
	
	private static String i_predicados_fijos(
								HashMap<String, Object> filtros_fijos,
								HashMap<String, Object> filtros_caracteristicas_fijos)
	{
		boolean son_filtros_de_caracteristicas = false;
		boolean buscar_con_string_exacto = false;
		String predicados_fijos = i_crea_predicados_where(son_filtros_de_caracteristicas, filtros_fijos, buscar_con_string_exacto);
		
		son_filtros_de_caracteristicas = true;
		String predicados_fijos_caracteristicas = i_crea_predicados_where(son_filtros_de_caracteristicas, filtros_caracteristicas_fijos, buscar_con_string_exacto);
	
		if (StringUtils.isNotEmpty(predicados_fijos_caracteristicas) == true)
			predicados_fijos += " AND " + predicados_fijos_caracteristicas;
		
		return predicados_fijos;
	}
	
	//---------------------------------------------------------------------------------------------------------------------
	
	private static String i_predicados_fijos_sin_caracteristicas(
			HashMap<String, Object> filtros_fijos)
	{
		boolean son_filtros_de_caracteristicas = false;
		boolean buscar_con_string_exacto = false;
		String predicados_fijos = i_crea_predicados_where(son_filtros_de_caracteristicas, filtros_fijos,buscar_con_string_exacto);
		
		return predicados_fijos;
	}
	
	/**
	 * Crea la clausula where de una sentencia sql en funcion de un mapa de filtros fijos y a los que se les añadiran
	 * los filtros optativos separados por la cláusula OR.
	 * @param filtros_fijos Mapa de campos que contiene los campos de los que se compondrá como mínimo la claúsula where
	 * @param filtros_optativos Mapa de campos que se añadiran a los campos fijos
	 * @param filtros_caracteristicas_optativos Mapa de campos del campo caracteristicas que se añadiran a los campos fijos  
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo La cláusula where resultante
	 */
	
	public static String creaClausulaWhereFiltrosSeparadosConOR(
								HashMap<String, Object> filtros_fijos,
								HashMap<String, Object> filtros_caracteristicas_fijos,
								HashMap<String, Object> filtros_optativos,
								HashMap<String, Object> filtros_caracteristicas_optativos)
	{
		boolean son_filtros_de_caracteristicas;
		String predicados_fijos;
		
		predicados_fijos = i_predicados_fijos(filtros_fijos, filtros_caracteristicas_fijos);
				
		son_filtros_de_caracteristicas = false;
		String predicados_separados_por_OR = i_concatena_a_predicados_fijos_cada_filtro_separado_por_OR(
													filtros_optativos, son_filtros_de_caracteristicas, predicados_fijos);
		
		if (filtros_caracteristicas_optativos != null && filtros_caracteristicas_optativos.size() > 0)
		{
			String predicadosCarac_separados_por_OR;
			
			if (StringUtils.isNotEmpty(predicados_separados_por_OR) == true)
				predicados_separados_por_OR += " OR ";
			
			son_filtros_de_caracteristicas = true;
			
			predicadosCarac_separados_por_OR = i_concatena_a_predicados_fijos_cada_filtro_separado_por_OR(
													filtros_caracteristicas_optativos, 
													son_filtros_de_caracteristicas, predicados_fijos);
			
			predicados_separados_por_OR += predicadosCarac_separados_por_OR;
		}
		
		predicados_separados_por_OR = "WHERE " + predicados_separados_por_OR;
		
		return predicados_separados_por_OR;
	}
	
	/**
	 * Crea clausula where para los recambios en funcion de filtros fijos y filtros optativos
	 * @param filtros_fijos
	 * @param filtros_optativos
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo La clausula where resultante
	 */
	public static String creaClausulaWhereRecambiosFiltrosSeparadosConOR(
			HashMap<String, Object> filtros_fijos,
			HashMap<String, Object> filtros_optativos)
	{
		boolean son_filtros_de_caracteristicas = false;
		String predicados_fijos;
		
		predicados_fijos = i_predicados_fijos_sin_caracteristicas(filtros_fijos);
		
		String predicados_separados_por_OR = i_concatena_a_predicados_fijos_cada_filtro_separado_por_OR(
										filtros_optativos, son_filtros_de_caracteristicas, predicados_fijos);
		
		predicados_separados_por_OR = "WHERE " + predicados_separados_por_OR;
		
		return predicados_separados_por_OR;
	}
	
	
	/**
	 * Crea la clausula order by de una sentencia sql en funcion de un campo, el orden y el tipo del campo.
	 * El campo por el que ordenamos esta en el xml de caracteristicas. Si no hacemos un cast sobre el campo
	 * de la sentencia order by siempre ordenara como si fuera una cadena de texto, eso quiere decir que 
	 * no ordenaria bien en casos como enteros o decimales.
	 * @param campo Campo por el que se quiere ordenar
	 * @param ascendente True si el orden se quiere ascendente, false si quiere descendente
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula order by
	 */
	
	public static String crearClausulaOrderByCaracteristicas(String numeroNodo, String tipoNodo, boolean ascendente)
	{
		String orderBy;
		
		String campoCast = i_campo_caracteristicas_segun_numero_nodo(numeroNodo, tipoNodo);
		orderBy = " ORDER BY " + campoCast + (!ascendente ? " DESC " : "");
		
		return orderBy;
	}
	
	private static String i_campo_caracteristicas_segun_numero_nodo(String numeroNodo, String tipoNodo)
	{
		// TODO ROBERTO: Llevar a función, estas tres líneas están repetidas
		String pre = " ExtractValue(caracteristicas,'/caracteristicas/campo[";
		String post = "]/valor') ";
		String campoCast = "";
		
		String campo = pre + numeroNodo + post;
		
		if(tipoNodo.equals("int"))
		{
			campoCast = "cast(" + campo + " as unsigned)";
		}
		else if(tipoNodo.equals("double"))
		{
			campoCast = "cast(" + campo + " as decimal)";
		}
		
		return campoCast;
	}
	
	private static boolean i_cadena_es_un_numero_entero(String cadena)
	{
		try
		{
			Integer.parseInt(cadena);
			return true;
		}
		catch(NumberFormatException e)
		{
			return false;
		}
	}
	
	/**
	 * Crea la clausula de Select mediante un arraylist de columnas que se quieren ver
	 * @param columnas Arraylist de String que simbolizan las columnas que se quieren ver 
	 * en la consulta
	 * @param tablas Arraylist de String que simboliza las tablas que se van a usar en 
	 * la consulta
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula Select con las tablas y columnas dadas
	 */
	
	public static String crearClausulaSelect(ArrayList<String> columnas, ArrayList<String> tablas){
		
		assert(columnas != null);
		assert(tablas != null);
		
		int num_columnas = columnas.size();
		int num_tablas = tablas.size();
		String retorno;
		
		if (num_columnas == 0 || num_tablas == 0) 
			return "";
		else 
			retorno = "SELECT " + columnas.get(0) + " ";
		
		for (int i = 1; i < num_columnas; i++)
			retorno += ", " + columnas.get(i) + " ";
	
		assert(num_tablas > 0);
		retorno += " FROM " + tablas.get(0);
		
		for (int i = 1;i < num_tablas; i++)
			retorno += ", " + tablas.get(i) + " ";

		return retorno;
	}
	
	/**
	 *  Crea la clausula de Insert mediante un arraylist de columnas que se quieren ver y la tabla 
	 *  a la que se va a insertar
	 * @param tabla Tabla relacionada
	 * @param nombre_columnas Columnas implicadas
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Clausula insert segun columnas y tabla dada
	 */
	
	public static String crearClausulaInsertPrepareStatement(
								String tabla, ArrayList<String> nombre_columnas){
		String retorno = null;
		
		assert (tabla != null);
		assert (tabla.length() > 0);
		assert (nombre_columnas != null);
		
		int num_columnas = nombre_columnas.size();
		
		if (num_columnas == 0) 
			return "";
		
		retorno = "INSERT INTO " + tabla + " (" + nombre_columnas.get(0);
		
		for (int i = 1 ; i < num_columnas ; i++)
			retorno += ", " + nombre_columnas.get(i);
		
		retorno += ") VALUES (?";
		
		for (int i = 1 ; i < num_columnas ; i++)
			retorno += ",?";
		
		retorno += ")";
		
		return retorno;
	}
}
