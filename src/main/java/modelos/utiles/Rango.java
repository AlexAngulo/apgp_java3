package modelos.utiles;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Rango<T> {
	private T minimo;
	private T maximo;

	public Rango() {
		this.minimo = null;
		this.maximo = null;
	}
	
	public Rango(T minimo, T maximo) {
		this.minimo = minimo;
		this.maximo = maximo;
	}
	
	public T getMinimo() {
		return minimo;
	}

	public void setMinimo(T minimo) {
		this.minimo = minimo;
	}

	public T getMaximo() {
		return maximo;
	}

	public void setMaximo(T maximo) {
		this.maximo = maximo;
	}

	public void setRango(T minimo, T maximo) {
		this.minimo = minimo;
		this.maximo = maximo;
	}
	
	public static Rango<String> i_rango_con_fecha_maxima_actual_menos_dias(int dias)
	{	
		boolean suma = false;
		return i_rango_con_fecha_maxima_actual_mas_o_menos_dias(dias, suma);
	}
	
	public static Rango<String> i_rango_con_fecha_maxima_actual_mas_dias(int dias)
	{	
		boolean suma = true;
		return i_rango_con_fecha_maxima_actual_mas_o_menos_dias(dias, suma);
	}
	
	private static Rango<String> i_rango_con_fecha_maxima_actual_mas_o_menos_dias(int dias, boolean suma)
	{	
		boolean maxima = true;
		return i_rango_con_fecha_maxima_o_minima_actual_mas_o_menos_dias(dias, suma, maxima);
	}
	
	public static Rango<String> i_rango_con_fecha_minima_actual_mas_dias(int dias)
	{
		boolean suma= true;
		return i_rango_con_fecha_minima_actual_mas_o_menos_dias(dias, suma);
	}
	
	public static Rango<String> i_rango_con_fecha_minima_actual_menos_dias(int dias)
	{
		boolean suma = false;
		return i_rango_con_fecha_minima_actual_mas_o_menos_dias(dias, suma);
	}
		
	private static Rango<String> i_rango_con_fecha_minima_actual_mas_o_menos_dias(int dias, boolean suma)
	{
		boolean maxima = false;
		return i_rango_con_fecha_maxima_o_minima_actual_mas_o_menos_dias(dias, suma, maxima);
	}
	
	private static Rango<String> i_rango_con_fecha_maxima_o_minima_actual_mas_o_menos_dias(int dias, boolean suma, boolean maxima)
	{	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Date dia = new Date();
		long ms = dia.getTime();
		long cantidadDeMsEnUnDia = 24 * 60 * 60 * 1000;
		long diasEnMs = cantidadDeMsEnUnDia * (long) dias;
		
		if(suma == true)
			ms = ms + diasEnMs;
		else
			ms = ms - diasEnMs;
		
		dia.setTime(ms);
		
		Rango<String> rangoFecha = new Rango<String>();
		
		if(maxima == true){
			rangoFecha.setMinimo(sdf.format(dia));
			
			if(suma == true)
				rangoFecha.setMaximo("");
			else
				rangoFecha.setMaximo(sdf.format(new Date()));
		} else {
			if(suma == false)
				rangoFecha.setMaximo("");
			else
				rangoFecha.setMaximo(sdf.format(new Date()));
			
			rangoFecha.setMinimo(sdf.format(dia));
		}
		
		return rangoFecha;
	}
	
	public static Rango<String> i_rango_con_fecha_maxima_con_mas_dias(int dias){
		boolean suma = true;
		return i_rango_con_fecha_maxima_con_mas_o_menos_dias(dias, suma);
	}
	
	public static Rango<String> i_rango_con_fecha_maxima_con_menos_dias(int dias){
		boolean suma = false;
		return i_rango_con_fecha_maxima_con_mas_o_menos_dias(dias, suma);
	}
	
	private static Rango<String> i_rango_con_fecha_maxima_con_mas_o_menos_dias(int dias, boolean suma){
		Rango<String> rango = new Rango<String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dia = new Date();
		
		long ms = dia.getTime();
		long cantidadDeMsEnUnDia = 24 * 60 * 60 * 1000;
		long diasEnMs = cantidadDeMsEnUnDia * (long) dias;
		
		if(suma == true)
			ms = ms + diasEnMs;
		else
			ms = ms - diasEnMs;
		
		dia.setTime(ms);
		rango.setMaximo(sdf.format(dia));
		rango.setMinimo("");
		
		return rango;
	}
	
	public static Rango<String> i_rango_con_fecha_maxima_actual()
	{	
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Rango<String> rangoFecha = new Rango<String>();
		rangoFecha.setMinimo("");
		rangoFecha.setMaximo(sdf.format(new Date()));
		
		return rangoFecha;
	}
	
	public static Rango<String> i_rango_con_fecha_minima_actual()
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Rango<String> rangoFecha = new Rango<String>();
		rangoFecha.setMinimo(sdf.format(new Date()));
		rangoFecha.setMaximo("");
		
		return rangoFecha;
	}
}
