package modelos.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.clasesAbstractas.IntMiembroGrupo;

import org.javatuples.Pair;

public class TopePermisos {
	public static int CREAR_TAREAS = 0;
	public static int CREAR_PROYECTOS = 1;
	public static int EDITAR_TAREAS = 2;
	public static int ASIGNAR_TAREAS = 3;
	public static int CREAR_EQUIPO = 4;
	public static int EDITAR_EQUIPO = 5;
	public static int ELIMINAR_EQUIPO = 6;
	public static int EDITAR_ASIGNAMIENTO = 7;
	public static int ELIMINAR_ASIGNAMIENTO = 8;
	public static int EDITAR_PROYECTOS = 9;
	public static int COMPARTIR_PROYECTO = 10;
	public static int EDITAR_COMPARTIR = 11;
	public static int ELIMINAR_COMPARTIR = 12;
	public static int ELIMINAR_TAREAS = 13;
	public static int ELIMINAR_PROYECTOS = 14;
	public static int CREAR_SUBGRUPO = 15;
	public static int INVITAR_MIEMBRO = 16;
	public static int ELIMINAR_SUBGRUPO = 17;
	public static int EDITAR_MIEMBRO = 18;
	public static int EDITAR_GRUPO = 19;
	public static int ELIMINAR_MIEMBROS = 20;
	public static int ELIMINAR_GRUPO = 21;
	
	public static int CANTIDAD_PERMISOS = 22;
	
	public static List<String> getTopePermisosGrupo(){
		List<String> lista = new ArrayList<String>();
		lista.add("NINGUNA");
		lista.add("CREAR TAREAS");
		lista.add("CREAR PROYECTOS");
		lista.add("EDITAR TAREAS");
		lista.add("ASIGNAR TAREAS");
		lista.add("CREAR EQUIPO");
		lista.add("EDITAR EQUIPO");
		lista.add("ELIMINAR EQUIPO");
		lista.add("EDITAR ASIGNADOS");
		lista.add("ELIMINAR ASIGNADOS");
		lista.add("EDITAR PROYECTOS");
		lista.add("COMPARTIR PROYECTOS");
		lista.add("EDITAR COMPARTIR");
		lista.add("DESVINCULAR PROYECTOS");
		lista.add("ELIMINAR TAREAS");
		lista.add("ELIMINAR PROYECTOS");
		lista.add("CREAR SUBGRUPO");
		lista.add("INVITAR MIEMBROS");
		lista.add("ELIMINAR SUBGRUPO");
		lista.add("EDITAR MIEMBROS");
		lista.add("EDITAR PROYECTO");
		lista.add("ELIMINAR MIEMBROS");
		lista.add("ELIMINAR PROYECTO");
		return lista;
	}
	
	public static Pair<Integer,String> obtenerNombrePermisoMasAlto(boolean crearTareas, boolean crearProyectos, boolean editarTareas,
			boolean editarProyectos, boolean eliminarTareas, boolean eliminarProyectos, boolean crearSubgrupo,
			boolean invitarMiembros, boolean eliminarSubgrupo, boolean editarMiembros, boolean editarGrupo,
			boolean eliminarMiembros, boolean eliminarGrupo,boolean compartirProyectos, boolean editarCompartir, boolean eliminarCompartir,
			boolean asignarTareas, boolean editarAsignamiento, boolean eliminarAsignamiento, boolean crearEquipo, boolean editarEquipo,
			boolean eliminarEquipo){
		return aux_obtenerNombrePermisoMasAlto(crearTareas, crearProyectos, editarTareas, asignarTareas, crearEquipo, editarEquipo,
				eliminarEquipo, editarAsignamiento, eliminarAsignamiento, editarProyectos, compartirProyectos, editarCompartir, 
				eliminarCompartir, eliminarTareas, eliminarProyectos, crearSubgrupo, invitarMiembros, eliminarSubgrupo, editarMiembros, 
				editarGrupo, eliminarMiembros, eliminarGrupo);
	}
	
	private static Pair<Integer,String> aux_obtenerNombrePermisoMasAlto(boolean p1, boolean p2, boolean p3,
			boolean p4, boolean p5, boolean p6, boolean p7,
			boolean p8, boolean p9, boolean p10, boolean p11,
			boolean p12, boolean p13, boolean p14, boolean p15,
			boolean p16, boolean p17, boolean p18, boolean p19,
			boolean p20, boolean p21, boolean p22){
		List<String> p = getTopePermisosGrupo();
		if(!p1 && !p2 && !p3 && !p4 && !p5 && !p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(0,p.get(0));
		else if(p1 && !p2 && !p3 && !p4 && !p5 && !p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(1,p.get(1));
		else if(p2 && !p3 && !p4 && !p5 && !p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(2,p.get(2));
		else if(p3 && !p4 && !p5 && !p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(3,p.get(3));
		else if(p4 && !p5 && !p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(4,p.get(4));
		else if(p5 && !p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(5,p.get(5));
		else if(p6 && !p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(6,p.get(6));
		else if(p7 && !p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(7,p.get(7));
		else if(p8 && !p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(8,p.get(8));
		else if(p9 && !p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(9,p.get(9));
		else if(p10 && !p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(10,p.get(10));
		else if(p11 &&!p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(11,p.get(11));
		else if(p12 && !p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(12,p.get(12));
		else if(p13 && !p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(13,p.get(13));
		else if(p14 && !p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(14,p.get(14));
		else if(p15 && !p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(15,p.get(15));
		else if(p16 && !p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(16,p.get(16));
		else if(p17 && !p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(17,p.get(17));
		else if(p18 && !p19 && !p20 && !p21 && !p22)
			return Pair.with(18,p.get(18));
		else if(p19 && !p20 && !p21 && !p22)
			return Pair.with(19,p.get(19));
		else if(p20 && !p21 && !p22)
			return Pair.with(20,p.get(20));
		else if(p21 && !p22)
			return Pair.with(21,p.get(21));
		else if(p22)
			return Pair.with(22,p.get(22));
		else return Pair.with(0,p.get(0));
	}

	public static HashMap<Integer, Boolean> MapeoDePermisos(
			boolean crearTareas, boolean crearProyectos, boolean editarTareas,
			boolean editarProyectos, boolean eliminarTareas,
			boolean eliminarProyectos, boolean crearSubgrupo,
			boolean invitarMiembros, boolean eliminarSubgrupo,
			boolean editarMiembros, boolean editarGrupo,
			boolean eliminarMiembros, boolean eliminarGrupo,
			boolean compartirProyectos, boolean editarCompartir,
			boolean eliminarCompartir, boolean asignarTareas,
			boolean editarAsignamiento, boolean eliminarAsignamiento, 
			boolean crearEquipo, boolean editarEquipo,
			boolean eliminarEquipo,
			HashMap<Integer, Boolean> mapping) {
		return aux_mapeoDePermisos(crearTareas, crearProyectos, editarTareas, asignarTareas, 
				crearEquipo, editarEquipo, eliminarEquipo, editarAsignamiento,
				eliminarAsignamiento, editarProyectos, compartirProyectos, editarCompartir, eliminarCompartir, 
				eliminarTareas, eliminarProyectos, crearSubgrupo, invitarMiembros, eliminarSubgrupo, editarMiembros, 
				editarGrupo, eliminarMiembros, eliminarGrupo, mapping);
	}

	private static HashMap<Integer, Boolean> aux_mapeoDePermisos(
			boolean p1, boolean p2, boolean p3,
			boolean p4, boolean p5, boolean p6, boolean p7,
			boolean p8, boolean p9, boolean p10, boolean p11,
			boolean p12, boolean p13, boolean p14, boolean p15,
			boolean p16, boolean p17, boolean p18, boolean p19,
			boolean p20, boolean p21, boolean p22,
			HashMap<Integer, Boolean> mapping) {
		mapping.put(1,p1);
		mapping.put(2,p2);
		mapping.put(3,p3);
		mapping.put(4,p4);
		mapping.put(5,p5);
		mapping.put(6,p6);
		mapping.put(7,p7);
		mapping.put(8,p8);
		mapping.put(9,p9);
		mapping.put(10,p10);
		mapping.put(11,p11);
		mapping.put(12,p12);
		mapping.put(13,p13);
		mapping.put(14,p14);
		mapping.put(15,p15);
		mapping.put(16,p16);
		mapping.put(17,p17);
		mapping.put(18,p18);
		mapping.put(19,p19);
		mapping.put(20,p20);
		mapping.put(21,p21);
		mapping.put(22,p22);
		return mapping;
	}

	public static String ObtenerOrderByPermisos() {
		String orderBy = " ORDER BY ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.CREAR_TAREAS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.CREAR_PROYECTOS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_TAREAS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ASIGNAR_TAREAS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.CREAR_EQUIPO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_EQUIPO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_EQUIPO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_ASIGNAMIENTO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_ASIGNAMIENTO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_PROYECTOS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.COMPARTIR_PROYECTO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_COMPARTIR + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_COMPARTIR + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_TAREAS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_PROYECTOS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.CREAR_SUBGRUPO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.INVITAR_MIEMBROS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_SUBGRUPO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_MIEMBROS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.EDITAR_GRUPO + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_MIEMBROS + " DESC, ";
		orderBy += IntMiembroGrupo.CAMPOS_MG.ELIMINAR_GRUPO + " DESC";
		return orderBy;
	}

	public static String obtenerNombrePermisoMasAlto(boolean[] permisos) {
		return obtenerNombrePermisoMasAlto(
				permisos[TopePermisos.CREAR_TAREAS],permisos[TopePermisos.CREAR_PROYECTOS],permisos[TopePermisos.EDITAR_TAREAS],
				permisos[TopePermisos.EDITAR_PROYECTOS],permisos[TopePermisos.ELIMINAR_TAREAS],permisos[TopePermisos.ELIMINAR_PROYECTOS],
				permisos[TopePermisos.CREAR_SUBGRUPO],permisos[TopePermisos.INVITAR_MIEMBRO],permisos[TopePermisos.ELIMINAR_SUBGRUPO],
				permisos[TopePermisos.EDITAR_MIEMBRO],permisos[TopePermisos.EDITAR_GRUPO],permisos[TopePermisos.ELIMINAR_MIEMBROS],
				permisos[TopePermisos.ELIMINAR_GRUPO],permisos[TopePermisos.COMPARTIR_PROYECTO],permisos[TopePermisos.EDITAR_COMPARTIR],
				permisos[TopePermisos.ELIMINAR_COMPARTIR],permisos[TopePermisos.ASIGNAR_TAREAS],permisos[TopePermisos.EDITAR_ASIGNAMIENTO],
				permisos[TopePermisos.ELIMINAR_ASIGNAMIENTO],permisos[TopePermisos.CREAR_EQUIPO],permisos[TopePermisos.EDITAR_EQUIPO],
				permisos[TopePermisos.ELIMINAR_EQUIPO]
				).getValue1();
	}

	public static List<Boolean> getListPermisos(
			boolean p1, boolean p2, boolean p3,
			boolean p4, boolean p5, boolean p6, boolean p7,
			boolean p8, boolean p9, boolean p10, boolean p11,
			boolean p12, boolean p13, boolean p14, boolean p15, 
			boolean p16, boolean p17, boolean p18, boolean p19,
			boolean p20, boolean p21, boolean p22,
			List<Boolean> lista) {
		lista.add(p1);
		lista.add(p2);
		lista.add(p3);
		lista.add(p4);
		lista.add(p5);
		lista.add(p6);
		lista.add(p7);
		lista.add(p8);
		lista.add(p9);
		lista.add(p10);
		lista.add(p11);
		lista.add(p12);
		lista.add(p13);
		lista.add(p14);
		lista.add(p15);
		lista.add(p16);
		lista.add(p17);
		lista.add(p18);
		lista.add(p19);
		lista.add(p20);
		lista.add(p21);
		lista.add(p22);
		
		return lista;
	}

	public static String generarPermisosEnString(boolean[] p) {
		String cad = "";
		List<String> lcad = getTopePermisosGrupo();
		for(int i=0;i<CANTIDAD_PERMISOS;i++){
			if(p[i]) cad+= " "+lcad.get(i);
		}
		
		/*if(p[CREAR_TAREAS]) cad+= " "+lcad.get(CREAR_TAREAS);
		if(p[CREAR_PROYECTOS]) cad+= " "+lcad.get(CREAR_PROYECTOS);
		if(p[EDITAR_TAREAS]) cad+= " "+lcad.get(EDITAR_TAREAS);
		if(p[ASIGNAR_TAREAS]) cad+= " "+lcad.get(ASIGNAR_TAREAS);
		if(p[EDITAR_ASIGNAMIENTO]) cad+= " "+lcad.get(EDITAR_ASIGNAMIENTO);
		if(p[ELIMINAR_ASIGNAMIENTO]) cad+= " "+lcad.get(ELIMINAR_ASIGNAMIENTO);
		if(p[EDITAR_PROYECTOS]) cad+= " "+lcad.get(EDITAR_PROYECTOS);
		if(p[COMPARTIR_PROYECTO]) cad+= " "+lcad.get(COMPARTIR_PROYECTO);
		if(p[ELIMINAR_TAREAS]) cad+= " "+lcad.get(ELIMINAR_TAREAS);
		if(p[ELIMINAR_PROYECTOS]) cad+= " "+lcad.get(ELIMINAR_PROYECTOS);
		if(p[CREAR_SUBGRUPO]) cad+= " "+lcad.get(CREAR_SUBGRUPO);
		if(p[INVITAR_MIEMBRO]) cad+= " "+lcad.get(INVITAR_MIEMBRO);
		if(p[ELIMINAR_SUBGRUPO]) cad+= " "+lcad.get(ELIMINAR_SUBGRUPO);
		if(p[EDITAR_MIEMBRO]) cad+= " "+lcad.get(EDITAR_MIEMBRO);
		if(p[ELIMINAR_MIEMBROS]) cad+= " "+lcad.get(ELIMINAR_MIEMBROS);
		if(p[EDITAR_GRUPO]) cad+= " "+lcad.get(EDITAR_GRUPO);
		if(p[ELIMINAR_GRUPO]) cad+= " "+lcad.get(ELIMINAR_GRUPO);*/
		return cad;
	}

	public static List<Boolean> getListaPermisosCompletos() {
			List<Boolean> lista = new ArrayList<Boolean>();
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			lista.add(true);
			
			return lista;
	}
}
