package modelos.clases;

import java.sql.Connection;

public class AsignamientoTarea {
	protected Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	protected int idTarea;
	protected int porcentaje;
	protected int completado;
	
	public int getIdTarea() {return idTarea;}
	public void setIdTarea(int idTarea) {this.idTarea = idTarea;}
	
	public int getPorcentaje() {return porcentaje;}
	public void setPorcentaje(int porcentaje) {this.porcentaje = porcentaje;}
	
	public int getCompletado() {return completado;}
	public void setCompletado(int completado) {this.completado = completado;}
}
