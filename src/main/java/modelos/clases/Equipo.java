package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import modelos.conexion.Conexion;
import controlador.logica.LogicaEquipo;

public class Equipo {
	protected Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	public static class CAMPOS {
		public final static String ID_EQUIPO = "idEquipo";
		public final static String GRUPO = "idGrupo";
		public final static String NOMBRE_EQUIPO = "nombreEquipo";
		public final static String DESC_EQUIPO = "descEquipo";
		
		public final static String NOMBRE_TABLA = "equipo";
	}
	
	private int idEquipo = 0;
	private int idGrupo = 0;
	private String nombreEquipo = "";
	protected String descEquipo = "";

	public Equipo(int idEquipo, Connection conn) {
		this.setConn(conn);
		this.obtenerEquipoPorId(idEquipo);
	}
	
	public Equipo() {}
	public int getIdEquipo() {return idEquipo;}
	public void setIdEquipo(int idEquipo) {this.idEquipo = idEquipo;}
	
	public int getIdGrupo() {return idGrupo;}
	public void setIdGrupo(int idGrupo) {this.idGrupo = idGrupo;}
	
	public String getNombreEquipo() {return nombreEquipo;}
	public void setNombreEquipo(String nombreEquipo) {this.nombreEquipo = nombreEquipo;}
	
	public String getDescEquipo() {return descEquipo;}
	public void setDescEquipo(String descEquipo) {this.descEquipo = descEquipo;}
	
	public static String getSelect(String pre){
		return pre+CAMPOS.ID_EQUIPO+","+pre+CAMPOS.GRUPO+","+pre+CAMPOS.NOMBRE_EQUIPO+","+pre+CAMPOS.DESC_EQUIPO;
	}
	public boolean guardarEquipo() {
		boolean guardado = false;
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idEquipo == 0) {
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.GRUPO+","+CAMPOS.NOMBRE_EQUIPO+
						","+CAMPOS.DESC_EQUIPO+") " +
						"VALUES ("+this.idGrupo+",'"+this.nombreEquipo+"','"+this.descEquipo+"')";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				if (ps.executeUpdate() > 0) {
					guardado = true;
					
					ResultSet rs = ps.getGeneratedKeys();
					if (rs.next()) {
						this.idEquipo = rs.getInt(1);
					}
				}
			} else {
				// ACTUALIZAR
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.NOMBRE_EQUIPO+"='"+this.nombreEquipo+"',"+
				CAMPOS.DESC_EQUIPO+"='"+this.descEquipo+
				"' WHERE "+CAMPOS.ID_EQUIPO+"="+this.idEquipo;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					guardado = true;
				}
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el equipo: " + e.getMessage());
		}
		
		return guardado;
	}
	public static boolean eliminarEquipo(int idEquipo, Connection conn) {
		try{
			String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_EQUIPO+"="+idEquipo;
			PreparedStatement ps = conn.prepareStatement(query);
			
			if (ps.executeUpdate() > 0) {
				return true;
			} else{
				return false;
			}
		}catch(Exception e){
			return false;
		}
	}
	private boolean obtenerEquipoPorId(int idEquipo) {
		boolean encontrado = false;
		try {
			ResultSet rs = Conexion.obtenerValoresDeUnaQuery("SELECT "+getSelect("") +
				" FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_EQUIPO+"="+idEquipo, conn);
			
			if (rs.next()) {
				LogicaEquipo.pasarDatosDelResultSetAlGrupo("",this, rs);
				encontrado = true;
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo equipo por id : " + e.getMessage());
		} 
		
		return encontrado;
	}
}
