package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelos.conexion.Conexion;
import controlador.logica.LogicaAviso;

public class Aviso {
	private Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	public static class CAMPOS {
		public final static String ID_AVISO = "idAviso";
		public final static String AVISADO = "idAvisado";
		public final static String MENSAJE = "mensaje";
		
		public final static String NOMBRE_TABLA = "Aviso";
	}
	
	private int idAviso;
	private int idAvisado;
	private String mensaje;
	
	public Aviso(int idAvisado,String mensaje) {
		this.idAviso = 0;
		this.idAvisado = idAvisado;
		this.mensaje = mensaje;
	}
	
	public Aviso() {
		this.idAviso = 0;
		this.idAvisado = 0;
		this.mensaje = "";
	}
	
	public Aviso(int idAviso, Connection conn) {
		this.setConn(conn);
		this.obtenerAvisoPorId(idAviso);
	}
	
	public boolean obtenerAvisoPorId(int idAviso) {
		boolean encontrado = false;
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (this.conn != null) {
				String query = selectTodo+" WHERE "+CAMPOS.ID_AVISO+"="+idAviso;
				PreparedStatement ps = this.conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					LogicaAviso.pasarDatosDelResultSetAlAviso(this,"", rs);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo invitacion por id : " + e.getMessage());
		} 
		
		return encontrado;
	}
	public int getIdAvisado(){ return this.idAvisado; }
	public void setIdAvisado(int idAvisado){this.idAvisado= idAvisado;}
	
	public int getIdAviso() {return idAviso;}
	public void setIdAviso(int idInvitacion) {this.idAviso = idInvitacion;}
	
	public String getMensaje() {return mensaje;}
	public void setMensaje(String mensaje) {this.mensaje = mensaje;}
	
	private final static String selectTodo = "SELECT "+CAMPOS.ID_AVISO + ","+CAMPOS.AVISADO+","+CAMPOS.MENSAJE+
		" FROM "+CAMPOS.NOMBRE_TABLA;
	
	public static List<Aviso> busquedaAvanzadaInvitacion(String where, String orderBy,Connection conn){
		List<Aviso> miembrosGrupos = new ArrayList<Aviso>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
				ejecutarQuery(miembrosGrupos, conn, query);
			}
		} catch (Exception e) {
			miembrosGrupos = new ArrayList<Aviso>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return miembrosGrupos;
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public boolean guardarInvitacion() { 
		boolean guardado = false;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.AVISADO+","+CAMPOS.MENSAJE+") "+
					"VALUES ("+this.idAvisado+",'"+this.mensaje+"')";
			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			if (ps.executeUpdate() > 0) {
				guardado = true;
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
	
	/**
	 * Metodo que obtiene los usuarios de una select
	 * @param usuarios Lista donde se añadiran los usuarios
	 * @param conn Conexion
	 * @param query Query
	 * @throws SQLException
	 */
	public static void ejecutarQuery(List<Aviso> miembrosGrupos, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Aviso miembroGrupo = new Aviso();
			LogicaAviso.pasarDatosDelResultSetAlAviso(miembroGrupo, "",rs);
			miembrosGrupos.add(miembroGrupo);
		}
	}

	public static boolean eliminarAviso(int idInvitacion,
			Connection conn) {
		boolean eliminado = false;
		
		if (idInvitacion>0) {
			try {
				if(conn==null)
					conn = Conexion.obtenerConexion();
				PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.ID_AVISO+"="+idInvitacion);
				if (ps.executeUpdate() > 0) {
					eliminado = true;
				}
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando miembros de grupo: " + e.getMessage());
			}
		}
		
		return eliminado;
	}
	public static List<Aviso> obtenerAvisosPorUsuario(int idUsuario, Connection conn) {
		return  busquedaAvanzadaInvitacion(" WHERE "+CAMPOS.AVISADO+"="+idUsuario, " ORDER BY "+CAMPOS.ID_AVISO,conn);
	}
	public static boolean guardarNuevoAviso(int idUsuario, String mensaje, Connection conn) {
		boolean guardado = false;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.AVISADO+","+CAMPOS.MENSAJE+") "+
					"VALUES ("+idUsuario+",'"+mensaje+"')";
			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			if (ps.executeUpdate() > 0) {
				guardado = true;
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
}
