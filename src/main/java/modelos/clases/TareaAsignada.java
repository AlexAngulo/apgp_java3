package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Proyecto.CAMPOS;
import modelos.conexion.Conexion;

import org.apache.commons.lang.StringUtils;

public class TareaAsignada extends AsignamientoTarea{
	
	
	public static class CAMPOS {
		public final static String USUARIO = "idAsignado";
		public final static String TAREA = "idTarea";
		public final static String PORCENTAJE = "porcentaje";
		public final static String COMPLETADO = "completado";
		
		public final static String NOMBRE_TABLA = "tareaasignada";
	}
	
	private static final String selectTodo = CAMPOS.COMPLETADO+","+CAMPOS.PORCENTAJE;
	
	protected int idUsuario;
	
	private String login;
	
	public int getIdUsuario() {return idUsuario;}
	public void setIdUsuario(int idUsuario) {this.idUsuario = idUsuario;}
	
	public String getLogin() {return login;}
	public void setLogin(String login) {this.login = login;}
	
	public boolean updateIdUsuario(){
		if(StringUtils.isBlank(this.login)) return false;
		Usuario usuario = new Usuario(this.login,this.conn);
		if(usuario.getIdUsuario()==0) return false;
		else {this.idUsuario = usuario.getIdUsuario(); return true;}
	}
	
	public boolean guardarNuevaTareaAsignada() { 
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idTarea > 0 && this.idUsuario>0) {
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.USUARIO+","+CAMPOS.TAREA+","+
					CAMPOS.PORCENTAJE+","+CAMPOS.COMPLETADO+") " +
					"VALUES ("+this.idUsuario+","+this.idTarea+","+this.porcentaje+","+this.completado+")";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				return (ps.executeUpdate() > 0);
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error guardando el proyectoCompartido: " + e.getMessage());
			return false;
		}
	}
	
	public boolean updateTareaAsignada() { 
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idTarea > 0 && this.idUsuario>0) {
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.COMPLETADO+"="+this.completado+","+
					CAMPOS.PORCENTAJE+"="+this.porcentaje+" WHERE "+
					CAMPOS.USUARIO+"="+this.idUsuario+" AND "+CAMPOS.TAREA+"="+this.idTarea;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando la tareaAsignada: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean eliminarTareaAsignada(int idUsuario, int idTarea, Connection conn){
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (idTarea > 0 && idUsuario>0) {
				String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.USUARIO+"="+idUsuario+" AND "+CAMPOS.TAREA+"="+idTarea;
				PreparedStatement ps = conn.prepareStatement(query);
				
				return (ps.executeUpdate() > 0);
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando la tareaAsignada: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean existeTareaAsignada(int idUsuario, int idTarea, Connection conn){
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (idTarea > 0 && idUsuario>0) {
				String query = "SELECT "+CAMPOS.TAREA+" FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.USUARIO+"="+idUsuario+" AND "+CAMPOS.TAREA+"="+idTarea;
				ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				
				if (rs.next()) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando la tareaAsignada: " + e.getMessage());
			return false;
		}
	}
	
	public boolean obtenerTareaAsignada(int idUsuario, int idTarea) {
		boolean encontrado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT "+selectTodo+" FROM "+CAMPOS.NOMBRE_TABLA+
					" WHERE "+CAMPOS.USUARIO+"="+idUsuario+" AND "+CAMPOS.TAREA+"="+idTarea;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					pasarDatosDelResultSetATareaAsignada(this, rs);
					this.setIdTarea(idTarea);
					this.setIdUsuario(idUsuario);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	private static void pasarDatosDelResultSetATareaAsignada(TareaAsignada ta, ResultSet rs) throws SQLException {
		ta.setCompletado(rs.getInt(CAMPOS.COMPLETADO));
		ta.setPorcentaje(rs.getInt(CAMPOS.PORCENTAJE));
	}
	
	public static List<TareaAsignada> obtenerAsignamientosPorTareaAsignada(int idTarea, Connection conn){
		List<TareaAsignada> lta = new ArrayList<TareaAsignada>();
		try{
			if(conn==null) conn = Conexion.obtenerConexion();
			String query = "SELECT "+selectTodo+","+CAMPOS.USUARIO+","+Usuario.CAMPOS.LOGIN+" FROM "+
				CAMPOS.NOMBRE_TABLA+","+Usuario.CAMPOS.NOMBRE_TABLA+" WHERE "+Usuario.CAMPOS.ID_USUARIO+"="+
				CAMPOS.USUARIO+" AND "+CAMPOS.TAREA+"="+idTarea;
			PreparedStatement ps = conn.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			TareaAsignada ta;
			while (rs.next()) {
				ta = new TareaAsignada();
				pasarDatosDelResultSetATareaAsignada(ta, rs);
				ta.setIdTarea(idTarea);
				ta.setIdUsuario(rs.getInt(CAMPOS.USUARIO));
				ta.setLogin(rs.getString(Usuario.CAMPOS.LOGIN));
				lta.add(ta);
			}
		} catch (Exception e){
			System.out.println("Error obteniendo asignamientos por tarea : " + e.getMessage());
			return new ArrayList<TareaAsignada>();
		}
		return lta;
	}
	/*public static boolean updateTareaAsignadaConSusTareasYProyectosActualizados(TareaAsignada ta,int codProyecto, Connection conn) {
		return ta.updateTareaAsignada() && updateTareasYProyectosALosQueAfecta(ta,codProyecto,conn) && 
				Proyecto.updateProyectoSegunSusTareas(codProyecto,conn);
	}
	private static boolean updateTareasYProyectosALosQueAfecta(TareaAsignada ta, int idProyecto, Connection conn) {
		List<TareaAsignada> lta = obtenerAsignamientosPorTarea(ta.getIdTarea(), conn);
		return Tarea.updateCompletadoTareaSegunTareasAsignadas(lta,ta.getIdTarea(),idProyecto,conn);
	}*/
	public static void eliminarTareasAsignadasSegunTarea(int idTarea,Connection conn) {
		String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.TAREA+"="+idTarea;
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
