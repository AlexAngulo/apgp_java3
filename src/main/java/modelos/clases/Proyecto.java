package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import modelos.conexion.Conexion;

import org.apache.commons.lang.StringUtils;

public class Proyecto {
	
	protected Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	public static class CAMPOS {
		public final static String ID_PROYECTO = "idProyecto";
		public final static String CREADOR = "idCreador";
		public final static String GRUPO_PERTENECIENTE = "idGrupoPerteneciente";
		public final static String NOMBRE_PROYECTO = "nombreProyecto";
		public final static String DESC_PROYECTO = "descProyecto";
		public final static String FECHA_FIN = "fechaFinIdeal";
		public final static String COMPLETADO = "completado";
		public final static String DURACION = "duracion";
		public final static String FECHA_FIN_PREVISTA = "fechaFinPrevista";
		public static final String ES_PLANTILLA = "esPlantilla";
		public static final String ORDEN_PROYECTO = "ordenProyecto";
		public static final String ORDENACION_TAREAS = "ordenacionTareas";
		public static final String PRIORIDAD = "prioridadProyecto";
		
		public final static String NOMBRE_TABLA = "proyecto";
	}
	
	private int plantilla;
	private int idProyecto;
	private int idCreador;
	private int idGrupoPerteneciente;
	private String nombreProyecto;
	private String descProyecto;
	private Date fechaFin;
	private int completado;
	private int duracion;
	private Date fechaFinPrevista;
	private boolean esPlantilla;
	private int ordenProyecto;
	private int ordenacionTareas;
	private int prioridad;
	
	private String fechaFinString;
	
	public Proyecto() {
		this.plantilla = 0;
		this.idProyecto = 0;
		this.idCreador = 0;
		this.idGrupoPerteneciente = 0;
		this.nombreProyecto = "";
		this.descProyecto = "";
		this.fechaFin = null;
	}
	
	public Proyecto(int idProyecto, int idCreador, int idGrupoPerteneciente, String nombreProyecto, String descProyecto, Date fechaFin) {
		this.idProyecto = idProyecto;
		this.idCreador = idCreador;
		this.idGrupoPerteneciente = idGrupoPerteneciente;
		this.nombreProyecto = nombreProyecto;
		this.descProyecto = descProyecto;
		this.fechaFin = fechaFin;
		this.plantilla = 0;
	}
	
	public Proyecto(int idProyecto,Connection conn) {
		this.setConn(conn);
		this.obtenerProyectoPorId(idProyecto,true);
	}
	
	public Proyecto(int idProyecto,boolean importanEsPlantilla, Connection conn) {
		this.setConn(conn);
		this.obtenerProyectoPorId(idProyecto,importanEsPlantilla);
	}
	
	public int getIdProyecto() {return idProyecto;}
	public void setIdProyecto(int idProyecto) {this.idProyecto = idProyecto;}
	
	public int getIdCreador() {return idCreador;}
	public void setIdCreador(int idCreador) {this.idCreador = idCreador;}
	
	public int getIdGrupoPerteneciente() {return idGrupoPerteneciente;}
	public void setIdGrupoPerteneciente(int idGrupoPerteneciente) {this.idGrupoPerteneciente = idGrupoPerteneciente;}
	
	public String getNombreProyecto() {return nombreProyecto;}
	public void setNombreProyecto(String nombreProyecto) {this.nombreProyecto = nombreProyecto;}
	
	public String getDescProyecto() {return descProyecto;}
	public void setDescProyecto(String descProyecto) {this.descProyecto = descProyecto;}
	
	public Date getFechaFin() {return fechaFin;}
	public void setFechaFin(Date fechaFin) {this.fechaFin = fechaFin;}
	
	public int getCompletado() {return completado;}
	public void setCompletado(int completado) {this.completado = completado;}
	
	public int getDuracion() {return duracion;}
	public void setDuracion(int duracion) {this.duracion = duracion;}
	
	public Date getFechaFinPrevista() {return fechaFinPrevista;}
	public void setFechaFinPrevista(Date fechaFinPrevista) {this.fechaFinPrevista = fechaFinPrevista;}
	
	public int getPrioridad() {return prioridad;}
	public void setPrioridad(int prioridad) {this.prioridad = prioridad;}
	
	public boolean updateFechaFin(){
		try{
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(StringUtils.isBlank(this.fechaFinString)) 
				this.fechaFin = null;
			else	
				this.fechaFin = sdf.parse(this.fechaFinString);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	public static boolean updateProyecto(HashMap<String,String> hm, int idProyecto, Connection conn) { 
		boolean guardado = false;
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET ";
			
			boolean empezado = false;
			if(hm.containsKey(CAMPOS.COMPLETADO)){
				query += CAMPOS.COMPLETADO+"="+hm.get(CAMPOS.COMPLETADO);
				empezado = true;
			}
			if(hm.containsKey(CAMPOS.DURACION)){
				if(empezado) query += ",";
				query += CAMPOS.DURACION+"="+hm.get(CAMPOS.DURACION);
				empezado = true;
			}	
			query += " WHERE "+CAMPOS.ID_PROYECTO+"="+idProyecto;
			ps = conn.prepareStatement(query);
			
			if (ps.executeUpdate() > 0) {
				guardado = true;
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		} 
		
		return guardado;
	}
	
	public boolean isEsPlantilla() {return esPlantilla;}
	public void setEsPlantilla(boolean esPlantilla) {this.esPlantilla = esPlantilla;}

	public int getOrdenProyecto() {return ordenProyecto;}
	public void setOrdenProyecto(int ordenProyecto) {this.ordenProyecto = ordenProyecto;}

	public int getOrdenacionTareas() {return ordenacionTareas;}
	public void setOrdenacionTareas(int ordenacionTareas) {this.ordenacionTareas = ordenacionTareas;}

	protected final static String selectProyecto = " "+CAMPOS.ID_PROYECTO+","+
		CAMPOS.CREADOR+","+CAMPOS.GRUPO_PERTENECIENTE+","+
		CAMPOS.NOMBRE_PROYECTO+","+CAMPOS.DESC_PROYECTO+","+
		CAMPOS.FECHA_FIN+","+CAMPOS.ES_PLANTILLA+","+CAMPOS.ORDEN_PROYECTO+","+
		CAMPOS.ORDENACION_TAREAS+","+CAMPOS.DURACION+","+CAMPOS.COMPLETADO+","+CAMPOS.PRIORIDAD;
	
	protected final static String selectTodo = "SELECT "+selectProyecto+
		" FROM "+CAMPOS.NOMBRE_TABLA+" ";
	
	public boolean obtenerProyectoPorId(int idProyecto, boolean importaSiEsPlantilla){
		return obtenerProyectoOPlantillaPorId(idProyecto, false, importaSiEsPlantilla);
	}
	
	public boolean obtenerProyectoOPlantillaPorId(int idProyecto, boolean esPlantilla, boolean importaSiEsPlantilla) {
		boolean encontrado = false;
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo+" WHERE "+CAMPOS.ID_PROYECTO+"="+idProyecto;
				if(importaSiEsPlantilla)
					query += " AND "+CAMPOS.ES_PLANTILLA+(esPlantilla ? "=1" : "=0");
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					//pasarDatosDelResultSetAProyecto("",this, rs);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	
	public static List<Proyecto> busquedaAvanzadaProyecto(String where, String orderBy,Connection conn){
		List<Proyecto> tareas = new ArrayList<Proyecto>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
			}
		} catch (Exception e) {
			tareas = new ArrayList<Proyecto>();
			System.out.println("Error en busqueda avanzada proyecto: " + e.getMessage());
		}
		
		return tareas;
	}

	/**
	 * Elimina el usuario cargado en funcion de su identificador
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha borrado alguna fila
	 */
	public boolean eliminarProyecto() {
		boolean eliminado = false;
		
		if (this.idProyecto != 0) {
			try {
				if(this.conn==null)
					this.conn = Conexion.obtenerConexion();
				if (conn != null) {
					PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_PROYECTO+"="+this.idProyecto);
					if (ps.executeUpdate() > 0) {
						eliminado = true;
					}
				}
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			}
		}
		
		return eliminado;
	}
	
	public String getFechaFinFormateada(){
		if(this.fechaFin==null){
			return "";
		} else {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm");
			return sdf.format(this.fechaFin);
		}
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public boolean guardarProyecto() { 
		boolean guardado = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idProyecto == 0) {
				List<Proyecto> proyectos = busquedaAvanzadaProyecto("", " ORDER BY "+CAMPOS.ORDEN_PROYECTO+" DESC", conn);
				if(proyectos.size()==0)
					this.ordenProyecto = 0;
				else
					this.ordenProyecto = proyectos.get(0).getOrdenProyecto()+1;
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.GRUPO_PERTENECIENTE+","+CAMPOS.NOMBRE_PROYECTO+","+CAMPOS.DESC_PROYECTO+
						","+CAMPOS.FECHA_FIN+","+CAMPOS.CREADOR+","+CAMPOS.ORDEN_PROYECTO+","+CAMPOS.ORDENACION_TAREAS+","+CAMPOS.ES_PLANTILLA+","+CAMPOS.PRIORIDAD+") " +
						"VALUES ("+(this.idGrupoPerteneciente==0 ? "null" : this.idGrupoPerteneciente)+",'"+this.nombreProyecto+"','"+
						this.descProyecto+"',"+(this.fechaFin==null ? "null" : "'"+sdf.format(this.fechaFin)+"'")+","+(this.idCreador==0 ? "null" : this.idCreador)+","+
						this.ordenProyecto+","+this.ordenacionTareas+","+this.esPlantilla+","+this.prioridad+")";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				if (ps.executeUpdate() > 0) {
					guardado = true;
					
					ResultSet rs = ps.getGeneratedKeys();
					if (rs.next()) {
						this.idProyecto = rs.getInt(1);
					}
				}
				if(plantilla>0){
				}
			} else {
				// ACTUALIZAR
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.GRUPO_PERTENECIENTE+"="+(this.idGrupoPerteneciente==0 ? "null" : this.idGrupoPerteneciente)+","+
						CAMPOS.NOMBRE_PROYECTO+"='"+this.nombreProyecto+"',"+
						CAMPOS.DESC_PROYECTO+"='"+this.descProyecto+"',"+CAMPOS.FECHA_FIN+"="+(fechaFin!=null ? "'"+sdf.format(this.fechaFin)+"'" : "null")+","+
						CAMPOS.CREADOR+"="+(this.idCreador==0 ? "null" : this.idCreador)+","+CAMPOS.ORDEN_PROYECTO+"="+this.ordenProyecto+","+
						CAMPOS.ORDENACION_TAREAS+"="+this.ordenacionTareas+","+CAMPOS.PRIORIDAD+"="+this.prioridad+
						" WHERE "+CAMPOS.ID_PROYECTO+"="+this.idProyecto;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					guardado = true;
				}
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
}
