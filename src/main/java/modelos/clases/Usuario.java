package modelos.clases;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.conexion.Conexion;
import modelos.utiles.UtilBD;

import org.apache.commons.lang.StringUtils;

public class Usuario implements Serializable {
	
	private Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}

	private static final long serialVersionUID = 1L;
	
	public static class CAMPOS {
		public final static String ID_USUARIO = "idUsuario";
		public final static String LOGIN = "login";
		public final static String EMAIL = "email";
		public final static String PASSWORD = "password";
		public final static String E_VERIFICADO = "eVerificado";
		public final static String ORDEN_PROYECTOS = "ordenProyectosUsuario";
		public final static String ORDEN_TAREAS = "ordenTareasUsuario";
		public final static String ROL = "rol";
		public final static String DIAS = "diasLaborablesSemana";
		public final static String HORAS = "horasLaborablesDia";
		
		public final static String NOMBRE_TABLA = "Usuario";
	}
	
	private int idUsuario;
	private String login;
	private String email;
	private String password;
	private boolean emailVerificado;
	private int diasSemana;
	private int horasDia;
	private int rol;

	private int ordenTareas;
	private int ordenProyectos;
	
	public void setOrdenTareas(int ordenTareas) {this.ordenTareas = ordenTareas;}
	
	public Usuario() {
		this.idUsuario = 0;
		this.login = "";
		this.email = "";
		this.password = "";
		this.emailVerificado = false;
	}
	
	public Usuario(int idUsuario, String login, String email, boolean emailVerificado, int rol) {
		this.idUsuario = idUsuario;
		this.login = login;
		this.email = email;
		this.emailVerificado = emailVerificado;
		this.rol = rol;
	}
	
	public Usuario(int idUsuario,Connection conn) {
		this.setConn(conn);
		this.obtenerUsuarioPorId(idUsuario);
	}
	
	public Usuario(String login,Connection conn) {
		this.setConn(conn);
		this.obtenerUsuarioPorNombreDeUsuario(login);
	}
	
	public int getIdUsuario(){ return this.idUsuario; }
	public void setIdUsuario(int idUsuario){this.idUsuario = idUsuario;}
	
	public String getLogin(){return this.login;}
	public void setLogin(String login){this.login = login;}

	public String getEmail() {return email;}
	public void setEmail(String email) {this.email = email;}

	public String getPassword() {return password;}
	public void setPassword(String password) {this.password = password;}
	
	public boolean getEmailVerificado() {return this.emailVerificado;}
	public void setEmailVerificado(boolean emailVerificado) {this.emailVerificado = emailVerificado;}

	public int getDiasSemana() {return diasSemana;}
	public void setDiasSemana(int diasSemana) {this.diasSemana = diasSemana;}
	
	public int getHorasDia() {return horasDia;}
	public void setHorasDia(int horasDia) {this.horasDia = horasDia;}
	
	public double getCoeficiente(){
		double resul = horasDia*diasSemana;
		resul = resul/7.0;
		return resul;}
	
	public int getRol() {return rol;}
	public void setRol(int rol) {this.rol = rol;}

	private final static String selectTodo = "SELECT * ";
	
	public static List<Usuario> obtenerUsuarios(Connection conn) {
		return busquedaAvanzadaUsuario(new HashMap<String, Object>(),conn);
	}

	public static List<Usuario> busquedaAvanzadaUsuario(HashMap<String, Object> filtros, Connection conn) {
		return busquedaAvanzadaUsuario(filtros, "", true,conn);
	}

	public static List<Usuario> busquedaAvanzadaUsuario(HashMap<String, Object> filtros, String campoOrdenacion, boolean ascendente, Connection conn) {		
		return busquedaAvanzadaUsuario(UtilBD.crearClausulaWhereStringExacto(filtros), UtilBD.crearClausulaOrderBy(campoOrdenacion, ascendente),conn);
	}
	
	public static List<Usuario> busquedaAvanzadaUsuario(String where, String orderBy, Connection conn){
		List<Usuario> usuarios = new ArrayList<Usuario>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				
				String query = selectTodo + where + orderBy;
				ejecutarQuery(usuarios, conn, query);
			}
		} catch (Exception e) {
			usuarios = new ArrayList<Usuario>();
			System.out.println("Error en busqueda avanzada usuario: " + e.getMessage());
		} finally {
			
		}
		
		return usuarios;
	}
	
	/**
	 * Obtiene un usuario por su identificador
	 * @param idUsuario Identificador de la comunidad
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si lo ha encontrado
	 */
	
	public boolean obtenerUsuarioPorId(int idUsuario) {
		return obtenerUsuarioPorLoginOPorId(null, idUsuario);
	}
	
	/**
	 * Obtiene un usuario por su nombre
	 * @param nombreUsuario Nombre del usuario
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si lo ha encontrado
	 */
	public boolean obtenerUsuarioPorNombreDeUsuario(String nombreUsuario) {
		return obtenerUsuarioPorLoginOPorId(nombreUsuario, 0) ;
	}
	
	private boolean obtenerUsuarioPorLoginOPorId(String login, int idUsuario) {
		boolean encontrado = false;
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo+" WHERE ";
				PreparedStatement ps = null;
				if(StringUtils.isNotBlank(login) == true){
					query += " "+CAMPOS.LOGIN+"='"+login+"'";
				} else {
					query += " "+CAMPOS.ID_USUARIO+"="+idUsuario;
				}
				ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					pasarDatosDelResultSetAlUsuario(this, "",rs);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage()+ " el nombre es: "+login);
		} finally {
			
		}
		
		return encontrado;
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public boolean guardarUsuario() { 
		boolean guardado = false;
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idUsuario == 0) {
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.LOGIN+","+CAMPOS.EMAIL+","+CAMPOS.E_VERIFICADO+
						","+CAMPOS.PASSWORD+","+CAMPOS.DIAS+","+CAMPOS.HORAS+") " +
						"VALUES ('"+this.login+"','"+this.email+"',"+this.emailVerificado+",'"+this.password+"',"+this.diasSemana+","+this.horasDia+")";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				if (ps.executeUpdate() > 0) {
					guardado = true;
					
					ResultSet rs = ps.getGeneratedKeys();
					if (rs.next()) {
						this.idUsuario = rs.getInt(1);
					}
				}
			} else {
				// ACTUALIZAR
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.LOGIN+"='"+this.login+"',"+
				CAMPOS.EMAIL+"='"+this.email+"',"+CAMPOS.E_VERIFICADO+"=";
				query += this.emailVerificado ? "1" : "0";
				if(StringUtils.isNotBlank(this.password))
					query += ","+CAMPOS.PASSWORD+"='"+this.password+"'";
				query += ","+CAMPOS.DIAS+"="+this.diasSemana+","+CAMPOS.HORAS+"="+this.horasDia;
				query += " WHERE "+CAMPOS.ID_USUARIO+"="+this.idUsuario;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					guardado = true;
				}
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		} finally {
			try {
				
			} catch (Exception e) {
			}
		}
		
		return guardado;
	}
	
	/**
	 * Metodo que obtiene los usuarios de una select
	 * @param usuarios Lista donde se añadiran los usuarios
	 * @param conn Conexion
	 * @param query Query
	 * @throws SQLException
	 */
	public static void ejecutarQuery(List<Usuario> usuarios, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Usuario usuario = new Usuario();
			pasarDatosDelResultSetAlUsuario(usuario, "",rs);
			usuarios.add(usuario);
		}
	}
	
	public static void pasarDatosDelResultSetAlUsuario(Usuario usuario, String pre, ResultSet rs) throws SQLException{
		usuario.setIdUsuario(rs.getInt(pre+Usuario.CAMPOS.ID_USUARIO));
		usuario.setLogin(rs.getString(pre+Usuario.CAMPOS.LOGIN));
		usuario.setEmail(rs.getString(pre+Usuario.CAMPOS.EMAIL));
		usuario.setEmailVerificado(rs.getString(pre+Usuario.CAMPOS.E_VERIFICADO).equals("1"));
		usuario.setRol(rs.getInt(pre+Usuario.CAMPOS.ROL));
		usuario.setOrdenProyectos(rs.getInt(pre+Usuario.CAMPOS.ORDEN_PROYECTOS));
		usuario.setOrdenTareas(rs.getInt(pre+Usuario.CAMPOS.ORDEN_TAREAS));
		usuario.setDiasSemana(rs.getInt(pre+Usuario.CAMPOS.HORAS));
		usuario.setHorasDia(rs.getInt(pre+Usuario.CAMPOS.DIAS));
	}
	
	public static Usuario obtenerUsuarioPorEmailOLoginYPassword(String emailOLogin,String password,Connection conn) {
		String where = " WHERE (" + CAMPOS.EMAIL + "='" + emailOLogin + "' OR "+CAMPOS.LOGIN+"='"+emailOLogin+"') AND " + CAMPOS.PASSWORD + "='" + password + "'";
		List<Usuario> listUsuarios = busquedaAvanzadaUsuario(where,"",conn);
		if(listUsuarios.size()==1){
			return listUsuarios.get(0);
		} else {
			return null;
		}
	}
	public static String obtenerPasswordUsuario(int idUsuario, Connection conn) {
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			String query = "SELECT "+CAMPOS.PASSWORD+" FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_USUARIO+"="+idUsuario;
			PreparedStatement ps = conn.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getString(CAMPOS.PASSWORD);
			} else {
				return "";
			}
		} catch (Exception e) {
			return "";
		}
	}
	public boolean isEsAdministrador(){
		return this.rol>=9;
	}
	public int getOrdenProyectos() {
		return ordenProyectos;
	}
	public void setOrdenProyectos(int ordenProyectos) {
		this.ordenProyectos = ordenProyectos;
	}
	public static List<String> obtenerMiembrosDeGrupoSoloLogin(int idGrupo, Connection conn) {
		List<String> listaMiembros = new ArrayList<String>();
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT u."+CAMPOS.LOGIN+", u."+CAMPOS.ID_USUARIO+",mg."+MiembroGrupo.CAMPOS.ID_GRUPO+" FROM "+CAMPOS.NOMBRE_TABLA+" u,"+
					MiembroGrupo.CAMPOS.NOMBRE_TABLA+" mg WHERE mg."+MiembroGrupo.CAMPOS.ID_USUARIO+"=u."+CAMPOS.ID_USUARIO+" AND mg."+
					MiembroGrupo.CAMPOS.ID_GRUPO+"="+idGrupo+" ORDER BY u."+CAMPOS.LOGIN;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					listaMiembros.add(rs.getString("u."+CAMPOS.LOGIN));
				}
			}
		} catch (Exception e) {
			System.out.println("Error en busqueda avanzada usuario: " + e.getMessage());
			return new ArrayList<String>();
		} finally {
			
		}
		return listaMiembros;
	}
	public int getOrdenTareas() {
		return this.ordenTareas;
	}
	
	public static boolean updateOrdenProyectos(int idUsuario, int codOrdenacion, Connection conn){
		return updateOrdenTareasOProyectos(idUsuario, codOrdenacion, conn, false);
	}
	public static boolean updateOrdenTareas(int idUsuario, int codOrdenacion, Connection conn){
		return updateOrdenTareasOProyectos(idUsuario, codOrdenacion, conn, true);
	}
	
	public static boolean updateOrdenTareasOProyectos(int idUsuario, int codOrdenacion, Connection conn, boolean esTareas){
		boolean updated = false;
		
		if (idUsuario != 0) {
			try {
				if(conn==null)
				conn = Conexion.obtenerConexion();
				if (conn != null) {
					String campo;
					if(esTareas){
						campo = CAMPOS.ORDEN_TAREAS;
					} else {
						campo = CAMPOS.ORDEN_PROYECTOS;
					}
					PreparedStatement ps = conn.prepareStatement("UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+campo+"="+codOrdenacion+" WHERE "+
						CAMPOS.ID_USUARIO+"="+idUsuario);
					if (ps.executeUpdate() > 0) {
						updated = true;
					}
				}
			} catch (Exception e) {
				updated = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			} finally {
				
			}
		}
		
		return updated;
	}
	
	public static List<String> obtenerCompartidosDeProyectoSoloLogin(int idProyecto,Connection conn) {
		List<String> listaCompartidos = new ArrayList<String>();
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT u."+CAMPOS.LOGIN+", u."+CAMPOS.ID_USUARIO+",pc."+ProyectoCompartido.CAMPOS.PROYECTO+" FROM "+CAMPOS.NOMBRE_TABLA+" u,"+
					ProyectoCompartido.CAMPOS.NOMBRE_TABLA+" pc WHERE pc."+ProyectoCompartido.CAMPOS.USUARIO+"=u."+CAMPOS.ID_USUARIO+" AND pc."+
					ProyectoCompartido.CAMPOS.PROYECTO+"="+idProyecto+" ORDER BY u."+CAMPOS.LOGIN;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					listaCompartidos.add(rs.getString("u."+CAMPOS.LOGIN));
				}
			}
		} catch (Exception e) {
			System.out.println("Error en busqueda avanzada usuario: " + e.getMessage());
			return new ArrayList<String>();
		} finally {
			
		}
		return listaCompartidos;
	}
	
	public static HashMap<Integer,Usuario> obtenerCompartidosDeProyecto(int idProyecto,Connection conn) {
		return obtenerCompartidosDeProyecto(new HashMap<Integer,Usuario>(),idProyecto,conn);
	}
	
	public static HashMap<Integer,Usuario> obtenerCompartidosDeProyecto(HashMap<Integer,Usuario> hm, int idProyecto,Connection conn) {
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT u."+CAMPOS.LOGIN+", u."+CAMPOS.ID_USUARIO+", u."+CAMPOS.DIAS+", u."+CAMPOS.HORAS+", u."+
					CAMPOS.E_VERIFICADO+", u."+CAMPOS.EMAIL+", u."+CAMPOS.ORDEN_PROYECTOS+", u."+CAMPOS.ORDEN_TAREAS+", u."+
					CAMPOS.ROL+",pc."+ProyectoCompartido.CAMPOS.PROYECTO+" FROM "+CAMPOS.NOMBRE_TABLA+" u,"+
					ProyectoCompartido.CAMPOS.NOMBRE_TABLA+" pc WHERE pc."+ProyectoCompartido.CAMPOS.USUARIO+"=u."+CAMPOS.ID_USUARIO+" AND pc."+
					ProyectoCompartido.CAMPOS.PROYECTO+"="+idProyecto+" ORDER BY u."+CAMPOS.LOGIN;
				PreparedStatement ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				Usuario u;
				while(rs.next()){
					u = new Usuario();
					Usuario.pasarDatosDelResultSetAlUsuario(u, "u.",rs);
					hm.put(u.getIdUsuario(), u);
				}
			}
		} catch (Exception e) {
			System.out.println("Error en busqueda avanzada usuario: " + e.getMessage());
			return new HashMap<Integer,Usuario>();
		} finally {
			
		}
		return hm;
	}
	public static HashMap<Integer, Usuario> obtenerMiembrosDeGrupo(int idGrupo, Connection conn) {
		HashMap<Integer, Usuario> hmUsuarios = new HashMap<Integer, Usuario>();
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT "+getSelect("u.")+",mg."+MiembroGrupo.CAMPOS.ID_GRUPO+" FROM "+CAMPOS.NOMBRE_TABLA+" u,"+
					MiembroGrupo.CAMPOS.NOMBRE_TABLA+" mg WHERE mg."+MiembroGrupo.CAMPOS.ID_USUARIO+"=u."+CAMPOS.ID_USUARIO+" AND mg."+
					MiembroGrupo.CAMPOS.ID_GRUPO+"="+idGrupo+" ORDER BY u."+CAMPOS.LOGIN;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				Usuario usuario;
				while(rs.next()){
					usuario = new Usuario();
					Usuario.pasarDatosDelResultSetAlUsuario(usuario, "u.", rs);
					hmUsuarios.put(rs.getInt("u."+CAMPOS.ID_USUARIO),usuario);
				}
			}
		} catch (Exception e) {
			System.out.println("Error en busqueda avanzada usuario: " + e.getMessage());
			return new HashMap<Integer, Usuario>();
		} finally {
			
		}
		return hmUsuarios;
	}
	private static String getSelect(String pre) {
		return pre+CAMPOS.ID_USUARIO+","+pre+CAMPOS.DIAS+","+pre+CAMPOS.E_VERIFICADO+","+pre+CAMPOS.EMAIL+","+
			pre+CAMPOS.HORAS+","+pre+CAMPOS.LOGIN+","+pre+CAMPOS.ORDEN_PROYECTOS+","+pre+CAMPOS.ORDEN_TAREAS+","+
			pre+CAMPOS.ROL;
	}
}
