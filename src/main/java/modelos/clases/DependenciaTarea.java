package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelos.clasesAbstractas.IntDependenciaTarea;
import modelos.conexion.Conexion;

public class DependenciaTarea extends IntDependenciaTarea{
	
	private int idTareaDependiente;
	
	public DependenciaTarea() {
		this.idTareaDependiente = 0;
		this.idTareaDepende = 0;
	}
	
	public DependenciaTarea(int idTareaDependiente,int idTareaDepende) {
		this.idTareaDependiente = idTareaDependiente;
		this.idTareaDepende = idTareaDepende;
	}
	
	public DependenciaTarea(int idTareaDependiente, int idTareaDepende,Connection conn) {
		this.setConn(conn);
		this.obtenerDependenciaTareaPorIds(idTareaDependiente, idTareaDepende);
	}
	
	public int getIdTareaDependiente(){ return this.idTareaDependiente; }
	public void setIdTareaDependiente(int idGrupo){this.idTareaDependiente = idGrupo;}
	
	public boolean obtenerDependenciaTareaPorIds(int idTareaDependiente, int idTareaDepende) {
		boolean encontrado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo+" WHERE "+CAMPOS.TAREA_DEPENDIENTE+"="+idTareaDependiente+" AND "+CAMPOS.TAREA_DEPENDE+"="+idTareaDepende;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					//LogicaDependenciaTarea.pasarDatosDelResultSetADependencia(this, rs);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	
	public static List<DependenciaTarea> busquedaAvanzadaDependenciaTarea(String where, String orderBy,Connection conn){
		List<DependenciaTarea> miembrosGrupos = new ArrayList<DependenciaTarea>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
				ejecutarQuery(miembrosGrupos, conn, query);
			}
		} catch (Exception e) {
			miembrosGrupos = new ArrayList<DependenciaTarea>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return miembrosGrupos;
	}

	/**
	 * Elimina el usuario cargado en funcion de su identificador
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha borrado alguna fila
	 */
	public boolean eliminarGrupo() {
		boolean eliminado = false;
		
		if (this.idTareaDepende!=0 && this.idTareaDepende!=0) {
			try {
				if(this.conn==null)
					this.conn = Conexion.obtenerConexion();
				if (conn != null) {
					PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.TAREA_DEPENDIENTE+"="+this.idTareaDepende + 
							" AND "+CAMPOS.TAREA_DEPENDE+"="+this.idTareaDepende);
					if (ps.executeUpdate() > 0) {
						eliminado = true;
					}
				}
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			}
		}
		
		return eliminado;
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public boolean guardarDependenciaTarea() { 
		boolean guardado = false;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.TAREA_DEPENDIENTE+","+CAMPOS.TAREA_DEPENDE+") "+
					"VALUES ("+this.idTareaDependiente+","+this.idTareaDepende+")";
			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			if (ps.executeUpdate() > 0) {
				guardado = true;
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public static boolean guardarDependenciasTareas(int idTarea, List<Integer> dependencias, Connection conn) { 
		if(dependencias.size()==0){
			return true;
		}
		boolean guardado = false;
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.TAREA_DEPENDIENTE+","+CAMPOS.TAREA_DEPENDE+") "+
					"VALUES ";
			boolean b =true;
			for(int dependencia : dependencias){
				if(b){
					query += "("+idTarea+","+dependencia+")";
					b=false;
				} else {
					query += ",("+idTarea+","+dependencia+")";
				}
			}
			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.executeUpdate();
			guardado = true;
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
	
	public static boolean eliminarDependenciasTareas(int idTarea,
			List<Integer> dependencias, Connection conn) {
		if(dependencias.size()==0){
			return true;
		}
		boolean eliminados = false;
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE ";
			boolean b=true;
			for(int dependencia : dependencias){
				if(b){
					query += "("+CAMPOS.TAREA_DEPENDIENTE+"="+idTarea+" AND "+CAMPOS.TAREA_DEPENDE+"="+dependencia+")";
					b=false;
				} else {
					query += " OR ("+CAMPOS.TAREA_DEPENDIENTE+"="+idTarea+" AND "+CAMPOS.TAREA_DEPENDE+"="+dependencia+")";
				}
			}
			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.executeUpdate();
			eliminados = true;
		} catch (Exception e) {
			eliminados = false;
			System.out.println("Error eliminando dependencias: " + e.getMessage());
		}
		
		return eliminados;
	}
	
	/**
	 * Metodo que obtiene los usuarios de una select
	 * @param usuarios Lista donde se añadiran los usuarios
	 * @param conn Conexion
	 * @param query Query
	 * @throws SQLException
	 */
	public static void ejecutarQuery(List<DependenciaTarea> dependenciaTareas, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			DependenciaTarea dependenciaTarea = new DependenciaTarea();
			//LogicaDependenciaTarea.pasarDatosDelResultSetADependencia(dependenciaTarea, rs);
			dependenciaTareas.add(dependenciaTarea);
		}
	}

	public static boolean eliminarDependenciasSegunTarea(int idTarea, boolean dependiente, boolean depende,
			Connection conn) {
		boolean eliminados = false;
		
		if (idTarea!=0) {
			try {
				if(conn==null)
					conn = Conexion.obtenerConexion();
				if (conn != null) {
					String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE ";
					if(dependiente){
						query += CAMPOS.TAREA_DEPENDIENTE+"="+idTarea;
						if(depende){
							query += " OR "+CAMPOS.TAREA_DEPENDE+"="+idTarea;
						}
					}else if(depende){
						query += CAMPOS.TAREA_DEPENDE+"="+idTarea;
					}
					PreparedStatement ps = conn.prepareStatement(query);
					ps.executeUpdate();
					eliminados = true;
				}
			} catch (Exception e) {
				eliminados = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			}
		}
		
		return eliminados;
	}
}
