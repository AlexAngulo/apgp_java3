package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modelos.conexion.Conexion;
import controlador.logica.VistaProyecto;

public class PermisoProyecto {
	private boolean crearTareas = false;
	private boolean editarTareas = false;
	private boolean eliminarTareas = false;
	private boolean editarProyecto = false;
	private boolean eliminarProyecto = false;
	private boolean compartir = false;
	private boolean editarCompartir = false;
	private boolean asignarTarea = false;
	private boolean editarAsignar = false;
	private boolean eliminarAsignar = false;
	public boolean isEditarCompartir() {return editarCompartir;}
	public void setEditarCompartir(boolean editarCompartir) {this.editarCompartir = editarCompartir;}
	
	public boolean isEliminarCompartir() {return eliminarCompartir;}
	public void setEliminarCompartir(boolean eliminarCompartir) {this.eliminarCompartir = eliminarCompartir;}

	private boolean eliminarCompartir = false;
	private String login = null;
	
	public boolean isCrearTareas() {return crearTareas;}
	public void setCrearTareas(boolean crearTareas) {this.crearTareas = crearTareas;}
	
	public boolean isEditarTareas() {return editarTareas;}
	public void setEditarTareas(boolean editarTareas) {this.editarTareas = editarTareas;}
	
	public boolean isEliminarTareas() {return eliminarTareas;}
	public void setEliminarTareas(boolean eliminarTareas) {this.eliminarTareas = eliminarTareas;}
	
	public boolean isEditarProyecto() {return editarProyecto;}
	public void setEditarProyecto(boolean editarProyecto) {this.editarProyecto = editarProyecto;}
	
	public boolean isEliminarProyecto() {return eliminarProyecto;}
	public void setEliminarProyecto(boolean eliminarProyecto) {this.eliminarProyecto = eliminarProyecto;}
	
	public boolean isCompartir() {return compartir;}
	public void setCompartir(boolean compartir) {this.compartir = compartir;}
	
	public String getLogin() {return login;}
	public void setLogin(String login) {this.login = login;}
	
	public boolean isAsignarTarea() {return asignarTarea;}
	public void setAsignarTarea(boolean asignarTarea) {this.asignarTarea = asignarTarea;}
	
	public boolean isEditarAsignar() {return editarAsignar;}
	public void setEditarAsignar(boolean editarAsignar) {this.editarAsignar = editarAsignar;}
	
	public boolean isEliminarAsignar() {return eliminarAsignar;}
	public void setEliminarAsignar(boolean eliminarAsignar) {this.eliminarAsignar = eliminarAsignar;}

	private static final String PC = "pc.";
	private static final String U = "u.";
	
	public static List<PermisoProyecto> busquedaAvanzadaVistaProyecto(String where, String orderBy,Connection conn){
		List<PermisoProyecto> tareas = new ArrayList<PermisoProyecto>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT "+VistaProyecto.selectProyectoCompartido+","+U+Usuario.CAMPOS.LOGIN+" FROM "+
					ProyectoCompartido.CAMPOS.NOMBRE_TABLA+" pc,"+Usuario.CAMPOS.NOMBRE_TABLA+" u WHERE "+
					U+Usuario.CAMPOS.ID_USUARIO+"="+PC+ProyectoCompartido.CAMPOS.USUARIO + where + orderBy;
				ejecutarQuery(tareas, conn, query);
			}
		} catch (Exception e) {
			tareas = new ArrayList<PermisoProyecto>();
			System.out.println("Error en busqueda avanzada permisoProyecto: " + e.getMessage());
		}
		
		return tareas;
	}
	
	public static void ejecutarQuery(List<PermisoProyecto> tareas, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			PermisoProyecto proyecto= new PermisoProyecto();
			pasarDatosDelResultSetAPermisoProyecto(proyecto, rs);
			tareas.add(proyecto);
		}
	}
	
	public static void pasarDatosDelResultSetAPermisoProyecto(PermisoProyecto proyecto, ResultSet rs) throws SQLException{
		proyecto.setLogin(rs.getString(U+Usuario.CAMPOS.LOGIN));
		proyecto.setCrearTareas(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.CREAR_TAREAS));
		proyecto.setEditarTareas(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_TAREAS));
		proyecto.setEliminarTareas(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_TAREAS));
		proyecto.setEditarProyecto(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_PROYECTO));
		proyecto.setEliminarProyecto(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_PROYECTO));
		proyecto.setCompartir(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.COMPARTIR));
		proyecto.setEditarCompartir(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_COMPARTIR));
		proyecto.setEliminarCompartir(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_COMPARTIR));
		proyecto.setAsignarTarea(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ASIGNAR_TAREAS));
		proyecto.setEditarAsignar(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.EDITAR_ASIGNAR));
		proyecto.setEliminarAsignar(rs.getBoolean(PC+ProyectoCompartido.CAMPOS.ELIMINAR_ASIGNAR));
	}
	
	public static List<PermisoProyecto> obtenerPermisosGrupoDeProyecto(int codProyecto,Connection conn){
		return busquedaAvanzadaVistaProyecto(" AND "+PC+ProyectoCompartido.CAMPOS.PROYECTO+"="+codProyecto, "",conn);
	}
}
