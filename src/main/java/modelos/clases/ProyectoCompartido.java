package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import modelos.conexion.Conexion;

import org.apache.commons.lang.StringUtils;

public class ProyectoCompartido {
	Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}

	public static class CAMPOS {
		public final static String USUARIO = "idUsuario";
		public final static String PROYECTO = "idProyecto";
		public final static String CREAR_TAREAS = "crearTareas";
		public final static String EDITAR_TAREAS = "editarTareas";
		public final static String EDITAR_PROYECTO = "editarProyecto";
		public final static String ELIMINAR_TAREAS = "eliminarTareas";
		public static final String ELIMINAR_PROYECTO = "eliminarProyecto";
		public static final String COMPARTIR = "compartir";
		public static final String EDITAR_COMPARTIR= "editarCompartir";
		public static final String ELIMINAR_COMPARTIR = "eliminarCompartir";
		public static final String ASIGNAR_TAREAS = "asignarTareas";
		public static final String EDITAR_ASIGNAR = "editarAsignar";
		public static final String ELIMINAR_ASIGNAR = "eliminarAsignar";
		
		public final static String NOMBRE_TABLA = "proyectocompartido";
	}
	
	private static final String selectTodo = CAMPOS.COMPARTIR+","+CAMPOS.EDITAR_TAREAS+","+
		CAMPOS.CREAR_TAREAS+","+CAMPOS.EDITAR_COMPARTIR+","+CAMPOS.EDITAR_PROYECTO+","+
		CAMPOS.ELIMINAR_COMPARTIR+","+CAMPOS.ELIMINAR_PROYECTO+","+CAMPOS.ELIMINAR_TAREAS+","+
		CAMPOS.ASIGNAR_TAREAS+","+CAMPOS.EDITAR_ASIGNAR+","+CAMPOS.ELIMINAR_ASIGNAR;
	
	private int idProyecto = 0;
	private int idUsuario = 0;
	private boolean crearTareas = false;
	private boolean editarTareas = false;
	private boolean eliminarTareas = false;
	private boolean editarProyecto = false;
	private boolean eliminarProyecto = false;
	private boolean compartir = false;
	private boolean editarCompartir = false;
	private boolean eliminarCompartir = false;
	private boolean asignarTareas = false;
	private boolean editarAsignar = false;
	private boolean eliminarAsignar = false;
	
	private String loginMiembro = "";

	public int getIdProyecto() {return idProyecto;}
	public void setIdProyecto(int idProyecto) {this.idProyecto = idProyecto;}
	
	public int getIdUsuario() {return idUsuario;}
	public void setIdUsuario(int idUsuario) {this.idUsuario = idUsuario;}
	
	public boolean isCrearTareas() {return crearTareas;}
	public void setCrearTareas(boolean crearTareas) {this.crearTareas = crearTareas;}
	
	public boolean isEditarTareas() {return editarTareas;}
	public void setEditarTareas(boolean editarTareas) {this.editarTareas = editarTareas;}

	public boolean isEliminarTareas() {return eliminarTareas;}
	public void setEliminarTareas(boolean eliminarTareas) {this.eliminarTareas = eliminarTareas;}

	public boolean isEditarProyecto() {return editarProyecto;}
	public void setEditarProyecto(boolean editarProyecto) {this.editarProyecto = editarProyecto;}

	public boolean isEliminarProyecto() {return eliminarProyecto;}
	public void setEliminarProyecto(boolean eliminarProyecto) {this.eliminarProyecto = eliminarProyecto;}

	public boolean isCompartir() {return compartir;}
	public void setCompartir(boolean compartir) {this.compartir = compartir;}

	public boolean isEditarCompartir() {return editarCompartir;}
	public void setEditarCompartir(boolean editarCompartir) {this.editarCompartir = editarCompartir;}

	public boolean isEliminarCompartir() {return eliminarCompartir;}
	public void setEliminarCompartir(boolean eliminarCompartir) {this.eliminarCompartir = eliminarCompartir;}

	public String getLoginMiembro() {return loginMiembro;}
	public void setLoginMiembro(String loginMiembro) {this.loginMiembro = loginMiembro;}
	
	public boolean isAsignarTareas() {return asignarTareas;}
	public void setAsignarTareas(boolean asignarTareas) {this.asignarTareas = asignarTareas;}
	
	public boolean isEditarAsignar() {return editarAsignar;}
	public void setEditarAsignar(boolean editarAsignar) {this.editarAsignar = editarAsignar;}
	
	public boolean isEliminarAsignar() {return eliminarAsignar;}
	public void setEliminarAsignar(boolean eliminarAsignar) {this.eliminarAsignar = eliminarAsignar;}
	
	public boolean updateIdUsuario(){
		if(StringUtils.isBlank(this.loginMiembro)) return false;
		Usuario usuario = new Usuario(this.loginMiembro,this.conn);
		if(usuario.getIdUsuario()==0) return false;
		else {this.idUsuario = usuario.getIdUsuario(); return true;}
	}
	
	public boolean guardarNuevoProyectoCompartido() { 
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idProyecto > 0 && this.idUsuario>0) {
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.USUARIO+","+CAMPOS.PROYECTO+","+CAMPOS.CREAR_TAREAS+
						","+CAMPOS.EDITAR_TAREAS+","+CAMPOS.ELIMINAR_TAREAS+","+CAMPOS.EDITAR_PROYECTO+","+CAMPOS.ELIMINAR_PROYECTO+","+
						CAMPOS.COMPARTIR+","+CAMPOS.EDITAR_COMPARTIR+","+CAMPOS.ELIMINAR_COMPARTIR+","+
						CAMPOS.ASIGNAR_TAREAS+","+CAMPOS.EDITAR_ASIGNAR+","+CAMPOS.ELIMINAR_ASIGNAR+") " +
						"VALUES ("+this.idUsuario+","+this.idProyecto+","+this.crearTareas+","+
						this.editarTareas+","+this.eliminarTareas+","+this.editarProyecto+","+this.eliminarProyecto+","+
						this.compartir+","+this.editarCompartir+","+this.eliminarCompartir+","+this.asignarTareas+","+
						this.editarAsignar+","+this.eliminarAsignar+")";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				return (ps.executeUpdate() > 0);
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error guardando el proyectoCompartido: " + e.getMessage());
			return false;
		}
	}
	
	public boolean updateProyectoCompartido() { 
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idProyecto > 0 && this.idUsuario>0) {
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.CREAR_TAREAS+"="+this.crearTareas+","+
						CAMPOS.EDITAR_TAREAS+"="+this.editarTareas+","+CAMPOS.ELIMINAR_TAREAS+"="+this.eliminarTareas+","+
						CAMPOS.EDITAR_PROYECTO+"="+this.editarProyecto+","+CAMPOS.ELIMINAR_PROYECTO+"="+this.eliminarProyecto+","+
						CAMPOS.COMPARTIR+"="+this.compartir+","+CAMPOS.EDITAR_COMPARTIR+"="+this.editarCompartir+","+
						CAMPOS.ELIMINAR_COMPARTIR+"="+this.eliminarCompartir+","+CAMPOS.ASIGNAR_TAREAS+"="+this.asignarTareas+","+
						CAMPOS.EDITAR_ASIGNAR+"="+this.editarAsignar+","+CAMPOS.ELIMINAR_ASIGNAR+"="+this.eliminarAsignar+
						" WHERE "+CAMPOS.USUARIO+"="+this.idUsuario+" AND "+CAMPOS.PROYECTO+"="+this.idProyecto;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando el proyectoCompartido: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean eliminarProyectoCompartido(int idUsuario, int idProyecto, Connection conn){
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (idProyecto > 0 && idUsuario>0) {
				String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.USUARIO+"="+idUsuario+" AND "+CAMPOS.PROYECTO+"="+idProyecto;
				PreparedStatement ps = conn.prepareStatement(query);
				
				return (ps.executeUpdate() > 0);
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando el proyectoCompartido: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean existeProyectoCompartido(int idUsuario, int idProyecto, Connection conn) { 
		return usuarioPuedeProyecto(idUsuario, idProyecto, "", conn);
	}
	
	public static boolean usuarioPuedeProyecto(int idUsuario, int idProyecto, String columnaPuede, Connection conn){
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (idProyecto > 0 && idUsuario>0) {
				String query = "SELECT "+CAMPOS.PROYECTO+" FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.USUARIO+"="+idUsuario+" AND "+CAMPOS.PROYECTO+"="+idProyecto;
				if(StringUtils.isNotBlank(columnaPuede)) query += " AND "+columnaPuede+"=1";
				ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				
				if (rs.next()) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando el proyectoCompartido: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean usuarioPuedeCompartir(int idUsuario, int idProyecto, Connection conn){
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.COMPARTIR, conn);
	}
	public static boolean usuarioPuedeEditarCompartir(int idUsuario, int idProyecto, Connection conn){
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.EDITAR_COMPARTIR, conn);
	}
	public static boolean usuarioPuedeEliminarCompartir(int idUsuario, int idProyecto, Connection conn){
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.ELIMINAR_COMPARTIR, conn);
	}
	public static boolean usuarioPuedeEditarProyecto(int idUsuario, int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.EDITAR_PROYECTO, conn);
	}
	public static boolean usuarioPuedeEliminarProyecto(int idUsuario, int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.ELIMINAR_PROYECTO, conn);
	}
	public static boolean usuarioPuedeCrearTareas(int idUsuario,int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.CREAR_TAREAS, conn);
	}
	public static boolean usuarioPuedeEditarTareas(int idUsuario,int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.EDITAR_TAREAS, conn);
	}
	public static boolean usuarioPuedeEliminarTareas(int idUsuario,int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.ELIMINAR_TAREAS, conn);
	}
	public static boolean usuarioPuedeAsignarTareas(int idUsuario,int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.ASIGNAR_TAREAS, conn);
	}
	public static boolean usuarioPuedeEditarAsignar(int idUsuario,int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.EDITAR_ASIGNAR, conn);
	}
	public static boolean usuarioPuedeEliminarAsignar(int idUsuario,int idProyecto, Connection conn) {
		return usuarioPuedeProyecto(idUsuario, idProyecto, CAMPOS.ELIMINAR_ASIGNAR, conn);
	}
	
	public boolean obtenerProyectoCompartido(int idUsuario, int idProyecto) {
		boolean encontrado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT "+selectTodo+" FROM "+CAMPOS.NOMBRE_TABLA+
					" WHERE "+CAMPOS.USUARIO+"="+idUsuario+" AND "+CAMPOS.PROYECTO+"="+idProyecto;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					pasarDatosDelResultSetAlProyectoCompartido(this, rs);
					this.setIdProyecto(idProyecto);
					this.setIdUsuario(idUsuario);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	private void pasarDatosDelResultSetAlProyectoCompartido(ProyectoCompartido pc, ResultSet rs) throws SQLException {
		pc.setCompartir(rs.getBoolean(CAMPOS.COMPARTIR));
		pc.setCrearTareas(rs.getBoolean(CAMPOS.CREAR_TAREAS));
		pc.setEditarCompartir(rs.getBoolean(CAMPOS.EDITAR_COMPARTIR));
		pc.setEditarProyecto(rs.getBoolean(CAMPOS.EDITAR_PROYECTO));
		pc.setEditarTareas(rs.getBoolean(CAMPOS.EDITAR_TAREAS));
		pc.setEliminarCompartir(rs.getBoolean(CAMPOS.ELIMINAR_COMPARTIR));
		pc.setEliminarProyecto(rs.getBoolean(CAMPOS.ELIMINAR_PROYECTO));
		pc.setEliminarTareas(rs.getBoolean(CAMPOS.ELIMINAR_TAREAS));
		pc.setAsignarTareas(rs.getBoolean(CAMPOS.ASIGNAR_TAREAS));
		pc.setEditarAsignar(rs.getBoolean(CAMPOS.EDITAR_ASIGNAR));
		pc.setEliminarAsignar(rs.getBoolean(CAMPOS.ELIMINAR_ASIGNAR));
	}
	public static void eliminarProyectosCompartidosSegunProyecto(
			int idProyecto, Connection conn) {
		String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.PROYECTO+"="+idProyecto;
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
