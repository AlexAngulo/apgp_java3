package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.conexion.Conexion;
import modelos.utiles.Orden;

import org.javatuples.Triplet;

public class TareaAsignadaCompleta extends TareaAsignada {
	private String nombreTarea;
	private String descTarea;
	private int idProyecto;
	private String nombreProyecto;
	private int idGrupo;
	private String nombreGrupo;
	private int idSupertarea;
	private int duracion;
	private int holgura;
	private int completadoTotal;
	private int nivel;
	private String numTarea;
	private String superclase;
	private boolean puedeEditar;
	private boolean puedeEliminar;
	private boolean puedeAsignar;
	private boolean puedeEditarAsignar;
	private boolean puedeEliminarAsignar;
	private List<Triplet<String,Integer,Integer>> usuariosAsignados;
	
	private static final String T = "t.";
	private static final String P = "p.";
	private static final String G = "g.";
	private static final String TA = "ta.";
	private static final String MG = "mg.";
	
	public String getNombreTarea() {return nombreTarea;}
	public void setNombreTarea(String nombreTarea) {this.nombreTarea = nombreTarea;}

	public String getDescTarea() {return descTarea;}
	public void setDescTarea(String descTarea) {this.descTarea = descTarea;}

	public int getIdProyecto() {return idProyecto;}
	public void setIdProyecto(int idProyecto) {this.idProyecto = idProyecto;}

	public String getNombreProyecto() {return nombreProyecto;}
	public void setNombreProyecto(String nombreProyecto) {this.nombreProyecto = nombreProyecto;}

	public int getIdGrupo() {return idGrupo;}
	public void setIdGrupo(int idGrupo) {this.idGrupo = idGrupo;}

	public String getNombreGrupo() {return nombreGrupo;}
	public void setNombreGrupo(String nombreGrupo) {this.nombreGrupo = nombreGrupo;}

	public int getIdSupertarea() {return idSupertarea;}
	public void setIdSupertarea(int idSupertarea) {this.idSupertarea = idSupertarea;}

	public int getDuracion() {return duracion;}
	public void setDuracion(int duracion) {this.duracion = duracion;}

	public int getHolgura() {return holgura;}
	public void setHolgura(int holgura) {this.holgura = holgura;}

	public int getCompletadoTotal() {return completadoTotal;}
	public void setCompletadoTotal(int completadoTotal) {this.completadoTotal = completadoTotal;}

	public int getNivel() {return nivel;}
	public void setNivel(int nivel) {this.nivel = nivel;}

	public String getNumTarea() {return numTarea;}
	public void setNumTarea(String numTarea) {this.numTarea = numTarea;}

	public List<Triplet<String, Integer, Integer>> getUsuariosAsignados() {return usuariosAsignados;}
	public void setUsuariosAsignados(List<Triplet<String, Integer, Integer>> usuariosAsignados) {this.usuariosAsignados = usuariosAsignados;}

	public String getSuperclase() {return superclase;}
	public void setSuperclase(String superclase) {this.superclase = superclase;}
	
	public boolean isPuedeEditar() {return puedeEditar;}
	public void setPuedeEditar(boolean puedeEditar) {this.puedeEditar = puedeEditar;}
	
	public boolean isPuedeEliminar() {return puedeEliminar;}
	public void setPuedeEliminar(boolean puedeEliminar) {this.puedeEliminar = puedeEliminar;}
	
	public boolean isPuedeAsignar() {return puedeAsignar;}
	public void setPuedeAsignar(boolean puedeAsignar) {this.puedeAsignar = puedeAsignar;}
	
	public boolean isPuedeEditarAsignar() {return puedeEditarAsignar;}
	public void setPuedeEditarAsignar(boolean puedeEditarAsignar) {this.puedeEditarAsignar = puedeEditarAsignar;}
	
	public boolean isPuedeEliminarAsignar() {return puedeEliminarAsignar;}
	public void setPuedeEliminarAsignar(boolean puedeEliminarAsignar) {this.puedeEliminarAsignar = puedeEliminarAsignar;}
	
	public int getDuracionAsignada(){return this.duracion * this.porcentaje / 100;}
	
	public String getEspacios(){
		String espacios = "";
		int i=0;
		while(i<nivel){
			espacios += "&nbsp;&nbsp;";
			i++;
		}
		return espacios;
	}
	
	
	
	
}
