package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelos.clases.Proyecto.CAMPOS;
import modelos.conexion.Conexion;

public class TareaAsignadaEquipo extends AsignamientoTarea{
	
	public static class CAMPOS {
		public final static String EQUIPO = "idEquipoAsignado";
		public final static String TAREA = "idTarea";
		public final static String PORCENTAJE = "porcentaje";
		public final static String COMPLETADO = "completado";
		
		public final static String NOMBRE_TABLA = "tareaasignadaequipo";
	}
	
	private static final String selectTodo = CAMPOS.COMPLETADO+","+CAMPOS.PORCENTAJE;
	
	protected int idEquipo;
	protected String nombreEquipo;
	
	public int getIdEquipo() {return idEquipo;}
	public void setIdEquipo(int idEquipo) {this.idEquipo = idEquipo;}
	
	public String getNombreEquipo() {return nombreEquipo;}
	public void setNombreEquipo(String nombreEquipo) {this.nombreEquipo= nombreEquipo;}
	
	public boolean guardarNuevaTareaAsignadaEquipo() { 
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idTarea > 0 && this.idEquipo>0) {
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.EQUIPO+","+CAMPOS.TAREA+","+
					CAMPOS.PORCENTAJE+","+CAMPOS.COMPLETADO+") " +
					"VALUES ("+this.idEquipo+","+this.idTarea+","+this.porcentaje+","+this.completado+")";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				return (ps.executeUpdate() > 0);
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error guardando el proyectoCompartido: " + e.getMessage());
			return false;
		}
	}
	
	public boolean updateTareaAsignada() { 
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idTarea > 0 && this.idEquipo>0) {
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.COMPLETADO+"="+this.completado+","+
					CAMPOS.PORCENTAJE+"="+this.porcentaje+" WHERE "+
					CAMPOS.EQUIPO+"="+this.idEquipo+" AND "+CAMPOS.TAREA+"="+this.idTarea;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando la tareaAsignada: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean eliminarTareaAsignada(int idEquipo, int idTarea, Connection conn){
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (idTarea > 0 && idEquipo>0) {
				String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.EQUIPO+"="+idEquipo+" AND "+CAMPOS.TAREA+"="+idTarea;
				PreparedStatement ps = conn.prepareStatement(query);
				
				return (ps.executeUpdate() > 0);
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando la tareaAsignada: " + e.getMessage());
			return false;
		}
	}
	
	public static boolean existeTareaAsignada(int idEquipo, int idTarea, Connection conn){
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (idTarea > 0 && idEquipo>0) {
				String query = "SELECT "+CAMPOS.TAREA+" FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.EQUIPO+"="+idEquipo+" AND "+CAMPOS.TAREA+"="+idTarea;
				ps = conn.prepareStatement(query);
				ResultSet rs = ps.executeQuery();
				
				if (rs.next()) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Error editando la tareaAsignada: " + e.getMessage());
			return false;
		}
	}
	
	public boolean obtenerTareaAsignada(int idEquipo, int idTarea) {
		boolean encontrado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT "+selectTodo+" FROM "+CAMPOS.NOMBRE_TABLA+
					" WHERE "+CAMPOS.EQUIPO+"="+idEquipo+" AND "+CAMPOS.TAREA+"="+idTarea;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					pasarDatosDelResultSetATareaAsignada(this, rs);
					this.setIdTarea(idTarea);
					this.setIdEquipo(idEquipo);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	private static void pasarDatosDelResultSetATareaAsignada(TareaAsignadaEquipo ta, ResultSet rs) throws SQLException {
		try{
			ta.setNombreEquipo(rs.getString("e."+Equipo.CAMPOS.NOMBRE_EQUIPO));
		} catch(Exception e){}
		ta.setCompletado(rs.getInt(CAMPOS.COMPLETADO));
		ta.setPorcentaje(rs.getInt(CAMPOS.PORCENTAJE));
	}
	
	public static List<TareaAsignadaEquipo> obtenerAsignamientosPorTareaEquipo(int idTarea, Connection conn){
		List<TareaAsignadaEquipo> lta = new ArrayList<TareaAsignadaEquipo>();
		try{
			if(conn==null) conn = Conexion.obtenerConexion();
			String query = "SELECT "+selectTodo+",tae."+CAMPOS.EQUIPO+",e."+Equipo.CAMPOS.NOMBRE_EQUIPO+",e."+Equipo.CAMPOS.ID_EQUIPO+" FROM "+
				CAMPOS.NOMBRE_TABLA+" tae,"+Equipo.CAMPOS.NOMBRE_TABLA+" e WHERE e."+Equipo.CAMPOS.ID_EQUIPO+"=tae."+CAMPOS.EQUIPO+" AND tae."+
				CAMPOS.TAREA+"="+idTarea;
			PreparedStatement ps = conn.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			TareaAsignadaEquipo ta;
			while (rs.next()) {
				ta = new TareaAsignadaEquipo();
				pasarDatosDelResultSetATareaAsignada(ta, rs);
				ta.setIdTarea(idTarea);
				ta.setIdEquipo(rs.getInt(CAMPOS.EQUIPO));
				lta.add(ta);
			}
		} catch (Exception e){
			System.out.println("Error obteniendo asignamientos por tarea : " + e.getMessage());
			return new ArrayList<TareaAsignadaEquipo>();
		}
		return lta;
	}
	public static String getSelect(String pre) {
		return pre+CAMPOS.TAREA+","+pre+CAMPOS.EQUIPO+","+pre+CAMPOS.COMPLETADO+","+pre+CAMPOS.PORCENTAJE;
	}
	public static void eliminarTareasAsignadasEquiposSegunTarea(int idTarea,Connection conn) {
		String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.TAREA+"="+idTarea;
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
