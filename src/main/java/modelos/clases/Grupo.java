package modelos.clases;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.conexion.Conexion;
import modelos.utiles.UtilBD;
import controlador.logica.LogicaGrupo;

public class Grupo implements Serializable {
private static final long serialVersionUID = 1L;

	protected Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	public static class CAMPOS {
		public final static String ID_GRUPO = "idGrupo";
		public final static String CREADOR = "idCreador";
		public final static String SUPERGRUPO = "idSupergrupo";
		public final static String NOMBRE_GRUPO = "nombreGrupo";
		public final static String DESC_GRUPO = "descGrupo";
		public final static String ORDEN_PROYECTOS = "ordenProyectos";
		
		public final static String NOMBRE_TABLA = "Grupo";
	}
	
	private int idGrupo;
	private int idCreador;
	private int idSupergrupo;
	private String nombreGrupo;
	private String descGrupo;
	private int ordenProyectos;
	
	public Grupo() {
		this.idGrupo = 0;
		this.idCreador = 0;
		this.idSupergrupo = 0;
		this.nombreGrupo = "";
		this.descGrupo = "";
	}
	
	public Grupo(int idGrupo, int idCreador, int idSupergrupo, String nombreGrupo, String descGrupo) {
		this.idGrupo = idGrupo;
		this.idCreador = idCreador;
		this.idSupergrupo = idSupergrupo;
		this.nombreGrupo = nombreGrupo;
		this.descGrupo = descGrupo;
	}
	
	public Grupo(int idCreador, int idSupergrupo, String nombreGrupo, String descGrupo) {
		this.idCreador = idCreador;
		this.idSupergrupo = idSupergrupo;
		this.nombreGrupo = nombreGrupo;
		this.descGrupo = descGrupo;
	}
	
	public Grupo (int idGrupo,Connection conn) {
		this.setConn(conn);
		this.obtenerGrupoPorId(idGrupo);
	}
	
	public int getIdGrupo(){ return this.idGrupo; }
	public void setIdGrupo(int idGrupo){this.idGrupo = idGrupo;}
	
	public int getIdCreador(){return this.idCreador;}
	public void setIdCreador(int idCreador){this.idCreador = idCreador;}

	public int getIdSupergrupo() {return idSupergrupo;}
	public void setIdSupergrupo(int idSupergrupo) {this.idSupergrupo = idSupergrupo;}

	public String getNombreGrupo() {return nombreGrupo;}
	public void setNombreGrupo(String nombreGrupo) {this.nombreGrupo = nombreGrupo;}
	
	public String getDescGrupo() {return descGrupo;}
	public void setDescGrupo(String descGrupo) {this.descGrupo = descGrupo;}

	public int getOrdenProyectos() {return ordenProyectos;}
	public void setOrdenProyectos(int ordenProyectos) {this.ordenProyectos = ordenProyectos;}

	protected final static String selectGrupo = CAMPOS.ID_GRUPO+","+CAMPOS.CREADOR+","+CAMPOS.SUPERGRUPO+","+CAMPOS.NOMBRE_GRUPO+","+ CAMPOS.DESC_GRUPO+","+CAMPOS.ORDEN_PROYECTOS;
	
	private final static String selectTodo = "SELECT "+selectGrupo+
			" FROM "+CAMPOS.NOMBRE_TABLA+ " ";
	
	public static ResultSet devolverResultSet(String query, Connection conn) throws Exception{
		if(conn==null)
			conn = Conexion.obtenerConexion();
		if (conn != null) {
			PreparedStatement ps = conn.prepareStatement(query);
			return ps.executeQuery();
		}
		return null;
	}
	
	public boolean obtenerGrupoPorId(int idGrupo) {
		boolean encontrado = false;
		try {
			ResultSet rs = devolverResultSet(selectTodo+" WHERE "+CAMPOS.ID_GRUPO+"="+idGrupo, conn);
			if (rs.next()) {
				LogicaGrupo.pasarDatosDelResultSetAlGrupo(this, rs);
				encontrado = true;
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		} 
		
		return encontrado;
	}
	
	public static List<Grupo> busquedaAvanzadaGrupo(String where, String orderBy,Connection conn){
		List<Grupo> usuarios = new ArrayList<Grupo>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
				ejecutarQuery(usuarios, conn, query);
			}
		} catch (Exception e) {
			usuarios = new ArrayList<Grupo>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return usuarios;
	}

	/**
	 * Elimina el usuario cargado en funcion de su identificador
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha borrado alguna fila
	 */
	public boolean eliminarGrupo() {
		boolean eliminado = false;
		
		if (this.idGrupo != 0) {
			try {
				if(this.conn==null)
					this.conn = Conexion.obtenerConexion();
				if (conn != null) {
					PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_GRUPO+"="+this.idGrupo);
					if (ps.executeUpdate() > 0) {
						eliminado = true;
					}
				}
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			} 
		}
		
		return eliminado;
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public boolean guardarGrupo() { 
		boolean guardado = false;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			if (this.idGrupo == 0) {
				// INSERTAR
				String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.CREADOR+","+CAMPOS.SUPERGRUPO+","+CAMPOS.NOMBRE_GRUPO+
						","+CAMPOS.DESC_GRUPO+") " +
						"VALUES ("+(this.idCreador==0 ? "null" : this.idCreador)+","+
						(this.idSupergrupo==0 ? "null" : this.idSupergrupo)+",'"+this.nombreGrupo+"','"+this.descGrupo+"')";
				ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
				if (ps.executeUpdate() > 0) {
					guardado = true;
					
					ResultSet rs = ps.getGeneratedKeys();
					if (rs.next()) {
						this.idGrupo = rs.getInt(1);
					}
				}
			} else {
				// ACTUALIZAR
				String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.CREADOR+"='"+(this.idCreador==0 ? "null" : this.idCreador)+"',"+
				CAMPOS.SUPERGRUPO+"="+(this.idSupergrupo==0 ? "null" : this.idSupergrupo)+","+
				CAMPOS.NOMBRE_GRUPO+"='"+this.nombreGrupo+"',"+CAMPOS.DESC_GRUPO+"='"+this.descGrupo+"',"+
				CAMPOS.ORDEN_PROYECTOS+"="+this.ordenProyectos+
				" WHERE "+CAMPOS.ID_GRUPO+"="+this.idGrupo;
				ps = conn.prepareStatement(query);
				
				if (ps.executeUpdate() > 0) {
					guardado = true;
				}
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
	
	/**
	 * Metodo que obtiene los usuarios de una select
	 * @param usuarios Lista donde se añadiran los usuarios
	 * @param conn Conexion
	 * @param query Query
	 * @throws SQLException
	 */
	public static void ejecutarQuery(List<Grupo> grupos, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Grupo grupo = new Grupo();
			LogicaGrupo.pasarDatosDelResultSetAlGrupo(grupo, rs);
			grupos.add(grupo);
		}
	}
	
	public static boolean actualizarGruposHijoDeUnGrupo(int idSupergrupo,
			Connection conn) {
		boolean eliminados = false;
		
		if (idSupergrupo != 0) {
			try {
				if(conn==null)
					conn = Conexion.obtenerConexion();
				if (conn != null) {
					PreparedStatement ps = conn.prepareStatement("UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+
							CAMPOS.SUPERGRUPO+"=null WHERE "+CAMPOS.SUPERGRUPO+"="+idSupergrupo);
					ps.executeUpdate();
					eliminados = true;
				}
			} catch (Exception e) {
				eliminados = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			} 
		}
		
		return eliminados;
	}
}
