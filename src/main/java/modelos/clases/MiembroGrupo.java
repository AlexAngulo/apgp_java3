package modelos.clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.clasesAbstractas.IntMiembroGrupo;
import modelos.conexion.Conexion;
import modelos.enums.TopePermisos;
import controlador.logica.LogicaMiembroGrupo;

public class MiembroGrupo extends IntMiembroGrupo {
	protected static final long serialVersionUID = 1L;
	
	private int idUsuario;
	private int idGrupo;
	
	public MiembroGrupo() {
		super();
		this.idUsuario = 0;
		this.idGrupo = 0;
	}
	
	public MiembroGrupo(int idUsuario, int idGrupo, boolean[] permisos) {
		super(
			permisos[TopePermisos.CREAR_TAREAS],
			permisos[TopePermisos.CREAR_PROYECTOS],
			permisos[TopePermisos.EDITAR_TAREAS],
			permisos[TopePermisos.EDITAR_PROYECTOS],
			permisos[TopePermisos.ELIMINAR_TAREAS],
			permisos[TopePermisos.ELIMINAR_PROYECTOS],
			permisos[TopePermisos.CREAR_SUBGRUPO],
			permisos[TopePermisos.INVITAR_MIEMBRO],
			permisos[TopePermisos.ELIMINAR_SUBGRUPO],
			permisos[TopePermisos.EDITAR_MIEMBRO],
			permisos[TopePermisos.EDITAR_GRUPO],
			permisos[TopePermisos.ELIMINAR_MIEMBROS],
			permisos[TopePermisos.ELIMINAR_GRUPO],
			permisos[TopePermisos.COMPARTIR_PROYECTO],
			permisos[TopePermisos.EDITAR_COMPARTIR],
			permisos[TopePermisos.ELIMINAR_COMPARTIR],
			permisos[TopePermisos.ASIGNAR_TAREAS],
			permisos[TopePermisos.EDITAR_ASIGNAMIENTO],
			permisos[TopePermisos.ELIMINAR_ASIGNAMIENTO],
			permisos[TopePermisos.CREAR_EQUIPO],
			permisos[TopePermisos.EDITAR_EQUIPO],
			permisos[TopePermisos.ELIMINAR_EQUIPO]
			);
		this.idUsuario = idUsuario;
		this.idGrupo = idGrupo;
	}
	
	public MiembroGrupo(int idUsuario, int idGrupo, boolean crearTareas, boolean crearProyectos, boolean editarTareas,
			boolean editarProyectos, boolean eliminarTareas, boolean eliminarProyectos, boolean crearSubgrupo,
			boolean invitarMiembros, boolean eliminarSubgrupo, boolean editarMiembros, boolean editarGrupo,
			boolean eliminarMiembros, boolean eliminarGrupo, boolean compartirProyecto, boolean editarCompartir,
			boolean eliminarCompartir,boolean asignarTareas, boolean editarAsignamiento, boolean eliminarAsignamiento,
			boolean crearEquipo, boolean editarEquipo, boolean eliminarEquipo) {
		super(crearTareas, crearProyectos, editarTareas, editarProyectos, eliminarTareas, 
			  eliminarProyectos, crearSubgrupo, invitarMiembros, eliminarSubgrupo, editarMiembros, editarGrupo, 
			  eliminarMiembros, eliminarGrupo, compartirProyecto, editarCompartir, eliminarCompartir, 
			  asignarTareas, editarAsignamiento, eliminarAsignamiento,crearEquipo, editarEquipo, eliminarEquipo);
		this.idUsuario = idUsuario;
		this.idGrupo = idGrupo;
	}
	
	public MiembroGrupo(int idUsuario, int idGrupo,Connection conn) {
		this.setConn(conn);
		this.obtenerMiembroGrupoPorIds(idUsuario, idGrupo);
	}
	
	public int getIdUsuario(){ return this.idUsuario; }
	public void setIdUsuario(int idUsuario){this.idUsuario = idUsuario;}
	
	public int getIdGrupo(){ return this.idGrupo; }
	public void setIdGrupo(int idGrupo){this.idGrupo = idGrupo;}

	
	
	private final static String selectTodo = "SELECT "+CAMPOS_MG.ID_USUARIO +","+CAMPOS_MG.ID_GRUPO+","+selectPermisosMG+
	" FROM "+CAMPOS_MG.NOMBRE_TABLA;
	
	public boolean obtenerMiembroGrupoPorIds(int idUsuario, int idGrupo) {
		boolean encontrado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo+" WHERE "+CAMPOS_MG.ID_USUARIO+"="+idUsuario+" AND "+CAMPOS_MG.ID_GRUPO+"="+idGrupo;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					LogicaMiembroGrupo.pasarDatosDelResultSetAlMiembroGrupo(this, rs);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo usuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	
	public static List<MiembroGrupo> busquedaAvanzadaMiembroGrupo(String where, String orderBy,Connection conn){
		List<MiembroGrupo> miembrosGrupos = new ArrayList<MiembroGrupo>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = selectTodo + where + orderBy;
				ejecutarQuery(miembrosGrupos, conn, query);
			}
		} catch (Exception e) {
			miembrosGrupos = new ArrayList<MiembroGrupo>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return miembrosGrupos;
	}

	/**
	 * Elimina el usuario cargado en funcion de su identificador
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha borrado alguna fila
	 */
	public boolean eliminarGrupo() {
		boolean eliminado = false;
		
		if (this.idUsuario!=0 && this.idGrupo!=0) {
			try {
				if(this.conn==null)
					this.conn = Conexion.obtenerConexion();
				if (conn != null) {
					PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_USUARIO+"="+this.idUsuario + 
							" AND "+CAMPOS.ID_GRUPO+"="+this.idGrupo);
					if (ps.executeUpdate() > 0) {
						eliminado = true;
					}
				}
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			}
		}
		
		return eliminado;
	}
	
	/**
	 * Crea o actualiza en base de datos. Si el identificador del
	 * usuario cargado en el objeto es 0 crea un nuevo usuario,
	 * si es distinto lo actualiza.
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha guardado/actualizado
	 */
	public boolean guardarMiembroGrupo() { 
		boolean guardado = false;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			String query = "INSERT INTO "+CAMPOS.NOMBRE_TABLA+" ("+CAMPOS.ID_USUARIO+","+CAMPOS.ID_GRUPO+","+selectPermisos+","+CAMPOS.HORAS+","+CAMPOS.DIAS+") "+
					"VALUES ("+this.idUsuario+","+this.idGrupo+","+this.crearTareas+","+this.crearProyectos+","+
					this.crearSubgrupo+","+this.editarGrupo+","+this.editarMiembros+","+this.editarProyectos+","+this.editarTareas+","+
					this.eliminarGrupo+","+this.eliminarMiembros+","+this.eliminarProyectos+","+this.eliminarSubgrupo+","+
					this.eliminarTareas+","+this.invitarMiembros+","+this.compartirProyecto+","+this.editarCompartir+","+
					this.eliminarCompartir+","+this.asignarTareas+","+this.editarAsignamiento+","+this.eliminarAsignamiento+","+
					this.crearEquipo+","+this.editarEquipo+","+this.eliminarEquipo+","+this.horasDia+","+this.diasSemana+
					")";
			ps = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			if (ps.executeUpdate() > 0) {
				guardado = true;
			}
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error guardando el usuario: " + e.getMessage());
		}
		
		return guardado;
	}
	
	public boolean editarMiembroGrupo() { 
		boolean guardado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			PreparedStatement ps = null;
			
			String query = "UPDATE "+CAMPOS.NOMBRE_TABLA+" SET "+CAMPOS.CREAR_PROYECTOS+"="+this.crearProyectos+","+
				CAMPOS.CREAR_SUBGRUPO+"="+this.crearSubgrupo+","+CAMPOS.CREAR_TAREAS+"="+this.crearTareas+","+
				CAMPOS.EDITAR_GRUPO+"="+this.editarGrupo+","+CAMPOS.EDITAR_MIEMBROS+"="+this.editarMiembros+","+
				CAMPOS.EDITAR_PROYECTOS+"="+this.editarProyectos+","+CAMPOS.EDITAR_TAREAS+"="+this.editarTareas+","+
				CAMPOS.ELIMINAR_GRUPO+"="+this.eliminarGrupo+","+CAMPOS.ELIMINAR_MIEMBROS+"="+this.eliminarMiembros+","+
				CAMPOS.ELIMINAR_PROYECTOS+"="+this.eliminarProyectos+","+CAMPOS.ELIMINAR_SUBGRUPO+"="+this.eliminarSubgrupo+","+
				CAMPOS.ELIMINAR_TAREAS+"="+this.eliminarTareas+","+CAMPOS.INVITAR_MIEMBROS+"="+this.invitarMiembros+","+
				CAMPOS.ASIGNAR_TAREAS+"="+this.asignarTareas+","+CAMPOS.COMPARTIR_PROYECTO+"="+this.compartirProyecto+","+
				CAMPOS.EDITAR_COMPARTIR+"="+this.editarCompartir+","+CAMPOS.ELIMINAR_COMPARTIR+"="+this.eliminarCompartir+","+
				CAMPOS.EDITAR_ASIGNAMIENTO+"="+this.editarAsignamiento+","+CAMPOS.ELIMINAR_ASIGNAMIENTO+"="+this.eliminarAsignamiento+","+
				CAMPOS.CREAR_EQUIPO+"="+this.crearEquipo+","+CAMPOS.EDITAR_EQUIPO+"="+this.editarEquipo+","+
				CAMPOS.ELIMINAR_EQUIPO+"="+this.eliminarEquipo+","+CAMPOS.DIAS+"="+this.diasSemana+","+
				CAMPOS.HORAS+"="+this.horasDia+
				" WHERE "+CAMPOS.ID_USUARIO+"="+this.idUsuario+" AND "+CAMPOS.ID_GRUPO+"="+this.idGrupo;
			ps = conn.prepareStatement(query);
			
			if (ps.executeUpdate() > 0) {
				guardado = true;
			}
			
		} catch (Exception e) {
			guardado = false;
			System.out.println("Error editando el miembro: " + e.getMessage());
		}
		
		return guardado;
	}
	
	/**
	 * Metodo que obtiene los usuarios de una select
	 * @param usuarios Lista donde se añadiran los usuarios
	 * @param conn Conexion
	 * @param query Query
	 * @throws SQLException
	 */
	public static void ejecutarQuery(List<MiembroGrupo> miembrosGrupos, Connection conn,
			String query) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			MiembroGrupo miembroGrupo = new MiembroGrupo();
			LogicaMiembroGrupo.pasarDatosDelResultSetAlMiembroGrupo(miembroGrupo, rs);
			miembrosGrupos.add(miembroGrupo);
		}
	}

	public static boolean eliminarMiembrosGrupoDeUnGrupo(int idGrupo,
			Connection conn) {
		boolean eliminado = false;
		
		if (idGrupo>0) {
			try {
				if(conn==null)
					conn = Conexion.obtenerConexion();
				PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+
						CAMPOS.ID_GRUPO+"="+idGrupo);
				ps.executeUpdate();
				eliminado = true;
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando miembros de grupo: " + e.getMessage());
			}
		}
		
		return eliminado;
	}

	public void setPermisos(boolean[] permisos) {
		this.setPermisos(
				permisos[TopePermisos.CREAR_TAREAS],permisos[TopePermisos.CREAR_PROYECTOS],permisos[TopePermisos.EDITAR_TAREAS],
				permisos[TopePermisos.EDITAR_PROYECTOS],permisos[TopePermisos.ELIMINAR_TAREAS],permisos[TopePermisos.ELIMINAR_PROYECTOS],
				permisos[TopePermisos.CREAR_SUBGRUPO],permisos[TopePermisos.INVITAR_MIEMBRO],permisos[TopePermisos.ELIMINAR_SUBGRUPO],
				permisos[TopePermisos.EDITAR_MIEMBRO],permisos[TopePermisos.EDITAR_GRUPO],permisos[TopePermisos.ELIMINAR_MIEMBROS],
				permisos[TopePermisos.ELIMINAR_GRUPO], permisos[TopePermisos.COMPARTIR_PROYECTO], permisos[TopePermisos.EDITAR_COMPARTIR],
				permisos[TopePermisos.ELIMINAR_COMPARTIR],permisos[TopePermisos.ASIGNAR_TAREAS],
				permisos[TopePermisos.EDITAR_ASIGNAMIENTO],permisos[TopePermisos.ELIMINAR_ASIGNAMIENTO],
				permisos[TopePermisos.CREAR_EQUIPO],
				permisos[TopePermisos.EDITAR_EQUIPO],permisos[TopePermisos.ELIMINAR_EQUIPO]
				);
		
	}

	public static HashMap<Integer, MiembroGrupo> obtenerMiembrosGrupo(int idGrupo, Connection conn) {
		HashMap<Integer, MiembroGrupo> hmMiembros = new HashMap<Integer, MiembroGrupo>();
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT mg."+CAMPOS.ID_GRUPO+",mg."+CAMPOS.ID_USUARIO+",mg."+CAMPOS.HORAS+",mg."+CAMPOS.DIAS+
					" FROM "+CAMPOS.NOMBRE_TABLA+" mg WHERE mg."+CAMPOS.ID_GRUPO+"="+idGrupo;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				MiembroGrupo mg;
				while(rs.next()){
					mg = new MiembroGrupo();
					mg.setIdGrupo(rs.getInt("mg."+CAMPOS.ID_GRUPO));
					mg.setIdUsuario(rs.getInt("mg."+CAMPOS.ID_USUARIO));
					mg.setDiasSemana(rs.getInt("mg."+CAMPOS.DIAS));
					mg.setHorasDia(rs.getInt("mg."+CAMPOS.HORAS));
					hmMiembros.put(mg.getIdUsuario(),mg);
				}
			}
		} catch (Exception e) {
			System.out.println("Error en busqueda avanzada usuario: " + e.getMessage());
			return new HashMap<Integer, MiembroGrupo>();
		} finally {
			
		}
		return hmMiembros;
	}

	public static void eliminarMiembroGrupoSegunUsuario(int idUsuario,
			Connection conn) {
		String query = "DELETE FROM "+CAMPOS.NOMBRE_TABLA+" WHERE "+CAMPOS.ID_USUARIO+"="+idUsuario;
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
