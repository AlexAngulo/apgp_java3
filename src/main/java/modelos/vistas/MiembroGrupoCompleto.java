package modelos.vistas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.clases.Grupo;
import modelos.clases.Proyecto;
import modelos.clases.Usuario;
import modelos.clasesAbstractas.IntMiembroGrupo;
import modelos.conexion.Conexion;
import modelos.utiles.UtilBD;

public class MiembroGrupoCompleto extends IntMiembroGrupo {
	protected static final long serialVersionUID = 1L;
	
	private Proyecto proyecto;
	private Usuario usuario;
	private Grupo grupo;
	
	public MiembroGrupoCompleto() {
		super();
		this.proyecto = null;
		this.usuario = null;
		this.grupo = null;
	}
	
	public MiembroGrupoCompleto(Usuario usuario, Grupo grupo,boolean crearTareas, boolean crearProyectos, boolean editarTareas,
			boolean editarProyectos, boolean eliminarTareas, boolean eliminarProyectos, boolean crearSubgrupo,
			boolean invitarMiembros, boolean eliminarSubgrupo, boolean editarMiembros, boolean editarGrupo,
			boolean eliminarMiembros, boolean eliminarGrupo, boolean compartirProyecto, boolean editarCompartir,
			boolean eliminarCompartir, boolean asignarTareas, boolean editarAsignamiento, boolean eliminarAsignamiento,
			boolean crearEquipo, boolean editarEquipo, boolean eliminarEquipo) {
		super(crearTareas, crearProyectos, editarTareas, editarProyectos, eliminarTareas, 
			  eliminarProyectos, crearSubgrupo, invitarMiembros, eliminarSubgrupo, editarMiembros, editarGrupo, 
			  eliminarMiembros, eliminarGrupo, compartirProyecto, editarCompartir, eliminarCompartir, asignarTareas, 
			  editarAsignamiento, eliminarAsignamiento,crearEquipo, editarEquipo, eliminarEquipo);
		this.usuario = usuario;
		this.grupo = grupo;
	}
	
	public MiembroGrupoCompleto(int idUsuario, int idGrupo,boolean hallarTodo,Connection conn) {
		this.setConn(conn);
		this.obtenerMiembroGrupoPorIds(idUsuario, idGrupo, hallarTodo);
	}
	
	public Proyecto getProyecto() {return proyecto;}
	public void setProyecto(Proyecto proyecto) {this.proyecto = proyecto;}

	public Usuario getUsuario(){ return this.usuario; }
	public void setUsuario(Usuario usuario){this.usuario = usuario;}
	
	public Grupo getGrupo(){ return this.grupo; }
	public void setGrupo(Grupo grupo){this.grupo = grupo;}
	
	private final static String P = "p.";
	private final static String U = "u.";
	//private final static String MG = "mg.";
	private final static String G = "g.";

	private final static String selectProyecto = P+Proyecto.CAMPOS.ID_PROYECTO+","+P+Proyecto.CAMPOS.NOMBRE_PROYECTO+","+P+Proyecto.CAMPOS.DESC_PROYECTO+","+
			P+Proyecto.CAMPOS.FECHA_FIN+","+P+Proyecto.CAMPOS.CREADOR+","+P+Proyecto.CAMPOS.GRUPO_PERTENECIENTE+","+P+Proyecto.CAMPOS.ORDENACION_TAREAS;
	private final static String selectUsuario = U+Usuario.CAMPOS.ID_USUARIO+","+U+Usuario.CAMPOS.LOGIN+","+U+Usuario.CAMPOS.EMAIL+","+U+Usuario.CAMPOS.E_VERIFICADO+","+U+Usuario.CAMPOS.ROL;
	private final static String selectGrupo = G+Grupo.CAMPOS.ID_GRUPO+","+G+Grupo.CAMPOS.NOMBRE_GRUPO+","+G+Grupo.CAMPOS.DESC_GRUPO+","+G+Grupo.CAMPOS.CREADOR+","+
			G+Grupo.CAMPOS.SUPERGRUPO;
	
	private final static String selectTodo = "SELECT "+selectUsuario+","+selectPermisosMG+","+selectGrupo+
			" FROM "+Usuario.CAMPOS.NOMBRE_TABLA+" u, "+CAMPOS_MG.NOMBRE_TABLA+", "+Grupo.CAMPOS.NOMBRE_TABLA+" g";
	private final static String selectTodoProyecto = "SELECT "+selectProyecto+","+selectPermisosMG+","+selectUsuario+
			" FROM "+Proyecto.CAMPOS.NOMBRE_TABLA+" p, "+CAMPOS_MG.NOMBRE_TABLA+", "+Usuario.CAMPOS.NOMBRE_TABLA+" u,"+Grupo.CAMPOS.NOMBRE_TABLA+" g ";
	
	public boolean obtenerMiembroGrupoPorIds(int idUsuario, int idGrupoOProyecto, boolean hallarTodo) {
		boolean encontrado = false;
		
		try {
			if(this.conn==null)
				this.conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "";
				
				if(hallarTodo)
					query += selectTodo+" WHERE ";
				else
					query += selectTodoProyecto +" WHERE ";
				
				if(hallarTodo)
					query += U+Usuario.CAMPOS.ID_USUARIO+"="+CAMPOS_MG.ID_USUARIO+" AND "+
						G+Grupo.CAMPOS.ID_GRUPO+"="+CAMPOS_MG.ID_GRUPO+" AND "+
						CAMPOS_MG.ID_USUARIO+"="+idUsuario+" AND "+CAMPOS_MG.ID_GRUPO+"="+idGrupoOProyecto;
				else
					query += P+Proyecto.CAMPOS.ID_PROYECTO+"="+idGrupoOProyecto+" AND "+
						P+Proyecto.CAMPOS.GRUPO_PERTENECIENTE+"="+G+Grupo.CAMPOS.ID_GRUPO+" AND "+
						G+Grupo.CAMPOS.ID_GRUPO+"="+CAMPOS_MG.ID_GRUPO+" AND "+
						CAMPOS_MG.ID_USUARIO+"="+U+Usuario.CAMPOS.ID_USUARIO+" AND "+
						U+Usuario.CAMPOS.ID_USUARIO+"="+idUsuario;
				PreparedStatement ps = conn.prepareStatement(query);
				
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {
					pasarDatosDelResultSetAlMiembroGrupo(this, rs, hallarTodo);
					encontrado = true;
				}
			}
		} catch (Exception e) {
			encontrado = false;
			System.out.println("Error obteniendo idUsuario por nombre : " + e.getMessage());
		}
		
		return encontrado;
	}
	
	public static List<MiembroGrupoCompleto> obtenerMiembrosGrupos(Connection conn, boolean hallarTodo) {
		return busquedaAvanzadaMiembroGrupo(new HashMap<String, Object>(),conn,hallarTodo);
	}

	public static List<MiembroGrupoCompleto> busquedaAvanzadaMiembroGrupo(HashMap<String, Object> filtros,Connection conn, boolean hallarTodo) {
		return busquedaAvanzadaMiembroGrupo(filtros, "", true,conn,hallarTodo);
	}

	public static List<MiembroGrupoCompleto> busquedaAvanzadaMiembroGrupo(HashMap<String, Object> filtros, String campoOrdenacion, boolean ascendente,Connection conn, boolean hallarTodo) {		
		return busquedaAvanzadaMiembroGrupo(UtilBD.crearClausulaWhereStringExacto(filtros), UtilBD.crearClausulaOrderBy(campoOrdenacion, ascendente),conn,hallarTodo);
	}
	
	public static List<MiembroGrupoCompleto> busquedaAvanzadaMiembroGrupo(String where, String orderBy,Connection conn, boolean hallarTodo){
		List<MiembroGrupoCompleto> miembrosGrupos = new ArrayList<MiembroGrupoCompleto>();
		
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "";
				if(hallarTodo)
					query += selectTodo;
				else
					query += selectTodoProyecto;
				query += where + orderBy;
				ejecutarQuery(miembrosGrupos, conn, query,hallarTodo);
			}
		} catch (Exception e) {
			miembrosGrupos = new ArrayList<MiembroGrupoCompleto>();
			System.out.println("Error en busqueda avanzada grupo: " + e.getMessage());
		}
		
		return miembrosGrupos;
	}

	/**
	 * Elimina el usuario cargado en funcion de su identificador
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo true si ha borrado alguna fila
	 */
	public boolean eliminarGrupo() {
		boolean eliminado = false;
		
		if (this.usuario.getIdUsuario()!=0 && this.grupo.getIdGrupo()!=0) {
			try {
				if(this.conn==null)
					this.conn = Conexion.obtenerConexion();
				if (conn != null) {
					PreparedStatement ps = conn.prepareStatement("DELETE FROM "+CAMPOS_MG.NOMBRE_TABLA+" WHERE "+CAMPOS_MG.ID_USUARIO+"="+this.usuario + 
							" AND "+CAMPOS_MG.ID_GRUPO+"="+this.grupo);
					if (ps.executeUpdate() > 0) {
						eliminado = true;
					}
				}
			} catch (Exception e) {
				eliminado = false;
				System.out.println("Error eliminando usuario: " + e.getMessage());
			}
		}
		
		return eliminado;
	}
	
	/**
	 * Metodo que obtiene los usuarios de una select
	 * @param usuarios Lista donde se añadiran los usuarios
	 * @param conn Conexion
	 * @param query Query
	 * @throws SQLException
	 */
	public static void ejecutarQuery(List<MiembroGrupoCompleto> miembrosGrupos, Connection conn,
			String query, boolean hallarTodo) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			MiembroGrupoCompleto miembroGrupo = new MiembroGrupoCompleto();
			pasarDatosDelResultSetAlMiembroGrupo(miembroGrupo, rs,hallarTodo);
			miembrosGrupos.add(miembroGrupo);
		}
	}
	
	private static void pasarDatosDelResultSetAlMiembroGrupoSoloUsuario(MiembroGrupoCompleto mg, ResultSet rs) throws SQLException{
		Usuario usuario = new Usuario();
		usuario.setLogin(rs.getString(U+Usuario.CAMPOS.LOGIN));
		mg.setUsuario(usuario);
		pasarDatosDelResultSetAlIntMiembroGrupo(mg, rs);
		//mg.setPermisos(rs.getInt(CAMPOS_MG.PERMISOS));
	}
	
	private static void pasarDatosDelResultSetAlMiembroGrupo(MiembroGrupoCompleto mg, ResultSet rs, boolean hallarTodo) throws SQLException{
		Usuario usuario = new Usuario(
				rs.getInt(U+Usuario.CAMPOS.ID_USUARIO),
				rs.getString(U+Usuario.CAMPOS.LOGIN),
				rs.getString(U+Usuario.CAMPOS.EMAIL),
				rs.getString(U+Usuario.CAMPOS.E_VERIFICADO).equals("1"),
				rs.getInt(U+Usuario.CAMPOS.ROL)
				);
		mg.setUsuario(usuario);
		pasarDatosDelResultSetAlIntMiembroGrupo(mg, rs);
		if(hallarTodo){
			Grupo grupo = new Grupo(
					rs.getInt(G+Grupo.CAMPOS.ID_GRUPO),
					rs.getInt(G+Grupo.CAMPOS.CREADOR),
					rs.getInt(G+Grupo.CAMPOS.SUPERGRUPO),
					rs.getString(G+Grupo.CAMPOS.NOMBRE_GRUPO),
					rs.getString(G+Grupo.CAMPOS.DESC_GRUPO)
					);
			mg.setGrupo(grupo);
		} else {
			Proyecto proyecto = new Proyecto(
					rs.getInt(P+Proyecto.CAMPOS.ID_PROYECTO),
					rs.getInt(P+Proyecto.CAMPOS.CREADOR),
					rs.getInt(P+Proyecto.CAMPOS.GRUPO_PERTENECIENTE),
					rs.getString(P+Proyecto.CAMPOS.NOMBRE_PROYECTO),
					rs.getString(P+Proyecto.CAMPOS.DESC_PROYECTO),
					rs.getDate(P+Proyecto.CAMPOS.FECHA_FIN)
					);
			mg.setProyecto(proyecto);
		}
	}

	public static List<MiembroGrupoCompleto> busquedaAvanzadaUsuarioPermisos(
			String where, String orderBy, Connection conn) {
		List<MiembroGrupoCompleto> miembrosGrupo = new ArrayList<MiembroGrupoCompleto>();
		try {
			if(conn==null)
				conn = Conexion.obtenerConexion();
			if (conn != null) {
				String query = "SELECT "+U+Usuario.CAMPOS.LOGIN+","+selectPermisosMG+" FROM "+
					CAMPOS_MG.NOMBRE_TABLA+ ","+Usuario.CAMPOS.NOMBRE_TABLA+" u "+ where + " AND "+CAMPOS_MG.ID_USUARIO+"="+U+Usuario.CAMPOS.ID_USUARIO + orderBy;
				ResultSet rs = Conexion.obtenerValoresDeUnaQuery(query, conn);
				while (rs.next()) {
					MiembroGrupoCompleto miembroGrupo = new MiembroGrupoCompleto();
					pasarDatosDelResultSetAlMiembroGrupoSoloUsuario(miembroGrupo, rs);
					miembrosGrupo.add(miembroGrupo);
				}
			}
		} catch (Exception e) {
			miembrosGrupo = new ArrayList<MiembroGrupoCompleto>();
			System.out.println("Error en busqueda avanzada miembrogrupo: " + e.getMessage());
		}
		
		return miembrosGrupo;
	}
}
