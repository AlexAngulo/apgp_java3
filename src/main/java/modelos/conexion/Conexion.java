package modelos.conexion;

//import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
//import java.util.Properties;
import java.sql.PreparedStatement;

public class Conexion {

	/**
	 * Metodo para obtener una conexion abierta con la base de datos
* @return Devuelve 1 en caso de tener �xito o un mensaje de error en caso de no tenerlo Conexion abierta con la base de datos
	 * @throws Exception
	 */
	public static Connection obtenerConexion() throws Exception {
		Connection conn = null;
		
		/*Properties prop = new Properties();
		InputStream stream = Conexion.class.getResourceAsStream("/conexion.properties");
		prop.load(stream);*/

		/*String servidor = prop.getProperty("servidor");
		String puerto = prop.getProperty("puerto");
		String usuario = prop.getProperty("usuario");
		String pass = prop.getProperty("pass");*/

		//String cadena = "jdbc:mysql://" + servidor + ":" + puerto + "/apgp";

		String cadena = "jdbc:mysql://localhost:3306/tienda";
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(cadena, "tienda", "tienda");

		return conn;
	}
	
	public static boolean realizarQuery(String query, Connection conn){
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public static ResultSet obtenerValoresDeUnaQuery(String query, Connection conn){
		try {
			PreparedStatement ps = conn.prepareStatement(query);
			return ps.executeQuery();
		} catch (Exception e) {
			return null;
		}
	}

}
