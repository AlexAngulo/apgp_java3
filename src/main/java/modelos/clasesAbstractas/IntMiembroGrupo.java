package modelos.clasesAbstractas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import modelos.enums.TopePermisos;

import org.javatuples.Pair;

public abstract class IntMiembroGrupo implements Serializable {
	protected static final long serialVersionUID = 1L;

	protected Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	public static class CAMPOS {
		public final static String ID_USUARIO = "idUsuario";
		public final static String ID_GRUPO = "idGrupo";
		
		public final static String CREAR_TAREAS = "crearTareas";
		public final static String CREAR_PROYECTOS = "crearProyectos";
		public final static String EDITAR_TAREAS = "editarTareas";
		public final static String EDITAR_PROYECTOS = "editarProyectos";
		public final static String ELIMINAR_TAREAS = "eliminarTareas";
		public final static String ELIMINAR_PROYECTOS = "eliminarProyectos";
		public final static String INVITAR_MIEMBROS = "invitarMiembros";
		public final static String EDITAR_MIEMBROS = "editarMiembros";
		public final static String ELIMINAR_MIEMBROS = "eliminarMiembros";
		public final static String CREAR_SUBGRUPO = "crearSubgrupos";
		public final static String ELIMINAR_SUBGRUPO = "eliminarSubgrupos";
		public final static String EDITAR_GRUPO = "editarGrupo";
		public final static String ELIMINAR_GRUPO = "eliminarGrupo";
		public final static String ASIGNAR_TAREAS = "asignarTareas";
		public static final String EDITAR_ASIGNAMIENTO = "editarAsignamientoTareas";
		public static final String ELIMINAR_ASIGNAMIENTO = "eliminarAsignamientoTareas";
		public final static String COMPARTIR_PROYECTO = "compartirProyecto";
		public final static String EDITAR_COMPARTIR = "editarCompartirProyecto";
		public final static String ELIMINAR_COMPARTIR = "eliminarCompartirProyecto";
		public final static String CREAR_EQUIPO = "crearEquipo";
		public final static String EDITAR_EQUIPO = "editarEquipo";
		public final static String ELIMINAR_EQUIPO = "eliminarEquipo";
		public final static String DIAS = "diasLaborablesSemana";
		public final static String HORAS = "horasLaborablesDia";
		
		public final static String NOMBRE_TABLA = "miembrogrupo";
	}
	
	public static final String MG = "mg.";
	
	public static class CAMPOS_MG {
		public final static String ID_USUARIO = MG+CAMPOS.ID_USUARIO;
		public final static String ID_GRUPO = MG+CAMPOS.ID_GRUPO;
		
		public final static String CREAR_TAREAS = MG+CAMPOS.CREAR_TAREAS;
		public final static String CREAR_PROYECTOS = MG+CAMPOS.CREAR_PROYECTOS;
		public final static String EDITAR_TAREAS = MG+CAMPOS.EDITAR_TAREAS;
		public final static String EDITAR_PROYECTOS = MG+CAMPOS.EDITAR_PROYECTOS;
		public final static String ELIMINAR_TAREAS = MG+CAMPOS.ELIMINAR_TAREAS;
		public final static String ELIMINAR_PROYECTOS = MG+CAMPOS.ELIMINAR_PROYECTOS;
		public final static String INVITAR_MIEMBROS = MG+CAMPOS.INVITAR_MIEMBROS;
		public final static String EDITAR_MIEMBROS = MG+CAMPOS.EDITAR_MIEMBROS;
		public final static String ELIMINAR_MIEMBROS = MG+CAMPOS.ELIMINAR_MIEMBROS;
		public final static String CREAR_SUBGRUPO = MG+CAMPOS.CREAR_SUBGRUPO;
		public final static String ELIMINAR_SUBGRUPO = MG+CAMPOS.ELIMINAR_SUBGRUPO;
		public final static String EDITAR_GRUPO = MG+CAMPOS.EDITAR_GRUPO;
		public final static String ELIMINAR_GRUPO = MG+CAMPOS.ELIMINAR_GRUPO;
		public final static String ASIGNAR_TAREAS = MG+CAMPOS.ASIGNAR_TAREAS;
		public final static String EDITAR_ASIGNAMIENTO = MG+CAMPOS.EDITAR_ASIGNAMIENTO;
		public final static String ELIMINAR_ASIGNAMIENTO = MG+CAMPOS.ELIMINAR_ASIGNAMIENTO;
		public final static String COMPARTIR_PROYECTO = MG+CAMPOS.COMPARTIR_PROYECTO;
		public final static String EDITAR_COMPARTIR = MG+CAMPOS.EDITAR_COMPARTIR;
		public final static String ELIMINAR_COMPARTIR = MG+CAMPOS.ELIMINAR_COMPARTIR;
		public final static String CREAR_EQUIPO = MG+CAMPOS.CREAR_EQUIPO;
		public final static String EDITAR_EQUIPO = MG+CAMPOS.EDITAR_EQUIPO;
		public final static String ELIMINAR_EQUIPO= MG+CAMPOS.ELIMINAR_EQUIPO;
		public final static String DIAS = MG+CAMPOS.DIAS;
		public final static String HORAS = MG+CAMPOS.HORAS;
		
		public final static String NOMBRE_TABLA = CAMPOS.NOMBRE_TABLA+" mg";
	}
	
	protected boolean crearTareas;
	
	public boolean isCrearTareas() {return crearTareas;}
	public void setCrearTareas(boolean crearTareas) {this.crearTareas = crearTareas;}
	
	public boolean isCrearProyectos() {return crearProyectos;}
	public void setCrearProyectos(boolean crearProyectos) {this.crearProyectos = crearProyectos;}
	
	public boolean isEditarTareas() {return editarTareas;}
	public void setEditarTareas(boolean editarTareas) {this.editarTareas = editarTareas;}
	
	public boolean isEditarProyectos() {return editarProyectos;}
	public void setEditarProyectos(boolean editarProyectos) {this.editarProyectos = editarProyectos;}
	
	public boolean isEliminarTareas() {return eliminarTareas;}
	public void setEliminarTareas(boolean eliminarTareas) {this.eliminarTareas = eliminarTareas;}
	
	public boolean isEliminarProyectos() {return eliminarProyectos;}
	public void setEliminarProyectos(boolean eliminarProyectos) {this.eliminarProyectos = eliminarProyectos;}
	
	public boolean isInvitarMiembros() {return invitarMiembros;}
	public void setInvitarMiembros(boolean invitarMiembros) {this.invitarMiembros = invitarMiembros;}
	
	public boolean isEditarMiembros() {return editarMiembros;}
	public void setEditarMiembros(boolean editarMiembros) {this.editarMiembros = editarMiembros;}
	
	public boolean isEliminarMiembros() {return eliminarMiembros;}
	public void setEliminarMiembros(boolean eliminarMiembros) {this.eliminarMiembros = eliminarMiembros;}
	
	public boolean isCrearSubgrupo() {return crearSubgrupo;}
	public void setCrearSubgrupo(boolean crearSubgrupo) {this.crearSubgrupo = crearSubgrupo;}
	
	public boolean isEliminarSubgrupo() {return eliminarSubgrupo;}
	public void setEliminarSubgrupo(boolean eliminarSubgrupo) {this.eliminarSubgrupo = eliminarSubgrupo;}
	
	public boolean isEditarGrupo() {return editarGrupo;}
	public void setEditarGrupo(boolean editarGrupo) {this.editarGrupo = editarGrupo;}
	
	public boolean isEliminarGrupo() {return eliminarGrupo;}
	public void setEliminarGrupo(boolean eliminarGrupo) {this.eliminarGrupo = eliminarGrupo;}

	public boolean isAsignarTareas() {return asignarTareas;}
	public void setAsignarTareas(boolean asignarTareas) {this.asignarTareas = asignarTareas;}
	
	public boolean isEditarAsignamiento() {return editarAsignamiento;}
	public void setEditarAsignamiento(boolean editarAsignamiento) {this.editarAsignamiento = editarAsignamiento;}
	
	public boolean isEliminarAsignamiento() {return eliminarAsignamiento;}
	public void setEliminarAsignamiento(boolean eliminarAsignamiento) {this.eliminarAsignamiento = eliminarAsignamiento;}
	
	public boolean isCompartirProyecto() {return compartirProyecto;}
	public void setCompartirProyecto(boolean compartirProyecto) {this.compartirProyecto = compartirProyecto;}
	
	public boolean isEditarCompartir() {return editarCompartir;}
	public void setEditarCompartir(boolean editarCompartir) {this.editarCompartir = editarCompartir;}
	
	public boolean isEliminarCompartir() {return eliminarCompartir;}
	public void setEliminarCompartir(boolean eliminarCompartir) {this.eliminarCompartir = eliminarCompartir;}

	public boolean isCrearEquipo() {return crearEquipo;}
	public void setCrearEquipo(boolean crearEquipo) {this.crearEquipo = crearEquipo;}
	
	public boolean isEditarEquipo() {return editarEquipo;}
	public void setEditarEquipo(boolean editarEquipo) {this.editarEquipo = editarEquipo;}
	
	public boolean isEliminarEquipo() {return eliminarEquipo;}
	public void setEliminarEquipo(boolean eliminarEquipo) {this.eliminarEquipo = eliminarEquipo;}

	public int getDiasSemana() {return diasSemana;}
	public void setDiasSemana(int diasSemana) {this.diasSemana = diasSemana;}
	
	public int getHorasDia() {return horasDia;}
	public void setHorasDia(int horasDia) {this.horasDia = horasDia;}
	
	public float getCoeficiente(){return horasDia*diasSemana/7;}

	protected boolean crearProyectos;
	protected boolean editarTareas;
	protected boolean editarProyectos;
	protected boolean eliminarTareas;
	protected boolean eliminarProyectos;
	protected boolean invitarMiembros;
	protected boolean editarMiembros;
	protected boolean eliminarMiembros;
	protected boolean crearSubgrupo;
	protected boolean eliminarSubgrupo;
	protected boolean editarGrupo;
	protected boolean eliminarGrupo;
	protected boolean asignarTareas;
	protected boolean editarAsignamiento;
	protected boolean eliminarAsignamiento;
	protected boolean compartirProyecto;
	protected boolean editarCompartir;
	protected boolean eliminarCompartir;
	protected boolean crearEquipo;
	protected boolean editarEquipo;
	protected boolean eliminarEquipo;
	protected int diasSemana = 5;
	protected int horasDia = 4;
	
	public IntMiembroGrupo() {
		crearTareas = false;
		crearProyectos = false;
		editarTareas = false;
		editarProyectos = false;
		eliminarTareas = false;
		eliminarProyectos = false;
		invitarMiembros = false;
		editarMiembros = false;
		eliminarMiembros = false;
		crearSubgrupo = false;
		eliminarSubgrupo = false;
		editarGrupo = false;
		eliminarGrupo = false;
		compartirProyecto = false;
		editarCompartir = false;
		eliminarCompartir = false;
		asignarTareas = false;
		editarAsignamiento = false;
		eliminarAsignamiento = false;
	}
	
	public IntMiembroGrupo(boolean crearTareas, boolean crearProyectos,
			boolean editarTareas, boolean editarProyectos,
			boolean eliminarTareas, boolean eliminarProyectos,
			boolean crearSubgrupo, boolean invitarMiembros,
			boolean eliminarSubgrupo, boolean editarMiembros,
			boolean editarGrupo, boolean eliminarMiembros,
			boolean eliminarGrupo, boolean compartirProyecto, 
			boolean editarCompartir, boolean eliminarCompartir, 
			boolean asignarTareas, boolean editarAsignamiento, 
			boolean eliminarAsignamiento, boolean crearEquipo, 
			boolean editarEquipo, boolean eliminarEquipo) {
		this.crearTareas = crearTareas;
		this.crearProyectos = crearProyectos;
		this.editarTareas = editarTareas;
		this.editarProyectos = editarProyectos;
		this.eliminarTareas = eliminarTareas;
		this.eliminarProyectos = eliminarProyectos;
		this.invitarMiembros = invitarMiembros;
		this.editarMiembros = editarMiembros;
		this.eliminarMiembros = eliminarMiembros;
		this.crearSubgrupo = crearSubgrupo;
		this.eliminarSubgrupo = eliminarSubgrupo;
		this.editarGrupo = editarGrupo;
		this.eliminarGrupo = eliminarGrupo;
		this.asignarTareas = asignarTareas;
		this.editarAsignamiento = editarAsignamiento;
		this.eliminarAsignamiento = eliminarAsignamiento;
		this.compartirProyecto = compartirProyecto;
		this.editarCompartir = editarCompartir;
		this.eliminarCompartir = eliminarCompartir;
		this.crearEquipo = crearEquipo;
		this.editarEquipo = editarEquipo;
		this.eliminarEquipo = eliminarEquipo;
	}
	
	public final static String selectPermisos = CAMPOS.CREAR_TAREAS+","+CAMPOS.CREAR_PROYECTOS+","+CAMPOS.CREAR_SUBGRUPO+","+
			CAMPOS.EDITAR_GRUPO+","+CAMPOS.EDITAR_MIEMBROS+","+CAMPOS.EDITAR_PROYECTOS+","+CAMPOS.EDITAR_TAREAS+","+
			CAMPOS.ELIMINAR_GRUPO+","+CAMPOS.ELIMINAR_MIEMBROS+","+CAMPOS.ELIMINAR_PROYECTOS+","+CAMPOS.ELIMINAR_SUBGRUPO+","+
			CAMPOS.ELIMINAR_TAREAS+","+CAMPOS.INVITAR_MIEMBROS+","+CAMPOS.COMPARTIR_PROYECTO+","+CAMPOS.EDITAR_COMPARTIR+","+
			CAMPOS.ELIMINAR_COMPARTIR+","+CAMPOS.ASIGNAR_TAREAS+","+CAMPOS.EDITAR_ASIGNAMIENTO+","+CAMPOS.ELIMINAR_ASIGNAMIENTO+","+
			CAMPOS.CREAR_EQUIPO+","+CAMPOS.EDITAR_EQUIPO+","+CAMPOS.ELIMINAR_EQUIPO;
	
	public final static String selectPermisosMG = CAMPOS_MG.CREAR_TAREAS+","+CAMPOS_MG.CREAR_PROYECTOS+","+CAMPOS_MG.CREAR_SUBGRUPO+","+
		CAMPOS_MG.EDITAR_GRUPO+","+CAMPOS_MG.EDITAR_MIEMBROS+","+CAMPOS_MG.EDITAR_PROYECTOS+","+CAMPOS_MG.EDITAR_TAREAS+","+
		CAMPOS_MG.ELIMINAR_GRUPO+","+CAMPOS_MG.ELIMINAR_MIEMBROS+","+CAMPOS_MG.ELIMINAR_PROYECTOS+","+CAMPOS_MG.ELIMINAR_SUBGRUPO+","+
		CAMPOS_MG.ELIMINAR_TAREAS+","+CAMPOS_MG.INVITAR_MIEMBROS+","+CAMPOS_MG.COMPARTIR_PROYECTO+","+CAMPOS_MG.ASIGNAR_TAREAS+","+
		CAMPOS_MG.EDITAR_ASIGNAMIENTO+","+CAMPOS_MG.ELIMINAR_ASIGNAMIENTO+","+CAMPOS_MG.EDITAR_COMPARTIR+","+CAMPOS_MG.ELIMINAR_COMPARTIR+","+
		CAMPOS_MG.CREAR_EQUIPO+","+CAMPOS_MG.EDITAR_EQUIPO+","+CAMPOS_MG.ELIMINAR_EQUIPO+","+CAMPOS_MG.DIAS+","+CAMPOS_MG.HORAS;
	
	public Pair<Integer, String> getTieneDerechoA(){
		return TopePermisos.obtenerNombrePermisoMasAlto(crearTareas, crearProyectos, editarTareas, 
				editarProyectos, eliminarTareas, eliminarProyectos, crearSubgrupo, invitarMiembros, 
				eliminarSubgrupo, editarMiembros, editarGrupo, eliminarMiembros, eliminarGrupo,
				compartirProyecto, editarCompartir, eliminarCompartir, asignarTareas, 
				editarAsignamiento, eliminarAsignamiento, crearEquipo, editarEquipo, eliminarEquipo);
	}
	public static void pasarDatosDelResultSetAlIntMiembroGrupo(IntMiembroGrupo mg,
			ResultSet rs) throws SQLException {
		mg.setCrearProyectos(rs.getBoolean(CAMPOS_MG.CREAR_PROYECTOS));
		mg.setCrearSubgrupo(rs.getBoolean(CAMPOS_MG.CREAR_SUBGRUPO));
		mg.setCrearTareas(rs.getBoolean(CAMPOS_MG.CREAR_TAREAS));
		mg.setEditarGrupo(rs.getBoolean(CAMPOS_MG.EDITAR_GRUPO));
		mg.setEditarProyectos(rs.getBoolean(CAMPOS_MG.EDITAR_PROYECTOS));
		mg.setEditarMiembros(rs.getBoolean(CAMPOS_MG.EDITAR_MIEMBROS));
		mg.setEditarTareas(rs.getBoolean(CAMPOS_MG.EDITAR_TAREAS));
		mg.setEliminarGrupo(rs.getBoolean(CAMPOS_MG.ELIMINAR_GRUPO));
		mg.setEliminarMiembros(rs.getBoolean(CAMPOS_MG.ELIMINAR_MIEMBROS));
		mg.setEliminarProyectos(rs.getBoolean(CAMPOS_MG.ELIMINAR_PROYECTOS));
		mg.setEliminarSubgrupo(rs.getBoolean(CAMPOS_MG.ELIMINAR_SUBGRUPO));
		mg.setEliminarTareas(rs.getBoolean(CAMPOS_MG.ELIMINAR_TAREAS));
		mg.setInvitarMiembros(rs.getBoolean(CAMPOS_MG.INVITAR_MIEMBROS));
		mg.setAsignarTareas(rs.getBoolean(CAMPOS_MG.ASIGNAR_TAREAS));
		mg.setEditarAsignamiento(rs.getBoolean(CAMPOS_MG.EDITAR_ASIGNAMIENTO));
		mg.setEliminarAsignamiento(rs.getBoolean(CAMPOS_MG.ELIMINAR_ASIGNAMIENTO));
		mg.setCompartirProyecto(rs.getBoolean(CAMPOS_MG.COMPARTIR_PROYECTO));
		mg.setEditarCompartir(rs.getBoolean(CAMPOS_MG.EDITAR_COMPARTIR));
		mg.setEliminarCompartir(rs.getBoolean(CAMPOS_MG.ELIMINAR_COMPARTIR));
		mg.setCrearEquipo(rs.getBoolean(CAMPOS_MG.CREAR_EQUIPO));
		mg.setEditarEquipo(rs.getBoolean(CAMPOS_MG.EDITAR_EQUIPO));
		mg.setEliminarEquipo(rs.getBoolean(CAMPOS_MG.ELIMINAR_EQUIPO));
		mg.setDiasSemana(rs.getInt(CAMPOS_MG.DIAS));
		mg.setHorasDia(rs.getInt(CAMPOS_MG.HORAS));
	}
	
	public boolean[] getArrayPermisos(){
		boolean[] arrayPermisos = {
				this.crearTareas,
				this.crearProyectos,
				this.editarTareas,
				this.asignarTareas,
				this.crearEquipo,
				this.editarEquipo,
				this.eliminarEquipo,
				this.editarAsignamiento,
				this.eliminarAsignamiento,
				this.editarProyectos,
				this.compartirProyecto,
				this.editarCompartir,
				this.eliminarCompartir,
				this.eliminarTareas,
				this.eliminarProyectos,
				this.crearSubgrupo,
				this.invitarMiembros,
				this.eliminarSubgrupo,
				this.editarMiembros,
				this.editarGrupo,
				this.eliminarMiembros,
				this.eliminarGrupo
		};
		return arrayPermisos;
	}
	
	public HashMap<Integer,Boolean> getHashMapPermisos(){
		HashMap<Integer,Boolean> mapping = new HashMap<Integer, Boolean>();
		mapping.put(0, true);
		return TopePermisos.MapeoDePermisos(crearTareas, crearProyectos, editarTareas, 
				editarProyectos, eliminarTareas, eliminarProyectos, crearSubgrupo, invitarMiembros, 
				eliminarSubgrupo, editarMiembros, editarGrupo, eliminarMiembros, eliminarGrupo, 
				compartirProyecto, editarCompartir, eliminarCompartir, asignarTareas, editarAsignamiento, 
				eliminarAsignamiento, crearEquipo, editarEquipo, eliminarEquipo, mapping);
	}
	
	public void setPermisos(boolean crearTareas, boolean crearProyectos,
			boolean editarTareas, boolean editarProyectos,
			boolean eliminarTareas, boolean eliminarProyectos,
			boolean crearSubgrupo, boolean invitarMiembros,
			boolean eliminarSubgrupo, boolean editarMiembros,
			boolean editarGrupo, boolean eliminarMiembros,
			boolean eliminarGrupo, boolean compartirProyecto,
			boolean editarCompartir, boolean eliminarCompartir,
			boolean asignarTareas, boolean editarAsignamiento,
			boolean eliminarAsignamiento, boolean crearEquipo,
			boolean editarEquipo, boolean eliminarEquipo) {
		this.crearTareas = crearTareas;
		this.crearProyectos = crearProyectos;
		this.editarTareas = editarTareas;
		this.editarProyectos = editarProyectos;
		this.eliminarTareas = eliminarTareas;
		this.eliminarProyectos = eliminarProyectos;
		this.invitarMiembros = invitarMiembros;
		this.editarMiembros = editarMiembros;
		this.eliminarMiembros = eliminarMiembros;
		this.crearSubgrupo = crearSubgrupo;
		this.eliminarSubgrupo = eliminarSubgrupo;
		this.editarGrupo = editarGrupo;
		this.eliminarGrupo = eliminarGrupo;
		this.compartirProyecto = compartirProyecto;
		this.editarCompartir = editarCompartir;
		this.eliminarCompartir = eliminarCompartir;
		this.asignarTareas = asignarTareas;
		this.editarAsignamiento = editarAsignamiento;
		this.eliminarAsignamiento = eliminarAsignamiento;
	}
	
	public List<Boolean> getListPermisos(){
		List<Boolean> lista = new ArrayList<Boolean>();
		lista.add(true);
		return TopePermisos.getListPermisos(crearTareas, crearProyectos, editarTareas, asignarTareas, crearEquipo, editarEquipo, eliminarEquipo, editarAsignamiento,
				eliminarAsignamiento, editarProyectos, compartirProyecto, editarCompartir, eliminarCompartir, eliminarTareas, 
				eliminarProyectos, crearSubgrupo, invitarMiembros, eliminarSubgrupo, editarMiembros, editarGrupo, 
				eliminarMiembros, eliminarGrupo, lista);
	}
}


