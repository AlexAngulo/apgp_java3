package modelos.clasesAbstractas;

import java.sql.Connection;

public abstract class IntDependenciaTarea {
	
	protected Connection conn = null;
	
	public Connection getConn() {return conn;}
	public void setConn(Connection conn) {this.conn = conn;}
	
	public static class CAMPOS {
		public final static String TAREA_DEPENDIENTE = "idTareaDependiente";
		public final static String TAREA_DEPENDE = "idTareaDepende";
		
		public final static String NOMBRE_TABLA = "tareadependiente";
	}
	
	protected int idTareaDepende;
	
	public IntDependenciaTarea() {
		this.idTareaDepende = 0;
	}
	
	public int getIdTareaDepende(){ return this.idTareaDepende; }
	public void setIdTareaDepende(int idUsuario){this.idTareaDepende = idUsuario;}

	protected final static String selectTodo = "SELECT "+CAMPOS.TAREA_DEPENDIENTE + ","+CAMPOS.TAREA_DEPENDE+
	" FROM "+CAMPOS.NOMBRE_TABLA;
}
